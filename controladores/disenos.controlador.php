<?php

class ControladorDisenos{

   static public function consultaDisenos($tabla1, $tabla2, $tabla3, $filtro, $limitante){
     $respuesta = disenosModelo::consultaDiseno($tabla1, $tabla2, $tabla3, $filtro, $limitante);
     return $respuesta;
  }

    static public function consultaImgSlider($tabla1, $idDiseno){
      $respuesta = disenosModelo::consultaImgSlider($tabla1, $idDiseno);
      return $respuesta;
    }

    static public function consultaVotos($tabla1, $idDiseno){
      $respuesta = disenosModelo::consultaVotos($tabla1, $idDiseno);
      return $respuesta;
    }

    static public function consultaComentarios($tabla1, $idDiseno){
      $respuesta = disenosModelo::consultaComentarios($tabla1, $idDiseno);
      return $respuesta;
    }

    static public function consultaSlider($tabla1, $key){
      $respuesta = disenosModelo::consultaSlider($tabla1, $key);
      return $respuesta;
    }

    static public function mostrarProducto($tabla, $item, $valor){

      $respuesta = disenosModelo::consultaRutaProducto($tabla, $item, $valor);
      return $respuesta;

    }

    static public function consultaVotado($tabla, $idUsuario, $idDiseno){

      $respuesta = disenosModelo::consultaVotado($tabla, $idUsuario, $idDiseno);
      return $respuesta;

    }

    static public function insertarVoto($tabla, $idUsuario, $idDiseno){

      $respuesta = disenosModelo::insertarVoto($tabla, $idUsuario, $idDiseno);
      return $respuesta;

    }

    static public function quitarVoto($tabla, $idUsuario, $idDiseno){

      $respuesta = disenosModelo::quitarVoto($tabla, $idUsuario, $idDiseno);
      return $respuesta;

    }

    static public function regresarVoto($tabla, $idUsuario, $idDiseno){

      $respuesta = disenosModelo::regresarVoto($tabla, $idUsuario, $idDiseno);
      return $respuesta;

    }

    static public function registrarComentario($tabla, $idUsuario, $idDiseno, $comentario, $fecha){

      $respuesta = disenosModelo::registrarComentario($tabla, $idUsuario, $idDiseno, $comentario, $fecha);
      return $respuesta;

    }

    static public function totalDisenos($tabla,$condicion){

      $respuesta = disenosModelo::totalDisenos($tabla,$condicion);
      return $respuesta;

    }

    static public function actualizarVoto($tabla, $idUsuario, $idDiseno){

      $respuesta = disenosModelo::actualizarVoto($tabla, $idUsuario, $idDiseno);

      if ($respuesta['total'] == 0){
        self::insertarVoto($tabla, $idUsuario, $idDiseno);
      }else{
        self::regresarVoto($tabla, $idUsuario, $idDiseno);
        echo "hola2";
      }

    }

    static public function disenoPublicado($tabla, $id_diseno){

      $respuesta = disenosModelo::disenoPublicado($tabla, $id_diseno);
      return $respuesta;

    }

    static public function insertarImagen($tabla, $id_diseno, $ruta_img){

      $respuesta = disenosModelo::insertarImagen($tabla, $id_diseno, $ruta_img);
      return $respuesta;

    }

    static public function imgDisenoPublicado($tabla, $id_diseno){

      $respuesta = disenosModelo::imgDisenoPublicado($tabla, $id_diseno);
      return $respuesta;

    }

    static public function coleccionesDiseno($tabla){

      $respuesta = disenosModelo::coleccionesDiseno($tabla);
      return $respuesta;

    }

    static public function datosMetas($tabla, $ruta){

      $respuesta = disenosModelo::datosMetas($tabla, $ruta);
      return $respuesta;

    }

    static public function ctrBorrar($tabla, $campo, $valor){

      $respuesta = disenosModelo::mdlBorrar($tabla, $campo, $valor);
      return $respuesta;

    }

}
