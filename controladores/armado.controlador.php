<?php

	class ArmadoControlador{

		public static function ctrArmadoTabla(){

			$tabla = "usuarios";

			$respuesta = ArmadoModelo::mdlArmadoTabla($tabla);

			return $respuesta;

		}


		public static function ctrCambioPass($password_encriptada, $usuario){

			$respuesta = ArmadoModelo::mdlCambioPass($password_encriptada, $usuario);

			return $respuesta;

		}

	}