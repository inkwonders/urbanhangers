<?php
  class ControladorUsuarios{

    /*=============================================
  	INGRESO DE USUARIO
  	=============================================*/

  	static public function ingresoUsuario($email_login,$pass_login,$recordar,$ruta){

  		if(isset($email_login)){

        // '/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})$/'

        // '/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/'

  			if(preg_match('/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})$/', $email_login)){

  			  $encriptar = crypt($pass_login, '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

  				$tabla = "usuarios";

  				$correo = $email_login;

  				$respuesta = ModeloUsuarios::mostrarUsuarios($tabla, $correo);

  				if($respuesta["correo"] == $email_login && $respuesta["password"] == $encriptar){
            if($respuesta['verificacion'] == 0){
              if($respuesta['activo'] == 1){
                if($respuesta["tipo_usuario"] != 3){
                  if($respuesta['modo_registro'] == "directo"){

                    session_start();

        						$_SESSION["iniciarSesion"] = "ok";
        						$_SESSION["tipoUsuario"] = $respuesta["tipo_usuario"];
                    $_SESSION["id_usuario"] = $respuesta["id_usuario"];
                    $_SESSION["usuario"] = $respuesta["usuario"];
        						$_SESSION["nombre"] = $respuesta["nombre"];
                    $_SESSION["apellido"] = $respuesta["apellido"];
                    $_SESSION["avatar"] = $respuesta["foto"];
                    $_SESSION["sexo"] = $respuesta["sexo"];
                    $_SESSION["correo"] = $respuesta["correo"];
                    $_SESSION["modo_registro"] = $respuesta['modo_registro'];
                    $_SESSION["id_fb"] = NULL;
                    if($respuesta["tipo_usuario"]){
                      $_SESSION["banner"] = $respuesta['banner'];
                      $_SESSION["ruta_perfil"] = $respuesta["ruta"];
                      $_SESSION["carpeta"] = $respuesta["carpeta"];
                    }

                    if($recordar == "marcado"){

                      mt_srand(time());
                      $rand = mt_rand(1000000,9999999);

                      $agregar_cookie = ModeloUsuarios::agregarCookie($rand, $_SESSION["id_usuario"], "usuarios");

                      setcookie("usuario", $_SESSION["id_usuario"], time()+(60*60*24*365),"/php/");
                      setcookie("marca", $rand, time()+(60*60*24*365),"/php/");

                    }
                    if(isset($ruta) && $ruta =="inicio-sesion"){
        						//echo "<script> location.href = 'home'; </script>";
                    return "recarga-home";
                  }else{
                    //echo "<script> location.reload(); </script>";
                    return "recarga";
                  }

                  }else {
                    return "modo";
                  }
                }else {
                  return "datos";
                }
              }else {
                return "usuario_inactivo";
              }
            }else {
              return "verificacion";
            }
  				}else{

            return "datos";

  				}

  			}

  		}

  	}

    static public function ingresoUsuarioFB($id_usuario,$id_fb,$usuario,$nombre,$nombre_facebook,$apellido,$correo,$foto,$banner,$tipo_usuario,$ruta,$carpeta,$activo){
      // JOSUE terminar , hacer consulta previa para obtener los datos del usuario
  		if(!empty($id_fb) && !empty($correo)){
        if($activo == 1){
          if($tipo_usuario != 3){
            if (headers_sent()) {
            }else {
              session_start();
            }

            $_SESSION["iniciarSesion"] = "ok";
            $_SESSION["nombre_facebook"] = $nombre_facebook;
            $_SESSION["tipoUsuario"] = $tipo_usuario;
            $_SESSION["id_usuario"] = $id_usuario;
            $_SESSION["id_fb"] = $id_fb;
            $_SESSION["usuario"] = $usuario;
            $_SESSION["nombre"] = $nombre;
            $_SESSION["apellido"] = $apellido;
            $_SESSION["avatar"] = $foto;
            $_SESSION["facebook"] = "1";
            $_SESSION["correo"] = $correo;
            $_SESSION["modo_registro"] = "facebook";
            if($tipo_usuario == "1"){
              $_SESSION["banner"] = $banner;
              $_SESSION["ruta_perfil"] = $ruta;
              $_SESSION["carpeta"] = $carpeta;
            }

            echo '<script>

            location.href = "home";

            </script>';

            // print_r($_SESSION);

          }else {
            echo "datos";
          }

        }else {
          echo "usuario_inactivo";
        }



  		}

  	}


    /*=============================================
  	REGISTRO DE USUARIO
  	=============================================*/

  	static public function registroUsuario($id_usuario,$usuario,$correo,$contrasenia,$contrasenia_repetir,$genero,$tipo_usuario,$fecha_registro){

      if(!empty($usuario) && !empty($correo) && !empty($contrasenia) && !empty($contrasenia_repetir)){

        // '/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})$/'
        // '/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/'

        if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ-]+$/', $usuario) &&
			   preg_match('/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})$/', $correo) ){

          if($contrasenia == $contrasenia_repetir){

            $encriptar = crypt($contrasenia, '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$$2a$07$asxx54ahjppf45sd87a5auxq/SS293XhTEeizKWMnfhnpfay0AALe');

          }else{

            return "contrasenias";

            exit;

          }

			   	$encriptarEmail = md5($correo);

          $avatar = null;

          if($genero == "hombre"){
            $sexo = "h";
          }else{
            $sexo = "m";
          }

          if($tipo_usuario == "1"){

            $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
            $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
            $ruta = utf8_decode($usuario);
            $ruta = strtr($ruta, utf8_decode($originales), $modificadas);
            $ruta = strtolower($ruta);
            $ruta = str_replace(" ","-",$ruta);
          }else {
            $ruta = NULL;
          }

  				$datos = array(
                   "id_usuario"=>$id_usuario,
                   "usuario"=>$usuario,
  							   "password"=> $encriptar,
  							   "email"=> $correo,
  							   "foto"=> $avatar,
  							   "modo_registro"=> "directo",
                   "tipo_usuario"=> $tipo_usuario,
  							   "verificacion"=> 0,
  							   "emailEncriptado"=>$encriptarEmail,
                   "sexo" => $sexo,
                   "fecha_registro" => $fecha_registro,
                   "ruta" => $ruta
                 );

  				$tabla = "usuarios";

  				$respuesta = ModeloUsuarios::registroUsuario($tabla, $datos);

  				if($respuesta == "ok"){

            //$img_logo = "https://clientes.ink/urbanhangers/vistas/assets/img/correo/logo-urban.png";
            //$ruta_btn = "https://clientes.ink/urbanhangers/verificar.php?ee=".$encriptarEmail;

            $img_logo = "https://urbanhangerscommunity.com/vistas/assets/img/correo/logo-urban.png";
            $ruta_btn = "https://urbanhangerscommunity.com/verificar.php?ee=".$encriptarEmail;

            //$to = "josue.vargas24@gmail.com,".$correo;
            $to = $correo;

            $subject = "Bienvenido a Urban Hangers";

            $message = "<html>
                          <head>
                            <meta charset='utf-8'>
                            <title>Verficación de cuenta Urban Hangers</title>
                          </head>
                          <body style='padding:0; margin:0; color:#000;'>
                            <table align='center' border='0' cellpadding='0' cellspacing='0' width='600'>

                            <tr>

                            <td align='center' bgcolor='#FFF' style='padding: 40px 0 30px 0; '>

                            <img src='$img_logo' width='80px' height='80px' style='display: block;' />
                            <span style='font-family: arial; font-size: 45px; font-weight: 900; color: #1A1A1A'>¡BIENVENIDO A URBAN HANGERS!</span>
                            </td>


                            </tr>

                            <tr>

                            <td align='center' bgcolor='#FFF' style='padding: 40px 0 30px 0;'>

                            <span style='font-family: arial; font-size: 22px; color: #1A1A1A'>Hola, $usuario. <br /> Gracias por registrarte, ahora solo necesitas confirmar <br />tu correo, para hacerlo da click en el siguiente botón.</span>

                            </td>

                            </tr>

                            <tr>

                            <td align='center' bgcolor='#FFF' style='padding: 40px 0 30px 0;'>

                              <a  class=”link” href='$ruta_btn' target='_blank' style='padding: 10px 20px; border: 1px solid #FC5D2B;border-radius: 2px;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #FFF;text-decoration: none;font-weight:bold;display: inline-block; background-color:#FC5D2B; letter-spacing: 6px; border-radius: 25px;'>
                                CONFIRMAR CORREO
                              </a>

                            </td>

                            </tr>

                            </table>
                          </body>
                        </html>";

              $headers = "MIME-Version: 1.0" . "\r\n";
              $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
              $headers .= 'From: Urban Hangers<noreply@urbanhangerscommunity.com>' . "\r\n";

              // if(mail($to,$subject,$message,$headers)){

              //     $msj = "ok";

              // }else{

              //     $msj = "error";

              // }

              $msj = "ok";
              return $msj;
              
          }else{

            return "error";

          }

        }else{

          return "caracteres";

        }

      }else {
        return "vacio";
      }

    }


    static public function mostrarUsuario($item, $valor){

      $tabla = "usuarios";

      $respuesta = ModeloUsuarios::consultaUsuarioDashboard($tabla, $item, $valor);

      return $respuesta;

    }

    static public function consultaUsuarioProducto($tabla1, $tabla2, $idDiseno){

      $respuesta = ModeloUsuarios::consultaUsuarioProducto($tabla1, $tabla2, $idDiseno);

      return $respuesta;

    }

    static public function consultaUsuarios($id_fb,$correo){

      $tabla = "usuarios";
      $item1 = "id_fb";
      $item2 = "correo";

      $respuesta = ModeloUsuarios::consultaUsuarioExistente($tabla, $item1, $item2, $id_fb, $correo);

      return $respuesta;

    }

    static public function consultaUsuariosVotante($id_usuario,$correo,$modo_registro){

      $tabla = "usuarios";
      $item1 = "id_usuario";
      $item2 = "correo";
      $item3 = "modo_registro";

      $respuesta = ModeloUsuarios::consultaUsuariosVotante($tabla, $item1, $item2, $item3, $id_usuario, $correo, $modo_registro);

      return $respuesta;

    }

    static public function consultaUsuariosCorreo($item1,$correo){

      $tabla = "usuarios";
      // $item1 = "correo";
      if($item1 == "email_encriptado"){

        $condicion = "AND verificacion = 1";

      }else{

        $condicion = "";

      }

      $respuesta = ModeloUsuarios::consultaUsuarioExistenteCorreo($tabla, $item1, $correo, $condicion);

      return $respuesta;

    }

    static public function consultaUsuarioUnico($item1,$usuario){

      $tabla = "usuarios";

      $respuesta = ModeloUsuarios::consultaUsuarioUnico($tabla, $item1, $usuario);

      return $respuesta;

    }

    static public function registroUsuarioFB($id_usuario,$id,$nombre,$correo,$ruta_img,$tipo_usuario,$fecha_registro){

      if(!empty($id)){

        $tabla = "usuarios";

        $idfb = "fb-".$id;
        
        if($tipo_usuario == "1"){
          $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
          $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
          $ruta = utf8_decode($idfb);
          $ruta = strtr($ruta, utf8_decode($originales), $modificadas);
          $ruta = strtolower($ruta);
          $ruta = str_replace(" ","-",$ruta);
          $carpeta = "usuario_fb_".$id_usuario."_".date('Y-m-d');
          $ruta_carpeta = '../vistas/assets/hangers/'.$carpeta;

          mkdir($ruta_carpeta, 0755);
        }else {
          $ruta = NULL;
          $carpeta = NULL;
        }


        $datos = array(
                   "id_usuario"=>$id_usuario,
                   'nombre_facebook'=>$nombre,
                   "usuario"=>$idfb,
                   "email"=> $correo,
                   "foto"=> $ruta_img,
                   "modo_registro"=> "facebook",
                   "tipo_usuario"=> $tipo_usuario,
                   "verificacion"=> "0",
                   "id_fb" => $id,
                   "ruta" => $ruta,
                   "carpeta" => $carpeta,
                   "fecha_registro" => $fecha_registro
                 );


        $respuesta = ModeloUsuarios::registroUsuarioFB($tabla, $datos);

        return $respuesta;

      }else{

          return "error_controlador_vacio";

      }

    }

    static public function actualizarDatosPerfil($tabla, $id_usuario, $query_update){

      // var_dump($datos_perfil);

      $respuesta = ModeloUsuarios::actualizarDatosPerfil($tabla, $id_usuario, $query_update);

      return $respuesta;

    }

    static public function revisionContrasena($tabla, $id_usuario){

      $respuesta = ModeloUsuarios::revisionContrasena($tabla, $id_usuario);

      return $respuesta;

    }

    static public function actualizaVerficacion($ee){

      $respuesta = ModeloUsuarios::actualizaVerficacion($ee);

      return $respuesta;

    }

    static public function verficarCookie($id, $cookie){

      $tabla = "usuarios";
      $item1 = "id_usuario";
      $item2 = "cookie";

      $respuesta = ModeloUsuarios::verficarCookie($id, $cookie, $tabla, $item1, $item2);

      return $respuesta;

    }

    static public function actualizaHanger($id_usuario,$correo,$modo_registro,$tipo_usuario,$ruta,$carpeta){

      $tabla = "usuarios";
      $item1 = "id_usuario";
      $item2 = "correo";
      $item3 = "modo_registro";
      $item4 = "tipo_usuario";
      $item5 = "ruta";
      $item6 = "carpeta";

      $respuesta = ModeloUsuarios::actualizaHanger($tabla,$id_usuario,$correo,$modo_registro,$tipo_usuario,$ruta,$carpeta,$item1,$item2,$item3,$item4,$item5,$item6);

      if($respuesta == "ok"){
        $_SESSION["tipoUsuario"] = 1;
        $_SESSION["ruta_perfil"] = $ruta;
        $_SESSION["carpeta"] = $carpeta;
      }

      return $respuesta;

    }

    static public function vaciaCampo($tabla, $campo_vaciar, $identificador, $valor_identificador){

      $respuesta = ModeloUsuarios::vaciaCampo($tabla, $campo_vaciar, $identificador, $valor_identificador);

      return $respuesta;

    }

    static public function actualizaCampo($tabla,$campo_actualizar,$valor_insertar,$identificador,$valor_identificador){

      $respuesta = ModeloUsuarios::actualizaCampo($tabla,$campo_actualizar,$valor_insertar,$identificador,$valor_identificador);

      return $respuesta;

    }

    static public function generaUUID(){

      $uuid = ModeloUsuarios::generaUUID();

      return $uuid;

    }

    static public function reestablecerContrasena($tabla, $email_encriptado, $correo){

        $respuesta = ModeloUsuarios::reestablecerContrasena($tabla, $email_encriptado, $correo);

        if(!empty($respuesta)){

          //$img_logo = "https://clientes.ink/urbanhangers/vistas/assets/img/correo/logo-urban.png";
          //$img_correo = "https://clientes.ink/urbanhangers/vistas/assets/img/correo/correo.png";
          //$ruta_btn = "https://clientes.ink/urbanhangers/restablecer.php?ee=".$email_encriptado;

          $img_logo = "https://urbanhangerscommunity.com/vistas/assets/img/correo/logo-urban.png";
          $img_correo = "https://urbanhangerscommunity.com/vistas/assets/img/correo/correo.png";
          $ruta_btn = "https://urbanhangerscommunity.com/restablecer.php?ee=".$email_encriptado;

          //$to = "josue.vargas24@gmail.com,".$correo;
          $to = $correo;

          $subject = "Restablecer contraseña Urban Hangers";

          $message = "<html>
                        <head>
                          <meta charset='utf-8'>
                          <title>Restablecer contraseña Urban Hangers</title>
                          </head>
                          <body style='padding:0; margin:0; color:#000;'>
                          <table align='center' border='0' cellpadding='0' cellspacing='0' width='600'>

                          <tr>

                          <td align='center' bgcolor='#FFF' style='padding: 40px 0 30px 0; '>

                          <img src='$img_logo' width='80px' height='80px' style='display: block;' />
                          <span style='font-family: arial; font-size: 45px; font-weight: 900; color: #1A1A1A'>¿Olvidaste tu contraseña?</span>
                          </td>


                          </tr>

                          <tr>

                          <td align='center' bgcolor='#FFF' style='padding: 40px 0 30px 0;'>

                          <img src='$img_correo' width='180px' height='160px' style='display: block;' />
                          <span style='font-family: arial; font-size: 22px; color: #1A1A1A'>Haz click en el botón para restablecer tu contraseña</span><br /> <br />

                          </td>

                          </tr>

                          <tr>

                          <td align='center' bgcolor='#FFF' style='padding: 40px 0 30px 0;'>

                            <a  class=”link” href='$ruta_btn' target='_blank' style='padding: 10px 20px; border: 1px solid #FC5D2B;border-radius: 2px;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #FFF;text-decoration: none;font-weight:bold;display: inline-block; background-color:#FC5D2B; letter-spacing: 6px; border-radius: 25px;'>
                              Restablecer contraseña
                            </a>

                          </td>

                          </tr>

                          </table>
                          </body>
                      </html>";

            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= 'From: Urban Hangers<noreply@urbanhangerscommunity.com>' . "\r\n";

            if(mail($to,$subject,$message,$headers)){

                $msj = "ok";

            }else{

                $msj = "error";

            }

          return $msj;
        }

    }

    static public function consultaUsuariosContrasenaOlvidada($correo){

      $tabla = "usuarios";

      $respuesta = ModeloUsuarios::consultaUsuariosContrasenaOlvidada($tabla, $correo);

      return $respuesta;

    }

    static public function realizarRestablecimientoContrasena($tabla, $correo, $contrasena_encriptada){

      $respuesta = ModeloUsuarios::realizarRestablecimientoContrasena($tabla, $correo, $contrasena_encriptada);

      return $respuesta;

    }

    static public function agregarNuevoDiseno($tabla, $nombre_diseno, $ruta_diseno, $usuario_diseno, $inspiracion_diseno, $fecha_diseno, $clave, $id_diseno, $coleccion_diseno,$nombre_imagen,$carpeta_mock_up){

      $respuesta = ModeloUsuarios::agregarNuevoDiseno($tabla, $nombre_diseno, $ruta_diseno, $usuario_diseno, $inspiracion_diseno, $fecha_diseno, $clave, $id_diseno, $coleccion_diseno,$nombre_imagen,$carpeta_mock_up);

      return $respuesta;

    }

    static public function agregarNuevoMockup($tabla, $nombre_archivo, $nombre_original, $fk_diseno,$clave,$fecha_mock){

      $respuesta = ModeloUsuarios::agregarNuevoMockup($tabla, $nombre_archivo, $nombre_original, $fk_diseno,$clave,$fecha_mock);

      return $respuesta;

    }

    static public function consultaPaises($tabla){

      $respuesta = ModeloUsuarios::consultaPaises($tabla);

      return $respuesta;

    }

    static public function consultaEstados($tabla){

      $respuesta = ModeloUsuarios::consultaEstados($tabla);

      return $respuesta;

    }

    static public function consultaIdPais($tabla, $pais){
      $respuesta = ModeloUsuarios::consultaIdPais($tabla, $pais);

      return $respuesta;
    }

    static public function consultaIdEstado($tabla, $estado){
      $respuesta = ModeloUsuarios::consultaIdEstado($tabla, $estado);

      return $respuesta;
    }

    static public function consultaPaisHanger($tabla, $id_pais){
      $respuesta = ModeloUsuarios::consultaPaisHanger($tabla, $id_pais);

      return $respuesta;
    }

    static public function consultaEstadoHanger($tabla, $id_estado){
      $respuesta = ModeloUsuarios::consultaEstadoHanger($tabla, $id_estado);

      return $respuesta;
    }

    static public function paisHanger($tabla, $idUsuario){
      $respuesta = ModeloUsuarios::paisHanger($tabla, $idUsuario);

      return $respuesta;
    }

    static public function datosMetas($tabla, $ruta){
      $respuesta = ModeloUsuarios::datosMetas($tabla, $ruta);

      return $respuesta;
    }

    static public function coleccionesUsuario($id_usuario, $tienda){

      $respuesta = ModeloUsuarios::coleccionesUsuario($id_usuario, $tienda);
      return $respuesta;
    }

  }
?>
