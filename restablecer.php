<?php
  if(isset($_GET['ee'])){

    require_once "controladores/controlador.usuario.php";
    require_once "modelos/usuarios.modelo.php";

    $consulta_usuario = ControladorUsuarios::consultaUsuariosContrasenaOlvidada($_GET['ee']);

    $id_usuario_consulta = $consulta_usuario['id_usuario'];
    $correo_consulta = $consulta_usuario['correo'];
    $modo_registro_consulta = $consulta_usuario['modo_registro'];
    $correo_encriptado = md5($correo_consulta);


    if(!empty($id_usuario_consulta) && !empty($correo_consulta) && $modo_registro_consulta == 'directo'){

          header("Location: restablecer-contrasena/$correo_encriptado");

      }else {
        header("Location: 404");
      }
    }
