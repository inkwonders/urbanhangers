var bool_disenos  = false ;
var archivos = [];
var archivos_aux = [];

//funcion para imprimir modal de subirDiseno

$(document).on("click","#btn_publicar_subir_diseno",function(){
  titulo_diseno = $("#input_titulo").val();
  inspiracion_diseno = $("#input_inspiracion").val();
  coleccion_diseno = $('input[name=coleccion]:checked', '#form_subir_diseno').attr('nombre_coleccion');

  $("#nombre_subir_diseno").html(titulo_diseno);
  $("#inspiracion_subir_diseno").html(inspiracion_diseno);
  $("#coleccion_subir_diseno").html(coleccion_diseno);

});

$('.slider').slider();

  $(".ico-titulo-duda").mouseover(function(){
    $(".msj-titulo").css("opacity","1");
  });

  $(".ico-titulo-duda").mouseout(function(){
    $(".msj-titulo").css("opacity","0");
  });

  $(".ico-inspiracion-duda").mouseover(function(){
    $(".msj-inspiracion").css("opacity","1");
  });

  $(".ico-inspiracion-duda").mouseout(function(){
    $(".msj-inspiracion").css("opacity","0");
  });

  $(".ico-coleccion-duda").mouseover(function(){
    $(".msj-coleccion").css("opacity","1");
  });

  $(".ico-coleccion-duda").mouseout(function(){
    $(".msj-coleccion").css("opacity","0");
  });

  $(".ico-canvas").mouseover(function(){
    $(".msj-oculto-btn-canvas").css("opacity","1");
  });

  $(".ico-canvas").mouseout(function(){
    $(".msj-oculto-btn-canvas").css("opacity","0");
  });

  $("#cambiarimagen").click(function(){
    // $("#vista_previa").empty();
    $("#vista_previa").css("height","0");
    $("#inputdiseno").click();
  });

  $(".btn_regresar").click(function(){
    $(".modal-confirmacion").addClass("oculto");
  });

  $(".btn_publicar").click(function(){
    $(".modal-confirmacion").removeClass("oculto");
  });

  // $(".btn_cancelar").click(function(){
  //   // $("#inputdiseno").val("");
  //   $(".cont-previsualizacion").addClass("oculto");
  //   // $("#vista_previa").html('');
  //   $("#vista_previa").css('height','0');
  //   $(".msj_carga_imagen").addClass("oculto");
  //   $(".btn_canvas").removeClass("oculto");
  // });


  /** vista previa de imagenes **/

  $("#inputdiseno").on("change", function(){    

      /* Limpiar vista previa */ 
      $(".cont-previsualizacion").addClass("oculto");
      $(".msj_carga_imagen span").html('');         
      $("#vista_previa").html('');
      $("#vista_previa").css('height','100%');
      //$(".btn_canvas").addClass("oculto");
      $(".msj_carga_imagen").removeClass("oculto");      
      var archivos = document.getElementById('inputdiseno').files;
      var navegador = window.URL || window.webkitURL;

      var file = this.files[0];
      var img = new Image();
      var objectUrl = navegador.createObjectURL(file);
      let width, height;

      img.onload = function () {

        width = this.width;
        height = this.height;

        /* Validar tamaño y tipo de archivo */
        var size = archivos[0].size;
        var type = archivos[0].type;
        var name = archivos[0].name;     
        
        if ((width <= 1000 && width > 60) || (height <= 1000 && height > 60)) {
          var truena_diseno = false;
        } else {
          var truena_diseno = true;
        }

        if(size > ((1024*1024)*10)){    
            $("#inputdiseno").val("");        
            $(".cont-previsualizacion").addClass("oculto");
            $(".msj_carga_imagen span").html('');            
            $(".msj_error_img").removeClass('oculto');
            $(".msj_error_img span").html("<p style='color: red'>El archivo "+name+" supera el máximo permitido 10MB</p>");
        }else if( type != 'image/png'){         
            $("#inputdiseno").val("");   
            $(".cont-previsualizacion").addClass("oculto");
            $(".msj_carga_imagen span").html('');            
            $(".msj_error_img").removeClass('oculto');
            $(".msj_error_img").html("<p style='color: red'>El archivo "+name+" no es del tipo de imagen permitida.</p>");
        }else if(truena_diseno){   
            $("#inputdiseno").val("");         
            $(".cont-previsualizacion").addClass("oculto");
            $(".msj_carga_imagen span").html('');            
            $(".msj_error_img").removeClass('oculto');
            $(".msj_error_img").html("<p style='color: red'>El archivo "+name+" no cumple con las dimensiones permitidas.</p>");
        }else{
            var objeto_url = navegador.createObjectURL(archivos[0]);
            $(".cont-previsualizacion").removeClass("oculto");
            $("#vista_previa").append("<div class='vista_previa_img' data-num='"+0+"' style='width: 100%; height: 100%; border-radius: 20px; background-image: url("+objeto_url+"); background-size: cover; background-repeat: none; background-position: center;'></div>");
            $('.card-content-izq').html("<div class='vista_previa_img' data-num='"+0+"' style='width: 100%; height: 100%; border-radius: 20px; background-image: url("+objeto_url+"); background-size: cover; background-repeat: none; background-position: center;'></div>");
            $(".msj_error_img").addClass('oculto');
            $(".msj_error_img").html('');
            $(".msj_carga_imagen").removeClass('oculto');
            $(".msj_carga_imagen span").html(name);
        }

      };    
      
      img.src = objectUrl;
      
      validar_campos_diseno();
  });

  $("#input_titulo").on("keyup", function(){

    validar_campos_diseno();
  });

  $("#input_inspiracion").on("keyup", function(){

    validar_campos_diseno();
  });


  function validar_campos_diseno(){
    
    let titulo = $("#input_titulo").val();
    let inspiracion = $("#input_inspiracion").val();
    let archivo = $("#inputdiseno").val();

    let bool_titulo = false, bool_insp = false, bool_archivo = false;


    if(titulo != '' && titulo != undefined){
      bool_titulo = true;
    }

    if(inspiracion != '' && inspiracion != undefined){
      bool_insp = true;
    }

    if(archivo != '' && archivo != undefined){
      bool_archivo = true;
    }
    console.log(bool_titulo+" "+bool_insp+" "+bool_archivo+ " "+ bool_disenos);
    if(bool_titulo && bool_insp && bool_archivo){
      // console.log('entro habilitado');
      $("#btn_publicar_subir_diseno").removeClass('deshabilitado');
    }else {
      // console.log('entro deshabilitado');
      $("#btn_publicar_subir_diseno").addClass('deshabilitado');
    }
  }

  // esto lo agregue atte Guillo  
  function validar_mockup() {
        
    cont = 1;
    cont_aux = -1;
    cont_true = 0;   
    archivos =[];
    $(".input_mockup").each(function() {      
      cont_aux++;      
      value = $(this)[0].files[0];

      if (cont_aux == 2) {
        cont++;
        cont_aux=0;
      }
      
      clave = $('#clave'+cont).val();
      
      if (value != '' && value != undefined) {        
        
        cont_true ++;        
        archivos_aux.push(clave,value);
        archivos.push(archivos_aux);
        archivos_aux = [];
      }

    });

    // console.log(archivos);    
    if (cont_true > 0) {
      bool_disenos = true;
    }else{
      bool_disenos = false;
    }
    validar_campos_diseno();    

  }

  function disenios_pares(num){
    $("#disenios_file_"+num).trigger('click');
  }

  function disenios_pares_dos(num){
    $("#disenios_file_dos"+num).trigger('click');
  }


  function validar_disenios(num_inp){
    var file = document.getElementById("disenios_file_"+num_inp).files;
    var size_disenios = file[0].size;
    var type_disenios = file[0].type;
    var name_disenios = file[0].name;
    var tamanio_max = (1024*1024)*10;

    var navegador = window.URL || window.webkitURL;
    
    var img = new Image();
    var objectUrl = navegador.createObjectURL(file[0]);
    let width, height;

    img.onload = function () {
      
      width = this.width;
      height = this.height;

      if ((width <= 920 && width > 60) || (height <= 1229 && height > 60)) {
        var truena_dimensiones = false;
      } else {
        var truena_dimensiones = true;
      }      

      if(size_disenios > (tamanio_max)){
        
        $('#error_modelo_'+num_inp).html('El archivo '+name_disenios+' supera el máximo permitido 10MB.');
        $('#disenios_file_'+num_inp).val('');
      }else if(type_disenios != 'image/jpeg' && type_disenios != 'image/jpg' /*&& type_disenios != 'image/png'*/){
        
        $('#error_modelo_'+num_inp).html('El archivo '+name_disenios+'  no es del tipo de imagen permitida.');
        $('#disenios_file_'+num_inp).val('');
      }else if(truena_dimensiones){
        
       $('#error_modelo_'+num_inp).html('El archivo '+name_disenios+'  no cumple las dimensiones permitidas.');
       $('#disenios_file_'+num_inp).val('');
      }else{
        
        var objeto_url = navegador.createObjectURL(file[0]);
        $('#btn_mas_centro_'+num_inp).attr("src",'vistas/assets/img/menos.svg');
        $('#btn_mas_centro_'+num_inp).attr("title",name_disenios);
        $('#btn_mas_centro_'+num_inp).attr("onclick","quitar_disenio('"+num_inp+"','1')");
        $('#previa_disenios_'+num_inp).css({"background-image":"url("+objeto_url+")","display":"block","background-size":"100%","background-position":"center","background-repeat":"no-repeat","background-size":"cover"});
        $('#error_modelo_'+num_inp).html('');
        //$('#btn_mas_centro_'+num_inp).css('left','30%');
        $('#btn_mas_centro_der_'+num_inp).css('left','70%');
        $('#btn_mas_centro_der_'+num_inp).css('display','none');
 
         if($('#disenios_file_'+num_inp).val() != '' &&   $('#disenios_file_dos'+num_inp).val() == '') {
          
           $('.circulo_prev'+num_inp).css('visibility','visible');
           $('.circulo_prev_dos'+num_inp).css('visibility','hidden');
           $('.circulo_prev'+num_inp).css('opacity','1');
           $('.circulo_prev_dos'+num_inp).css('opacity','.5');
         }else if($('#disenios_file_'+num_inp).val() == '' && $('#disenios_file_dos'+num_inp).val() != ''){
          
           $('.circulo_prev'+num_inp).css('visibility','hidden');
           $('.circulo_prev_dos'+num_inp).css('visibility','visible');
           $('.circulo_prev'+num_inp).css('opacity','.5');
           $('.circulo_prev_dos'+num_inp).css('opacity','1');
         }else if ($('#disenios_file_'+num_inp).val() != '' && $('#disenios_file_dos'+num_inp).val() != '') {
          
           $('.circulo_prev'+num_inp).css('visibility','visible');
           $('.circulo_prev_dos'+num_inp).css('visibility','visible');
           $('.circulo_prev'+num_inp).css('opacity','.5');
           $('.circulo_prev_dos'+num_inp).css('opacity','1');
         }
 
      }

      validar_mockup();  

    };

    img.src = objectUrl;      

  }



  function validar_disenios_dos(num_inp){
      var file = document.getElementById("disenios_file_dos"+num_inp).files;
      var size_disenios = file[0].size;
      var type_disenios = file[0].type;
      var name_disenios = file[0].name;
      var tamanio_max = (1024*1024)*10;

      var navegador = window.URL || window.webkitURL;

      var img = new Image();
      var objectUrl = navegador.createObjectURL(file[0]);
      let width, height;

      img.onload = function () {
      
        width = this.width;
        height = this.height;
  
        if ((width <= 920 && width > 60) || (height <= 1229 && height > 60)) {
          var truena_dimensiones = false;
        } else {
          var truena_dimensiones = true;
        }         

        if(size_disenios > (tamanio_max)){
          $('#error_modelo_'+num_inp).html('El archivo '+name_disenios+' supera el máximo permitido 10MB.');
          $('#disenios_file_dos'+num_inp).val('');
        }else if(type_disenios != 'image/jpeg' && type_disenios != 'image/jpg' /*&& type_disenios != 'image/png'*/){
          $('#error_modelo_'+num_inp).html('El archivo '+name_disenios+'  no es del tipo de imagen permitida.');
          $('#disenios_file_dos'+num_inp).val('');
        }else if(truena_dimensiones){
         $('#error_modelo_'+num_inp).html('El archivo '+name_disenios+'  no cumple las dimensiones permitidas.');
         $('#disenios_file_dos'+num_inp).val('');
        }else{
          var objeto_url = navegador.createObjectURL(file[0]);
          $('#btn_mas_centro_der_'+num_inp).attr("src",'vistas/assets/img/menos.svg');
          $('#btn_mas_centro_der_'+num_inp).attr("title",name_disenios);
          $('#btn_mas_centro_der_'+num_inp).attr("onclick","quitar_disenio('"+num_inp+"','2')");
          $('#previa_disenios_dos_'+num_inp).css({"background-image":"url("+objeto_url+")","display":"block","background-size":"100%","background-position":"center","background-repeat":"no-repeat","background-size":"cover"});
          $('#error_modelo_'+num_inp).html('');
           if($('#disenios_file_'+num_inp).val() != '' &&  $('#disenios_file_dos'+num_inp).val() == '') {
           $('.circulo_prev'+num_inp).css('visibility','visible');
           $('.circulo_prev_dos'+num_inp).css('visibility','hidden');
           $('.circulo_prev'+num_inp).css('opacity','1');
           $('.circulo_prev_dos'+num_inp).css('opacity','.5');
           }else if($('#disenios_file_'+num_inp).val() == '' && $('#disenios_file_dos'+num_inp).val() != ''){
           $('.circulo_prev'+num_inp).css('visibility','hidden');
           $('.circulo_prev_dos'+num_inp).css('visibility','visible');
           $('.circulo_prev'+num_inp).css('opacity','.5');
           $('.circulo_prev_dos'+num_inp).css('opacity','1');
           }else if ($('#disenios_file_'+num_inp).val() != '' && $('#disenios_file_dos'+num_inp).val() != '') {
           $('.circulo_prev'+num_inp).css('visibility','visible');
           $('.circulo_prev_dos'+num_inp).css('visibility','visible');
           $('.circulo_prev'+num_inp).css('opacity','.5');
           $('.circulo_prev_dos'+num_inp).css('opacity','1');
         }
       }
       
       validar_mockup();
      
      };

      img.src = objectUrl;      
       
    }

   function quitar_disenio(num_inpa,num){
     if (num == '1') {
       $('#btn_mas_centro_'+num_inpa).attr("src",'vistas/assets/img/mas.svg');
       $('#disenios_file_'+num_inpa).val('');
       $('#btn_mas_centro_'+num_inpa).attr("onclick","disenios_pares('"+num_inpa+"')");
       $('#error_modelo_'+num_inpa).html('');
       $('#previa_disenios_'+num_inpa).css("background-image","url()");
       $('#previa_disenios_'+num_inpa).css("display","none");
     }else{
       $('#btn_mas_centro_der_'+num_inpa).attr("src",'vistas/assets/img/mas.svg');
       $('#disenios_file_dos'+num_inpa).val('');
       $('#btn_mas_centro_der_'+num_inpa).attr("onclick","disenios_pares_dos('"+num_inpa+"')");
       $('#error_modelo_'+num_inpa).html('');
       $('#previa_disenios_dos_'+num_inpa).css("background-image","url()");
       $('#previa_disenios_dos_'+num_inpa).css("display","none");
     }
     if ($('#disenios_file_'+num_inpa).val() == '' && $('#disenios_file_dos'+num_inpa).val() == '') {
       $('#btn_mas_centro_der_'+num_inpa).css({"display":'none',"left":"50%"});
       $('#btn_mas_centro_'+num_inpa).css({"left":"50%"});

     }
     if ($('#disenios_file_'+num_inpa).val() == '' &&   $('#disenios_file_dos'+num_inpa).val() == '') {
       $('.circulo_prev'+num_inpa).css('visibility','hidden');
       $('.circulo_prev_dos'+num_inpa).css('visibility','hidden');
     }else if ($('#disenios_file_'+num_inpa).val() != '' &&   $('#disenios_file_dos'+num_inpa).val() == '') {
       $('.circulo_prev'+num_inpa).css('visibility','visible');
       $('.circulo_prev_dos'+num_inpa).css('visibility','hidden');
     }else if($('#disenios_file_'+num_inpa).val() == '' && $('#disenios_file_dos'+num_inpa).val() != ''){
       $('.circulo_prev'+num_inpa).css('visibility','hidden');
       $('.circulo_prev_dos'+num_inpa).css('visibility','visible');
     }else if ($('#disenios_file_'+num_inpa).val() != '' && $('#disenios_file_dos'+num_inpa).val() != '') {
       $('.circulo_prev'+num_inpa).css('visibility','visible');
       $('.circulo_prev_dos'+num_inpa).css('visibility','visible');
     }
     validar_mockup();

   }

   function ver_disenio(num,num_dos) {
     if (num_dos == '1') {
       $('#previa_disenios_'+num).css('z-index','4');
       $('#previa_disenios_dos_'+num).css('z-index','3');
       $('.circulo_prev'+num).css('opacity','1');
       $('.circulo_prev_dos'+num).css('opacity','.5');
     }else{
       $('#previa_disenios_'+num).css('z-index','3');
       $('#previa_disenios_dos_'+num).css('z-index','4');
       $('.circulo_prev'+num).css('opacity','.5');
       $('.circulo_prev_dos'+num).css('opacity','1');
     }

   }
