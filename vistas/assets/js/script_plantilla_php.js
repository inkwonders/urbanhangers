<script type="text/javascript">

    <?php
      if(isset($_SESSION['iniciarSesion']) && $_SESSION['iniciarSesion'] == 'ok' ){
        ?>
        var sesion_modal = 'ok';
        var id_usuario_modal = '<?php echo $_SESSION['id_usuario']; ?>';
        <?php
      }else {
        ?>
        var sesion_modal = 'no';
        var id_usuario_modal = 'no';
        <?php
      }
    ?>

      $(document).ready(function(){
        console.log("<?php echo $condicion_total_dis; ?>");

        $('.tooltipped').tooltip();

        $('.collapsible').collapsible();

        $('.modal_imagenes').modal({
            backdrop: 'static',
            keyboard: true,
            dismissible: true,
            container: 'body'
        });

        $('.modal-inicio').modal({
            backdrop: 'static',
            keyboard: false,
            dismissible: false,
            container: 'body'
        });
        $('.modal-registrarse').modal({
            backdrop: 'static',
            keyboard: false,
            dismissible: false,
            container: 'body'
        });
        $('.modal-hanger').modal({
            backdrop: 'static',
            keyboard: false,
            dismissible: false,
            container: 'body'
        });

        $('#cambio_exitoso_generico,#error_modal_generico,#modal_error_hanger,#error_modal,#registrado_directo,#registrado_facebook,#hanger_upgrade_modal, #modal_completa_formulario').modal({
            backdrop: 'static',
            keyboard: false,
            dismissible: false,
            container: 'body'
        });

        $('#reestablecer_contrasena').modal({
            backdrop: 'static',
            keyboard: false,
            dismissible: false,
            container: 'body'
        });



        // $('#tabs-hanger-votante').tabs('updateTabIndicator');

        $("#btn_modal_registrado_fb, #btn_modal_registrado_dir").click(function(){
          inicio_sesion();
        });


        $(".custom-select").trigger("change");

        $(document).on("click", ".evento_filtro_home .custom-select .select-items", function(){

          let valor = $("#select_filtro").val();
          let valor_movil = $("#select_filtro_movil").val();
          console.log(valor+" "+valor_movil);
          if(valor != 0){
            location.href = "<?php echo $ruta_global; ?>home/"+valor;
          }else if(valor_movil != 0){
            location.href = "<?php echo $ruta_global; ?>home/"+valor_movil;
          }

        });

        //editar estados de méxico EDITAR PERFIL

        $('#select_pais').change(function(){
          console.log($(this).val());
          if($(this).val() != 'México'){
            $("#select_estado_oculto").css("display", "none");
            $("#select_estado").remove();
            $("#input_estado_extranjero").remove();
            $("#input_estado_extranjero_oculto").css("display", "inherit");
          }else{
            $("#input_estado_extranjero").remove();
            $("#select_estado_oculto").css("display", "block");
            $("#select_estado").remove();
            $("#input_estado_extranjero_oculto").css("display", "none");
          }
        });


        $("#form_registro,#form_registro_hanger").submit(function(e){
          e.preventDefault();
          var id = $(this).attr("id");
          let datos = $(this).serialize();
          if(id == "form_registro"){
            var id_contenedor = "contenedor_botones_registro";
            var id_contenedor_loader = "cont_loader_registro";
            var cont_msj_error = "cont_error_usuario";
            var msj_error = "msj_error_registro";
          }else if(id == "form_registro_hanger"){
            var id_contenedor = "cont_botones_hanger";
            var id_contenedor_loader = "cont_loader_hanger";
            var cont_msj_error = "cont_error_usuario_hanger";
            var msj_error = "msj_error_registro_hanger";
          }
          $.ajax({
            url: "<?php echo $ruta_global; ?>ajax/registro.ajax.php",
            type: "POST",
            data: datos,
            beforeSend : function(){
              $("#"+id_contenedor_loader).css("display","block");
              $("#"+id_contenedor).removeClass("habilitado");
              $("#"+id_contenedor).addClass("deshabilitado");
            },
            success: function(response){
              // console.log(response);
              $("#"+id_contenedor).removeClass("deshabilitado");
              $("#"+id_contenedor).addClass("habilitado");
              $("#"+id_contenedor_loader).css("display","none");
              // console.log("resp: "+response);
              if(response == "error_usuario"){
                $('#'+msj_error).html('El usuario ingresado no está permitido');
                $("#"+cont_msj_error).css("display","block");
              }else if(response == "usuario_duplicado"){
                $('#'+msj_error).html('El usuario ingresado ya existe');
                $("#"+cont_msj_error).css("display","block");
              }else if(response == "contrasenias"){
                $('#'+msj_error).html('Las contraseñas no coinciden');
                $("#"+cont_msj_error).css("display","block");
              }else if(response == "caracteres"){
                $('#'+msj_error).html('Verificar formato de usuario, no se permiten espacios ni carácteres especiales.');
                $("#"+cont_msj_error).css("display","block");
              }else{
                $('#'+msj_error).html('');
                $("#"+cont_msj_error).css("display","none");
                $("#registrarse_modal").modal("close");
                $("#hanger_modal").modal("close");
                $("#form_registro")[0].reset();
                $("#form_registro_hanger")[0].reset();
                if(response == "error"){
                  $("#error_modal").modal("open");
                }else if(response == "directo"){
                  $("#registrado_directo").modal("open");
                }else if(response == "facebook"){
                  $("#registrado_facebook").modal("open");
                }else{
                  let r  = response.split('/');
                  let correo = r[1];
                  let usuario = r[2];
                  let onclick = "envia_otra_vez('"+correo+"','"+usuario+"')";
                  $("#enviar_nuevamente").attr("correo", correo);
                  $("#enviar_nuevamente").attr("usuario", usuario);
                  $("#registro_exitoso").modal("open");

                }
              }

            }
          });

        });

        $(document).on('click', '#enviar_nuevamente', function(){
          let correo = $(this).attr('correo');
          let usuario = $(this).attr('usuario');

          $.ajax({
            url: "<?php echo $ruta_global; ?>ajax/reenviar.ajax.php",
            type: "POST",
            data: {
              'correo' : correo,
              'usuario' : usuario
            },
            beforeSend : function(){
              $("#reenvio_correo").modal('open');
            },
            success: function(response){
              if(response == 'ok'){
                console.log('');
              }else{
                console.log('');
              }
            }
          });
        });



//form login modal
        $("#form_login").submit(function(e){
          e.preventDefault();

          let check;

          if($("#check_recordar").prop("checked")){

            check = "marcado";

          }else{

            check = "nomarcado";

          }

          let datos = $(this).serialize() + '&recordar=' + check;

          $.ajax({
            url: "<?php echo $ruta_global; ?>ajax/ingreso.ajax.php",
            type: "POST",
            data: datos,
            beforeSend : function(){
              $("#cont_botones_inicio").addClass("deshabilitado");
              $("#footer_modal_inicio").addClass("deshabilitado");
              $("#div_respuesta_login").html("<img src='<?php echo $ruta_global; ?>vistas/assets/css/ajax-loader.gif' alt='Cargando...'>");
            },
            success: function(response){
              $("#cont_botones_inicio").removeClass("deshabilitado");
              $("#footer_modal_inicio").removeClass("deshabilitado");
              $("#cont_botones_inicio").addClass("habilitado");
              $("#footer_modal_inicio").addClass("habilitado");
               //console.log(response);
              if(response == 'usuario_inactivo'){
                $("#div_respuesta_login").html("<p class='error_fb'>Usuario inactivo.</p>");
              }else if(response == "datos"){
                $("#div_respuesta_login").html("<p class='error_fb'>Datos incorrectos, favor de revisarlos.</p>");
              }else if(response == "verificacion"){
                $("#div_respuesta_login").html("<p class='error_fb'>Favor de verificar cuenta.</p>");
              }else if(response == "modo"){
                $("#div_respuesta_login").html("<p class='error_fb'>Modo de registro incorrecto.</p>");
              }else if(response == "recarga"){
                recarga_general();
              }else if(response == "recarga-home"){
                home();
              }else {
                $("#div_respuesta_login").html(response);
              }
            }
          });

        });

        //form registro página directa


        $("#form_registro_pagina").submit(function(e){
          e.preventDefault();
          var id = $(this).attr("id");
          let datos = $(this).serialize();
          if(id == "form_registro_pagina"){
            var id_contenedor = "contenedor_botones_registro";
            var id_contenedor_loader = "cont_loader_registro";
          }else if(id == "form_registro_hanger"){
            var id_contenedor = "cont_botones_hanger";
            var id_contenedor_loader = "cont_loader_hanger";
          }
          $.ajax({
            url: "<?php echo $ruta_global; ?>ajax/registro.ajax.php",
            type: "POST",
            data: datos,
            beforeSend : function(){
              $("#"+id_contenedor_loader).css("display","block");
              $("#"+id_contenedor).removeClass("habilitado");
              $("#"+id_contenedor).addClass("deshabilitado");
            },
            success: function(response){
              // console.log(response);
              $("#"+id_contenedor).removeClass("deshabilitado");
              $("#"+id_contenedor).addClass("habilitado");
              $("#"+id_contenedor_loader).css("display","none");
              // console.log("resp: "+response);
              if(response == "error_usuario"){
                $('#msj_error_registro').html('El usuario ingresado no está permitido.');
                $("#cont_error_usuario").css("display","block");
              }else if(response == "usuario_duplicado"){
                $('#msj_error_registro').html('El usuario ingresado ya existe.');
                $("#cont_error_usuario").css("display","block");
              }else if(response == "contrasenias"){
                $('#msj_error_registro').html('Las contraseñas no coinciden.');
                $("#cont_error_usuario").css("display","block");
              }else if(response == "caracteres"){
                $('#msj_error_registro').html('Verificar formato de usuario, no se permiten espacios ni carácteres especiales.');
                $("#cont_error_usuario").css("display","block");
              }else{
                $('#msj_error_registro').html('');
                $("#cont_error_usuario").css("display","none");
                $("#registrarse_modal").modal("close");
                $("#hanger_modal").modal("close");
                $("#form_registro_pagina")[0].reset();
                if(response == "error"){
                  $("#error_modal").modal("open");
                }else if(response == "directo"){
                  $("#registrado_directo").modal("open");
                }else if(response == "facebook"){
                  $("#registrado_facebook").modal("open");
                }else{
                  let r  = response.split('/');
                  let correo = r[1];
                  let usuario = r[2];
                  let onclick = "envia_otra_vez('"+correo+"','"+usuario+"')";
                  $("#enviar_nuevamente").attr("correo", correo);
                  $("#enviar_nuevamente").attr("usuario", usuario);
                  $("#registro_exitoso").modal("open");

                }
              }

            }
          });

        });





      //ajax enviar correo restablecer contraseña
      $("#form_reestablecer").submit(function(e){
        e.preventDefault();

        let datos = $(this).serialize();

        $.ajax({
          url: "ajax/cambio.ajax.php",
          type: "POST",
          data: datos,
          success: function(response){
            // console.log(response);
            $('#correoRestablecer').val('');
            $("#respuesta_contrasena_olvidada").html("<p class='error_fb'>Se ha enviado un correo para restablecer tu contraseña, por favor revisa tu bandeja de entrada</p>");
            $("#btn_olvidaste_contrasena").remove();
          }
        });

      });

      //Ajax formulario de contacto

      $("#form_contacto").submit(function(e){
        e.preventDefault();

        let datos = $(this).serialize();

        $.ajax({
          url: "ajax/contacto.ajax.php",
          type: "POST",
          data: datos,
          success: function(response){
            if(response=="okok"){
            $('#modal_contacto').modal("open");
            document.getElementById("form_contacto").reset();
          }else{
            $('#modal_completa_formulario').modal("open");
          }
          }
        });

      });


      //ajax restablecer contraseña
      $("#form_restablecer_contrasena").submit(function(e){
        e.preventDefault();

          url = window.location.pathname;
          correo = url.split('/');

        let datos = $(this).serialize() + '&correo=' + correo[3];


        $.ajax({
          url: "../ajax/restablecer_contrasena.ajax.php",
          type: "POST",
          data: datos,
          success: function(response){
            if(response == 'ok'){
              $("#form_restablecer_contrasena").trigger("reset");
              $("#respuesta_restablecer_contrasena").html("<a href='https://clientes.ink/urbanhangers'><input type='button' class='button-inicio  margin-log' value='INICIAR SESIÓN'/></a><p class='error_fb'>Se ha realizado el cambio de contraseña correctamente</p> ");
              $("#restablecer_boton").remove();

            }
          }
        });

      });



      //ajax subir diseño
      // $("#form_subir_diseno").submit(function(e){
      $(document).on("click","#btn_confirmar_subir_diseno",function(e){

         $('.card-modal').append('<div  class="contenedor_subiendo_diseno"><span>Subiendo diseño <br /> espere por favor...</span> <div id="modal-confirmacion" style="width: 100%;"></div></div>');
         $('.contenedor_subiendo_diseno').css('display','flex');




        e.preventDefault();
        let titulo_diseno = $("#input_titulo").val();
        let cp_user = $("#cp_user").val();
        let usuario = $('#hdnSession').attr('data-value');
        let input_titulo = $("#input_titulo").val();
        let input_inspiracion = $("#input_inspiracion").val();
        let coleccion = $('input:radio[name=coleccion]:checked').val();
        let input_diseno = $("#inputdiseno")[0].files[0];
        let arreglo_long = archivos.length;
        // console.log(input_diseno);
        // console.log(archivos);
        // var form = $('#form_subir_diseno')[0];

        var formData = new FormData();

        formData.append('cp_user', cp_user);
        formData.append('usuario', usuario);
        formData.append('inputdiseno', input_diseno);
        formData.append('nombre_diseno', input_titulo);
        formData.append('inspiracion_diseno', input_inspiracion);
        formData.append('coleccion', coleccion);
        formData.append('arreglo_long', arreglo_long);

        for(let i=0; i<= arreglo_long ; i++){
          formData.append('imagenes_areglo['+i+']', archivos[i])
        }

        $.ajax({
          url: "ajax/subir_diseno.ajax.php",
          type: "POST",
          data: formData,
          contentType: false,
          processData: false,
          cache: false,
          beforeSend : function(){
            // for(var x of formData.entries()) {
            //   console.log(x[0]+ ', '+ x[1]);
            // }
            divResultado = document.getElementById('modal-confirmacion');

            divResultado.innerHTML = loader();

          },
          success: function(response){
            // console.log(response);
            $('.contenedor_subiendo_diseno').css('display','none');
            if(response == 'error'){
              $('#error_modal').modal('open');
            }else{
              location.href =  "<?php echo $ruta_global; ?>publicado/"+response;
            }
          }
        });

      });

/*recargar en home inicio sesion aparte
$(".inicio-sesion-aparte").click(function(){
  setTimeout(function(){ location.href = "home"; }, 1000);
});*/



        $("#btn_confirmacion_hanger").click(function(){
          let actualiza = true;
          let tipo = "hanger";

          $.ajax({
            url: "<?php echo $ruta_global; ?>ajax/registro.ajax.php",
            type: "POST",
            data: {
              'actualiza' : actualiza,
              'tipo' : tipo
            },
            beforeSend : function(){
              $(".btn_modal_confirmar_upgrade").attr("disabled",true);
            },
            success: function(response){
              // console.log(response);
              if(response == "error"){
                $('#error_modal').modal('open');
              }else if(response == "error_hanger"){
                $("#modal_error_hanger").modal('open');
              }else{
                location.href =  "<?php echo $ruta_global; ?>"+response;

              }
            }
          });
        });

        $("#votante-tab").click(function(){

          $("#tipo_usuario").val("2");

        });

        $("#hanger-tab").click(function(){

          $("#tipo_usuario").val("1");

        });

      });


      function quitarAcentos(cadena){
        const acentos = {'á':'a','é':'e','í':'i','ó':'o','ú':'u','Á':'A','É':'E','Í':'I','Ó':'O','Ú':'U'};
        return cadena.split('').map( letra => acentos[letra] || letra).join('').toString();
      }




      $(document).on("click",".tipo_vista",function(){

        $(".tipo_vista").removeClass("tipo_vista_active");
        $(this).addClass("tipo_vista_active");

        let opcion = $(this).attr("op");

        if(opcion == "list"){
          $(".contenedor").removeClass("oculto");
          $(".cont_grid").addClass("oculto");
          $("html, body").animate({ scrollTop: 0 }, "slow");
        }else{
          $(".cont_grid").removeClass("oculto");
          $(".contenedor").addClass("oculto");
          $("html, body").animate({ scrollTop: 0 }, "slow");
        }

      });




      $(".cerrar_modal").click(function(){

        $(".modal_resultado").modal('close');

      });


      $(document).on("change", "#categorias", function(){
        console.log("cambio");
      });

      function inicio_sesion(){
        $('#inicio_sesion').modal("open");
      }

      function reestablecer_contrasena(){
        $('#inicio_sesion').modal("close");
        $('#reestablecer_contrasena').modal("open");
      }
      function cerrar_modal_reestablecer_contrasena(){
        $('#reestablecer_contrasena').modal("close");
      }

      function cerrar_modal_contacto(){
        $('#modal_contacto').modal("close");
      }

      function hanger(){
        $('#hanger_modal').modal("open");
      }

      function hanger_upgrade(){
        $('#hanger_upgrade_modal').modal("open");
      }

      function cerrar_hanger(){
        $('#hanger_modal').modal("close");
      }

      function cerrar_modal_inicio(){
        $('#inicio_sesion').modal("close");
      }

      function cerrar_modal_registrarse(){
        $('#registrarse_modal').modal("close");
      }

      function cerrar_modal_registro_exitoso(){
        $('#registro_exitoso').modal("close");
      }

      $(document).on('click', '.mostrar_password',  function(){

        let input = $(this).attr("inp");
        let tipo = $("#"+input).attr("type");
        // console.log("tipo "+tipo+" input "+input);
        if(tipo == 'password'){
          $('#'+input).attr('type','text');
          $(this).attr('src','<?php echo $ruta_global; ?>vistas/assets/img/icon-ocultar.svg');
          $(this).attr('title','Ocultar contraseña');

        }else{
          $('#'+input).attr('type','password');
          $(this).attr('src','<?php echo $ruta_global; ?>vistas/assets/img/icon-mostrar.svg');
          $(this).attr('title','Mostrar contraseña');

        }

      });

      function registrarse(){
        $('#inicio_sesion').modal("close");
        $('#registrarse_modal').modal("open");
        setTimeout(function(){ $('.tabs').tabs(); }, 1000);
      }

      $(".btn_dashboard_usuario").click(function(){

        let ruta = $(this).attr("user");

        window.location  = "<?php echo $ruta_global; ?>"+ruta;


      });

      function scrollDown(){
        $(".comentarios-scroll").animate({ scrollTop: $(".comentarios-scroll")[0].scrollHeight}, 1000);
      }

      function home(){
        location.href = "<?php echo $ruta_global; ?>home";
      }

      function regresa_general(){
          window.history.back();
      }

      function recarga_general(){
          location.reload(true);
      }

      // evento voto home y diseño
      $(document).on('click', '.evento_voto', function(){

        let id = $(this).attr('id');
        id = atob(id);
        let status = $(this).attr('status');
        let votos = document.getElementById('numero_votos'+id);
        let numero_votos = $(votos).attr('numero_de_votos');

        if (status == 'novotado') {
          $('.identificador_corazon_'+id).addClass("animate__animated animate__heartBeat");
          $('.identificador_corazon_'+id).css("background-image", "url(<?php echo $ruta_global; ?>vistas/assets/img/icon-votado.svg)");
          votos.innerHTML = parseInt(numero_votos)+1 + '<br/>' + ' VOTOS';
        }else if (status == 'votado') {
          $('.identificador_corazon_'+id).addClass("animate__animated animate__heartBeat");
          $('.identificador_corazon_'+id).css("background-image", "url(<?php echo $ruta_global; ?>vistas/assets/img/icon-no-votado.svg)");
          if(parseInt(numero_votos) == 1){
          votos.innerHTML = 'SIN' + '<br/>' + ' VOTOS';

          }else{
            votos.innerHTML = parseInt(numero_votos)-1 + '<br/>' + ' VOTOS';
          }
        }


      });



   // evento uno modal dashborad
   $(document).on('click', '.evento_voto_modal', function(){

     let id = $(this).attr('id');
     id = atob(id);
     let status = $(this).attr('status');
     let votos = document.getElementById('numero_votos'+id);
     let numero_votos = $(votos).attr('numero_de_votos');

     if (status == 'novotado') {
       $('.identificador_corazon_'+id).addClass("animate__animated animate__heartBeat");
       $('.identificador_corazon_'+id).css("background-image", "url(<?php echo $ruta_global; ?>vistas/assets/img/icon-votado.svg)");
       votos.innerHTML = parseInt(numero_votos)+1 + '<br/>' + ' VOTOS';
     }else if (status == 'votado') {
       $('.identificador_corazon_'+id).addClass("animate__animated animate__heartBeat");
       $('.identificador_corazon_'+id).css("background-image", "url(<?php echo $ruta_global; ?>vistas/assets/img/icon-no-votado.svg)");
       if(parseInt(numero_votos) == 1){
       votos.innerHTML = 'SIN' + '<br/>' + ' VOTOS';

       }else{
         votos.innerHTML = parseInt(numero_votos)-1 + '<br/>' + ' VOTOS';
       }
     }


   });

   //evento voto home y diseño
   var click= true;
   $(document).on('click', '.evento_voto', function(){

     let id = $(this).attr('id');
     id = atob(id);
     let status = $(this).attr('status');
     let votos = document.getElementById('numero_votos'+id);
     let numero_votos = $(votos).attr('numero_de_votos');
     let usuario = $(this).attr('usuario');
     usuario = atob(usuario);

    let datos = {
      "id": id,
      "status": status,
      "id_usuario": usuario

    }

    let datos2 = {
      "id_diseno": id,
      "id_usuario": usuario,
      "sesion" : sesion_modal,
      "tipo" : "votos"
    }
    let datos_grid = {
      "id": id,
      "id_usuario": usuario,
      "grid": 'grid',
      "sesion": sesion_modal
    }

    if(click){
        click= false;
        //AJAX
        $.ajax({
          url: '<?php echo $ruta_global; ?>ajax/likes.ajax.php',
          data: datos,
          type: "POST",
          success: function(response){
            // console.log(response);
            setTimeout(function(){
              $.ajax({
                url: '<?php echo $ruta_global; ?>ajax/comentarios_votos_home_diseno.ajax.php',
                data: datos2,
                type: "POST",
                success: function(response){
                  $("#conteo_votos"+id).html(response);
                }
              });
            }, 1000);
            setTimeout(function(){
              $.ajax({
                url: '<?php echo $ruta_global; ?>ajax/comentarios_votos_home_diseno.ajax.php',
                data: datos_grid,
                type: "POST",
                success: function(response){
                  $("#conteo_grid"+id).html(response);
                }
              });
            }, 1000);

          },
          error: function(){
            console.log("No se pudo realizar la acción");
          }
        });
        setTimeout(function(){
          click = true;
        }, 1000);
    }

   });
   //evento comentario home y diseño
   $(document).on('click', '.div-button-enviar', function(){

       let id = $(this).attr('usuario');
       let diseno = $(this).attr('diseno');
       let comentario = document.getElementById('input_comentario'+diseno).value;
       let fecha = $(this).attr('fecha');

       let datos_comentario ={
         "id":id,
         "diseno":diseno,
         "comentario":comentario,
         "fecha":fecha
       }

       let datos2 = {
         "id_diseno": diseno,
         "id_usuario": id,
         "sesion" : sesion_modal,
         "tipo" : "comentarios"
       }

       $.ajax({
         url: '<?php echo $ruta_global; ?>ajax/comentarios.ajax.php',
         data: datos_comentario,
         type: "POST",
         success: function(response){
           // console.log(response);
           $.ajax({
             url: '<?php echo $ruta_global; ?>ajax/comentarios_votos_home_diseno.ajax.php',
             data: datos2,
             type: "POST",
             success: function(response){
               $("#input_comentario"+diseno).val('');
               $("#comentarios"+diseno).html(response);
             }
           });
         },
         error: function(){
           console.log("No se pudo realizar la acción");
         }
       });

       $( "#comentarios"+diseno ).animate({ scrollTop: $( "#comentarios"+diseno )[0].scrollHeight}, 1000);

     });

     //evento voto con enter home y diseño
     $(document).on('keypress', '.input-comentario , .input-comentario-diseno', function(e){
        if(e.which === 13){
         let sesion = $(this).attr('flag');
         console.log(sesion);
         if(sesion == '1'){
           let id = $(this).attr('usuario');
           let diseno = $(this).attr('diseno');
           let comentario = document.getElementById('input_comentario'+diseno).value;
           let fecha = $(this).attr('fecha');

           let datos_comentario_enter ={
             "id":id,
             "diseno":diseno,
             "comentario":comentario,
             "fecha":fecha
           }

           let datos2 = {
             "id_diseno": diseno,
             "id_usuario": id,
             "sesion" : sesion_modal,
             "tipo" : "comentarios"
           }

           $.ajax({
             url: '<?php echo $ruta_global; ?>ajax/comentarios.ajax.php',
             data: datos_comentario_enter,
             type: "POST",
             success: function(response){
               // console.log(response);
               $.ajax({
                 url: '<?php echo $ruta_global; ?>ajax/comentarios_votos_home_diseno.ajax.php',
                 data: datos2,
                 type: "POST",
                 success: function(response){
                   // console.log(response);
                   $("#input_comentario"+diseno).val('');
                   $("#comentarios"+diseno).html(response);
                 }
               });
             },
             error: function(){
               console.log("No se pudo realizar la acción");
             }
           });
           $( "#comentarios"+diseno ).animate({ scrollTop: $( "#comentarios"+diseno )[0].scrollHeight}, 1000);
         }else {
          inicio_sesion();
         }

       }
  });

   // evento voto modal dashboard
   $(document).on('click', '.evento_voto_modal', function(){

     let id = $(this).attr('id');
     id = atob(id);
     let status = $(this).attr('status');
     let votos = document.getElementById('numero_votos'+id);
     let numero_votos = $(votos).attr('numero_de_votos');
     let usuario = $(this).attr('usuario');
     usuario = atob(usuario);

    let datos = {
      "id": id,
      "status": status,
      "id_usuario": usuario
    }

    let datos2 = {
      "id_diseno": id,
      "id_usuario": usuario,
      "sesion" : sesion_modal,
      "tipo" : "votos"
    }

    if(click){
        click= false;
        //AJAX
        $.ajax({
          url: '<?php echo $ruta_global; ?>ajax/likes.ajax.php',
          data: datos,
          type: "POST",
          success: function(response){
            // console.log(response);
            setTimeout(function(){
              $.ajax({
                url: '<?php echo $ruta_global; ?>ajax/comentarios_votos_dashboard.ajax.php',
                data: datos2,
                type: "POST",
                success: function(response){
                  // console.log(response);
                  $("#conteo_votos"+id).html(response);
                }
              });
            }, 1000);

          },
          error: function(){
            console.log("No se pudo realizar la acción");
          }
        });
        setTimeout(function(){
          click = true;
        }, 1000);
    }

   });


   //evento boton enviar comentario modal dashboard
   $(document).on('click', '.evento_comentario_modal_div', function(){

       let id = $(this).attr('usuario');
       let diseno = $(this).attr('diseno');
       let comentario = document.getElementById('input_comentario'+diseno).value;
       let fecha = $(this).attr('fecha');

       let datos_comentario ={
         "id":id,
         "diseno":diseno,
         "comentario":comentario,
         "fecha":fecha
       }

       let datos2 = {
         "id_diseno": diseno,
         "id_usuario": id,
         "sesion" : sesion_modal,
         "tipo" : "comentarios"
       }

       $.ajax({
         url: '<?php echo $ruta_global; ?>ajax/comentarios.ajax.php',
         data: datos_comentario,
         type: "POST",
         success: function(response){
           // console.log(response);
           $.ajax({
             url: '<?php echo $ruta_global; ?>ajax/comentarios_votos_dashboard.ajax.php',
             data: datos2,
             type: "POST",
             success: function(response){
               // console.log(response);
               $("#input_comentario"+diseno).val('');
               $("#comentarios"+diseno).html(response);
             }
           });
         },
         error: function(){
           console.log("No se pudo realizar la acción");
         }
       });

       $( "#comentarios"+diseno ).animate({ scrollTop: $( "#comentarios"+diseno )[0].scrollHeight}, 1000);

     });


     // funcion enviar comentario modal presionando enter
    $(document).on('keypress', '.evento_input_modal', function(e){
       // console.log("entro evento");
       if(e.which === 13){
        let sesion = $(this).attr('flag');
        console.log(sesion);
        if(sesion == '1'){
          let id = $(this).attr('usuario');
          let diseno = $(this).attr('diseno');
          let comentario = document.getElementById('input_comentario'+diseno).value;
          let fecha = $(this).attr('fecha');

          let datos_comentario_enter ={
            "id":id,
            "diseno":diseno,
            "comentario":comentario,
            "fecha":fecha
          }

          let datos2 = {
            "id_diseno": diseno,
            "id_usuario": id,
            "sesion" : sesion_modal,
            "tipo" : "comentarios"
          }

          $.ajax({
            url: '<?php echo $ruta_global; ?>ajax/comentarios.ajax.php',
            data: datos_comentario_enter,
            type: "POST",
            success: function(response){
              // console.log(response);
              $.ajax({
                url: '<?php echo $ruta_global; ?>ajax/comentarios_votos_dashboard.ajax.php',
                data: datos2,
                type: "POST",
                success: function(response){
                  // console.log(response);
                  $("#input_comentario"+diseno).val('');
                  $("#comentarios"+diseno).html(response);
                }
              });
            },
            error: function(){
              console.log("No se pudo realizar la acción");
            }
          });
          $( "#comentarios"+diseno ).animate({ scrollTop: $( "#comentarios"+diseno )[0].scrollHeight}, 1000);
        }else {
          inicio_sesion();
        }

      }
 });


       //funcion destruir slick

       function destroyCarousel() {
        if ($('#disenio_slider').hasClass('slick-initialized')) {
          $('#disenio_slider').slick('unslick');
        }
      }

        function rehacerCarousel(){
          $('#disenio_slider').slick({
            slide:'div',
            centerMode: true,
            centerPadding: '60px',
            slidesToShow: 1,
            infinite: true,
            variableWidth: true,
            responsive: [
              {
                breakpoint: 768,
                settings: {
                  arrows: false,
                  centerMode: true,
                  centerPadding: '40px',
                  slidesToShow: 3
                }
              },
              {
                breakpoint: 480,
                settings: {
                  arrows: false,
                  centerMode: true,
                  centerPadding: '40px',
                  slidesToShow: 1
                }
              }
            ]
          });

          }

      $(document).on("click", ".evento_filtro_hanger .custom-select .select-items", function(){

        let filtro = $("#select_filtro_dashboard").val();
        console.log(filtro);
        if(filtro != 0){
          console.log(' entro if ');
          if( $('#cambioTienda').hasClass('titulo') && $('#cambioNuevos').hasClass('titulo-inactivo') || $('#btn_sm_tienda').hasClass('active_btn')){
            disenos_dashboard('tienda',filtro);
          }else if( $('#cambioNuevos').hasClass('titulo') && $('#cambioTienda').hasClass('titulo-inactivo') ||  $('#btn_sm_votar').hasClass('active_btn')){
            disenos_dashboard('votacion',filtro);
          }else {
            console.log('else');
          }
          $("#select_filtro_dashboard").val(0);
        }

      });



      $("#usuarioHanger").keyup(function() {
        let usuario = $("#usuarioHanger").val();
        $("#usuarioHanger").val(usuario.toLowerCase());
      });

      $("#txtUsuarioRegistro").keyup(function() {
        let usuario = $("#txtUsuarioRegistro").val();
        $("#txtUsuarioRegistro").val(usuario.toLowerCase());
      });

      $("#correoHanger").keyup(function() {
        let correo = $("#correoHanger").val();
        $("#correoHanger").val(correo.toLowerCase());
      });

      $("#txtCorreoRegistro").keyup(function() {
        let correo = $("#txtCorreoRegistro").val();
        $("#txtCorreoRegistro").val(correo.toLowerCase());
      });

    $("#img_header_perfil").click(function(){
      let flag = $("#img_header_perfil").attr('flag');
      if(flag == '0'){ // esta cerrado
        muestra_menu();
      }else { // esta abierto
        oculta_menu();
      }
    });

    $(document).on("click",function(e) {
        var container = $("#cont_img_header");
          if (!container.is(e.target) && container.has(e.target).length === 0) {
            oculta_menu();
          }
    });

    function oculta_menu(){
      $("#img_header_perfil").attr('flag','0');
      $("#img_header_perfil").addClass('borde_img_perfil_hd');
      $("#img_header_perfil").removeClass('borde_img_menu');
      $("#nombreLog").removeClass('no_visible');
      $(".menu_sesion").addClass('oculto');
    }

    function muestra_menu(){
      $("#img_header_perfil").attr('flag','1');
      $("#img_header_perfil").removeClass('borde_img_perfil_hd');
      $("#img_header_perfil").addClass('borde_img_menu');
      $("#nombreLog").addClass('no_visible');
      $(".menu_sesion").removeClass('oculto');
    }

    function MostrarConsulta(url, key, carpeta, nombre_diseno, inspiracion_diseno){
      divResultado = document.getElementById('datos_modal_dashboard');
      // divResultado2 = document.getElementById('contenedor-datos');
      // let datos ={
      //   "key": key,
      //   "carpeta": carpeta,
      //   "consultaSlider": "modal"
      // }

      let datos2 = {
        "id_diseno": key,
        "carpeta": carpeta,
        "nombre_diseno" : nombre_diseno,
        "inspiracion_diseno" : inspiracion_diseno,
        "id_usuario": id_usuario_modal,
        "sesion": sesion_modal
      }
      // $.ajax({
      // 	url: url,
      //   data: datos,
      //   type: "POST",
      //   beforeSend : function(){
      //     // console.log(key+" "+carpeta);
      //   },
      // 	success: function(respuesta) {
      //     // console.log(respuesta);
      //     divResultado.innerHTML = respuesta;
      //
      // 	},
      // 	error: function() {
      //         console.log("No se ha podido obtener la información");
      //     }
      // });

      $.ajax({
      	url: 'ajax/modal_dashboard.ajax.php',
        data: datos2,
        type: "POST",
        beforeSend: function(){
          divResultado.innerHTML = loader();
        },
      	success: function(respuesta) {
          // console.log(respuesta);
          divResultado.innerHTML = respuesta;
      	},
      	error: function() {
              console.log("No se ha podido obtener la información");
        }
      });


    }

    $(document).on("click",".evento_modal_diseno",function(){
        $('#modal_detalle').modal('open');
    });

    $(document).on("click",".evento_modal_diseno_tienda",function(){
        let ruta = $(this).attr('tienda');
        console.log(ruta);
        window.open(ruta);
    });

    $(document).on("click","#btn_sm_tienda",function(){
      $('#btn_sm_tienda').addClass('active_btn');
      $('#btn_sm_votar').removeClass('active_btn');
      $('#btn_sm_votar').addClass('inactive_btn');
      $('.select-items').children().eq(2).addClass('eliminado');
      disenos_dashboard('tienda','nuevos');
    });
    $(document).on("click","#btn_sm_votar",function(){
      $('#btn_sm_votar').addClass('active_btn');
      $('#btn_sm_tienda').removeClass('active_btn');
      $('#btn_sm_tienda').addClass('inactive_btn');
      $('.select-items').children().eq(2).removeClass('eliminado');
      disenos_dashboard('votacion','nuevos');
    });

    $(document).on("click",".slick-slide",function(){
      let key = $(this).attr('key');
      let carpeta = $(this).attr('file');
      let nombre_diseno = $(this).attr('data-nombre');
      let inspiracion_diseno =  $(this).attr('data-inspo');
      MostrarConsulta('ajax/slider.ajax.php', key, carpeta, nombre_diseno, inspiracion_diseno);
    });

    $('.btn_cambio_diseno').on('click',function(){
        $('.select-selected').html('FILTRAR POR CATEGORÍA');
        if($(this).attr('id') == 'cambioNuevos'){
          $('#cambioNuevos').removeClass('titulo-inactivo');
          $('#cambioNuevos').addClass('titulo');
          $('#cambioTienda').removeClass('titulo');
          $('#cambioTienda').addClass('titulo-inactivo');
          $('.select-items').children().eq(2).removeClass('eliminado');
          disenos_dashboard('votacion','nuevos');
        }else {
          $('#cambioTienda').removeClass('titulo-inactivo');
          $('#cambioTienda').addClass('titulo');
          $('#cambioNuevos').removeClass('titulo');
          $('#cambioNuevos').addClass('titulo-inactivo');
          $('.select-items').children().eq(2).addClass('eliminado');
          disenos_dashboard('tienda','nuevos');
        }

    });

    function disenos_dashboard(tipo,filtro){
      let id_usuario = $("#select_filtro_dashboard").attr('key');
      $('.disenio_slider').slick('unslick');
      // console.log(id_usuario+" "+tipo+" "+filtro);
      $.ajax({
        url: "<?php echo $ruta_global; ?>ajax/disenosDashboard.ajax.php",
        type: "POST",
        data: {
          'id_usuario' : id_usuario,
          'filtro' : filtro,
          'tipo' : tipo
        },
        beforeSend : function(){

          $('#sinresultados_dash').addClass('oculto');
          $('#disenio_slider').removeClass('oculto');
          $("#disenos_tienda_dashboard_movil").html(loader());

        },
        success: function(response){
           console.log(response);
          if(response == '0'){
            $('#sinresultados_dash').removeClass('oculto');
            $('#disenio_slider').addClass('oculto');
          }else {
            $('#disenio_slider').removeClass('oculto');
            $('#sinresultados_dash').addClass('oculto');
            destroyCarousel();
            $("#disenos_tienda_dashboard_movil").html(response);
            rehacerCarousel();
            // $('.slideDiseño').on('click',function(){
            //     $('#modal_detalle').modal('open');
            // });
            $(".close-modal-diseño").click(function (){
              $('#modal_detalle').modal("close");
            });
            $('.modal').modal();
          }
        }
      });
    }





    // MENU MOVIL

    const menu_open = () => {
      let menu_status = $(".menu_movil").attr("estatus");

      if (menu_status == 0) {

        $("body").css("overflow", "hidden");
        $(".ln_mv_bt").css("transition-delay", "0s");
        $(".ln_mv_tp").css("transition-delay", "0s");
        $("header").css("transition-delay", "0s");
        $(".cls-1").css("transition-delay", "0s");
        $(".ln_mv_bt").css("background-color", "white");
        $(".ln_mv_tp").css("background-color", "white");
        $(".ln_mv_bt").css("width", "40px");
        $(".ln_mv_tp").css("width", "40px");
        $(".ln_mv_bt").css("height", "4px");
        $(".ln_mv_tp").css("height", "4px");
        $(".cls-1").css("fill", "white");
        $(".ln_mv_tp").css("margin-bottom", "-4px");
        $(".ln_mv_tp").css("transform", "rotate(45deg)");
        $(".ln_mv_bt").css("transform", "rotate(-45deg)");
        $(".menu_display").css("height", "100vh");
        $("header").css("background-color", "#FF5C1A");
        $(".menu_movil").attr("estatus" , "1");
        // $(".menu_cont_mv").css("display","flex");
        setTimeout(function(){ $(".menu_cont_mv").css("display","flex"); }, 500);

      }else {

        $("body").css("overflow", "auto");
        $(".menu_display").css("transition-delay", "0s");
        $(".ln_mv_bt").css("transition-delay", "1s");
        $(".ln_mv_tp").css("transition-delay", "1s");
        $("header").css("transition-delay", "1s");
        $(".cls-1").css("transition-delay", "1s");
        $(".ln_mv_tp").css("margin-bottom", "10px");
        $(".ln_mv_tp").css("transform", "rotate(0deg)");
        $(".ln_mv_bt").css("transform", "rotate(0deg)");
        $(".menu_display").css("height", "0vh");
        $("header").css("background-color", "white");
        $(".ln_mv_bt").css("background-color", "#FF5C1A");
        $(".ln_mv_tp").css("background-color", "#FF5C1A");
        $(".ln_mv_bt").css("width", "30px");
        $(".ln_mv_tp").css("width", "45px");
        $(".ln_mv_bt").css("height", "2px");
        $(".ln_mv_tp").css("height", "2px");
        $(".cls-1").css("fill", "#FF5C1A");
        $(".menu_movil").attr("estatus" , "0");
        $(".menu_cont_mv").css("display","none");

      }


    }

    $('.menu_movil').on('click', function(e){
      // console.log("MENU MOVIL");
      menu_open();
    });



    // FIN MENU MOVIL

    // MODAL

    // const modal_imagenes = () => {
    //
    //   let modal_contenido = `<div id="modal_imagen" class="modal modal-inicio h500">
    //       <div class="modal-content modal-padding">
    //         <span>Prueba de modal imagenes </span>
    //     </div>
    //   </div>`;
    //
    //   console.log("modal imagenes");
    //   return modal_contenido;
    //
    //
    // }



    $('.img_material').on('click', function(e){
      // console.log("modal click");
      // modal_imagenes();
      let ruta = $(this).attr("ruta");
      $(".imagen_full_view").attr("src",""+ruta);
      $("#modal_imagen").modal("open");
    });

    $('#modal_imagen').on('click', function(e){
      // console.log("modal click");
      // modal_imagenes();
      $("#modal_imagen").modal("close");
    });






    // FIN MODAL


    // LOADER

    const loader = () => {
      return `<div class="progress abs_center loader_colores">
                <div class="indeterminate loader_colores2"></div>
              </div>`;
    }

  // FIN LOADER

  function inicio_sesion_movil(){
    location.href = '<?php echo $ruta_global; ?>inicio-sesion';
  }


  // evento votos movil
  $(document).on('click', '.evento_voto_movil', function(){

    let id = $(this).attr('id');
    let status = $(this).attr('status');
    let numero_votos = $("#num_votos_mov_"+atob(id)).attr('numero_de_votos');
    let usuario = $(this).attr('usuario');
    let pantalla = $(this).attr('file');
    // 1 : home 2 : diseno 3: dash
    if (status == 'novotado') {
      $('.identificador_corazon_'+atob(id)).removeClass("no-votado-mov");
      $('.identificador_corazon_'+atob(id)).addClass("votado-mov");
      $('.identificador_corazon_'+atob(id)).addClass("animate__animated animate__heartBeat");
      $("#num_votos_mov_"+atob(id)).html(parseInt(numero_votos)+1);
    }else if (status == 'votado') {
      $('.identificador_corazon_'+atob(id)).removeClass("votado-mov");
      $('.identificador_corazon_'+atob(id)).addClass("no-votado-mov");
      $('.identificador_corazon_'+atob(id)).addClass("animate__animated animate__heartBeat");
      if(parseInt(numero_votos) == 1){
        $("#num_votos_mov_"+atob(id)).html('SIN');
      }else{
        $("#num_votos_mov_"+atob(id)).html(parseInt(numero_votos)-1);
      }
    }

   let datos = {
     "id": atob(id),
     "status": status,
     "id_usuario": atob(usuario)
   }

   let datos2 = {
     "id_diseno": id,
     "id_usuario": usuario,
     "sesion" : sesion_modal,
     "tipo" : "votos",
     "pantalla" : pantalla
   }

  if(click){
      click= false;
       //AJAX
      $.ajax({
        url: '<?php echo $ruta_global; ?>ajax/likes.ajax.php',
        data: datos,
        type: "POST",
        success: function(response){
          setTimeout(function(){
            $.ajax({
              url: '<?php echo $ruta_global; ?>ajax/comentarios_votos_movil.ajax.php',
              data: datos2,
              type: "POST",
              success: function(response){
                $("#sp_vot_movil_"+atob(id)).html(response);
              }
            });
          }, 1000);
        }
      });
      setTimeout(function(){
        click = true;
      }, 1000);
  }

  });
  // fin evento votos movil

  //evento comentarios con enter movil
  $(document).on('keypress', '.comen_enter_mov ', function(e){
    if(e.which === 13){
      let sesion = $(this).attr('flag');
      if(sesion == '1'){
        let id = $(this).attr('usuario');
        let diseno = $(this).attr('diseno');
        let comentario = document.getElementById('input_comentario_'+diseno).value;
        let fecha = $(this).attr('fecha');
        let pantalla = $(this).attr('file');

        if(comentario != ''){
          let datos_comentario_enter ={
            "id":id,
            "diseno":diseno,
            "comentario":comentario,
            "fecha":fecha
          }

          let datos2 = {
            "id_diseno": diseno,
            "id_usuario": id,
            "sesion" : sesion_modal,
            "tipo" : "comentarios",
            "pantalla" : pantalla
          }
          $.ajax({
            url: '<?php echo $ruta_global; ?>ajax/comentarios.ajax.php',
            data: datos_comentario_enter,
            type: "POST",
            success: function(response){
              // console.log(response);
              $.ajax({
                url: '<?php echo $ruta_global; ?>ajax/comentarios_votos_movil.ajax.php',
                data: datos2,
                type: "POST",
                success: function(response){
                  // console.log(response);
                  $("#input_comentario_"+diseno).val('');
                  $("#comentarios_mov_"+diseno).html(response);
                }
              });
            }
          });
          $( "#comentarios_mov_"+diseno ).animate({ scrollTop: $( "#comentarios_mov_"+diseno )[0].scrollHeight}, 1000);
        }
      }else {
       inicio_sesion_movil();
      }

    }
  });

  //evento flecha atrás versión movil
$(document).on('click', '#flecha_atras_diseno', function(){
  window.history.back();
});

  //evento comentario boton movil
  $(document).on('click', '.evento_btn_comen_movil', function(){

    let id = $(this).attr('usuario');
    let diseno = $(this).attr('diseno');
    let comentario = document.getElementById('input_comentario_'+diseno).value;
    let fecha = $(this).attr('fecha');
    let pantalla = $(this).attr('file');

    if(comentario != ''){
      let datos_comentario_enter ={
        "id":id,
        "diseno":diseno,
        "comentario":comentario,
        "fecha":fecha
      }

      let datos2 = {
        "id_diseno": diseno,
        "id_usuario": id,
        "sesion" : sesion_modal,
        "tipo" : "comentarios",
        "pantalla" : pantalla
      }
      $.ajax({
        url: '<?php echo $ruta_global; ?>ajax/comentarios.ajax.php',
        data: datos_comentario_enter,
        type: "POST",
        success: function(response){
          // console.log(response);
          $.ajax({
            url: '<?php echo $ruta_global; ?>ajax/comentarios_votos_movil.ajax.php',
            data: datos2,
            type: "POST",
            success: function(response){
              // console.log(response);
              $("#input_comentario_"+diseno).val('');
              $("#comentarios_mov_"+diseno).html(response);
            }
          });
        }
      });
      $( "#comentarios_mov_"+diseno ).animate({ scrollTop: $( "#comentarios_mov_"+diseno )[0].scrollHeight}, 1000);
    }

  });


// FUNCION PARA ABRIR Y CERRAL EL MENU DE SHARE
  const menu_open_share = (id) => {

      $("#share_menu_"+id).toggleClass("card_cont_info_share");

  }
  // FIN DE SHARE

// FUNCION PARA ANIMATED MOVIL

  const movil_animation = (id, animate_class) => {

    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){

      $("#"+id).removeClass("none_effect");

      $("#"+id).addClass(animate_class);

    }else{

      // $("#"+id).removeClass(animate_class);

    }
  }

// FIN FUNCION PARA ANIMATED

  </script>
