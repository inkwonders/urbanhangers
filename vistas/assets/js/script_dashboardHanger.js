
$(document).ready(function(){

  $('#disenio_slider').slick({
    slide:'div',
    centerMode: true,
    centerPadding: '60px',
    slidesToShow: 1,
    infinite: true,
    variableWidth: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 1
        }
      }
    ]
  });

});

  $('.modal').modal();
  $('.collapsible').collapsible();


$(".close_modal_detalle").click(function (){
  $('#modal_detalle').modal("close");
});


$(document).on("change", ".custom-select", function(){
  $(".select-items").children().eq(0).addClass("nuevos");
  $(".select-items").children().eq(3).addClass("colecciones");
});


$(document).ready(function(){
  movil_animation("dash_effect", "animate__backInDown");
});
