$("#cancelar_editar_perfil").click(function () {
  regresa_general();
});

function go_ruta(ruta) {
  window.location.href = ruta.toLowerCase();
}

$("#cambiarimagen_perfil").click(function () {
  $("#foto_perfil").click();
});

$("#cambiarimagen_banner").click(function () {
  console.log("entro ");
  $("#input_banner").click();
});

$("#foto_perfil").on("change", function () {
  let div_original = div_perfil;
  if ($("#foto_perfil").val() != "") { 
    $(".msj_carga_imagen_perfil").removeClass("oculto");
    var archivos = document.getElementById("foto_perfil").files;
    var navegador = window.URL || window.webkitURL;

    var file = this.files[0];
    var img = new Image();
    var objectUrl = navegador.createObjectURL(file);
    let width, height;

    img.onload = function () {
      width = this.width;
      height = this.height;

      /* Validar tamaño y tipo de archivo */
      var size = archivos[0].size;
      var type = archivos[0].type;
      var name = archivos[0].name;
      
      if ((width < 1500 && width > 180) || (height < 1500 && height > 180)) {
        var truena_dimensiones = false;
      } else {
        var truena_dimensiones = true;
      }

      if (size > 1024 * 1024 * 5) {
        $(".msj_carga_imagen_perfil").addClass("oculto");
        $("#foto_perfil").val("");
        $("#espacio_foto_perfil").html(div_original);
        // $(".cont_imagen_editar_perfil").append("<p style='color: red'>El archivo "+name+" supera el máximo permitido 5MB</p>");
        $("#btn_error_genercio").removeAttr("onclick");
        $("#msj_error_modal").css("overflow-wrap", "anywhere");
        $("#msj_error_modal").html("El archivo supera el máximo permitido 5MB.");
        $("#error_modal_generico").modal("open");
      } else if (
        type != "image/jpeg" &&
        type != "image/jpg" &&
        type != "image/png"
      ) {
        $(".msj_carga_imagen_perfil").addClass("oculto");
        $("#foto_perfil").val("");
        $("#espacio_foto_perfil").html(div_original);
        // $(".cont_imagen_editar_perfil").append("<p style='color: red'>El archivo "+name+" no es del tipo de imagen permitida.</p>");
        $("#btn_error_genercio").removeAttr("onclick");
        $("#msj_error_modal").css("overflow-wrap", "anywhere");
        $("#msj_error_modal").html(
          "El archivo no es del tipo de imagen permitida."
        );
        $("#error_modal_generico").modal("open");
      }else if (truena_dimensiones) {
        $(".msj_carga_imagen_perfil").addClass("oculto");
        $("#foto_perfil").val("");
        $("#espacio_foto_perfil").html(div_original);
        // $(".cont_imagen_editar_perfil").append("<p style='color: red'>El archivo "+name+" no es del tipo de imagen permitida.</p>");
        $("#btn_error_genercio").removeAttr("onclick");
        $("#msj_error_modal").css("overflow-wrap", "anywhere");
        $("#msj_error_modal").html(
          "La imagen no cumple con las dimensiones especificadas."
        );
        $("#error_modal_generico").modal("open");
      }else {
        var objeto_url = navegador.createObjectURL(archivos[0]);
        $("#espacio_foto_perfil").html(
          '<div class="imagen-perfil-editar" style="background: url(' +
            objeto_url +
            ')"></div>'
        );
        $(".msj_carga_imagen_perfil span").html(name);
      }

    };

    img.src = objectUrl;

  } else {
    $(".msj_carga_imagen_perfil").addClass("oculto");
    $("#espacio_foto_perfil").html(div_original);
  }
});

$("#input_banner").on("change", function () {
  let div_original = div_banner;
  if ($("#input_banner").val() != "") {
    $(".msj_carga_imagen_banner").removeClass("oculto");
    var archivos = document.getElementById("input_banner").files;
    var navegador = window.URL || window.webkitURL;

    var file = this.files[0];
    var img = new Image();
    var objectUrl = navegador.createObjectURL(file);
    let width, height;

    img.onload = function () {
      width = this.width;
      height = this.height;

      var size = archivos[0].size;
      var type = archivos[0].type;
      var name = archivos[0].name;

      if ((width < 2080 && width > 776) || (height < 840 && height > 169)) {
        var truena_dimensiones = false;
      } else {
        var truena_dimensiones = true;
      }

      if (size > 1024 * 1024 * 5) {
        $(".msj_carga_imagen_banner").addClass("oculto");
        $("#input_banner").val("");
        $("#espacio_banner").html(div_original);
        // $(".cont_input_banner").append("<p style='color: red'>El archivo "+name+" supera el máximo permitido 5MB</p>");

        $("#btn_error_genercio").removeAttr("onclick");
        $("#msj_error_modal").css("overflow-wrap", "anywhere");
        $("#msj_error_modal").html(
          "El archivo supera el máximo permitido 5MB."
        );
        $("#error_modal_generico").modal("open");
      } else if (
        type != "image/jpeg" &&
        type != "image/jpg" &&
        type != "image/png"
      ) {
        $(".msj_carga_imagen_banner").addClass("oculto");
        $("#input_banner").val("");
        $("#espacio_banner").html(div_original);
        // $(".cont_input_banner").append("<p style='color: red'>El archivo "+name+" no es del tipo de imagen permitida.</p>");
        $("#btn_error_genercio").removeAttr("onclick");
        $("#msj_error_modal").css("overflow-wrap", "anywhere");
        $("#msj_error_modal").html(
          "El archivo no es del tipo de imagen permitida."
        );
        $("#error_modal_generico").modal("open");
      } else if (truena_dimensiones) {
        $(".msj_carga_imagen_banner").addClass("oculto");
        $("#input_banner").val("");
        $("#espacio_banner").html(div_original);
        // $(".cont_input_banner").append("<p style='color: red'>El archivo "+name+" no es del tipo de imagen permitida.</p>");
        $("#btn_error_genercio").removeAttr("onclick");
        $("#msj_error_modal").css("overflow-wrap", "anywhere");
        $("#msj_error_modal").html(
          "La imagen no cumple con las dimensiones especificadas."
        );
        $("#error_modal_generico").modal("open");
      } else {
        var objeto_url = navegador.createObjectURL(archivos[0]);
        let vista_previa =
          '<div class="banner-perfil" style="background: url(' +
          objeto_url +
          ')"></div>';
        $("#espacio_banner").html(vista_previa);
        $(".msj_carga_imagen_banner span").html(name);
      }
    };
    img.src = objectUrl;
  } else {
    $(".msj_carga_imagen_banner").addClass("oculto");
    $("#espacio_banner").html(div_original);
  }
});

$("#editar_perfil").submit(function (e) {
  e.preventDefault();

  // var formData = new FormData();

  // var datos = $(this).serializeArray();
  // for(var i=0; i < datos.length; i++){
  //   formData.append(datos[i].name, datos[i].value);
  // }

  var form = $("#editar_perfil")[0];
  var formData = new FormData(form);

  let validaciones = validar_campos_obligatorios();

  if (validaciones) {
    $.ajax({
      url: "ajax/editarperfil.ajax.php",
      type: "POST",
      data: formData,
      contentType: false,
      processData: false,
      beforeSend: function () {
        // for(var x of formData.entries()) {
        //   console.log(x[0]+ ', '+ x[1]);
        // }
        $("#div_botones").addClass("oculto");
        $("#div_loader").removeClass("oculto");
      },
      success: function (response) {
        $("#div_loader").addClass("oculto");
        $("#div_botones").removeClass("oculto");
        // console.log(response);
        if (response == "error") {
          $("#error_modal").modal("open");
        } else if (response == "error_contrasenias") {
          $("#btn_error_genercio").attr("onclick", "recarga_general()");
          $("#msj_error_modal").html("Las contraseñas no coinciden");
          $("#error_modal_generico").modal("open");
        } else if (response == "error_contrasenia_actual") {
          $("#btn_error_genercio").attr("onclick", "recarga_general()");
          $("#msj_error_modal").html("La contraseña actual es incorrecta");
          $("#error_modal_generico").modal("open");
        } else if (response == "usuario_duplicado") {
          $("#btn_error_genercio").attr("onclick", "recarga_general()");
          $("#msj_error_modal").html("El usuario ingresado ya existe");
          $("#error_modal_generico").modal("open");
        } else if (response == "error_caracteres") {
          $("#btn_error_genercio").removeAttr("onclick");
          $("#msj_error_modal").html(
            "No se permiten caracteres especiales en los campos obligatorios."
          );
          $("#error_modal_generico").modal("open");
        } else if (response == "error_imagen_perfil") {
          $("#btn_error_genercio").attr("onclick", "recarga_general()");
          $("#msj_error_modal").html(
            "El peso de la imagen debe ser menor o igual a 5MB, y las dimensiones mínimas 180x180px y máximas 1500x1500px"
          );
          $("#error_modal_generico").modal("open");
        } else if (response == "error_imagen_banner") {
          $("#btn_error_genercio").attr("onclick", "recarga_general()");
          $("#msj_error_modal").html(
            "El peso de la imagen debe ser menor o igual a 5MB, y las dimensiones mínimas 840x169px y máximas 2080x776px"
          );
          $("#error_modal_generico").modal("open");
        } else if (response == "error_usuario") {
          $("#btn_error_genercio").attr("onclick", "recarga_general()");
          $("#msj_error_modal").html("El usuario ingresado no está permitido");
          $("#error_modal_generico").modal("open");
        } else {
          $("#btn_aceptar_cambio_exitoso").attr(
            "onclick",
            'go_ruta("' + response + '")'
          );
          $("#msj_cambio_exitoso").html(
            "Tu perfil ha sido actualizado correctamente"
          );
          $("#cambio_exitoso_generico").modal("open");
        }
      },
    });
  } else {
    $("html, body").animate(
      {
        scrollTop: $(".err_inp").offset().top - 300,
      },
      1000
    );
  }
});

function validar_campos_obligatorios() {
  let usuario = $("#input_usuario").val();
  let nombres = $("#input_nombres").val();
  let apellidos = $("#input_apellidos").val();
  let regex_usuario = /^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ-]+$/;
  let regex_nombres = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/;

  let bool_nombres = false,
    bool_usuario = false,
    bool_apellidos = false;

  if (!regex_usuario.test(usuario)) {
    $("#input_usuario").addClass("err_inp");
    $("#sp_error_usuario").removeClass("oculto");
    bool_usuario = false;
  } else {
    $("#input_usuario").removeClass("err_inp");
    $("#sp_error_usuario").addClass("oculto");
    bool_usuario = true;
  }

  if (!regex_nombres.test(nombres)) {
    $("#input_nombres").addClass("err_inp");
    $("#sp_error_nombres").removeClass("oculto");
    bool_nombres = false;
  } else {
    $("#input_nombres").removeClass("err_inp");
    $("#sp_error_nombres").addClass("oculto");
    bool_nombres = true;
  }

  if (!regex_nombres.test(apellidos)) {
    $("#input_apellidos").addClass("err_inp");
    $("#sp_error_apellidos").removeClass("oculto");
    bool_apellidos = false;
  } else {
    $("#input_apellidos").removeClass("err_inp");
    $("#sp_error_apellidos").addClass("oculto");
    bool_apellidos = true;
  }

  if (bool_usuario && bool_nombres && bool_apellidos) {
    return true;
  } else {
    return false;
  }
}

$("#input_usuario").keyup(function () {
  let usuario = $("#input_usuario").val();
  $("#input_usuario").val(usuario.toLowerCase());
});

$(".ico-titulo-duda").mouseover(function () {
  $(".msj-titulo").css("opacity", "1");
});

$(".ico-titulo-duda").mouseout(function () {
  $(".msj-titulo").css("opacity", "0");
});

$(".ico-banner-imagen").mouseover(function () {
  $(".msj-banner").css("opacity", "1");
});

$(".ico-banner-imagen").mouseout(function () {
  $(".msj-banner").css("opacity", "0");
});

$(".ico-nombre-perfil").mouseover(function () {
  $(".msj-nombre").css("opacity", "1");
});

$(".ico-nombre-perfil").mouseout(function () {
  $(".msj-nombre").css("opacity", "0");
});

$(".ico-ubicacion-perfil").mouseover(function () {
  $(".msj-ubicacion").css("opacity", "1");
});

$(".ico-ubicacion-perfil").mouseout(function () {
  $(".msj-ubicacion").css("opacity", "0");
});

$(".ico-ubicacion2-perfil").mouseover(function () {
  $(".msj-ubicacion2").css("opacity", "1");
});

$(".ico-ubicacion2-perfil").mouseout(function () {
  $(".msj-ubicacion2").css("opacity", "0");
});

$(".ico-biografia-perfil").mouseover(function () {
  $(".msj-biografia").css("opacity", "1");
});

$(".ico-biografia-perfil").mouseout(function () {
  $(".msj-biografia").css("opacity", "0");
});

/** redes sociales **/

$(".ico-be-perfil").mouseover(function () {
  $(".msj-behance").css("opacity", "1");
});

$(".ico-be-perfil").mouseout(function () {
  $(".msj-behance").css("opacity", "0");
});

$(".ico-dribble-perfil").mouseover(function () {
  $(".msj-dribble").css("opacity", "1");
});

$(".ico-dribble-perfil").mouseout(function () {
  $(".msj-dribble").css("opacity", "0");
});

$(".ico-insta-perfil").mouseover(function () {
  $(".msj-instagram").css("opacity", "1");
});

$(".ico-insta-perfil").mouseout(function () {
  $(".msj-instagram").css("opacity", "0");
});
