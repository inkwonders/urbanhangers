$(document).ready(function(){
  $('#txtNombre').on('input',function(){

    var txtNombre = $('#txtNombre').val();
    var txtCorreo = $('#txtCorreo').val();
    var txtMensaje = $('#txtMensaje').val();

    //TEST
    var correo = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var letras = /^[a-zA-Z áÁéÉíÍóÓúÚñÑüÜàè]+$/;

    //HABILITAR BOTON
    habilitar(txtNombre, txtCorreo, txtMensaje)

    //VALIDAR CAMPO
    if(txtNombre.length == 0 ||  !txtNombre.trim()){
      document.getElementById('msgNombre').innerHTML = "El campo 'Nombre' esta vacío<br>";
    }else if(txtNombre.length < 3){
      document.getElementById('msgNombre').innerHTML = "El campo 'Nombre' debe tener al menos 3 caracteres.<br>";
    }else if(!letras.test(txtNombre)){
      document.getElementById('msgNombre').innerHTML = "El campo 'Nombre' tiene caracteres invalidos.<br>";
    }else if(txtNombre.length > 120 ){
      document.getElementById('msgNombre').innerHTML = "El campo 'Nombre' sólo permite 120 caracteres.<br>";
    }else{
      document.getElementById('msgNombre').innerHTML = "";
    }
  });
  $('#txtCorreo').on('input', function() {
    var txtNombre = $('#txtNombre').val();
    var txtCorreo = $('#txtCorreo').val();
    var txtMensaje = $('#txtMensaje').val();

    //TEST
    var correo = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var letras = /^[a-zA-Z áÁéÉíÍóÓúÚñÑüÜàè]+$/;

    //HABILITAR BOTON
    habilitar(txtNombre, txtCorreo, txtMensaje);

    //VALIDAR CAMPO
    if(txtCorreo.length == 0 ||  !txtCorreo.trim()){
      document.getElementById('msgCorreo').innerHTML = "El campo 'Correo' esta vacío<br>";
    }else if(txtCorreo.length < 7){
      document.getElementById('msgCorreo').innerHTML = "El campo 'Correo' debe tener al menos 7 caracteres.<br>";
    }else if(!correo.test(txtCorreo)){
      document.getElementById('msgCorreo').innerHTML = "El campo 'Correo' no es un correo electronico.<br>";
    }else if(txtCorreo.length > 100){
        document.getElementById('msgCorreo').innerHTML = "El campo 'Correo' sólo permite 100 caracteres.<br>";
      }else{
      document.getElementById('msgCorreo').innerHTML = "";
    }
  });

  $('#txtMensaje').on('input', function() {

    var txtNombre = $('#txtNombre').val();
    var txtCorreo = $('#txtCorreo').val();
    var txtMensaje = $('#txtMensaje').val();

    //TEST
    var correo = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var letras = /^[a-zA-Z áÁéÉíÍóÓúÚñÑüÜàè]+$/;

    //HABILITAR BOTON
    habilitar(txtNombre, txtCorreo, txtMensaje);

    //VALIDAR CAMPO
    if(txtMensaje.length == 0 ||  !txtMensaje.trim()){
      document.getElementById('msgMensaje').innerHTML = "El campo 'Mensaje' esta vacío<br>";
    }else if(txtMensaje.length < 5){
      document.getElementById('msgMensaje').innerHTML = "El campo 'Mensaje' debe tener al menos 5 caracteres.<br>";
    }else if(txtMensaje.length > 500){
      document.getElementById('msgMensaje').innerHTML = "El campo 'Mensaje' sólo permite 500 caracteres.<br>";
    }else{
      document.getElementById('msgMensaje').innerHTML = "";
    }
  });
});

function habilitar(txtNombre, txtCorreo, txtMensaje){
  var correo = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  var letras = /^[a-zA-Z áÁéÉíÍóÓúÚñÑüÜàè]+$/;

  if(txtCorreo.length == 0 || !txtCorreo.trim() || txtNombre.length == 0 ||  !txtNombre.trim() || txtMensaje.length == 0 ||  !txtMensaje.trim()){
    document.getElementById('btnEnviar').disabled = 'true';
  }else if(txtCorreo.length < 7 || txtNombre.length < 3 || txtMensaje.length < 5 || txtNombre.length > 120 || txtCorreo.length > 100 || txtMensaje.length >500){
    document.getElementById('btnEnviar').disabled = 'true';
  }else if(!letras.test(txtNombre) || !correo.test(txtCorreo)){
    document.getElementById('btnEnviar').disabled = 'true';
  }else{
    $('#btnEnviar').removeAttr('disabled');
  }
}
function enviar_correo(){
  var txtNombre = $('#txtNombre').val();
  var txtMensaje = $('#txtMensaje').val();
  var txtCorreo = $('#txtCorreo').val();

  $.ajax({
    url: "enviarMail.php",
    type:"POST",
    data:{txtNombre:txtNombre, txtMensaje:txtMensaje, txtCorreo:txtCorreo},
    success: function (datos) {
      $("#contacto-resp").css('margin-bottom','150px');
      $("#contacto-resp").css('margin-top','50px');
      $("#contacto-resp").html(datos).show();

    }
  });
}
function cargar_div(){
  $("#contacto-resp").load(location.href + " #contacto-resp");
  $("#contacto-resp").css('margin-bottom','0px');
  $("#contacto-resp").css('margin-top','0px');
}
