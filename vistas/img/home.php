<?php

  if(isset($_POST['filtro'])){

    $filtro = $_POST['filtro'];

  }else{

    $filtro = 'NUEVOS';

  }

  $idUsuario = $_SESSION['id_usuario'];

?>

<div class="encabezado">

  <div class="cont_encabezado">

    <div class="encabezado-celda">

      <div class='cont_estilo_lista'>
        <div class='tipo_vista tipo_vista_list tipo_vista_active' op='list'>
           <i class='fas fa-list-ul'></i>
        </div>

        <div class='tipo_vista tipo_vista_grid' op='grid'>
           <i class='fas fa-th'></i>
        </div>
      </div>

      <form id="form_tipo_vista" method="post">
        <input id="opcion_vista" type="hidden" name="tipo_vista" value="">
      </form>
      </div>

    <div class="encabezado-celda">
      <div class="titulo"><?php echo $filtro; ?></div>
    </div>
    <div class="encabezado-celda" style="text-align:right">
      <div class="custom-select">
        <select>
          <option value="0" filtro="0">FILTRAR POR CATEGORÍA</option>
          <option value="1" filtro="1">NUEVOS</option>
          <option value="2" filtro="2">MAS VOTADOS</option>
          <option value="3" filtro="3">COLECCIONES</option>
          <option value="4" filtro="4">TENDENCIA</option>
        </select>

        <form class="form_filtros"  method="post">
          <input type="hidden" id="filtrar_diseno" name="filtro" value="">
        </form>

      </div>

  </div>

</div>
</div>

<?php

  $peticion = ControladorDisenos::consultaDisenos('diseno', 'usuarios','img_diseno', $filtro);

    foreach($peticion as $key => $value){
      $idDiseno = $value['id_diseno'];

?>

      <div class="contenedor">

        <div class="slide-contenedor">

          <div class="contenedor-celda">

            <div class="contenedor_usuario">
              <div class="cont_interno_nombre_hanger pointer btn_dashboard_usuario"  key="<?php echo $value["id_usuario"]; ?>" user="<?php echo $value["ruta"]; ?>">
              <?php
              if ($value["modo_registro"] == "directo"){ ?>
                <div class="img_perfil" style="background-image: url('vistas/assets/hangers/<?php echo $value["carpeta"]; ?>/<?php echo $value['foto'];?>')">
                </div>
              <?php }else{ ?>
                <div class="img_perfil" style="background-image: url('<?php echo $value['foto'];?>')">
                </div>
              <?php } ?>

                <span class="nombre-usuario">
                  <?php echo $value["nombre"]; ?>
                </span>
              </div>
            </div>

            <div class="descripcion-prenda">

              <h3 class="nombre-prenda"><?php echo $value["nombre_diseno"];?></h3>

              <ul class="collapsible">
                <li class="active">
                  <div class="collapsible-header background-head-collapse">INSPIRACIÓN</div>
                  <div class="collapsible-body">
                    <span> <?php echo $value["inspiracion"];?> </span>
                  </div>
                </li>
              </ul>

            </div>

          </div>

          <div class="contenedor-celda">

            <div class="slider height-slide">

              <ul class="slides height-slide">

               <?php

                  $slider = ControladorDisenos::consultaImgSlider('img_diseno', $idDiseno);
                  foreach($slider as $key => $valueSlider){
                    echo "<li class='height-slide'> <img src='vistas/assets/img/disenos/".$valueSlider['ruta_img']."'></li>";
                  }

                ?>

              </ul>

            </div>

          </div>

          <div class="contenedor-celda padding-celda">


            <br />
            <?php
            $ruta = ControladorDashboard::consultaRuta('diseno', $idDiseno);
            $rutaDiseno = $ruta["ruta"];
            ?>
            <div id="conteo_votos<?php echo $idDiseno ?>">
            <?php

              $votos = ControladorDisenos::consultaVotos('votos_diseno', $idDiseno);

              if(isset($_SESSION["iniciarSesion"]) && $_SESSION["iniciarSesion"] == "ok"){
                $votado = ControladorDisenos::consultaVotado('votos_diseno', $idUsuario, $idDiseno);
                if($votado[2] == $idUsuario && $votado[1] == 1){ ?>

                  <div id='<?php echo $idDiseno; ?>' usuario='<?php echo $idUsuario; ?>'  class='votado evento_voto' status='votado'></div>

            <?php
                  }else{
            ?>

              <div id='<?php echo $idDiseno; ?>' usuario='<?php echo $idUsuario; ?>' class='no-votado evento_voto' status='novotado'></div>

            <?php

              }}else{
            ?>

              <div id='<?php echo $idDiseno; ?>' class='no-votado' onclick='inicio_sesion()'></div>

            <?php

              }

            ?>
            <p id="numero_votos<?php echo $idDiseno; ?>" numero_de_votos="<?php echo $votos[0]?>" class="conteo"><?php echo $votos[0] > 0 ?  $votos[0]."<br /> VOTOS": "SIN <br /> VOTOS" ?></p>
            </div>
            <p><b>Compártelo</b></p>

            <div class="comentarios">
              <div class="redes">
              <a href="https://web.whatsapp.com/send?text=https://clientes.ink&#47urbanhangers/<?php echo $rutaDiseno ?>" data-action="share/whatsapp/share" target="_blank"><img class="icon-red" src="vistas/assets/img/icon-whats-o.svg" /></a>
              <a href="https://twitter.com/intent/tweet?text=¡Checa%20este%20diseño!&url=https%3A%2F%2clientes.ink&#47urbanhangers&#47<?php echo $rutaDiseno; ?>" target="_blank"><img class="icon-red" src="vistas/assets/img/icon-twitter-o.svg" /></a>
              <a href="http://www.facebook.com/sharer.php?u=https://clientes.ink&#47urbanhangers&#47<?php echo $rutaDiseno; ?>" target="_blank"><img class="icon-red" src="vistas/assets/img/icon-face-o.svg"  /></a>
              </div>

              <div id="comentarios<?php echo $idDiseno?>" class="comentarios-scroll">

                 <?php

                    $comentarios = ControladorDisenos::consultaComentarios('usuarios', $idDiseno);

                    if(empty($comentarios)){
                      echo "<span class='comentario-nombre centrado'>SIN COMENTARIOS</span>";
                    }else{

                      foreach($comentarios as $key => $valueComentarios){

                ?>

                <div class='comentario'>



                    <?php
                    if($valueComentarios['modo_registro'] == 'directo'){
                      if($valueComentarios['sexo'] == "h"){
                        $ruta_foto = "vistas/assets/img/icon-usuario-1.svg";
                        echo "<div class='comentario-perfil' style='background-image: url(".$ruta_foto.")'></div>";
                        
                      }else {
                        $ruta_foto = "vistas/assets/img/icon-usuario-5.svg";
                        echo "<div class='comentario-perfil' style='background-image: url(".$ruta_foto.")'></div>";
                      }

                    }else{
                      echo "<div class='comentario-perfil' style='background-image:
                      url(".$valueComentarios['foto'].")'>
                      </div>";
                    }
                     ?>

                  <div class='comentario-descripcion'>

                    <span class='comentario-nombre'>

                    <?php
                      echo $valueComentarios['nombre'];

                    ?>

                    </span>

                    <br />

                    <span class='comentario-texto'><?php echo $valueComentarios['comentario']; ?></span>

                    <br />

                    <?php
                    $mes = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Agos","Sept","Oct","Nov","Dic");
                    $mes_comentario = $mes[date('n', strtotime($valueComentarios['fecha']))-1];
                    $dia = date("d", strtotime($valueComentarios['fecha']));
                    $año = date("Y", strtotime($valueComentarios['fecha']));
                    $hora = date("g:i A",strtotime($valueComentarios['fecha']));
                    $fecha_hora =  $dia."-".$mes_comentario."-".$año." ".$hora;
                    ?>

                    <span class='comentario-tiempo'><?php echo $fecha_hora;?></span>

                  </div>

                </div>

              <?php

                      }

                    }

              ?>

              </div>

              <div class="comentar">

                <label class="comentario-label">Agregar comentario</label>
                <input type="text" id="input_comentario<?php echo $idDiseno; ?>" class="input-comentario" placeholder="Comparte tu opinión" />
                    <?php
                    date_default_timezone_set('America/Mexico_City');
                    $date = date('Y-m-d h:i:s', time());
                    ?>
                
                  <?php  if(isset($_SESSION["iniciarSesion"]) && $_SESSION["iniciarSesion"] == "ok"){ ?>
                  <div class="div-button-enviar" diseno="<?php echo $idDiseno; ?>" usuario="<?php echo $idUsuario; ?>" fecha="<?php echo $date;?>">
                  <span class="label-button">Comentar</span>
                  <img class="comentario-button" src="vistas/assets/img/icon-enviar-c.svg" />
                  </div>
                  <?php }else{ ?>
                    <div class="div-button-enviar" onclick="inicio_sesion()">
                  <span class="label-button">Comentar</span>
                  <img class="comentario-button" src="vistas/assets/img/icon-enviar-c.svg" />
                  </div>
                  <?php } ?>
                </div>

              </div>

            </div>

          </div>

        </div>

<?php

    }

    echo "<div class='cont_grid oculto'>";

    foreach($peticion as $key => $value){

      $idDiseno = $value['id_diseno'];

      echo "<div class='card_grid'>";

        echo "<div class='slider slider_grid'>

                <ul class='slides slider_grid'>";

                  $slider = ControladorDisenos::consultaImgSlider('img_diseno', $idDiseno);

                  foreach($slider as $key => $valueSlider){
                    echo "<li class='height_slide_grid'> <img src='vistas/assets/img/disenos/".$valueSlider['ruta_img']."'></li>";
                  }

            echo "</ul>";

      echo "</div>";

      $votos = ControladorDisenos::consultaVotos('votos_diseno', $idDiseno);
      ?>
                <div class='cont_datos_hanger_diseno'>
                  <div class='link_usuario btn_dashboard_usuario' user='<?php echo $value["ruta"];?>'>
                    
                  <?php
                  if ($value["modo_registro"] == "directo"){ ?>
                    <div class="img_perfil" style="background-image: url('vistas/assets/hangers/<?php echo $value["carpeta"]; ?>/<?php echo $value['foto'];?>')">
                    </div>
                  <?php }else{ ?>
                    <div class="img_perfil" style="background-image: url('<?php echo $value['foto'];?>')">
                    </div>
                    <?php } ?>

                    <span class='nombre-usuario'><?php echo $value["nombre"];?></span>
                    </div>
                  <div class='cont_votos_grid'>
                  <?php

                  $votos = ControladorDisenos::consultaVotos('votos_diseno', $idDiseno);

                  if(isset($_SESSION["iniciarSesion"]) && $_SESSION["iniciarSesion"] == "ok"){
                    $votado = ControladorDisenos::consultaVotado('votos_diseno', $idUsuario, $idDiseno);
                    if($votado[2] == $idUsuario && $votado[1] == 1){ ?>

                      <div id='<?php echo $idDiseno; ?>' usuario='<?php echo $idUsuario; ?>'  class='votado evento_voto' status='votado'></div>

                  <?php
                      }else{
                  ?>

                  <div id='<?php echo $idDiseno; ?>' usuario='<?php echo $idUsuario; ?>' class='no-votado evento_voto' status='novotado'></div>

                  <?php

                  }}else{
                  ?>

                  <div id='<?php echo $idDiseno; ?>' class='no-votado' onclick='inicio_sesion()'></div>

                  <?php

                  }

                  ?>
                  <p class='conteo_grid'>
                  <?php
                    if($votos[0] > 0){
                      echo $votos[0]."<br /> VOTOS";
                    }else{
                      echo "SIN <br /> VOTOS";
                    }

                  echo "</p>

                    </div>

                </div>";

    echo "</div>";

    }

    echo "</div>";

    echo "<div class='cont_paginador'>

           <ul class='pagination'>
              <li class='disabled'><a href='#!'><i class='material-icons'>chevron_left</i></a></li>";
              for($i=1;$i<=$total_num_hojas;$i++){
                echo "<li class='active'><a href='#!'>".$i."</a></li>";
              }
              echo "<li class='waves-effect'><a href='#!'><i class='material-icons'>chevron_right</i></a></li>
            </ul>

          </div>";



?>
<script>


$(document).on('click', '.div-button-enviar', function(){

    let id = $(this).attr('usuario');
    let diseno = $(this).attr('diseno');
    let comentario = document.getElementById('input_comentario'+diseno).value;
    let fecha = $(this).attr('fecha');

    let datos_comentario ={
      "id":id,
      "diseno":diseno,
      "comentario":comentario,
      "fecha":fecha
    }


    $.ajax({
      url: './ajax/comentarios.ajax.php',
      data: datos_comentario,
      type: "POST",
      success: function(response){
        console.log(response);
        $('#input_comentario'+diseno).val('');
        $( "#comentarios"+diseno ).load(window.location.href + " #comentarios"+diseno );
      },
      error: function(){
        console.log("No se pudo realizar la acción");
      }
    });

  });


  $(document).on('click', '.evento_voto', function(){

    let id = $(this).attr('id');
    let status = $(this).attr('status');
    let diseno = $(this).attr('diseno');
    let votos = document.getElementById('numero_votos'+id);
    let numero_votos = $(votos).attr('numero_de_votos');

    if (status == 'novotado') {
      $('#'+id).addClass("animate__animated animate__heartBeat");
      $('#'+id).css("background-image", "url(vistas/assets/img/icon-votado.svg)"); 
      votos.innerHTML = parseInt(numero_votos)+1 + '<br/>' + ' VOTOS';
    }else if (status == 'votado') {
      $('#'+id).addClass("animate__animated animate__heartBeat");
      $('#'+id).css("background-image", "url(vistas/assets/icon-no-votado.svg)");
      if(parseInt(numero_votos) == 1){
      votos.innerHTML = 'SIN' + '<br/>' + ' VOTOS'; 
      }else{
        votos.innerHTML = parseInt(numero_votos)-1 + '<br/>' + ' VOTOS'; 
      }
    }
    

  });

  var click= true;

  $(document).on('click', '.evento_voto', function(){

    let id = $(this).attr('id');
    let status = $(this).attr('status');
    let usuario = $(this).attr('usuario');
    
    let datos = {
      "id": id,
      "status": status,
      "id_usuario": usuario
    }
    
    if(click){
        click= false;
    //AJAX

    $.ajax({

      url: './ajax/likes.ajax.php',
      data: datos,
      type: "POST",
      success: function(response){
        console.log(response);
        $( "#conteo_votos"+id ).load(window.location.href + " #conteo_votos"+id );
      },
      error: function(){
        console.log("No se pudo realizar la acción");
      }

    });
    setTimeout(function(){ 
            click = true;
                 }, 3000);
    }

  });




</script>
