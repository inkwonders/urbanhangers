<?php

header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat, 1 Jul 2000 05:00:00 GMT");

session_start();

$ruta_global = Rutas::ruta_servidor();

if (isset($_COOKIE["usuario"]) && isset($_COOKIE["marca"]) && $_COOKIE["usuario"] != "" && $_COOKIE["marca"] != "") {

  $cookie_usuario = json_decode($_COOKIE["usuario"]);
  $cookie_marca = json_decode($_COOKIE["marca"]);

  $caducidad_usuario = date("d/m/Y H:i:s",$cookie_usuario->caducidad);
  $caducidad_marca = date("d/m/Y H:i:s",$cookie_marca->caducidad);
  
  echo "<script>console.log('".$_COOKIE["usuario"]." - ".$_COOKIE["marca"]."');</script>";
  echo "<script>console.log('".$cookie_usuario." - ".$cookie_marca."');</script>";
  echo "<script>console.log('".$cookie_usuario->caducidad." - ".$cookie_marca->caducidad."');</script>";
  echo "<script>console.log('".$caducidad_usuario." - ".$caducidad_marca."');</script>";

  $verificar_cookie = ControladorUsuarios::verficarCookie($_COOKIE["usuario"], $_COOKIE["marca"]);

  $respuesta = ModeloUsuarios::mostrarUsuariosCookie($verificar_cookie["id_usuario"], $_COOKIE["marca"], 'usuarios');

  $_SESSION["iniciarSesion"] = "ok";
  $_SESSION["tipoUsuario"] = $respuesta["tipo_usuario"];
  $_SESSION["id_usuario"] = $respuesta["id_usuario"];
  $_SESSION["nombre"] = $respuesta["nombre"] . " " . $respuesta["apellido"];
  $_SESSION["avatar"] = $respuesta["foto"];
  $_SESSION["ruta_perfil"] = $respuesta["ruta"];
  $_SESSION['correo'] = $respuesta['correo'];
  $_SESSION["usuario"] = $respuesta["usuario"];

  if($_SESSION["tipoUsuario"] != 3){
    $_SESSION["carpeta"] = $respuesta['carpeta'];
  }

}else{
  echo "<script>console.log('no encontro cookies')</script>";
}

$condicion_total_dis = " WHERE activo = 1 AND diseno_en_venta = 0 ";

$paginador =  ControladorDisenos::totalDisenos('diseno', $condicion_total_dis);

$total = $paginador["total"];

$num_hojas = $total / 20;

$total_num_hojas = ceil($num_hojas);
?>

<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <?php

  $bool_metas_diseno = false;
  $bool_metas_usuario = false;
  $bool_usuario_activo_diseno = false;
  if (isset($_GET["pagina"])) {

    $rutas_metas = explode("/", $_GET["pagina"]);

    $item = "ruta";
    $valor =  $rutas_metas[0];


    $rutaDis = ControladorDisenos::mostrarProducto('diseno', $item, $valor);

    if ($valor == $rutaDis["ruta"]) {

      $datosUsuarioDiseno = ControladorDisenos::mostrarProducto('usuarios', 'id_usuario', $rutaDis['id_usuario']);
      if (!empty($datosUsuarioDiseno)) {
        if ($datosUsuarioDiseno['activo'] == 1) {
          $bool_usuario_activo_diseno = true;
        }
      }
      $datosMetaDiseno = ControladorDisenos::datosMetas('diseno', $valor);

      if (!empty($datosMetaDiseno)) {
        $bool_metas_diseno = true;
      }
    }


    $rutaUsuario = controladorUsuarios::mostrarUsuario($item, $valor);

    if ($valor == $rutaUsuario["ruta"] && $rutaUsuario["tipo_usuario"] == 1) {

      $datosMetaUsuario = controladorUsuarios::datosMetas('usuarios', $valor);

      if (!empty($datosMetaUsuario)) {
        $bool_metas_usuario = true;
      }
    }
  }

  $ruta_meta = 'https://134.122.14.9/home';
  $title_meta = 'URBAN HANGERS®';
  $desc_meta = 'URBAN HANGERS®';
  $img_meta = 'https://134.122.14.9/vistas/assets/favicon/android-chrome-192x192.png';

  if ($bool_metas_usuario) {
    $ruta_meta = $ruta_global . $datosMetaUsuario['ruta'];
    $title_meta = mb_strtoupper($datosMetaUsuario['usuario']) . " | URBAN HANGERS®";
    $desc_meta = $datosMetaUsuario['descripcion'];

    if ($datosMetaUsuario['modo_registro'] == 'directo') {
      if ($datosMetaUsuario['foto'] == NULL || $datosMetaUsuario['foto'] == "") {
        if ($datosMetaUsuario['sexo'] == "h") {
          $img_meta = $ruta_global . "vistas/assets/img/icon-usuario-1.png";
        } else {
          $img_meta = $ruta_global . "vistas/assets/img/icon-usuario-5.png";
        }
      } else {
        $img_meta = $ruta_global . "vistas/assets/hangers/" . $datosMetaUsuario["carpeta"] . "/" . $datosMetaUsuario['foto'];
      }
    } else {
      $img_meta =  $datosMetaUsuario['foto'];
    }
  } else if ($bool_metas_diseno) {
    $ruta_meta = $ruta_global . $datosMetaDiseno['ruta'];
    $title_meta = mb_strtoupper($datosMetaDiseno['nombre_diseno']) . " | URBAN HANGERS®";
    $desc_meta = $datosMetaDiseno['inspiracion'];
    $img_meta = $ruta_global . "vistas/assets/hangers/" . $datosMetaDiseno['carpeta'] . "/" . $datosMetaDiseno['ruta_img'];
  }

  ?>

  <meta content="text/html; charset=UTF-8" name="Content-Type" />


  <meta http-equiv="Expires" content="0">
  <meta http-equiv="Last-Modified" content="0">
  <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
  <meta http-equiv="Pragma" content="no-cache">

  <meta name="Description" content="<?php echo $title_meta; ?>" />
  <meta name="meta_url" property="og:url" content="<?php echo $ruta_meta; ?>" />
  <meta name="meta_title" property="og:title" content="<?php echo $title_meta; ?>" />
  <meta name="meta_image" property="og:image" content="<?php echo $img_meta; ?>" />
  <meta name="meta_desc" property="og:description" content="<?php echo $desc_meta; ?>" />

  <!-- Open Graph / Facebook -->
  <meta property="og:type" content="website">
  <meta name="fb_url" property="og:url" content="<?php echo $ruta_meta; ?>">
  <meta name="fb_title" property="og:title" content="<?php echo $title_meta; ?>">
  <meta name="fb_desc" property="og:description" content="<?php echo $desc_meta; ?>">
  <meta name="fb_img" property="og:image" content="<?php echo $img_meta; ?>">

  <!-- Twitter -->
  <meta name="twitter:card" content="summary_large_image">
  <meta name="tw_url" property="twitter:url" content="<?php echo $ruta_meta; ?>">
  <meta name="tw_title" property="twitter:title" content="<?php echo $title_meta; ?>">
  <meta name="tw_desc" property="twitter:description" content="<?php echo $desc_meta; ?>">
  <meta name="tw_img" property="twitter:image" content="<?php echo $img_meta; ?>">

  <!-- <meta content="summary" name="twitter:card">
    <meta name="twitter:title" content="<?php echo $title_meta; ?>">
    <meta content="@Urbanhangers_mx" name="twitter:site">
    <meta content="@Urbanhangers_mx" name="twitter:creator">
    <meta name="twitter:image" content="<?php echo $img_meta; ?>"  >
    <meta name="twitter:description" content="<?php echo $desc_meta; ?>"> -->

  <!-- fuentes -->
  <link rel="preload" href="<?php echo $ruta_global; ?>vistas/assets/fonts/big_noodle_titling.ttf" as="font" type="font/ttf" crossorigin>
  <link rel="preload" href="<?php echo $ruta_global; ?>vistas/assets/fonts/big_noodle_titling_oblique.ttf" as="font" type="font/ttf" crossorigin>
  <link rel="preload" href="<?php echo $ruta_global; ?>vistas/assets/fonts/Hanson-Bold.ttf" as="font" type="font/ttf" crossorigin>
  <link rel="preload" href="<?php echo $ruta_global; ?>vistas/assets/fonts/gotham_book.otf" as="font" type="font/otf" crossorigin>
  <link rel="preload" href="<?php echo $ruta_global; ?>vistas/assets/fonts/gotham-bold.otf" as="font" type="font/otf" crossorigin>
  <link rel="preload" href="<?php echo $ruta_global; ?>vistas/assets/fonts/Hanson-Bold.ttf" as="font" type="font/ttf" crossorigin>
  <!-- end fuentes -->

  <title>URBAN HANGERS®</title>

  <!-- Favicon -->
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $ruta_global; ?>vistas/assets/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $ruta_global; ?>vistas/assets/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $ruta_global; ?>vistas/assets/favicon/favicon-16x16.png">
  <link rel="mask-icon" href="<?php echo $ruta_global; ?>vistas/assets/favicon/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <!-- estilos -->
  <link rel="preload stylesheet" type="text/css" href="<?php echo $ruta_global; ?>vistas/assets/css/estilos.min.css?v=<?php echo time(); ?>" as="style" />
  <link rel="preload stylesheet" type="text/css" href="<?php echo $ruta_global; ?>vistas/assets/css/estilos_editar_perfil.min.css?v=<?php echo time(); ?>" as="style">
  <link rel="preload stylesheet" type="text/css" href="<?php echo $ruta_global; ?>vistas/assets/css/custom-select.css" as="style" />
  <link rel="preload stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" as="style" />
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="preload stylesheet" as="style">
  <link type="text/css" rel="preload stylesheet" href="<?php echo $ruta_global; ?>vistas/assets/css/materialize.min.css" media="screen,projection" as="style" />

  <!---Slick-->
  <link rel="preload stylesheet" type="text/css" href="<?php echo $ruta_global; ?>vistas/assets/css/slick.css" as="style" />
  <link rel="preload stylesheet" type="text/css" href="<?php echo $ruta_global; ?>vistas/assets/css/slick-theme.css" as="style" />

  <!-- ANIMATED CSS -->
  <link rel="preload stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" as="style" />

  <!-- WEBMANIFEST -->
  <link rel='manifest' href='manifest.webmanifest'>

  <style media="screen">
    .add-button {
      position: absolute;
      top: 1px;
      left: 1px;
    }
  </style>

  <script>
    function statusChangeCallback(response) { // Called with the results from FB.getLoginStatus().
      console.log('statusChangeCallback');
      // console.log(response);                   // The current login status of the person.
      if (response.status === 'connected') { // Logged into your webpage and Facebook.
        testAPI();
      } else { // Not logged into your webpage or we are unable to tell.
        document.getElementById('status').innerHTML = 'Please log ' +
          'into this webpage.';
      }
    }

    function testAPI() { // Testing Graph API after login.  See statusChangeCallback() for when this call is made.      
      FB.api('/me?fields=name,email', function(response) { // petcion de los campos requeridos a la api de fb, separados por comas NOTA: Solicitar agregar API FB    
        console.log("nombre de usuario"+response.name);    
        const ruta_img_fb = "https://graph.facebook.com/" + response.id + "/picture?type=large&width=720&height=720";
        let tipo_usuario = $("#tipo_usuario").val();
        if (response.email == undefined) {
          $("#div_respuesta_login").html("<p class='error_fb'>Verifica permisos de la App Urban Hangers en Facebook</p>");
        } else {

          console.log("nombre de usuario"+response.name);

          $.ajax({
            url: '<?php echo $ruta_global; ?>ajax/registro.ajax.php',
            type: 'POST',
            data: {
              'txt_nombre_fb': response.name,
              'txt_correo_fb': response.email,
              'txt_id_fb': response.id,
              'ruta_img_fb': ruta_img_fb,
              'tipo_usuario': tipo_usuario
            },
            beforeSend: function() {
              $("#cont_botones_inicio").addClass("deshabilitado");
              $("#footer_modal_inicio").addClass("deshabilitado");
              $("#div_respuesta_login").html("<img src='<?php echo $ruta_global; ?>vistas/assets/css/ajax-loader.gif' alt='Cargando...'>");
            },
            success: function(respuesta) {
              $("#cont_botones_inicio").removeClass("deshabilitado");
              $("#footer_modal_inicio").removeClass("deshabilitado");
              $("#cont_botones_inicio").addClass("habilitado");
              $("#footer_modal_inicio").addClass("habilitado");
              console.log(respuesta + " respuesta");
              if (respuesta == 'usuario_inactivo') {
                $("#div_respuesta_login").html("<p class='error_fb'>Usuario inactivo.</p>");
              } else if (respuesta == 'datos') {
                $("#div_respuesta_login").html("<p class='error_fb'>Datos incorrectos, favor de revisarlos.</p>");
              }else if(respuesta == "usuario_duplicado"){
                $("#div_respuesta_login").html("<p class='error_fb'>El correo ya fue registrado sin facebook, ingresa con tus credenciales correspondientes</p>");
                $("#registrarse_modal").modal("close");
                $('#msj_error_modal').html('El correo ya fue registrado sin facebook, ingresa con tus credenciales correspondientes');
                $("#error_modal_generico").modal("open");
              } else {
                $("#div_respuesta_login").html(respuesta);
              }
            },
            error: function() {
              alert('Error plantilla');
            }
          });
        }
      });
    }

    window.fbAsyncInit = function() {
      FB.init({
        appId: '1298330253840627',
        cookie: true,
        xfbml: true,
        version: 'v9.0'
      });

      FB.AppEvents.logPageView();
    };

    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return;
      }
      js = d.createElement(s);
      js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    function checkLoginState() {
      FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
      });
    }
  </script>

  <!-- PWA -->

  <!-- <script type="module" src="pwabuilder-sw-register.js"></script>
  <script type="module" src="pwabuilder-sw.js"></script> -->

  <!-- PWA END -->

  <script type="text/javascript" src="<?php echo $ruta_global; ?>vistas/assets/js/jquery-3.5.1.min.js"></script>
  <script type="text/javascript" src="<?php echo $ruta_global; ?>vistas/assets/js/materialize.min.js"></script>

</head>

<body>

  <?php

  //include('secciones/header.php');

  if (isset($_GET["pagina"])) {

    $rutas = explode("/", $_GET["pagina"]);

    $item = "ruta";
    $valor =  $rutas[0];


    //echo "<script> alert('".$valor."'); </script>";

    // =============================================
    // URL'S AMIGABLES DE PERFILES
    // =============================================

    $rutaDis = ControladorDisenos::mostrarProducto('diseno', $item, $valor);

    if ($valor == $rutaDis["ruta"] && $bool_usuario_activo_diseno) {

      $rutaDiseno = $rutaDis["ruta"];
    } else {

      $rutaDiseno = null;
    }


    $rutaUsuario = controladorUsuarios::mostrarUsuario($item, $valor);

    if ($valor == $rutaUsuario["ruta"] && $rutaUsuario["tipo_usuario"] == 1) {

      $ruta = $rutaUsuario["ruta"];
    } else {

      $ruta = null;
    }

    if ($ruta != null) {

      include "secciones/header.php";
      include "secciones/dashboardHanger.php";
      include "secciones/footer.php";
    } else if ($rutaDiseno != null) {

      include "secciones/header.php";
      include "secciones/diseno.php";
      include "secciones/footer.php";
    } else if ($rutas[0] == "home") {
      echo '
      <script>
        var visto = sessionStorage.getItem("visto-playstation");
        if(visto != 1){
          setTimeout(function(){ $("#modal_playstation").modal("open"); },3500 );
        }
      </script>';

      $rutas_home = explode("/", $_GET["pagina"]);

      if (!empty($rutas_home[1])) { // si trae algo mas que home

        $rutaCole = ControladorDisenos::mostrarProducto('colecciones', $item, $rutas_home[1]);

        if ($rutas_home[1] == $rutaCole["ruta"]) {

          $fk_coleccion = $rutaCole['sk_coleccion'];
          $condicion_total_dis = "WHERE fk_coleccion = '$fk_coleccion' AND activo = 1 AND diseno_en_venta = 0 ";

          $paginador =  ControladorDisenos::totalDisenos('diseno', $condicion_total_dis);

          $total = $paginador["total"];

          $num_hojas = $total / 20;

          $total_num_hojas = ceil($num_hojas);

          if (!empty($rutas_home[2])) {
            if (is_numeric($rutas_home[2]) && $rutas_home[2] <= $total_num_hojas) {
              include "secciones/header.php";
              include "secciones/" . $rutas_home[0] . ".php";
              include "secciones/footer.php";
            } else {
              include "secciones/404.php";
            }
          } else {
            include "secciones/header.php";
            include "secciones/" . $rutas_home[0] . ".php";
            include "secciones/footer.php";
          }
        } else if (
          $rutas_home[1] == 'nuevos' ||
          $rutas_home[1] == 'mas-votados' ||
          $rutas_home[1] == 'tendencia' ||
          (is_numeric($rutas_home[1]) && $rutas_home[1] <= $total_num_hojas)
        ) {
          if (!empty($rutas_home[2])) {
            if (is_numeric($rutas_home[2]) && $rutas_home[2] <= $total_num_hojas) {
              include "secciones/header.php";
              include "secciones/" . $rutas_home[0] . ".php";
              include "secciones/footer.php";
            } else {
              include "secciones/404.php";
            }
          } else {
            include "secciones/header.php";
            include "secciones/" . $rutas_home[0] . ".php";
            include "secciones/footer.php";
          }
        } else {
          include "secciones/404.php";
        }
      } else {
        include "secciones/header.php";
        include "secciones/" . $rutas_home[0] . ".php";
        include "secciones/footer.php";
      }
    } else if ($rutas[0] == "salir") {

      if (isset($_SESSION["iniciarSesion"]) && $_SESSION["iniciarSesion"] == "ok") {
        include "secciones/" . $rutas[0] . ".php";
      } else {
        include "secciones/header.php";
        include "secciones/home.php";
        include "secciones/footer.php";
      }
    } else if (
      $rutas[0] == "editarPerfil" ||
      $rutas[0] == "subirDiseno"
    ) {

      if (isset($_SESSION["iniciarSesion"]) && $_SESSION["iniciarSesion"] == "ok" && $_SESSION["tipoUsuario"] == 1) {

        include "secciones/header.php";
        include "secciones/" . $rutas[0] . ".php";
        include "secciones/footer.php";
      } else {

        include "secciones/404.php";
      }
    } else if (
      $rutas[0] == "inicio-sesion" ||
      $rutas[0] == "registro"
    ) {

      include "secciones/" . $rutas[0] . ".php";
    } else if ($rutas[0] == "verificado") {
      $usuario_verificado = explode("/", $_GET["pagina"]);
      if (!empty($usuario_verificado[1])) {

        include "secciones/header.php";
        include "secciones/home.php";
        include "secciones/footer.php";

        echo "<script>
                      $('#verificado').modal({
                          backdrop: 'static',
                          keyboard: false,
                          dismissible: false,
                          container: 'body'
                      });
                      setTimeout(function(){ $('#verificado').modal('open'); },3500 );
                    </script>";
      } else {
        include "secciones/404.php";
      }
    } else if ($rutas[0] == "restablecer-contrasena") {
      $usuario_contrasena_olvidada = explode("/", $_GET["pagina"]);
      if (!empty($usuario_contrasena_olvidada[1])) {
        include "secciones/restablecer-contrasena.php";
      } else {
        include "secciones/404.php";
      }
    } else if ($rutas[0] == "publicado") {
      $modelo_publicado = explode("/", $_GET["pagina"]);
      if (!empty($modelo_publicado[1])) {
        include "secciones/header.php";
        include "secciones/publicado.php";
        include "secciones/footer.php";
      } else {
        include "secciones/404.php";
      }
    } else if ($rutas[0] == "thankYou") {
      include "secciones/header.php";
      include "secciones/thankYou.php";
      include "secciones/footer.php";
    }/*else if($rutas[0]=="armado"){

      include "secciones/armado.php";

    }*/ else {

      include "secciones/404.php";
    }
  } else {
    echo '
      <script>
        var visto = sessionStorage.getItem("visto-playstation");
        if(visto != 1){
          setTimeout(function(){ $("#modal_playstation").modal("open"); },3500 );
        }
      </script>';
    include "secciones/header.php";
    include "secciones/home.php";
    include "secciones/footer.php";
  }

  ?>

  <script type="text/javascript" src="<?php echo $ruta_global; ?>vistas/assets/js/custom-select.js"></script>
  <script type="text/javascript" src="<?php echo $ruta_global; ?>vistas/assets/js/envio_correo.js"></script>
  <script type="text/javascript" src="<?php echo $ruta_global; ?>vistas/assets/js/slick.min.js"></script>
  <script type="text/javascript" src="<?php echo $ruta_global; ?>vistas/assets/js/fontawesome.min.js"></script>
  <script type="text/javascript">
    var ruta_global = "<?php echo $ruta_global; ?>";
    <?php
    if (isset($_SESSION['iniciarSesion']) && $_SESSION['iniciarSesion'] == 'ok') {
    ?>
      var sesion_modal = 'ok';
      var id_usuario_modal = '<?php echo $_SESSION['id_usuario']; ?>';
    <?php
    } else {
    ?>
      var sesion_modal = 'no';
      var id_usuario_modal = 'no';
    <?php
    }
    ?>
  </script>
  <script type="text/javascript" src="<?php echo $ruta_global; ?>vistas/assets/js/script_plantilla.min.js?v=<?php echo time(); ?>"></script>
</body>

</html>