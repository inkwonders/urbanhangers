<script type='text/javascript'>
  window.__lo_site_id = 312183;
  (function() {
    var wa = document.createElement('script');
    wa.type = 'text/javascript';
    wa.async = true;
    wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wa, s);
  })();
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-159600964-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];

  function gtag() {
    dataLayer.push(arguments);
  }
  gtag('js', new Date());
  gtag('config', 'UA-159600964-2');
</script>

<div id="contenedor_padre_registro" class="contenedor-log-in">
  <img class="hanger-destacado" src="<?php echo $ruta_global; ?>vistas/assets/img/icon-destacado.svg" />



  <!--Registro-->

  <div class="div-log-cell padding-log animate__animated animate__slow none_effect" id="txtFormRegistro">
    <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal" /><br />
    <span class="titulo-modal">REGÍSTRATE</span>
    <div class="fx_tab">
      <ul class="tabs fx_tab_ul" id="tabs-hanger-votante">
        <li class="tab fx_tab_li tab_tipo" tipo="Votante"><a href="#tab-votante" id="votante-tab">VOTANTE</a></li>
        <li class="tab fx_tab_li tab_tipo active" tipo="Hanger"><a href="#tab-hanger" id="hanger-tab">HANGER</a></li>
      </ul>
    </div>
    <div class="">
      <div id="tab-hanger" class="col s12 texto-hanger">
        Cuando eres Hanger puedes votar y subir tus propios diseños.<br />
      </div>
      <div id="tab-votante" class="col s12 texto-hanger">
        Los votantes pueden votar por sus diseños favoritos, y cambiar a una cuenta de hanger cuando quieran.
      </div>
    </div>
    <div id="div_respuesta_registro"></div>
    <form id="form_registro_pagina" method="post">

      <input id="tipo_usuario" type="hidden" name="tipo_usuario" value="2">
      <div id="cont_error_usuario" style="display:none;">
        <p id="msj_error_registro"></p>
      </div>
      <div class="input-field col s12 margin-inicio input_usuario_registro">
        <input type="text" class="input-inicio" id="txtUsuarioRegistro" name="txtUsuarioRegistro" required />
        <label class="label-inicio">Usuario</label>
        <span class="helper-text" data-error="wrong" data-success="right" style="text-align:right">*Este será tu nombre de usuario visto por el público</span>
      </div>
      <div class="input-field col s12 margin-inicio input_correo_registro">
        <input type="email" class="input-inicio" id="txtCorreoRegistro" name="txtCorreoRegistro" required />
        <label class="label-inicio">CORREO</label>
      </div>
      <div class="input-field col s12 margin-inicio input_contrasena_registro">
        <img src="<?php echo $ruta_global; ?>vistas/assets/img/icon-mostrar.svg" class="mostrar-pass-sp mostrar_password" inp="txtPasswordRegistro" id="mostrar_pass_registro" title="Mostrar contraseña" />
        <input type="password" class="input-inicio" name="txtPasswordRegistro" id="txtPasswordRegistro" required />
        <label class="label-inicio">CONTRASEÑA</label>
        <span class="helper-text" data-error="wrong" data-success="right" style="text-align:right">*Contraseña de mínimo 9 carácteres</span>
      </div>
      <div class="input-field col s12 margin-inicio input_repetir_contrasena_registro">
        <img src="<?php echo $ruta_global; ?>vistas/assets/img/icon-mostrar.svg" class="mostrar-pass mostrar_password" inp="txtPasswordRepetir" id="mostrar_pass_repetir_registro" title="Mostrar contraseña" required />
        <input type="password" class="input-inicio" name="txtPasswordRepetir" id="txtPasswordRepetir" />
        <label class="label-inicio">REPETIR CONTRASEÑA</label>
      </div>
      <div class="input-field col s12 margin-inicio">
        <div class="cont_sexo">
          <p>
            <label>
              <input class="sexo_radio" name="sexoRegistro" type="radio" checked value="hombre" />
              <span class="sexo">Hombre</span>
            </label>
          </p>
          <p>
            <label>
              <input class="sexo_radio" name="sexoRegistro" type="radio" value="mujer" />
              <span class="sexo">Mujer</span>
            </label>
          </p>
        </div>
      </div>
      <div class="margin-inicio">
        <label>
          <input id="check_terminos_condiciones" type="checkbox" name="terminos" />
          <span>Acepto <a target="_blank" href='<?php echo $ruta_global; ?>vistas/assets/terminos_condiciones/General.pdf'>términos y condiciones </a> Urban Hangers.</span>
        </label>
      </div>
      <div id="cont_loader_registro" style="display:none;">
        <img src='<?php echo $ruta_global; ?>vistas/assets/css/ajax-loader.gif' alt='Cargando...'>
      </div>
      <div id="contenedor_botones_registro">
        <input type="submit" class="button-inicio" value="REGÍSTRATE" /><br />
        <!-- <fb:login-button
              scope="public_profile,email"
              onlogin="checkLoginState();">
            </fb:login-button> -->
        <div class="fb-login-button" data-size="large" data-button-type="continue_with" data-layout="rounded" data-auto-logout-link="false" data-use-continue-as="true" data-width="" scope="public_profile,email" onlogin="checkLoginState();"></div>

        <a href="<?php echo $ruta_global; ?>inicio-sesion" class="regresar desktop_no"><img src="<?php echo $ruta_global; ?>vistas/assets/img/flecha-atras-icon.svg" alt="regresar" class="regrer_img"></a>

      </div>

    </form>
  </div>


  <div class="div-log-cell background-hanger" id="txtBack">
    <span class="texto-back">
      <span id="txtColor">VOTA <br />POR TUS</span><br />
      <span class="texto-back-trans" id="txtTrans">FAVORITOS</span>
    </span><br />
  </div>
</div>


<div id="registro_exitoso" class="modal modal-inicio h500">
  <div class="modal-content modal-padding">
    <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal" /><br />
    <span class="titulo-modal">¡Registro exitoso!</span>
    <img id="icon_correo" src="<?php echo $ruta_global; ?>vistas/assets/img/icon-correo.svg">
    <div class="col s12 texto_modal_2">
      Tu registro fue realizado correctamente, por favor revisa tu bandeja de entrada para activar tu cuenta.
    </div>
    <button class="button-inicio modal-close">Aceptar</button><br />
    <div id="enviar_nuevamente">Enviar de nuevo</div>
  </div>
</div>

<div id="reenvio_correo" class="modal modal-inicio h500">
  <div class="modal-content modal-padding">
    <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal" /><br />
    <span class="titulo-modal">Correo reenviado</span>
    <img id="icon_correo" src="<?php echo $ruta_global; ?>vistas/assets/img/icon-correo.svg">
    <div class="col s12 texto_modal_2">
      El correo fue enviado nuevamente, por favor revisa tu bandeja de entrada para activar tu cuenta.
    </div>
    <button class="button-inicio modal-close">Aceptar</button><br />
  </div>
</div>

<div id="error_modal" class="modal modal-inicio h500">
  <div class="modal-content modal-padding">
    <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal" /><br />
    <span class="titulo-modal">¡Ocurrió un problema!</span>
    <div class="col s12 texto_modal_2">
      Favor de intentarlo más tarde.
    </div>
    <button class="button-inicio modal-close">Aceptar</button>
  </div>
</div>

<div id="error_modal_generico" class="modal modal-inicio h500">
  <div class="modal-content modal-padding">
    <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal" /><br />
    <span class="titulo-modal">¡Ocurrió un problema!</span>
    <div id="msj_error_modal" class="col s12 texto_modal_2"></div>
    <button id="btn_error_genercio" class="button-inicio modal-close">Aceptar</button>
  </div>
</div>

<div id="modal_error_hanger" class="modal modal_estilo">
  <div class="modal-content modal-padding">
    <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal" /><br />
    <span class="titulo-modal">¡Ya eres Hanger!</span>
    <div class="col s12 texto_modal_2">
      Favor de cerrar sesión y volver a iniciar.
    </div>
    <button class="button-inicio modal-close">Aceptar</button>
  </div>
</div>

<div id="registrado_facebook" class="modal modal_resultado modal_estilo">
  <div class="modal-content modal-padding">
    <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal" /><br />
    <span class="titulo-modal">¡Tu correo ya está registrado!</span>
    <div class="col s12 texto_modal_2">
      Ya puedes inicar sesión con facebook.
    </div>
    <button id="btn_modal_registrado_fb" class="button-inicio modal-close">Iniciar Sesión</button>
  </div>
</div>

<div id="registrado_directo" class="modal modal_resultado modal_estilo">
  <div class="modal-content modal-padding">
    <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal" /><br />
    <span class="titulo-modal">¡Tu correo ya está registrado!</span>
    <div class="col s12 texto_modal_2">
      Ya puedes inicar sesión.
    </div>
    <button id="btn_modal_registrado_dir" class="button-inicio modal-close">Iniciar Sesión</button>
  </div>
</div>
<script type="text/javascript">
  var ruta_global = "<?php echo $ruta_global; ?>";
</script>
<script type="text/javascript" src="<?php echo $ruta_global; ?>vistas/assets/js/script_registro.min.js?v=<?php echo time(); ?>"></script>