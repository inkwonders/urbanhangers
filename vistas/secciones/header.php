<?php
$ruta_global = Rutas::ruta_servidor();
?>
<div class="barra_urbangamer">
  <a href="vistas/assets/terminos_condiciones/Gamer.pdf" target="_blank"><b>Términos y condiciones Urban Gamer</b></a>
</div>
<header id="header">

  <div class="cont_header">

    <!-- MOVIL ESTRUCTURA -->

    <div class="esp_mv_hd inic_ses_movil">
      <div class="desktop_no">
        <?php if (!isset($_SESSION["tipoUsuario"]) && (!isset($rutas[0]) || $rutas[0] == 'home')) { ?>
          <a href="<?php echo $ruta_global; ?>inicio-sesion" class="center_mov_inic">
            <img src="<?php echo $ruta_global; ?>vistas/assets/img/logo-usuario.svg" alt="movil_ini" class="tam_inic_mov" ruta="<?php echo $ruta; ?>">
            <span class="span_txt_inic">Iniciar <br> Sesión</span>
          </a>
        <?php } elseif (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] != NULL && $rutas[0] == 'home' || !isset($rutas[0])) {
        } elseif ($rutas[0] != "home" && isset($rutas[0])) { ?>
          <img src="<?php echo $ruta_global; ?>vistas/assets/img/flecha-atras-icon.svg" id="flecha_atras_diseno" ruta="<?php echo $ruta; ?>">
        <?php } ?>

      </div>
    </div>
    <!-- FIN MOVIL -->

    <a href="<?php echo $ruta_global; ?>home" class="esp_mv_hd">

      <!-- <img class="logo-head-small" src="<?php echo $ruta_global; ?>vistas/assets/img/uh_small.svg" id="logo-ss-sm" /> -->

      <div class="logo-head-small">

        <svg class="tam_svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 97.99 97.64">
          <defs>
            <style>
              .cls-1 {
                fill: #ff5c1a;
              }
            </style>
          </defs>
          <title>uh_small</title>
          <g id="Layer_2" data-name="Layer 2">
            <g id="Layer_1-2" data-name="Layer 1">
              <path class="cls-1" d="M50.85,0V30.49h10c8.48,0,8.58,7.39,8.58,7.46,0,.87.1,3.53.1,3.53V64h5.52v3.71H65.57V39.35a4.63,4.63,0,0,0-1.39-3.46,5.22,5.22,0,0,0-3.62-1.38H50.85V97.64A48.85,48.85,0,0,0,50.85,0m5.22,91.89V39.72h4.29V72.93H80.25V58.78H74.79V41.38S74.7,38.79,74.7,38c0-4.9-3.75-12.68-13.8-12.68H56.07V5.75a43.65,43.65,0,0,1,0,86.14" />
              <path class="cls-1" d="M47.14,92.42V67.65h-10c-8.48,0-8.58-7.39-8.58-7.47,0-.86-.1-3.52-.1-3.52V34.12H22.94v-3.7h9.48V58.79a4.67,4.67,0,0,0,1.39,3.46,5.27,5.27,0,0,0,3.62,1.38h9.71V0a48.85,48.85,0,0,0,0,97.64ZM5.34,48.82A43.72,43.72,0,0,1,41.92,5.75V58.41H37.63V25.21H17.73V39.35h5.52v17.4s.09,2.6.09,3.43c0,4.9,3.75,12.68,13.8,12.68H42v19a43.72,43.72,0,0,1-36.63-43" />
            </g>
          </g>
        </svg>

      </div>

      <img class="logo-head" src="<?php echo $ruta_global; ?>vistas/assets/img/logo.svg" id="logo-ss" />

    </a>


    <div class="esp_mv_menu">

      <div class="menu_movil" estatus="0">

        <div class="ln_mv_tp"></div>

        <div class="ln_mv_bt"></div>

      </div>

    </div>




    <div class="menu-contender">
      <ul class="menu-nav" id="menu-slide">
        <a class="no_link" href="https://urbanhangers.com/" target="_blank" rel="noopener">
          <li id="bTienda" class="menu-el">Tienda</li>
        </a>
        <?php
        if (!isset($rutas[0]) || $rutas[0] == 'home') {
        } else {
        ?>
          <a class='no_link' href='<?php echo $ruta_global; ?>home'>
            <li id='bVota' class='menu-el'>Vota</li>
          </a>
        <?php
        }

        if (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 1) {

          echo "
              <a class='no_link' href='https://urbanhangers.com/pages/quienes-somos' rel='noopener' target='_blank'><li id='bComoF' class='menu-el'>¿Quiénes somos?</li></a>
              <a class='no_link' href='#contactanos_urban'><li id='bContacto' class='menu-el'>Contacto</li></a>";
          // echo "<a class='no_link' href='#contactanos_urban'><li id='bContacto' class='menu-el'><img class='icono-menu' src='".$ruta_global."vistas/assets/img/campana.svg'><img class='circulo-notificaciones' src='".$ruta_global."vistas/assets/img/circulo-notificaciones.svg' alt=''> </li></a>
          // <li id='bQuiero' class='menu-el border-hover'><a href='".$ruta_global.$_SESSION["ruta_perfil"]."'><div class='div-menu'>DASHBOARD</div></a></li>";
        } else if (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 2) {
          echo "<a class='no_link' href='https://urbanhangers.com/pages/quienes-somos' rel='noopener' target='_blank'><li id='bComoF' class='menu-el'>¿Quiénes somos?</li></a>
                    <a class='no_link' href='#contactanos_urban'><li id='bContacto' class='menu-el'>Contacto</li></a>
                    <li id='bQuiero' class='menu-el border-hover' onclick='hanger_upgrade()'><div class='div-menu'>Quiero Ser Hanger</div></li>";
        } else {
          echo "<a class='no_link' href='https://urbanhangers.com/pages/quienes-somos' rel='noopener' target='_blank'><li id='bComoF' class='menu-el'>¿Quiénes somos?</li></a>
                    <a class='no_link' href='#contactanos_urban'><li id='bContacto' class='menu-el'>Contacto</li></a>
                    <li id='bQuiero' class='menu-el border-hover' onclick='hanger()'><div class='div-menu'>Quiero Ser Hanger</div></li>";
        }

        ?>

        <!-- <li class="menu-el border-hover">
            <div class="icon-menu facebook" title="Facebook"></div>
          </li>
          <li class="menu-el border-hover">
            <div class="icon-menu instagram" title="Instagram"></div>
          </li> -->

        <?php
        if (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 1 || isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 2) {
          if (isset($_SESSION['facebook']) && $_SESSION['facebook'] == "1") {
            $ruta_foto = $_SESSION["avatar"];
          } else {
            if (!empty($_SESSION["avatar"])) {
              $ruta_foto = $ruta_global . "vistas/assets/hangers/" . $_SESSION['carpeta'] . "/" . $_SESSION["avatar"];
            } else {
              if ($_SESSION['sexo'] == "h") {
                $ruta_foto = $ruta_global . "vistas/assets/img/icon-usuario-1.svg";
              } else {
                $ruta_foto = $ruta_global . "vistas/assets/img/icon-usuario-5.svg";
              }
            }
          }

        ?>
          <div id='cont_img_header' class='icon-menu icon-avatar-menu' title='<?php echo $_SESSION["usuario"]; ?>' style='display:inline-block'>
            <div id='img_header_perfil' class='img_perfil_header borde_img_perfil_hd' flag='0' style='background-image: url(<?php echo $ruta_foto; ?>)'></div>
            <!-- <span class='usuario' id='nombreLog' title='<?php echo $_SESSION["usuario"]; ?>'><?php echo $_SESSION["usuario"]; ?></span> -->
            <?php
              if($_SESSION["tipoUsuario"] == 2 && $_SESSION["modo_registro"] == 'facebook' && $_SESSION["nombre_facebook"] != null){
                echo "<span class='usuario' id='nombreLog' title='".$_SESSION["nombre_facebook"]."'>".$_SESSION["nombre_facebook"]."</span>";
              }else{
                echo "<span class='usuario' id='nombreLog' title='".$_SESSION["usuario"]."'>".$_SESSION["usuario"]."</span>";
              }
            ?>
          </div>
          <div class='menu_sesion <?php echo ($_SESSION["tipoUsuario"] == 1) ? 'right_hanger' : 'right_votante'; ?> oculto'>
            <?php
            if (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 1) {
            ?>
              <a href='<?php echo $ruta_global . $_SESSION["ruta_perfil"]; ?>' class='elem_menu'>
                <div class='elem_menu_int'>Ver Perfil</div>
              </a>
              <a href='<?php echo $ruta_global; ?>subirDiseno' class='elem_menu'>
                <div class='elem_menu_int'>Subir Diseño</div>
              </a>
            <?php
            }
            ?>
            <a href='<?php echo $ruta_global; ?>salir' class='elem_menu'>
              <div class='elem_menu_int'>Cerrar Sesión</div>
            </a>
          </div>
        <?php

        } else {
          echo "<li class='menu-el border-hover hov-usuario login_usuario' style='text-align:center'        onclick='inicio_sesion()'>
                  <div class='icon_menu_log icon-usuario' title='Usuario' style='display:inline-block'></div>
                  <span class='usuario_login iniciar_sesion_sp' id='nombreLog'>Iniciar Sesión</span>
                  </li>";
        }
        ?>


      </ul>
    </div>


  </div>

  <div class="menu_display">




    <?php if (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 1 || isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 2) { ?>
      <!-- MENU SESION -->

      <div class="menu_cont_mv">

        <div class="mg_mv_txt">
          <?php if (isset($_SESSION['facebook']) && $_SESSION['facebook'] == "1") {
            $ruta_foto = $_SESSION["avatar"];
          } else {
            if (!empty($_SESSION["avatar"])) {
              $ruta_foto = $ruta_global . "vistas/assets/hangers/" . $_SESSION['carpeta'] . "/" . $_SESSION["avatar"];
            } else {
              if ($_SESSION['sexo'] == "h") {
                $ruta_foto = $ruta_global . "vistas/assets/img/icon-usuario-1.svg";
              } else {
                $ruta_foto = $ruta_global . "vistas/assets/img/icon-usuario-5.svg";
              }
            }
          } ?>
          <img src="<?php echo $ruta_foto; ?>" alt="perfil_img" class="perfil_mov">
          <?php if (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 1) { ?>
            <a id="link_perfil_hanger_mv" class="no_link" href="<?php echo $ruta_global . $_SESSION["ruta_perfil"]; ?>"><span class="menu_txt"><?php echo $_SESSION["usuario"]; ?></span></a>
          <?php } else { ?>
            <span class="menu_txt"><?php echo $_SESSION["usuario"]; ?></span>
          <?php } ?>
        </div>
        <?php if (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 2) { ?>
          <div class="mg_mv_txt">
            <span class="menu_txt" onclick="hanger_upgrade()">QUIERO SER HANGER</span>
          </div>
        <?php  }
        if (!isset($rutas[0]) || $rutas[0] == 'home') {
        } else {
        ?>

          <div class="mg_mv_txt">
            <a class="no_link" href="<?php echo $ruta_global . 'home'; ?>"><span class="menu_txt">HOME</span></a>
          </div>
          <div class="mg_mv_txt">
            <a class="no_link" href="<?php echo $ruta_global . 'home'; ?>"><span class="menu_txt">VOTA</span></a>
          </div>
        <?php } ?>
        <?php if (isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 1) { ?>

          <div class="mg_mv_txt">
            <a class="no_link" href="<?php echo $ruta_global . 'editarPerfil'; ?>"><span class="menu_txt">EDITAR PERFIL</span></a>
          </div>
        <?php } ?>

        <div class="mg_mv_txt">
          <a class="no_link" href="https://urbanhangers.com/" rel="noopener noreferrer" target="_blank"><span class="menu_txt">TIENDA</span></a>
        </div>

        <div class="mg_mv_txt">
          <span class="menu_txt"><a class="no_link c_white" href="https://urbanhangers.com/pages/quienes-somos" target="_blank" rel="noopener">¿QUIÉNES SOMOS?</a></span>
        </div>

        <div class="mg_mv_txt">
          <a class="no_link" href="<?php echo $ruta_global . '#contactanos_urban'; ?>"><span class="menu_txt menu_movil">CONTACTO</span></a>
        </div>

        <div class="mg_mv_txt">
          <a class="no_link" href="<?php echo $ruta_global . 'salir'; ?>"><span class="menu_txt">CERRAR SESIÓN</span></a>
        </div>

        <hr class="divisor_menu">

        <div class="mg_mv_txt" style="padding-top: 15px;">
          
          <span class="menu_txt anuncion_menu"> * Para subir un diseño entra desde una computadora de escritorio</span>
        
        </div>

      </div>
    <?php } else { ?>
      <!-- MENU SIN SESION -->

      <div class="menu_cont_mv">

        <div class="mg_mv_txt">
          <a class="no_link" href="<?php echo $ruta_global . 'home'; ?>"><span class="menu_txt">HOME</span></a>
        </div>

        <div class="mg_mv_txt">
          <a class="no_link" href="https://urbanhangers.com/" rel="noopener noreferrer" target="_blank"><span class="menu_txt">TIENDA</span></a>
        </div>

        <div class="mg_mv_txt">
          <a class="no_link" href="<?php echo $ruta_global . 'home'; ?>"><span class="menu_txt">VOTA</span></a>
        </div>

        <div class="mg_mv_txt">
          <span class="menu_txt"><a class="no_link c_white" href="https://urbanhangers.com/pages/quienes-somos" target="_blank" rel="noopener">¿QUIÉNES SOMOS?</a></span>
        </div>

        <div class="mg_mv_txt">
          <a class="no_link" href="<?php echo $ruta_global . '#contactanos_urban'; ?>"><span class="menu_txt menu_movil">CONTACTO</span></a>
        </div>

        <hr class="divisor_menu">

        <div class="mg_mv_txt" style="padding-top: 15px;">
          
            <span class="menu_txt anuncion_menu"> * Para subir un diseño entra desde una computadora de escritorio</span>
          
        </div>

      </div>
    <?php } ?>


  </div>

</header>



<!-- modal de inicio de sesion y modal de registro -->

<div id="inicio_sesion" class="modal modal-inicio">
  <form id="form_login" method="post">
    <div class="modal-content modal-padding">
      <span class="close-modal-inicio" onclick="cerrar_modal_inicio()">X</span>
      <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal" /><br />
      <span class="titulo-modal">INICIA SESIÓN</span>
      <div id="div_respuesta_login"></div>
      <div class="input-field col s12 margin-inicio input_correo_login">
        <input type="email" class="input-inicio" id="txtUsuario" name="email_login" required />
        <label class="label-inicio">CORREO</label>
      </div>
      <?php $ruta = explode("/", $_GET["pagina"]); ?>
      <input type="hidden" name="ruta" value="<?php echo $ruta[0]; ?>">
      <div class="input-field col s12 margin-inicio input_contrasena_login">
        <img src="<?php echo $ruta_global; ?>vistas/assets/img/icon-mostrar.svg" class="mostrar-pass mostrar_password" inp="txtPassword" id="mostrar_pass_login" title="Mostrar contraseña" />
        <input type="password" class="input-inicio" name="pass_login" id="txtPassword" required />
        <label class="label-inicio">CONTRASEÑA</label>
      </div>
      <div class="div-inicio-modal">
        <p>
          <label>
            <input id="check_recordar" type="checkbox" name="recordar" />
            <span>Recuérdame</span>
          </label>
        </p>
        <a class="enlace-pass" onclick="reestablecer_contrasena()">¿Olvidaste tu contraseña?</a>
      </div>
      <div id="cont_botones_inicio">
        <input type="submit" class="button-inicio" value="INICIAR SESIÓN" /> <br>
        <!-- <div class="fb-login-button" data-size="large" data-button-type="login_with" data-layout="default" data-auto-logout-link="false" data-use-continue-as="false" data-width=""></div>       -->
        <!-- <fb:login-button
        scope="public_profile,email"
        onlogin="checkLoginState();">
        </fb:login-button> -->
        <div class="fb-login-button" data-size="large" data-button-type="continue_with" data-layout="rounded" data-auto-logout-link="false" data-use-continue-as="true" data-width="" scope="public_profile,email" onlogin="checkLoginState();"></div>
      </div>

      <?php

      // $ingreso = new ControladorUsuarios;
      // $ingreso->ingresoUsuario();


      ?>

    </div>
  </form>
  <div id="footer_modal_inicio" class="modal-footer modal-inicio-padding" style="text-align:center;height:auto">
    <div class="inicio-footer"></div><br />
    <span class="texto-modal">¿Aún no tienes cuenta?</span><br />
    <a class="unete pointer" onclick="registrarse()">¡Únete!</a>
  </div>
</div>
<div id="registrarse_modal" class="modal modal-registrarse">
  <div class="modal-content modal-padding" id="contenido-registrarse">
    <span class="close-modal-inicio" onclick="cerrar_modal_registrarse()">X</span>
    <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal" /><br />
    <span class="titulo-modal">REGÍSTRATE</span>
    <div class="fx_tab">
      <ul class="tabs fx_tab_ul" id="tabs-hanger-votante">
        <li class="tab fx_tab_li"><a href="#tab-votante" id="votante-tab">VOTANTE</a></li>
        <li class="tab fx_tab_li active"><a href="#tab-hanger" id="hanger-tab">HANGER</a></li>
      </ul>
    </div>
    <div class="">
      <div id="tab-hanger" class="col s12 texto-hanger">
        Cuando eres Hanger puedes votar y subir tus propios diseños.<br />
      </div>
      <div id="tab-votante" class="col s12 texto-hanger">
        Los votantes pueden votar por sus diseños favoritos, y cambiar a una cuenta de hanger cuando quieran.
      </div>
    </div>

    <form id="form_registro" method="post">

      <input id="tipo_usuario" type="hidden" name="tipo_usuario" value="2">
      <div id="cont_error_usuario" style="display:none;">
        <p id="msj_error_registro"></p>
      </div>
      <div class="input-field col s12 margin-inicio input_usuario_registro">
        <input type="text" class="input-inicio" id="txtUsuarioRegistro" name="txtUsuarioRegistro" maxlength="15" required />
        <label class="label-inicio">Usuario</label>
        <span class="helper-text" data-error="wrong" data-success="right" style="text-align:right">*Este será tu nombre de usuario visto por el público</span>
      </div>
      <div class="input-field col s12 margin-inicio input_correo_registro">
        <input type="email" class="input-inicio" id="txtCorreoRegistro" name="txtCorreoRegistro" maxlength="100" required />
        <label class="label-inicio">CORREO</label>
      </div>
      <div class="input-field col s12 margin-inicio input_contrasena_registro">
        <img src="<?php echo $ruta_global; ?>vistas/assets/img/icon-mostrar.svg" class="mostrar-pass-sp mostrar_password" inp="txtPasswordRegistro" id="mostrar_pass_registro" title="Mostrar contraseña" />
        <input type="password" class="input-inicio" name="txtPasswordRegistro" id="txtPasswordRegistro" maxlength="100" required />
        <label class="label-inicio">CONTRASEÑA</label>
        <span class="helper-text" data-error="wrong" data-success="right" style="text-align:right">*Contraseña de mínimo 9 carácteres</span>
      </div>
      <div class="input-field col s12 margin-inicio input_repetir_contrasena_registro">
        <img src="<?php echo $ruta_global; ?>vistas/assets/img/icon-mostrar.svg" class="mostrar-pass mostrar_password" inp="txtPasswordRepetir" id="mostrar_pass_repetir_registro" title="Mostrar contraseña" required />
        <input type="password" class="input-inicio" name="txtPasswordRepetir" id="txtPasswordRepetir" maxlength="100" required />
        <label class="label-inicio">REPETIR CONTRASEÑA</label>
      </div>
      <div class="input-field col s12 margin-inicio">
        <div class="cont_sexo">
          <p>
            <label>
              <input class="sexo_radio" name="sexoRegistro" type="radio" checked value="hombre" />
              <span class="sexo">Hombre</span>
            </label>
          </p>
          <p>
            <label>
              <input class="sexo_radio" name="sexoRegistro" type="radio" value="mujer" />
              <span class="sexo">Mujer</span>
            </label>
          </p>
        </div>
      </div>
      <div class="margin-inicio">
        <label>
          <input id="check_terminos_condiciones" type="checkbox" name="terminos" />
          <span>Acepto <a target="_blank" href='<?php echo $ruta_global; ?>vistas/assets/terminos_condiciones/General.pdf'>términos y condiciones </a> Urban Hangers.</span>
        </label>
      </div>

      <div id="cont_loader_registro" style="display:none;">
        <img src='<?php echo $ruta_global; ?>vistas/assets/css/ajax-loader.gif' alt='Cargando...'>
      </div>
      <div id="contenedor_botones_registro">
        <input type="submit" class="button-inicio" value="REGÍSTRATE" /><br />
        <!-- <fb:login-button
          scope="public_profile,email"
          onlogin="checkLoginState();">
        </fb:login-button> -->
        <div class="fb-login-button" data-size="large" data-button-type="continue_with" data-layout="rounded" data-auto-logout-link="false" data-use-continue-as="true" data-width="" scope="public_profile,email" onlogin="checkLoginState();"></div>
      </div>

    </form>


  </div>
</div>

<!--modal hanger--->
<div id="hanger_modal" class="modal modal-hanger">
  <div class="modal-content modal-padding" id="contenido-registrarse">
    <span class="close-modal-inicio" onclick="cerrar_hanger()">X</span>
    <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal" /><br />
    <span class="titulo-modal">REGÍSTRATE</span>
    <div class="">
      <div class="col s12">
        <ul class="tabs" id="tabs-hanger">
          <li class="tab col s12"><a href="#tab-hanger" class="active" tipo="1">HANGER</a></li>
        </ul>
      </div>
      <div id="tab-hanger" class="col s12 texto-hanger">
        Cuando eres Hanger puedes votar y subir tus propios diseños.<br />
      </div>
    </div>

    <form id="form_registro_hanger" method="post">
      <div id="cont_error_usuario_hanger" style="display:none;">
        <p id="msj_error_registro_hanger"></p>
      </div>
      <input type="hidden" name="tipo_usuario" value="1">

      <div class="input-field col s12 margin-inicio input_usuario_hanger">
        <input type="text" class="input-inicio" id="usuarioHanger" name="usuarioHanger" maxlength="15" required />
        <label class="label-inicio">Usuario</label>
        <span class="helper-text" data-error="wrong" data-success="right" style="text-align:right">*Este será tu nombre de usuario visto por el público</span>
      </div>
      <div class="input-field col s12 margin-inicio input_correo_hanger">
        <input type="email" class="input-inicio" id="correoHanger" name="correoHanger" maxlength="100" required />
        <label class="label-inicio">CORREO</label>
      </div>
      <div class="input-field col s12 margin-inicio input_contrasena_hanger">
        <img src="<?php echo $ruta_global; ?>vistas/assets/img/icon-mostrar.svg" class="mostrar-pass-sp mostrar_password" inp="passwordHanger" id="mostrar_pass_hanger" title="Mostrar contraseña" />
        <input type="password" class="input-inicio" name="passwordHanger" id="passwordHanger" maxlength="100" required />
        <label class="label-inicio">CONTRASEÑA</label>
        <span class="helper-text" data-error="wrong" data-success="right" style="text-align:right">*Contraseña de mínimo 9 carácteres</span>
      </div>
      <div class="input-field col s12 margin-inicio input_repetir_contrasena_hanger">
        <img src="<?php echo $ruta_global; ?>vistas/assets/img/icon-mostrar.svg" class="mostrar-pass mostrar_password" inp="passwordHangerRepetir" id="mostrar_pass_repetir_hanger" title="Mostrar contraseña" />
        <input type="password" class="input-inicio" name="passwordHangerRepetir" id="passwordHangerRepetir" maxlength="100" required />
        <label class="label-inicio">REPETIR CONTRASEÑA</label>
      </div>
      <div class="input-field col s12 margin-inicio">
        <div class="cont_sexo">
          <p>
            <label>
              <input class="sexo_radio" name="sexoHanger" type="radio" checked value="hombre" />
              <span class="sexo">Hombre</span>
            </label>
          </p>
          <p>
            <label>
              <input class="sexo_radio" name="sexoHanger" type="radio" value="mujer" />
              <span class="sexo">Mujer</span>
            </label>
          </p>
        </div>
      </div>
      <div class="margin-inicio">
        <label>
          <input type="checkbox" name="terminos" />
          <span>Acepto <a target="_blank" href='<?php echo $ruta_global; ?>vistas/assets/terminos_condiciones/General.pdf'>términos y condiciones </a> Urban Hangers.</span>
        </label>
      </div>
      <div id=" cont_loader_hanger" style="display:none;">
        <img src='<?php echo $ruta_global; ?>vistas/assets/css/ajax-loader.gif' alt='Cargando...'>
      </div>
      <div id="cont_botones_hanger">
        <input type="submit" class="button-inicio" value="REGÍSTRATE" /><br />
        <!-- <input type="button" class="button-facebook" value="INICIAR CON FACEBOOK" />     -->
        <!-- <fb:login-button
          scope="public_profile,email"
          onlogin="checkLoginState();">
        </fb:login-button> -->
        <div class="fb-login-button" data-size="large" data-button-type="continue_with" data-layout="rounded" data-auto-logout-link="false" data-use-continue-as="true" data-width="" scope="public_profile,email" onlogin="checkLoginState();"></div>
      </div>

    </form>


  </div>
</div>


<!-- Modal playstation -->
<div id="modal_playstation" class="modal modal-transparente modal-playstation" style="scrollbar-width: thin; scrollbar-color: transparent transparent;">
  <div class="modal-content modal-padding modal-transparente" id="contenido-registrarse" style="overflow: hidden;">
    <span class="cerrar-transparente close-modal-inicio" onclick="cerrar_modal_playstation()">X</span>
    <img src="<?php echo $ruta_global; ?>vistas/assets/img/promociones/fin-convocatoria.jpg" alt="" class="imagen-play desk_play">
    <img src="<?php echo $ruta_global; ?>vistas/assets/img/promociones/fin-convocatoria.jpg" alt="" class="imagen-play mov_play">
    <br>
    <div style="display:flex; width:100%; justify-content: center; align-items:center;">
      
      <?php 
        if($_SESSION["iniciarSesion"] == "ok"){
      ?>
        <button class="button-inicio" onclick="cerrar_modal_playstation()">CONTINUAR</button>
      <?php
        }else{
      ?>
        <button class="button-inicio" onclick="inicio_sesion()">CONTINUAR</button>
      <?php
        }
      ?>
    </div>
    <a class="button-inicio btn_inicio_mov" href="vistas/assets/terminos_condiciones/Gamer.pdf" id="term_condi_modal"  target="_blank" style="color:white;">Terminos y condiciones</a>
    
    
  </div>
</div>
<!-- Fin modal playstation -->


<div id="registro_exitoso" class="modal modal-inicio h500">
  <div class="modal-content modal-padding">
    <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal" /><br />
    <span class="titulo-modal">¡Registro exitoso!</span>
    <img id="icon_correo" src="<?php echo $ruta_global; ?>vistas/assets/img/icon-correo.svg">
    <div class="col s12 texto_modal_2">
      <!-- Tu registro fue realizado correctamente, por favor revisa tu bandeja de entrada para activar tu cuenta. -->
      Tu registro fue realizado correctamente.
    </div>
    <button class="button-inicio modal-close">Aceptar</button><br />
    <!-- <div id="enviar_nuevamente">Enviar de nuevo</div> -->
  </div>
</div>

<div id="reenvio_correo" class="modal modal-inicio h500">
  <div class="modal-content modal-padding">
    <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal" /><br />
    <span class="titulo-modal">Correo reenviado</span>
    <img id="icon_correo" src="<?php echo $ruta_global; ?>vistas/assets/img/icon-correo.svg">
    <div class="col s12 texto_modal_2">
      El correo fue enviado nuevamente, por favor revisa tu bandeja de entrada para activar tu cuenta.
    </div>
    <button class="button-inicio modal-close">Aceptar</button><br />
  </div>
</div>

<div id="error_modal" class="modal modal-inicio h500">
  <div class="modal-content modal-padding">
    <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal" /><br />
    <span class="titulo-modal">¡Ocurrió un problema!</span>
    <div class="col s12 texto_modal_2">
      Favor de intentarlo más tarde.
    </div>
    <button class="button-inicio modal-close">Aceptar</button>
  </div>
</div>

<div id="error_modal_caracteres" class="modal modal-inicio h500">
  <div class="modal-content modal-padding">
    <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal" /><br />
    <span class="titulo-modal">¡Ocurrió un problema!</span>
    <div class="col s12 texto_modal_2">
      Favor de revisar, el nombre (20 caracteres permitidos) y la descripción del diseño (300 caracteres permititdos). <br>
      Recuerda que no se permiten caracteres especiales en el nombre del diseño.
    </div>
    <button class="button-inicio modal-close">Aceptar</button>
  </div>
</div>

<div id="error_modal_generico" class="modal modal-inicio h500">
  <div class="modal-content modal-padding">
    <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal" /><br />
    <span class="titulo-modal">¡Ocurrió un problema!</span>
    <div id="msj_error_modal" class="col s12 texto_modal_2"></div>
    <button id="btn_error_genercio" class="button-inicio modal-close">Aceptar</button>
  </div>
</div>

<div id="modal_error_hanger" class="modal modal_estilo">
  <div class="modal-content modal-padding">
    <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal" /><br />
    <span class="titulo-modal">¡Ya eres Hanger!</span>
    <div class="col s12 texto_modal_2">
      Favor de cerrar sesión y volver a iniciar.
    </div>
    <button class="button-inicio modal-close">Aceptar</button>
  </div>
</div>

<div id="modal_completa_formulario" class="modal modal_estilo">
  <div class="modal-content modal-padding">
    <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal" /><br />
    <span class="titulo-modal">Completa el formulario</span>
    <div class="col s12 texto_modal_2">
      Por favor, completa el formulario para ponernos en contacto contigo.
    </div>
    <button class="button-inicio modal-close">Aceptar</button>
  </div>
</div>

<div id="registrado_facebook" class="modal modal_resultado modal_estilo">
  <div class="modal-content modal-padding">
    <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal" /><br />
    <span class="titulo-modal">¡El correo ya esta registrado con facebook!</span>
    <div class="col s12 texto_modal_2">
      Por favor inicia sesion con facebook.
    </div>
    <button id="btn_modal_registrado_fb" class="button-inicio modal-close">Aceptar</button>
  </div>
</div>

<div id="registrado_directo" class="modal modal_resultado modal_estilo">
  <div class="modal-content modal-padding">
    <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal" /><br />
    <span class="titulo-modal">¡Tu correo ya está registrado!</span>
    <div class="col s12 texto_modal_2">
      Ya puedes inicar sesión.
    </div>
    <button id="btn_modal_registrado_dir" class="button-inicio modal-close">Iniciar Sesión</button>
  </div>
</div>

<div id="verificado" class="modal modal_resultado modal_estilo">
  <div class="modal-content modal-padding">
    <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal" /><br />
    <span class="titulo-modal">¡Verificación exitosa!</span>
    <div class="col s12 texto_modal_2">
      Tu verificación fue realizada correctamente, ya pudes iniciar sesión.
    </div>
    <a class="button-inicio modal-close" href="<?php echo $ruta_global; ?>home">Iniciar Sesión</a>
  </div>
</div>

<div id="hanger_upgrade_modal" class="modal modal_estilo">
  <div class="modal-content modal-padding">
    <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal" /><br />
    <span class="titulo-modal">¡Conviertete en Hanger!</span>
    <div class="col s12 texto_modal_2">
      ¿Estás seguro de querer cambiar tu perfil a Hanger?
    </div>
    <p id="btn_confirmacion_hanger" class="button-inicio btn_modal_confirmar_upgrade">Sí</p>
    <p class="button-inicio modal-close btn_modal_confirmar_upgrade">No</p>
  </div>
</div>

<div id="cambio_exitoso_generico" class="modal modal_estilo">
  <div class="modal-content modal-padding">
    <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal" /><br />
    <span class="titulo-modal">Cambio exitoso!</span>
    <div id="msj_cambio_exitoso" class="col s12 texto_modal_2"></div>
    <button id="btn_aceptar_cambio_exitoso" class="button-inicio modal-close">Aceptar</button>
  </div>
</div>

<!--modal olvidaste contraseña !-->
<div id="reestablecer_contrasena" class="modal modal-inicio">
  <form id="form_reestablecer" method="post">
    <div class="modal-content modal-padding">
      <span class="close-modal-inicio" onclick="cerrar_modal_reestablecer_contrasena()">X</span>
      <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal" /><br />
      <span class="titulo-modal">¿Olvidaste tu contraseña?</span>
      <div id="div_respuesta_login"></div>
      <div class="input-field col s12 margin-inicio input_correo_login">
        <input type="email" class="input-inicio" id="correoRestablecer" name="correoRestablecer" required />
        <label class="label-inicio">CORREO</label>
      </div>

      <div id="cont_botones_inicio">
        <input type="submit" class="button-inicio" id="btn_olvidaste_contrasena" value="Restablecer contraseña" /> <br>
      </div>

      <div id="respuesta_contrasena_olvidada">

      </div>

    </div>
  </form>
</div>


<!--modal contacto-->

<div id="modal_contacto" class="modal modal-inicio">

  <div class="modal-content modal-padding">
    <span class="close-modal-inicio" onclick="cerrar_modal_contacto()">X</span>
    <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal" /><br />
    <span class="titulo-modal">Mensaje enviado correctamente</span>
    <img src="<?php echo $ruta_global; ?>vistas/assets/img/correo/correo.png" width='180px' height='160px' />
    <div class="col s12 texto_modal_2">
      Gracias por contactarnos, nosotros nos pondremos en contacto contigo.
    </div>

    <div class="desktop_no movil_btn_contacto">

      <div class="btn_sm" onclick="cerrar_modal_contacto()">
        Aceptar
      </div>

    </div>
  </div>

</div>
</div>