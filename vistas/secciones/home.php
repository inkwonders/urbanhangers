<?php


$rutas = explode("/", $_GET["pagina"]);

$bool_default = false;
$item = "ruta";
$valor =  $rutas[0];

if (!empty($rutas[1])) {
  if ($rutas[1] == 'nuevos') {
    $ruta_paginador = "home/" . $rutas[1];
    $texto_filtro = "NUEVOS";
    $opt_nuevos = "activo";
    $filtro = $rutas[1];
  } else if ($rutas[1] == 'mas-votados') {
    $ruta_paginador = "home/" . $rutas[1];
    $texto_filtro = "MÁS VOTADOS";
    $opt_mas_votado = "activo";
    $filtro = $rutas[1];
  } else if ($rutas[1] == 'tendencia') {
    $ruta_paginador = "home/" . $rutas[1];
    $texto_filtro = "TENDENCIA";
    $opt_tendencia = "activo";
    $filtro = $rutas[1];
  } else {
    $rutaColeccion = ControladorDisenos::mostrarProducto('colecciones', $item, $rutas_home[1]);

    if (!empty($rutaColeccion)) {
      $ruta_paginador = "home/" . $rutas[1];
      $texto_filtro = strtoupper($rutaColeccion['nombre_coleccion']);
      $opt_coleccion = "activo";
      $filtro = $rutaColeccion['sk_coleccion'];
    } else {
      $ruta_paginador = "home";
      $texto_filtro = "NUEVOS";
      $bool_default = true;
      $filtro = 'nuevos';
    }
  }
  

} else {
  $ruta_paginador = "home";
  $filtro = 'nuevos';
  $texto_filtro = "NUEVOS";
}

if ($bool_default) {
  if (!empty($rutas[1])) {
    $indice = $rutas[1];
    $bool1 = is_numeric($indice);
    $bool2 = $indice <= $total_num_hojas;
    if ($bool1 = 1 && $bool2 = 1) {
      $pagina_anterior = $indice - 1;
      $pagina_url = $indice;
      $pagina_siguiente = $indice + 1;
    } else {
      $pagina_anterior = 0;
      $pagina_url = 1;
      $pagina_siguiente = 2;
    }
  }
} else {
  if (!empty($rutas[2])) {
    $indice = $rutas[2];
    $bool1 = is_numeric($indice);
    $bool2 = $indice <= $total_num_hojas;
    if ($bool1 = 1 && $bool2 = 1) {
      $pagina_anterior = $indice - 1;
      $pagina_url = $indice;
      $pagina_siguiente = $indice + 1;
    }
  } else {
    $pagina_anterior = 0;
    $pagina_url = 1;
    $pagina_siguiente = 2;
  }
}

if ($pagina_url == 1) {
  $limitante = 0;
} else {
  $limitante = ($pagina_url - 1) * 20;
}
?>

<script type='text/javascript'>
  window.__lo_site_id = 312183;
  (function() {
  var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
  wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
  })();
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-159600964-2"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-159600964-2');
</script>

<div class="encabezado barra_home">

  <div class="cont_encabezado_home">

    <div id="encabezado_vista" class="encabezado-celda">

      <div class='cont_estilo_lista'>
        <div class='tipo_vista tipo_vista_list tipo_vista_active' op='list'>
          <i class='fas fa-list-ul'></i>
        </div>

        <div class='tipo_vista tipo_vista_grid' op='grid'>
          <i class='fas fa-th'></i>
        </div>
      </div>

      <form id="form_tipo_vista" method="post">
        <input id="opcion_vista" type="hidden" name="tipo_vista" value="">
      </form>
    </div>

    <div id="encabezado_categoria">
      <div class="titulo movil_not"><?php echo $texto_filtro; ?></div>

      <div id="encabezado_filtro_movil" class="encabezado-celda encabezado_dashboard evento_filtro_home desktop_no" style="text-align:right">
        <div class="custom-select" id="filtro_home_movil">
          <select id="select_filtro_movil">
            <option value="0" filtro="0">FILTRAR POR CATEGORÍA</option>
            <option value="nuevos" filtro="1">NUEVOS</option>
            <option value="mas-votados" filtro="2">MÁS VOTADOS</option>
            <option value="tendencia" filtro="4">TENDENCIA</option>
            <option value="">COLECCIONES</option>
            <?php
            $peticion = ControladorDisenos::coleccionesDiseno('colecciones');
            foreach ($peticion as $key => $value) {
                if($value['nombre_coleccion'] == "URBAN GAMER. ACEPTO  TéRMINOS Y CONDICIONES  DEL CONCURSO"){
            ?>
                  <option value="urban-gamer"><?php echo strtoupper("urban gamer"); ?></option>
            <?php
                }else{
            ?>
              <option value="<?php echo $value['ruta']; ?>"><?php echo strtoupper($value['nombre_coleccion']); ?></option>
            <?php
                }
            ?>
            <?php
            }
            ?>
          </select>
        </div>
      </div>


    </div>
    <div id="encabezado_filtro" class="encabezado-celda evento_filtro_home" style="text-align:right">
      <div class="custom-select movil_not">
        <select id="select_filtro">
          <option value="0" filtro="0">FILTRAR POR CATEGORÍA</option>
          <option value="nuevos" filtro="1">NUEVOS</option>
          <option value="mas-votados" filtro="2">MÁS VOTADOS</option>
          <!-- <option value="colecciones" filtro="3">COLECCIONES</option> -->
          <option value="tendencia" filtro="4">TENDENCIA</option>
          <option value="">COLECCIONES</option>
          <?php
            $peticion = ControladorDisenos::coleccionesDiseno('colecciones');
            foreach ($peticion as $key => $value) {
                if($value['nombre_coleccion'] == "URBAN GAMER. ACEPTO  TéRMINOS Y CONDICIONES  DEL CONCURSO"){
            ?>
                  <option value="urban-gamer"><?php echo strtoupper("urban gamer"); ?></option>
            <?php
                }else{
            ?>
              <option value="<?php echo $value['ruta']; ?>"><?php echo strtoupper($value['nombre_coleccion']); ?></option>
            <?php
                }
            ?>
            <?php
            }
            ?>
        </select>

        <form class="form_filtros" method="post">
          <input type="hidden" id="filtrar_diseno" name="filtro" value="">
        </form>

      </div>

    </div>

  </div>
</div>


<?php

$peticion = ControladorDisenos::consultaDisenos('diseno', 'usuarios', 'img_diseno', $filtro, $limitante);

$contador_disenos = 0;

foreach ($peticion as $key => $value) {
  $contador_disenos++;
  $idDiseno = $value['id_diseno'];

?>
  <div class="contenedor_index" index_val="0">

    <div id="cont_home_<?php echo $contador_disenos ?>" class="contenedor">

      <div class="contenedor_disenos">

        <div class="contenedor_datos_usuario">
          <div class="cont_interno_nombre_hanger pointer btn_dashboard_usuario" title="<?php echo $value['ruta'] ?>" key="<?php echo $value["id_usuario"]; ?>" user="<?php echo $value["ruta"]; ?>">


            <?php
            if ($value["modo_registro"] == "directo") {
              if ($value['foto'] == NULL || $value['foto'] == "") {
                if ($value['sexo'] == "h") {
                  $ruta_foto = $ruta_global . "vistas/assets/img/icon-usuario-1.svg";
                  echo "<div class='img_perfil' style='background-image: url(" . $ruta_foto . ")' alt='" . $value["ruta"] . "' title='" . $value["ruta"] . "'></div>";
                } else {
                  $ruta_foto = $ruta_global . "vistas/assets/img/icon-usuario-5.svg";
                  echo "<div class='img_perfil' style='background-image: url(" . $ruta_foto . ")' alt='" . $value["ruta"] . "' title='" . $value["ruta"] . "'></div>";
                }
              } else {
                $foto = $ruta_global . "vistas/assets/hangers/" . $value["carpeta"] . "/" . $value['foto'];
            ?>
                <div class="img_perfil" style="background-image: url(<?php echo $foto; ?>)" alt="<?php echo $value["ruta"]; ?>" title="<?php echo $value["ruta"]; ?>">
                </div>
              <?php
              }
            } else {
              ?>
              <div class="img_perfil" style="background-image: url('<?php echo $value['foto']; ?>')" alt="<?php echo $value["ruta"]; ?>" title="<?php echo $value["ruta"]; ?>">
              </div>
            <?php
            }
            ?>
            <!-- <div class="cont_nom_hanger"> -->
            <span class="nombre-usuario" title="<?php echo $value["ruta"]; ?>">
              <?php echo $value["ruta"]; ?>
            </span>
            <!-- </div> -->
          </div>


          <div class="descripcion-prenda">

            <a id="ruta_<?php echo $idDiseno; ?>" title="<?php echo $value['nombre_diseno']; ?>" href='<?php echo $ruta_global . $value['ruta_diseno']; ?>'>
              <h3 id="nombre_<?php echo $idDiseno; ?>" class="nombre-prenda">"<?php echo $value["nombre_diseno"]; ?>"</h3>
            </a>

            <?php if ($value["inspiracion"] != NULL) { ?>
              <ul class="collapsible">
                <li class="active">
                  <div class="inspiracion_diseno  collapsible-header">INSPIRACIÓN</div>
                  <div class="collapsible-body">
                    <span id="desc_<?php echo $idDiseno; ?>"><?php echo $value["inspiracion"]; ?></span>
                  </div>
                </li>
              </ul>
            <?php } ?>

          </div>

        </div>
        <div class="contenedor_imagen_disenos">


          <?php


          $id_imagen = $idDiseno;
          echo "<img loading='lazy' id='$id_imagen' ruta='" . $ruta_global . "vistas/assets/hangers/" . $value["carpeta"] . "/" . $value['ruta_img'] . "' src='" . $ruta_global . "vistas/assets/hangers/" . $value["carpeta"] . "/" . $value['ruta_img'] . "' class='img_material' title='" . $value["nombre_diseno"] . "' alt='" . $value["nombre_diseno"] . "'>";


          ?>
        </div>

        <div class="contenedor_votos_comentarios">


          <br />
          <?php
          $ruta = ControladorDashboard::consultaRuta('diseno', $idDiseno);
          $rutaDiseno = $ruta["ruta"];
          ?>
          <div id="conteo_votos<?php echo $idDiseno ?>" class="contenedor_votos_corazon">
            <?php

            $votos = ControladorDisenos::consultaVotos('votos_diseno', $idDiseno);

            if (isset($_SESSION["iniciarSesion"]) && $_SESSION["iniciarSesion"] == "ok") {
              $idUsuario = $_SESSION['id_usuario'];
              $votado = ControladorDisenos::consultaVotado('votos_diseno', $idUsuario, $idDiseno);
              if ($votado[2] == $idUsuario && $votado[1] == 1) { ?>

                <div id='<?php echo base64_encode($idDiseno); ?>' usuario='<?php echo base64_encode($idUsuario); ?>' class='votado evento_voto identificador_corazon_<?php echo $idDiseno; ?>' status='votado'></div>
              <?php
              } else {
              ?>


                <div id='<?php echo base64_encode($idDiseno); ?>' usuario='<?php echo base64_encode($idUsuario); ?>' class='no-votado evento_voto identificador_corazon_<?php echo $idDiseno; ?>' status='novotado'></div>

              <?php

              }
            } else {
              ?>

              <div id='<?php echo  base64_encode($idDiseno); ?>' class='no-votado' onclick='inicio_sesion()'></div>

            <?php

            }
            if($votos[0]>0 && $votos[0]!=1){
              $texto_votos=$votos[0]."<br /> VOTOS";
            }else if($votos[0]==1){
              $texto_votos=$votos[0]."<br /> VOTO";
            }else{
              $texto_votos="SIN <br /> VOTOS";
            }

            ?>
            <p id="numero_votos<?php echo $idDiseno; ?>" numero_de_votos="<?php echo $votos[0] ?>" class="conteo"><?php echo $texto_votos;?></p>
          </div>


          <div class="comentarios">
            <div class="compartelo">
              <p><b>COMPÁRTELO</b></p>
            </div>
            <div class="redes">
              <?php
              $ruta_wa = "https://web.whatsapp.com/send?text=https://urbanhangerscommunity.com/" . $rutaDiseno;
              // $ruta_tw = "https://twitter.com/intent/tweet?text=¡Checa%20este%20diseño!&url=https%3A%2F%2clientes.ink&#47urbanhangers&#47".$rutaDiseno;
              $ruta_tw = "https://twitter.com/intent/tweet?text=¡Checa%20este%20diseño!&url=https://urbanhangerscommunity.com/" . $rutaDiseno;
              $ruta_fb = "http://www.facebook.com/sharer.php?u=https://urbanhangerscommunity.com/" . $rutaDiseno;
              ?>
              <a rel="noopener noreferrer" class="ev_meta" dis="<?php echo $idDiseno; ?>" href="<?php echo $ruta_wa ?>" data-action="share/whatsapp/share" target="_blank"><img loading='lazy' class="icon-red" alt="Whatsapp" src="<?php echo $ruta_global; ?>vistas/assets/img/icon-whats-o.svg" /></a>
              <a rel="noopener noreferrer" class="ev_meta" dis="<?php echo $idDiseno; ?>" href="<?php echo $ruta_tw; ?>" target="_blank"><img loading='lazy' class="icon-red" alt="Twitter" src="<?php echo $ruta_global; ?>vistas/assets/img/icon-twitter-o.svg" /></a>
              <a rel="noopener noreferrer" class="ev_meta" dis="<?php echo $idDiseno; ?>" href="<?php echo $ruta_fb; ?>" target="_blank"><img loading='lazy' class="icon-red" alt="Facebook" src="<?php echo $ruta_global; ?>vistas/assets/img/icon-face-o.svg" /></a>
            </div>

            <div id="comentarios<?php echo $idDiseno ?>" class="comentarios-scroll">

              <?php

              $comentarios = ControladorDisenos::consultaComentarios('usuarios', $idDiseno);

              if (empty($comentarios)) {
                echo "<div class='sin_comentarios'><span class='comentario-nombre'>Sin comentarios</span></div>";
              } else {

                foreach ($comentarios as $key => $valueComentarios) {

              ?>

                  <div class='agregar_comentario'>
                    <?php
                    if ($valueComentarios['modo_registro'] == 'directo') {
                      if ($valueComentarios['tipo_usuario'] == '1') {
                        if ($valueComentarios['foto'] == NULL || $valueComentarios['foto'] == "") {
                          if ($valueComentarios['sexo'] == "h") {
                            $ruta_foto = $ruta_global . "vistas/assets/img/icon-usuario-1.svg";
                            echo "<div class='comentario-perfil' title='" . $valueComentarios['usuario'] . "'><img loading='lazy' class='comentario-img' src='" . $ruta_foto . "' alt='" . $valueComentarios['usuario'] . "'></div>";
                          } else {
                            $ruta_foto = $ruta_global . "vistas/assets/img/icon-usuario-5.svg";
                            echo "<div class='comentario-perfil' title='" . $valueComentarios['usuario'] . "'><img loading='lazy' class='comentario-img' src='" . $ruta_foto . "' alt='" . $valueComentarios['usuario'] . "'></div>";
                          }
                        } else {
                          $url_foto = $ruta_global . "vistas/assets/hangers/" . $valueComentarios["carpeta"] . "/" . $valueComentarios['foto'];
                    ?>
                          <div class="comentario-perfil" title="<?php echo $valueComentarios['usuario']; ?>">
                            <img loading='lazy' class="comentario-img" src="<?php echo $url_foto; ?>" alt="<?php echo $valueComentarios['usuario'] ?>">
                          </div>

                    <?php }
                      } else {
                        if ($valueComentarios['sexo'] == "h") {
                          $ruta_foto = $ruta_global . "vistas/assets/img/icon-usuario-1.svg";
                          echo "<div class='comentario-perfil' title='" . $valueComentarios['usuario'] . "'><img loading='lazy' class='comentario-img' src='" . $ruta_foto . "' alt='" . $valueComentarios['usuario'] . "'></div>";
                        } else {
                          $ruta_foto = $ruta_global . "vistas/assets/img/icon-usuario-5.svg";
                          echo "<div class='comentario-perfil' title='" . $valueComentarios['usuario'] . "'><img loading='lazy' class='comentario-img' src='" . $ruta_foto . "' alt='" . $valueComentarios['usuario'] . "'></div>";
                        }
                      }
                    } else {
                      echo "<div class='comentario-perfil' title='" . $valueComentarios['usuario'] . "'><img loading='lazy' class='comentario-img' src='" . $valueComentarios['foto'] . "' alt='" . $valueComentarios['usuario'] . "'></div>";
                    }
                    ?>

                    <div class='comentario-descripcion'>

                      <span class='comentario-nombre negritas'>

                      <?php
                        if($valueComentarios['usuario'] == "urbanhangers"){
                          echo $valueComentarios['usuario']." ✅"; 
                        }else{
                          echo $valueComentarios['usuario'];
                        }

                        // echo $valueComentarios['usuario'];
                      ?> 

                      </span>

                      <br />

                      <span class='comentario-texto'><?php echo $valueComentarios['comentario']; ?></span>

                      <br />

                      <?php
                      $mes = array("Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Agos", "Sept", "Oct", "Nov", "Dic");
                      $mes_comentario = $mes[date('n', strtotime($valueComentarios['fecha'])) - 1];
                      $dia = date("d", strtotime($valueComentarios['fecha']));
                      $año = date("Y", strtotime($valueComentarios['fecha']));
                      $hora = date("g:i A", strtotime($valueComentarios['fecha']));
                      $fecha_hora =  $dia . "-" . $mes_comentario . "-" . $año . " " . $hora;
                      ?>

                      <span class='comentario-tiempo'><?php echo $fecha_hora; ?></span>

                    </div>

                  </div>

              <?php

                }
              }

              ?>

            </div>

            <div class="comentar_agregar">
              <?php
              date_default_timezone_set('America/Mexico_City');
              $date = date('Y-m-d h:i:s', time());
              ?>

              <label class="comentario-label">Agregar comentario</label>
              <input type="text" maxlength="140" id="input_comentario<?php echo $idDiseno; ?>" class="input-comentario" diseno="<?php echo $idDiseno; ?>" usuario="<?php echo base64_encode($idUsuario); ?>" fecha="<?php echo $date; ?>" flag="<?php echo ($_SESSION['iniciarSesion'] == 'ok') ? '1' : '0'; ?>" />


              <?php if (isset($_SESSION["iniciarSesion"]) && $_SESSION["iniciarSesion"] == "ok") { ?>
                <div class="div-button-enviar" id="div-button-enviar" diseno="<?php echo $idDiseno; ?>" usuario="<?php echo base64_encode($idUsuario); ?>" fecha="<?php echo $date; ?>">
                  <span class="label-button">Comentar</span>
                  <img loading='lazy' class="comentario-button" src="<?php echo $ruta_global; ?>vistas/assets/img/icon-enviar-c.svg" />
                </div>
              <?php } else { ?>
                <div class="div-button-enviar" onclick="inicio_sesion()">
                  <span class="label-button">Comentar</span>
                  <img loading='lazy' class="comentario-button" src="<?php echo $ruta_global; ?>vistas/assets/img/icon-enviar-c.svg" />
                </div>
              <?php } ?>
            </div>

          </div>

        </div>

      </div>
    </div>
  </div>

<?php

}

if ($contador_disenos == 0) {
?>
  <div class="contenedor_index2 h70vh">
    <div class="cont_no_resultados_home">
      <img loading='lazy' class="img_not_found" src="<?php echo $ruta_global . 'vistas/assets/img/not-found-gris.svg'; ?>">
      <p class="msj_no_resultados_home" style="font-size: 24px;">No se encontraron resultados.</p>
    </div>
  </div>
<?php
} else {
?>
  <div class='cont_grid oculto'>
    <?php

    foreach ($peticion as $key => $value) {

      $idDiseno = $value['id_diseno'];

    ?>
      <div class='card_grid'>


        <a href="<?php echo $ruta_global . $value['ruta_diseno'] ?>" title="<?php echo $value['nombre_diseno']; ?>" class="img_card_grid_a">

          <?php

          echo "<div class='img_card_grid' style='background-image: url(" . $ruta_global . "vistas/assets/hangers/" . $value['carpeta'] . "/" . $value['ruta_img'] . ")'>";

          // echo "<img id='img_grid' src='".$ruta_global."vistas/assets/hangers/".$value['carpeta']."/".$value['ruta_img']."'>";

          echo "</div>";


          $votos = ControladorDisenos::consultaVotos('votos_diseno', $idDiseno);
          ?>
        </a>
        <div class='cont_datos_hanger_grid'>

          <div class="grid_data_card_lf">

            <div class='link_usuario_card btn_dashboard_usuario' title="<?php echo $value['ruta']; ?>" user='<?php echo $value["ruta"]; ?>'>

              <?php
              if ($value["modo_registro"] == "directo") {
                if ($value['foto'] == NULL || $value['foto'] == "") {
                  if ($value['sexo'] == "h") {
                    $ruta_foto = $ruta_global . "vistas/assets/img/icon-usuario-1.svg";
                    echo "<div class='img_perfil_grid_card' style='background-image: url(" . $ruta_foto . ")' title='" . $value["ruta"] . "'></div>";
                  } else {
                    $ruta_foto = $ruta_global . "vistas/assets/img/icon-usuario-5.svg";
                    echo "<div class='img_perfil_grid_card' style='background-image: url(" . $ruta_foto . ")' title='" . $value["ruta"] . "'></div>";
                  }
                } else {
                  $url_foto = $ruta_global . "vistas/assets/hangers/" . $value["carpeta"] . "/" . $value['foto'];
              ?>

                  <div class="img_perfil_grid_card" style="background-image: url(<?php echo $url_foto; ?>)" title="<?php echo $value["ruta"]; ?>">
                  </div>
                <?php }
              } else { ?>
                <div class="img_perfil_grid_card" style="background-image: url('<?php echo $value['foto']; ?>')" title="<?php echo $value["ruta"]; ?>">
                </div>
              <?php } ?>

              <span class='nombre-usuario-card' title="<?php echo $value["ruta"]; ?>"><?php echo $value["ruta"]; ?></span>

            </div>

          </div>

          <div class="grid_data_card_rh">

            <div id="conteo_grid<?php echo $idDiseno ?>" class='cont_votos_grid'>
              <?php

              $votos = ControladorDisenos::consultaVotos('votos_diseno', $idDiseno);

              if (isset($_SESSION["iniciarSesion"]) && $_SESSION["iniciarSesion"] == "ok") {
                $votado = ControladorDisenos::consultaVotado('votos_diseno', $idUsuario, $idDiseno);
                if ($votado[2] == $idUsuario && $votado[1] == 1) { ?>

                  <div id_grid='<?php echo  base64_encode($idDiseno); ?>' usuario_grid='<?php echo base64_encode($idUsuario); ?>' class='votado_grid evento_voto_grid identificador_<?php echo $idDiseno; ?>' status_grid='votado'></div>

                <?php
                } else {
                ?>

                  <div id_grid='<?php echo base64_encode($idDiseno); ?>' usuario_grid='<?php echo base64_encode($idUsuario); ?>' class='no-votado-grid evento_voto_grid identificador_<?php echo $idDiseno; ?>' status_grid='novotado'></div>

                <?php

                }
              } else {
                ?>

                <div id_grid='<?php echo base64_encode($idDiseno); ?>' class='no-votado-grid' onclick='inicio_sesion()'></div>

              <?php

              }

              ?>
              <p id="numero_votos_grid<?php echo $idDiseno; ?>" numero_de_votos_grid='<?php echo $votos[0]; ?>' class='conteo_grid'>

                <?php if ($votos[0] == 0){ echo "SIN <br /> VOTOS"; } ?>
                <?php if ($votos[0] == 1){ echo $votos[0] . " VOTO"; } ?>
                <?php if ($votos[0] > 1){ echo $votos[0] . " VOTOS"; } ?>

              </p>
            </div>

          </div>

        </div>
      </div>
    <?php
    }
    ?>
  </div>

  <!-- VERSION MOVIL HOME -->

  <section class="mov_cont_home desktop_no">

    <?php

    $peticion = ControladorDisenos::consultaDisenos('diseno', 'usuarios', 'img_diseno', $filtro, $limitante);

    $contador_disenos_mov = 0;

    foreach ($peticion as $key => $value) {
      $contador_disenos_mov++;
      $idDiseno = $value['id_diseno'];

    ?>


      <div class="card_movil animate__animated animate__fadeInLeft">
        <a href='<?php echo $ruta_global . $value['ruta_diseno']; ?>' title="<?php echo $value["nombre_diseno"]; ?>">
          <div class="card_cont_img_mov">

            <?php


            $id_imagen = $idDiseno;

            echo "<img loading='lazy' src='" . $ruta_global . "vistas/assets/hangers/" . $value["carpeta"] . "/" . $value['ruta_img'] . "' class='img_fit_movil' alt='" . $value["nombre_diseno"] . "'>";


            ?>

          </div>
        </a>

        <div class="card_cont_info_mov">

          <div class="card_cont_tit">

            <a href='<?php echo $ruta_global . $value['ruta_diseno']; ?>' title="<?php echo $value["nombre_diseno"] ?>">
              <h3 class="txt_tit_mov">"<?php echo $value["nombre_diseno"]; ?>"</h3>
            </a>
            <a href="<?php echo $ruta_global . $value['ruta']; ?>" title="<?php echo $value["ruta"] ?>"><span class="txt_info_mov"><?php echo $value['ruta']; ?></span></a>


          </div>

          <div class="card_cont_vot">

            <div id="sp_vot_movil_<?php echo $idDiseno; ?>" class="cont_votos">

              <!-- <div class="no-votado-mov"></div> -->

              <?php
              if (isset($_SESSION["iniciarSesion"]) && $_SESSION["iniciarSesion"] == "ok") {
                $idUsuario = $_SESSION['id_usuario'];
                $votado = ControladorDisenos::consultaVotado('votos_diseno', $idUsuario, $idDiseno);
                if ($votado[2] == $idUsuario && $votado[1] == 1) {
                  $clase_votado_movil = "votado-mov";
                  $status_votado_movil = "votado";
                } else {
                  $clase_votado_movil = "no-votado-mov";
                  $status_votado_movil = "novotado";
                }
              ?>
                <div id='<?php echo base64_encode($idDiseno); ?>' usuario='<?php echo base64_encode($idUsuario); ?>' class='<?php echo $clase_votado_movil; ?> identificador_corazon_<?php echo $idDiseno; ?> evento_voto_movil' file='1' status='<?php echo $status_votado_movil; ?>'></div>
              <?php
              } else {
              ?>
                <div class='no-votado-mov' onclick='inicio_sesion_movil()'></div>
              <?php
              }

              ?>

              <div class="cont_votos_movil">

                <?php

                $votos = ControladorDisenos::consultaVotos('votos_diseno', $idDiseno);
                if($votos[0]==1){
                  $texto_votos="VOTO";
                }else{
                  $texto_votos="VOTOS";
                }

                ?>

                <span id="num_votos_mov_<?php echo $idDiseno; ?>" numero_de_votos="<?php echo $votos[0]; ?>" class="txt_tit_mov ln_hg_mov_fx font_fx">
                  <?php echo $votos[0] > 0 ?  $votos[0] : "SIN" ?>
                </span>
                <span class="txt_tit_mov ln_hg_mov_fx"><?php echo $texto_votos; ?></span>

              </div>

            </div>

            <div class="cont_comp">

              <img loading='lazy' src="<?php echo $ruta_global; ?>vistas/assets/img/icon-share-c.svg" alt="compartir_movil" class="compartir_mov" onclick="menu_open_share('<?php echo $idDiseno; ?>')">
              <a href="<?php echo $ruta_global . $value['ruta_diseno']; ?>" style="margin-left: 15%;">
                <img loading='lazy' src="<?php echo $ruta_global; ?>vistas/assets/img/cmt_btn_orange.svg" alt="comentar_movil" class="compartir_mov">
              </a>

            </div>

          </div>

          <div id="share_menu_<?php echo $idDiseno; ?>" class="card_cont_info_share_out">
            <a id="wa_<?php echo $idDiseno; ?>" rel="noopener noreferrer" class="ev_meta_mov gen_share anim_<?php echo $idDiseno; ?>" href="whatsapp://send?text=https://urbanhangerscommunity.com/<?php echo $value["ruta_diseno"]; ?>" data-action="share/whatsapp/share" target="_blank"><img loading='lazy' class="icon-red" alt="Whatsapp" src="<?php echo $ruta_global; ?>vistas/assets/img/icon-whats-o.svg"></a>
            <a id="tw_<?php echo $idDiseno; ?>" rel="noopener noreferrer" class="ev_meta_mov gen_share anim_<?php echo $idDiseno; ?>" href="https://twitter.com/intent/tweet?text=¡Checa%20este%20diseño!&amp;url=https://urbanhangerscommunity.com/<?php echo $value["ruta_diseno"]; ?>" target="_blank"><img loading='lazy' class="icon-red" alt="Twitter" src="<?php echo $ruta_global; ?>vistas/assets/img/icon-twitter-o.svg"></a>
            <a id="fb_<?php echo $idDiseno; ?>" rel="noopener noreferrer" class="ev_meta_mov gen_share anim_<?php echo $idDiseno; ?>" href="fb://faceweb/f?href=https%3A%2F%2Fm.facebook.com/sharer.php?u=https://urbanhangerscommunity.com/<?php echo $value["ruta_diseno"]; ?>" target="_blank"><img class="icon-red" alt="Facebook" src="<?php echo $ruta_global; ?>vistas/assets/img/icon-face-o.svg"></a>
          </div>

          <div class="card_cont_info">

            <span class="txt_info_mov_bold">INSPIRACIÓN</span>

            <span class="txt_info_mov"><?php echo $value["inspiracion"]; ?></span>

          </div>

        </div>

      </div>


    <?php

    }
    ?>

  </section>





  <!-- FIN VERSION MOVIL HOME -->

  <div class='cont_paginador'>
    <ul class='pagination'>
      <?php

      $ruta_completa_pag = $ruta_global . $ruta_paginador . "/";



      if ($pagina_url > 1) {
      ?>
        <li class='waves-effect'><a href='<?php echo $ruta_completa_pag . $pagina_anterior; ?>'><i class='material-icons'>chevron_left</i></a></li>
        <li class='waves-effect'><a href='<?php echo $ruta_completa_pag . "1"; ?>'><?php echo "1"; ?></a></li>
        <?php
        if ($pagina_anterior != 1) {
        ?>
          <li class='waves-effect'><i class='material-icons'>more_horiz</i></li>
          <li class='waves-effect'><a href='<?php echo $ruta_completa_pag . $pagina_anterior; ?>'><?php echo $pagina_anterior; ?></a></li>
      <?php
        }
      }
      ?>
      <li class='active waves-effect'><a href='<?php echo $ruta_completa_pag . $pagina_url; ?>'><?php echo $pagina_url; ?></a></li>
      <?php

      if ($pagina_url < $total_num_hojas) {
        if ($pagina_siguiente != $total_num_hojas) {
      ?>
          <li class='waves-effect'><a href='<?php echo $ruta_completa_pag . $pagina_siguiente; ?>'><?php echo $pagina_siguiente; ?></a></li>
          <li class='waves-effect'><i class='material-icons'>more_horiz</i></li>
        <?php
        }
        ?>
        <li class='waves-effect'><a href='<?php echo $ruta_completa_pag . $total_num_hojas; ?>'><?php echo $total_num_hojas; ?></a></li>
        <li class='waves-effect'><a href='<?php echo $ruta_completa_pag . $pagina_siguiente; ?>'><i class='material-icons'>chevron_right</i></a></li>
      <?php
      }
      ?>
    </ul>
  </div>
<?php
}

?>
<!-- MODAL DE IMAGENES -->
<div id="modal_imagen" class="modal modal_imagenes h600">
  <div class="cent_img">
    <img loading='lazy' src="" alt="imgview" class="imagen_full_view">
  </div>
</div>


<!-- IMAGENES MODAL FIN -->
<script>
  var ruta_global = "<?php echo $ruta_global; ?>";
  <?php
  if (isset($_SESSION['iniciarSesion']) && $_SESSION['iniciarSesion'] == 'ok') {
  ?>
    var sesion_modal = 'ok';
    var id_usuario_modal = '<?php echo $_SESSION['id_usuario']; ?>';
  <?php
  } else {
  ?>
    var sesion_modal = 'no';
    var id_usuario_modal = 'no';
  <?php
  }
  ?>

  $(document).on("change", ".custom-select", function() {
    switch ('<?php echo $texto_filtro; ?>') {
      case 'NUEVOS':
        $(".select-items").children().eq(0).addClass("activo");
        break;

      case 'TENDENCIA':
        $(".select-items").children().eq(2).addClass("activo");
        break;

      case 'MÁS VOTADOS':
        $(".select-items").children().eq(1).addClass("activo");
        break;
      default:
    }

    $('#encabezado_filtro_movil #filtro_home_movil .select-selected').html('<?php echo $texto_filtro; ?>');
    $(" #encabezado_filtro .custom-select .select-items").children().eq(0).addClass("nuevos");
    $(" #encabezado_filtro .custom-select .select-items").children().eq(3).addClass("colecciones");

    $(" #encabezado_filtro_movil .custom-select .select-items").children().eq(0).addClass("nuevos");
    $(" #encabezado_filtro_movil .custom-select .select-items").children().eq(3).addClass("colecciones");
  });
</script>
<script type="text/javascript" src="<?php echo $ruta_global; ?>vistas/assets/js/script_home.min.js?v=<?php echo time(); ?>"></script>