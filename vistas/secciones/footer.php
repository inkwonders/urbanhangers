<?php
  $ruta_global = Rutas::ruta_servidor();
?>
<footer id="contactanos_urban">
  <div class="div-celda-footer">
    <img class="logo-footer" src="<?php echo $ruta_global; ?>vistas/assets/img/logo-w.svg" alt="UrbanHangers"/>
    <div class="data-footer">
      <span class="texto-footer">Aviso de privacidad © <?php echo date('Y'); ?></span>
      <a href="https://inkwonders.com" rel="noopener noreferrer" target="_blank" class="enlace-footer" title="INKWONDERS">Desarrollado por INKWONDERS</a>
    </div>
  </div>
  <div class="div-celda-footer_form padding-footer" style="text-align:left">
    <h2 class="contacto">CONTÁCTANOS</h2>
    <div id="contacto-resp" class="cont_fx_form">
      <form id="form_contacto" method="POST">
      <div class="input-field col s12">
        <input type="text" class="input-contacto" name="txtNombre" id="txtNombre"/>
        <label for="txtNombre" class="label-contacto">NOMBRE</label>
        <span class="helper-text" data-error="wrong" data-success="right" id="msgNombre"></span>
      </div>
      <div class="input-field col s12">
        <input type="email" class="input-contacto" name="txtCorreo" id="txtCorreo"/>
        <label for="txtCorreo" class="label-contacto">CORREO</label>
        <span class="helper-text" data-error="wrong" data-success="right" id="msgCorreo"></span>
      </div>
      <div class="input-field col s12">
        <textarea class="input-contacto input-text-area" name="txtMensaje" id="txtMensaje"></textarea>
        <label for="txtMensaje" class="label-contacto">MENSAJE</label>
        <span class="helper-text" data-error="wrong" data-success="right" id="msgMensaje"></span>
      </div>
      <div class="div-contacto">
        <input type="submit" value="ENVIAR" class="button-contacto" id="btnEnviar"/>
        <button type="submit" name="" value=" " class="inpt_movil" id="btnEnviarmov"> <img src="<?php echo $ruta_global; ?>vistas/assets/img/icon-enviar-blanco.svg" alt="env_movil"> </button>
      </div>
    </form>
    </div>
    <div class="fx_cont_img">
      <div class="logo_footer_mov">
        <a href="https://es-la.facebook.com/urbanhangersmx/" rel="noopener noreferrer" target="_blank"><img title='Facebook' class="icon-footer" src="<?php echo $ruta_global; ?>vistas/assets/img/icon-facebook-w.svg" /></a>
        <a href="https://www.instagram.com/urbanhangersmx/?hl=es" rel="noopener noreferrer" target="_blank"><img title='Instagram' class="icon-footer" src="<?php echo $ruta_global; ?>vistas/assets/img/icon-insta-w.svg" /></a>
      </div>
    <div class="info_foot_movil">
        <span class="texto-footer">Aviso de privacidad © <?php echo date('Y'); ?></span>
        <a href="https://inkwonders.com" rel="noopener noreferrer" target="_blank" class="texto-footer" title="INKWONDERS">Desarrollado por INKWONDERS</a>
      </div>

    </div>
  </div>
</footer>
