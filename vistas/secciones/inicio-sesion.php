<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />

  <title>BIENVENIDO | URBAN HANGERS®</title>
  <!-- Favicon -->
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $ruta_global; ?>vistas/assets/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $ruta_global; ?>vistas/assets/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $ruta_global; ?>vistas/assets/favicon/favicon-16x16.png">
  <link rel="manifest" href="<?php echo $ruta_global; ?>vistas/assets/favicon/site.webmanifest">
  <link rel="mask-icon" href="<?php echo $ruta_global; ?>vistas/assets/favicon/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">
  <!-- Favicon -->
  <link rel="stylesheet" type="text/css" href="<?php echo $ruta_global; ?>vistas/assets/css/estilo.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $ruta_global; ?>vistas/assets/css/custom-select.css" />
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="<?php echo $ruta_global; ?>vistas/assets/css/materialize.min.css" media="screen,projection" />

</head>

<body>

  <script type='text/javascript'>
    window.__lo_site_id = 312183;
    (function() {
      var wa = document.createElement('script');
      wa.type = 'text/javascript';
      wa.async = true;
      wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(wa, s);
    })();
  </script>

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-159600964-2"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());
    gtag('config', 'UA-159600964-2');
  </script>

  <div class="contenedor-log-in">
    <div id="movil_animated" class="div-log-cell padding-log animate__animated animate__slow none_effect">
      <form id="form_login" method="post">
        <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal margin-log" /><br />
        <span class="titulo-modal">BIENVENIDO</span><br /><br />
        <div id="div_respuesta_login"></div>
        <div class="input-field col s12 margin-inicio">
          <label class="label-inicio">CORREO</label>
          <input type="email" class="input-inicio" id="txtUsuario" name="email_login" />
        </div>
        <?php $ruta = explode("/", $_GET["pagina"]); ?>
        <input type="hidden" name="ruta" value="<?php echo $ruta[0]; ?>">
        <input id="tipo_usuario" type="hidden" name="tipo_usuario" value="2">
        <div class="input-field col s12 margin-inicio">
          <img src="<?php echo $ruta_global; ?>vistas/assets/img/icon-mostrar.svg" class="mostrar-pass" id="mostrar_pass" onclick="mostrar_pass_po(1)" title="Mostrar contraseña" />
          <label class="label-inicio">CONTRASEÑA</label>
          <input type="password" class="input-inicio" name="pass_login" id="txtPassword" />
        </div>
        <div class="div-inicio-modal" style="padding: 0px 15px">
          <p>
            <label>
              <input type="checkbox" value="" class="" />
              <span>Recuérdame</span>
            </label>
          </p>
          <a class="enlace-pass" onclick="reestablecer_contrasena()">¿Olvidaste tu contraseña?</a>
        </div>
        <input type="submit" class="button-inicio margin-log inicio-sesion-aparte" value="INICIAR SESIÓN" /><br />
        <div class="fb-login-button" data-size="large" data-button-type="continue_with" data-layout="rounded" data-auto-logout-link="false" data-use-continue-as="true" data-width="" scope="public_profile,email" onlogin="checkLoginState();"></div>
        <div class="inicio-footer"></div><br />
        <span class="texto-modal">¿Aún no tienes cuenta?</span><br />
        <a href="registro" class="unete">¡Únete!</a> <br>
        <a href="<?php echo $ruta_global; ?>home" class="regresar desktop_no"><img src="<?php echo $ruta_global; ?>vistas/assets/img/flecha-atras-icon.svg" alt="regresar" class="regrer_img"></a>
      </form>
    </div>
    <div class="div-log-cell background-log"></div>
  </div>

  <div id="reestablecer_contrasena" class="modal modal-inicio">
    <form id="form_reestablecer" method="post">
      <div class="modal-content modal-padding">
        <span class="close-modal-inicio" onclick="cerrar_modal_reestablecer_contrasena()">X</span>
        <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal" /><br />
        <span class="titulo-modal">¿Olvidaste tu contraseña?</span>
        <div id="div_respuesta_login"></div>
        <div class="input-field col s12 margin-inicio input_correo_login">
          <input type="email" class="input-inicio" id="correoRestablecer" name="correoRestablecer" required />
          <label class="label-inicio">CORREO</label>
        </div>

        <div id="cont_botones_inicio">
          <button type="submit" class="button-inicio" id="btn_olvidaste_contrasena" /> Restablecer contraseña </button> <br>
        </div>

        <div id="respuesta_contrasena_olvidada">

        </div>

      </div>
    </form>
  </div>

  <script type="text/javascript">
    var ruta_global = "<?php echo $ruta_global; ?>";
  </script>
  <script type="text/javascript" src="<?php echo $ruta_global; ?>vistas/assets/js/script_inicio-sesion.min.js?v=<?php echo time(); ?>"></script>
</body>

</html>