<?php

  $ruta = $rutas[0];

  $rutaDis = ControladorDisenos::mostrarProducto('diseno', $item, $valor);
  $idDiseno = $rutaDis["id_diseno"];

  $usuario = ControladorUsuarios::consultaUsuarioProducto('usuarios', 'diseno', $idDiseno);
  echo "<script> $('title').html('".mb_strtoupper($rutaDis['nombre_diseno'])." | URBAN HANGERS®'); </script>";
?>

<script type='text/javascript'>
  window.__lo_site_id = 312183;
  (function() {
  var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
  wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
  })();
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-159600964-2"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-159600964-2');
</script>

<div class="ln_or"></div>
    <div id="contenedor_diseno_unico" class="contenedor">
        <div class="contenedor_disenos" id="contenedor_disenos_unicos">

          <div class="contenedor_datos_usuario">

              <div class="cont_interno_nombre_hanger pointer btn_dashboard_usuario"  key="<?php echo $usuario["id_usuario"]; ?>" user="<?php echo $usuario["ruta"]; ?>" title="<?php echo $usuario["ruta"];?>">
                <?php
                if(!empty($usuario['foto'])){
                  if($usuario['modo_registro'] == "directo"){
                    $url_foto = $ruta_global."vistas/assets/hangers/".$usuario['carpeta']."/".$usuario['foto'];
                  }else {
                    $url_foto = $usuario['foto'];
                  }
                }else {
                  if($usuario['sexo'] == "h"){
                    $url_foto = $ruta_global."vistas/assets/img/icon-usuario-1.svg";
                  }else {
                    $url_foto = $ruta_global."vistas/assets/img/icon-usuario-5.svg";
                  }
                }
                ?>
                <div class="img_perfil" style="background-image: url('<?php echo $url_foto;?>')" alt="<?php echo $usuario["ruta"];?>">
                </div>
                <span class="nombre-usuario" title="<?php echo $usuario["ruta"];?>">
                  <?php echo $usuario['usuario']; ?>
                </span>
              </div>

            <div class="descripcion-prenda">
              <input id="ruta_<?php echo $idDiseno; ?>" type="hidden" href='<?php echo $ruta_global.$rutaDis['ruta']; ?>'>
              <h3 id="nombre_<?php echo $idDiseno; ?>" class="nombre-prenda nombre-prenda-diseno">"<?php echo $rutaDis["nombre_diseno"];?>"</h3>

              <ul class="collapsible">
                <li class="active">
                  <div class="inspiracion_diseno collapsible-header">INSPIRACIÓN</div>
                  <div class="collapsible-body">
                    <span align="left" class="inspiracion_descripcion" id="desc_<?php echo $idDiseno; ?>"><?php echo $rutaDis["inspiracion"];?></span>
                  </div>
                </li>
              </ul>

            </div>

          </div>

          <div class="contenedor_imagen_disenos" title="<?php echo $rutaDis["nombre_diseno"]; ?>">

               <?php


                    $id_imagen = $idDiseno;
                    echo "<img title='".$rutaDis["nombre_diseno"]."' alt='".$rutaDis["nombre_diseno"]."' id='$id_imagen' ruta='".$ruta_global."vistas/assets/hangers/".$usuario["carpeta"]."/".$rutaDis['ruta_img']."' src='".$ruta_global."vistas/assets/hangers/".$usuario["carpeta"]."/".$rutaDis['ruta_img']."'>";


                ?>
          </div>

          <div class="contenedor_votos_comentarios">
            <div id="conteo_votos<?php echo $idDiseno; ?>" class="contenedor_votos_corazon">
            <?php
              $votos = ControladorDisenos::consultaVotos('votos_diseno', $idDiseno);
              if(isset($_SESSION["iniciarSesion"]) && $_SESSION["iniciarSesion"] == "ok"){
                $idUsuario = $_SESSION['id_usuario'];
                $votado = ControladorDisenos::consultaVotado('votos_diseno', $idUsuario, $idDiseno);
                if($votado[2] == $idUsuario && $votado[1] == 1){ ?>
                  <div id='<?php echo base64_encode($idDiseno); ?>' usuario='<?php echo base64_encode($idUsuario); ?>'  class='votado evento_voto  identificador_corazon_<?php echo $idDiseno; ?>' status='votado'></div>
                <?php
                }else{
                  ?>
                  <div id='<?php echo base64_encode($idDiseno); ?>' usuario='<?php echo base64_encode($idUsuario); ?>' class='no-votado evento_voto  identificador_corazon_<?php echo $idDiseno; ?>' status='novotado'></div>
                  <?php
               }
             }else{
               ?>
               <div id='<?php echo $idDiseno; ?>' class='no-votado' onclick='inicio_sesion()'></div>
               <?php
              }
            ?>
            <p id="numero_votos<?php echo $idDiseno; ?>" numero_de_votos="<?php echo $votos[0]?>" class="conteo"><?php echo $votos[0] > 0 ?  $votos[0]."<br /> VOTOS": "SIN <br /> VOTOS" ?></p>
          </div>

            <?php
              $ruta_wa = "https://web.whatsapp.com/send?text=https://urbanhangerscommunity.com/".$rutaDiseno;
              $ruta_tw = "https://twitter.com/intent/tweet?text=¡Checa%20este%20diseño!&url=https://urbanhangerscommunity.com/".$rutaDiseno;
              $ruta_fb = "http://www.facebook.com/sharer.php?u=https://urbanhangerscommunity.com/".$rutaDiseno;
            ?>
            <div class="comentarios">
              <div class="compartelo"><p><b>COMPÁRTELO</b></p></div>

              <div class="redes redes_escritorio">
                <a id="wa_<?php echo $idDiseno; ?>" class="ev_meta" dis="<?php echo $idDiseno; ?>" href="<?php echo $ruta_wa; ?>" data-action="share/whatsapp/share" target="_blank"><img class="icon-red" src="<?php echo $ruta_global; ?>vistas/assets/img/icon-whats-o.svg" /></a>
                <a id="tw_<?php echo $idDiseno; ?>" class="ev_meta" dis="<?php echo $idDiseno; ?>" href="<?php echo $ruta_tw; ?>" target="_blank"><img class="icon-red" src="<?php echo $ruta_global; ?>vistas/assets/img/icon-twitter-o.svg" /></a>
                <a id="fb_<?php echo $idDiseno; ?>" class="ev_meta" dis="<?php echo $idDiseno; ?>" href="<?php echo $ruta_fb; ?>" target="_blank"><img class="icon-red" src="<?php echo $ruta_global; ?>vistas/assets/img/icon-face-o.svg"  /></a>
              </div>

              <div id="comentarios<?php echo $idDiseno; ?>" class="comentarios-scroll">

                <?php

                    $comentarios = ControladorDisenos::consultaComentarios('usuarios', $idDiseno);

                    if(empty($comentarios)){
                      echo "<div class='sin_comentarios'><span class='comentario-nombre'>Sin comentarios</span></div>";
                    }else{

                      foreach($comentarios as $key => $valueComentarios){

                ?>

                <div class='agregar_comentario'>

                  <div class='comentario-perfil'>
                    <?php
                    if($valueComentarios['tipo_usuario'] == 2){
                      if($valueComentarios['sexo'] == "h"){
                        echo '<img class="comentario-img" src="'.$ruta_global.'vistas/assets/img/icon-usuario-1.svg" alt="'.$valueComentarios['usuario'].'" title="'.$valueComentarios['usuario'].'">';
                      }else {
                        echo '<img class="comentario-img" src="'.$ruta_global.'vistas/assets/img/icon-usuario-5.svg" alt="'.$valueComentarios['usuario'].'" title="'.$valueComentarios['usuario'].'" algo3="">';
                      }
                    }else if($valueComentarios['tipo_usuario'] == 1){
                      if($valueComentarios['modo_registro'] == "facebook"){
                        echo '<img class="comentario-img" src="'.$valueComentarios['foto'].'" alt="'.$valueComentarios['usuario'].'" title="'.$valueComentarios['usuario'].'">';
                      }else {
                        if(!empty($valueComentarios['foto'])){
                          echo '<img class="comentario-img" src="'.$ruta_global.'vistas/assets/hangers/'.$valueComentarios['carpeta'].'/'.$valueComentarios['foto'].'" alt="'.$valueComentarios['usuario'].'" title="'.$valueComentarios['usuario'].'">';
                        }else {
                          if($valueComentarios['sexo'] == "h"){
                            echo '<img class="comentario-img" src="'.$ruta_global.'vistas/assets/img/icon-usuario-1.svg" alt="'.$valueComentarios['usuario'].'" title="'.$valueComentarios['usuario'].'" algo1="">';
                          }else {
                            echo '<img class="comentario-img" src="'.$ruta_global.'vistas/assets/img/icon-usuario-5.svg" alt="'.$valueComentarios['usuario'].'" title="'.$valueComentarios['usuario'].'" algo2=""> ';
                          }
                        }
                      }
                    }else{

                    }
                    ?>
                  </div>

                  <div class='comentario_descripcion'>

                    <span class='comentario-nombre negritas'>

                    <?php
                      if($valueComentarios['usuario'] == "urbanhangers"){
                        echo $valueComentarios['usuario']." ✅"; 
                      }else{
                        echo $valueComentarios['usuario'];
                      }

                      // echo $valueComentarios['usuario'];
                    ?>    

                    </span>

                    <br />

                    <span class='comentario-texto'><?php echo $valueComentarios['comentario']; ?></span>

                    <br />
                    <?php
                    $mes = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Agos","Sept","Oct","Nov","Dic");
                    $mes_comentario = $mes[date('n', strtotime($valueComentarios['fecha']))-1];
                    $dia = date("d", strtotime($valueComentarios['fecha']));
                    $año = date("Y", strtotime($valueComentarios['fecha']));
                    $hora = date("g:i A",strtotime($valueComentarios['fecha']));
                    $fecha_hora =  $dia."-".$mes_comentario."-".$año." ".$hora;
                    ?>
                    <span class='comentario-tiempo'><?php echo $fecha_hora;?></span>

                  </div>

                </div>

              <?php

                      }

                    }

              ?>

              </div>

              <div class="comentar_agregar">
                <?php
                  date_default_timezone_set('America/Mexico_City');
                  $date = date('Y-m-d h:i:s', time());
                ?>
                <label class="comentario-label-diseno">Agregar comentario</label>
                <input type="text" id="input_comentario<?php echo $idDiseno; ?>" class="input-comentario-diseno"  diseno="<?php echo $idDiseno; ?>" usuario="<?php echo base64_encode($idUsuario); ?>" fecha="<?php echo $date; ?>" flag="<?php echo ($_SESSION['iniciarSesion'] == 'ok') ? '1' : '0'; ?>" />
                  <?php  if(isset($_SESSION["iniciarSesion"]) && $_SESSION["iniciarSesion"] == "ok"){ ?>
                  <div class="div-button-enviar" diseno="<?php echo $idDiseno; ?>" usuario="<?php echo base64_encode($idUsuario); ?>" fecha="<?php echo $date;?>">
                  <span class="label-button">Comentar</span>
                  <img class="comentario-button" src="<?php echo $ruta_global; ?>vistas/assets/img/icon-enviar-c.svg" />
                  </div>
                  <?php }else{ ?>
                    <div class="div-button-enviar" onclick="inicio_sesion()">
                  <span class="label-button">Comentar</span>
                  <img class="comentario-button" src="<?php echo $ruta_global; ?>vistas/assets/img/icon-enviar-c.svg" />
                  </div>
                  <?php } ?>
                </div>

            </div>

          </div>

        </div>
      </div>


      <section id="id_dis_unico" class="mov_dis_unico desktop_no animate__animated animate__fadeInLeft">

        <section class="img_info_cont_dis_mov">

          <div class="img_dis_cont">

            <img alt="<?php echo $rutaDis["nombre_diseno"];?>" src="<?php echo $ruta_global."vistas/assets/hangers/".$usuario["carpeta"]."/".$rutaDis['ruta_img']; ?>" class="img_dis_unico" title="<?php echo $rutaDis["nombre_diseno"];?>">

          </div>

          <div class="tit_cont_mov_dis" >

            <span class="txt_tit_mov" title="<?php echo $rutaDis["nombre_diseno"];?>">"<?php echo $rutaDis['nombre_diseno']; ?>"</span>


          </div>
          <a href="<?php echo $ruta_global.$usuario['ruta']; ?>" title="<?php echo $usuario["ruta"]?>"><span class="txt_info_mov"><?php echo $usuario['ruta']; ?></span></a>
        </section>



        <section class="sect_cont_txt">

          <div class="cont_txt_prin">

            <div class="txt_tit_cont">

              <span class="txt_info_mov_bold">INSPIRACIÓN</span>

            </div>

            <div id="sp_vot_movil_<?php echo $idDiseno; ?>" class="voto_dis_cont">

                <div class="cont_votos_movil_dist">

                  <span id="num_votos_mov_<?php echo $idDiseno; ?>" numero_de_votos="<?php echo $votos[0]; ?>" class="txt_tit_mov ln_hg_mov_fx font_fx"><?php echo $votos[0] > 0 ?  $votos[0] : "SIN" ?></span>
                  <span class="txt_tit_mov ln_hg_mov_fx">VOTOS</span>

                </div>

                <?php
                  if(isset($_SESSION["iniciarSesion"]) && $_SESSION["iniciarSesion"] == "ok"){

                    if($votado[2] == $idUsuario && $votado[1] == 1){
                      $clase_votado_movil = "votado-mov";
                      $status_votado_movil = "votado";
                    }else {
                      $clase_votado_movil = "no-votado-mov";
                      $status_votado_movil = "novotado";
                    }
                    ?>
                    <div id='<?php echo base64_encode($idDiseno); ?>' usuario='<?php echo base64_encode($idUsuario); ?>' class='<?php echo $clase_votado_movil; ?> identificador_corazon_<?php echo $idDiseno; ?> evento_voto_movil' file='2' status='<?php echo $status_votado_movil; ?>'></div>
                    <?php
                 }else{
                   ?>
                   <div id='<?php echo base64_encode($idDiseno); ?>' class='no-votado-mov' onclick='inicio_sesion_movil()'></div>
                   <?php
                  }

                ?>

            </div>

          </div>

          <div class="hr_ln_bl"></div>

          <div class="txt_cont_dis">

            <span class="txt_info_mov"><?php echo $rutaDis["inspiracion"];?></span>

          </div>

        </section>




        <section>


          <div class="comp_cont">

            <span class="txt_bold_comp_mov"><b>COMPÁRTELO</b></span>

          </div>

          <div class="icon_comp_cont">

            <div class="redes redes_movil">
              <a id="wa_<?php echo $idDiseno; ?>" class="ev_meta" dis="<?php echo $idDiseno; ?>" href="whatsapp://send?text=https://urbanhangerscommunity.com/<?php echo $rutaDis["ruta"]; ?>" data-action="share/whatsapp/share" target="_blank"><img class="icon-red" src="<?php echo $ruta_global; ?>vistas/assets/img/icon-whats-o.svg" /></a>
              <a id="tw_<?php echo $idDiseno; ?>" class="ev_meta" dis="<?php echo $idDiseno; ?>" href="<?php echo $ruta_tw; ?>" target="_blank"><img class="icon-red" src="<?php echo $ruta_global; ?>vistas/assets/img/icon-twitter-o.svg" /></a>
              <a id="fb_<?php echo $idDiseno; ?>" class="ev_meta" dis="<?php echo $idDiseno; ?>" href="fb://faceweb/f?href=https%3A%2F%2Fm.facebook.com/sharer.php?u=https://urbanhangerscommunity.com/<?php echo $rutaDis["ruta"]; ?>" target="_blank"><img class="icon-red" src="<?php echo $ruta_global; ?>vistas/assets/img/icon-face-o.svg"  /></a>
            </div>
          </div>

          <div id="comentarios_mov_<?php echo $idDiseno; ?>" class="comentarios-scroll">

            <?php
            $comentarios = ControladorDisenos::consultaComentarios('usuarios', $idDiseno);
            if(empty($comentarios)){
              echo "<div class='sin_comentarios'><span class='comentario-nombre'>Sin comentarios</span></div>";
            }else{
              foreach($comentarios as $key => $valueComentarios){
              ?>
                <div class="agregar_comentario">

                  <div class="comentario-perfil">
                    <!-- <img class="comentario-img" src="<?php echo $ruta_global; ?>vistas/assets/hangers/usuario_6_2020-12-17_5fdb97a60b444/perfil_usuario_6_2020-12-17_5fdb8737cc9b9.jpg"> -->
                    <?php
                    if($valueComentarios['tipo_usuario'] == 2){
                      if($valueComentarios['sexo'] == "h"){
                        echo "<!-- e1-->";
                        echo '<img class="comentario-img" src="'.$ruta_global.'vistas/assets/img/icon-usuario-1.svg" alt="'.$valueComentarios['usuario'].'" title="'.$valueComentarios['usuario'].'">';
                      }else {
                        echo "<!-- e2-->";
                        echo '<img class="comentario-img" src="'.$ruta_global.'vistas/assets/img/icon-usuario-5.svg" alt="'.$valueComentarios['usuario'].'" title="'.$valueComentarios['usuario'].'">';
                      }
                    }else if($valueComentarios['tipo_usuario'] == 1){
                      if($valueComentarios['modo_registro'] == "facebook"){
                        echo "<!-- e3-->";
                        echo '<img class="comentario-img" src="'.$valueComentarios['foto'].'" alt="'.$valueComentarios['usuario'].'" title="'.$valueComentarios['usuario'].'">';
                      }else {
                        if(!empty($valueComentarios['foto'])){
                          echo "<!-- e4-->";
                          echo '<img class="comentario-img" src="'.$ruta_global.'vistas/assets/hangers/'.$valueComentarios['carpeta'].'/'.$valueComentarios['foto'].'" alt="'.$valueComentarios['usuario'].'" title="'.$valueComentarios['usuario'].'">';
                        }else {
                          if($valueComentarios['sexo'] == "h"){
                            echo "<!-- e5-->";
                            echo '<img class="comentario-img" src="'.$ruta_global.'vistas/assets/img/icon-usuario-1.svg" alt="'.$valueComentarios['usuario'].'" title="'.$valueComentarios['usuario'].'">';
                          }else {
                            echo "<!-- e6-->";
                            echo '<img class="comentario-img" src="'.$ruta_global.'vistas/assets/img/icon-usuario-5.svg" alt="'.$valueComentarios['usuario'].'" title="'.$valueComentarios['usuario'].'">';
                          }
                        }
                      }
                    }
                    ?>
                  </div>

                  <div class="comentario-descripcion">

                    <div class="cmt_mov_desc">
                      <span class="comentario-nombre-mov negritas">
                        <?php
                          if($valueComentarios['usuario'] == "urbanhangers"){
                            echo $valueComentarios['usuario']." ✅"; 
                          }else{
                            echo $valueComentarios['usuario'];
                          }

                          // echo $valueComentarios['usuario'];
                        ?>
                      </span>
                      <span class="txt_mov_chat"><?php echo $valueComentarios['comentario']; ?></span>
                      <?php
                      $mes = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Agos","Sept","Oct","Nov","Dic");
                      $mes_comentario = $mes[date('n', strtotime($valueComentarios['fecha']))-1];
                      $dia = date("d", strtotime($valueComentarios['fecha']));
                      $anio = date("Y", strtotime($valueComentarios['fecha']));
                      $hora = date("g:i A",strtotime($valueComentarios['fecha']));
                      $fecha_hora =  $dia."-".$mes_comentario."-".$anio." ".$hora;
                      ?>
                      <span class="comentario-tiempo"><?php echo $fecha_hora;?></span>

                    </div>

                  </div>

                </div>
                <?php
              }
            }
            ?>
          </div>


          <div class="comentar_agregar">

              <label class="comentario-label-diseno-mov">Agregar comentario</label>
              <input type="text" maxlength="140" id="input_comentario_<?php echo $idDiseno; ?>" class="es_input_comentario_diseno comen_enter_mov" file="2" diseno="<?php echo $idDiseno; ?>" usuario="<?php echo base64_encode($idUsuario); ?>" fecha="<?php echo $date; ?>" flag="<?php echo ($_SESSION['iniciarSesion'] == 'ok') ? '1' : '0'; ?>" >
              <?php
                if(isset($_SESSION["iniciarSesion"]) && $_SESSION["iniciarSesion"] == "ok"){
                  ?>
                  <div class="es_div_button_enviar evento_btn_comen_movil" file="2" diseno="<?php echo $idDiseno; ?>" usuario="<?php echo base64_encode($idUsuario); ?>" fecha="<?php echo $date; ?>">
                      <span class="label-button-mov">Comentar</span>
                      <img class="comentario-button" src="<?php echo $ruta_global; ?>vistas/assets/img/icon-enviar-c.svg">
                  </div>
                  <?php
                }else {
                  ?>
                  <div class="es_div_button_enviar evento_btn_comen_movil" onclick="inicio_sesion_movil()">
                      <span class="label-button-mov">Comentar</span>
                      <img class="comentario-button" src="<?php echo $ruta_global; ?>vistas/assets/img/icon-enviar-c.svg">
                  </div>
                  <?php
                }
              ?>
          </div>



        </section>



      </section>
