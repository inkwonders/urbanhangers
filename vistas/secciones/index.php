<?php include('cn.php'); ?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />

  <title>URBAN HANGERS®</title>
  <!-- Favicon -->
  <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
  <link rel="manifest" href="favicon/site.webmanifest">
  <link rel="mask-icon" href="favicon/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">
  <!-- Favicon -->
  <link rel="stylesheet" type="text/css" href="estilo.css" />
  <link rel="stylesheet" type="text/css" href="css/custom-select.css" />
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />
  <script>
    function statusChangeCallback(response) { // Called with the results from FB.getLoginStatus().
      console.log('statusChangeCallback');
      console.log(response); // The current login status of the person.
      if (response.status === 'connected') { // Logged into your webpage and Facebook.
        testAPI();
      } else { // Not logged into your webpage or we are unable to tell.
        document.getElementById('status').innerHTML = 'Please log ' +
          'into this webpage.';
      }
    }

    function checkLoginState() { // Called when a person is finished with the Login Button.
      FB.getLoginStatus(function(response) { // See the onlogin handler
        statusChangeCallback(response);
      });
    }

    window.fbAsyncInit = function() {
      FB.init({
        appId: '1298330253840627',
        cookie: true, // Enable cookies to allow the server to access the session.
        xfbml: true, // Parse social plugins on this webpage.
        version: 'v8.0' // Use this Graph API version for this call.
      });

      FB.getLoginStatus(function(response) { // Called after the JS SDK has been initialized.
        statusChangeCallback(response); // Returns the login status.
      });
    };

    function testAPI() { // Testing Graph API after login.  See statusChangeCallback() for when this call is made.
      console.log('Welcome!  Fetching your information.... ');
      FB.api('/me', function(response) {
        console.log('Successful login for: ' + response.name);
        console.log('Correo: ' + response.email + ', Id: ' + response.id);
        document.getElementById('nombreLog').innerHTML = response.name;
        $.ajax({
          url: 'registro_usuario.php',
          type: 'POST',
          dataType: 'html',
          data: {
            txtNombre: response.name,
            txtCorreo: response.email,
            txtId: response.id
          },
          success: function(data) {
            $('#inicio_sesion').modal('close');
            $('#buttonOut').css('display', 'block');
          },
          error: function() {
            alert('Error');
          }
        });
      });
    }

    function logOut() {
      FB.logout(function(response) {
        // Person is now logged Out
        console.log('Sesion Cerrada');
        location.reload();
      });
    }
  </script>
</head>

<body>
  <?php
  include('header.php');
  ?>
  <div class="encabezado">
    <div class="encabezado-celda"></div>
    <div class="encabezado-celda">
      <div class="titulo">NUEVOS</div>
    </div>
    <div class="encabezado-celda" style="text-align:right">
      <div class="custom-select">
        <select>
          <option value="0">FILTRAR POR CATEGORÍA</option>
          <option value="1">NUEVOS</option>
          <option value="2">MÁS VOTADOS</option>
          <option value="3">COLECCIONES</option>
          <option value="4">TENDENCIA</option>
        </select>
      </div>
    </div>
  </div>
  <?php $consulta = "SELECT t1.nombre_diseno, t1.inspiracion, t2.nombre, t3.ruta_img, t2.foto, t1.id_diseno,t2.id_usuario FROM diseno AS t1, usuarios AS t2, img_diseno AS t3 WHERE t1.id_usuario= t2.id_usuario";
  $resultado_diseno = mysqli_query($conexion, $consulta) or die("Error en la consulta a la base de datos");
  $consulta_numero = "SELECT `id_diseno` FROM `diseno`";
  $resultado_largo = mysqli_query($conexion, $consulta_numero) or die("Error en la consulta a la base de datos");
  $a = 0;
  $b = 0;
  while ($largo = mysqli_fetch_array($resultado_largo)) {
    $a++;
  }
  while ($design_data = mysqli_fetch_array($resultado_diseno)) {
    if ($b < $a) { ?>
      <!--CONTENEDOR DISEÑO---->
      <div class="contenedor">
        <div class="slide-contenedor">
          <div class="contenedor-celda">
            <a href="/urban/dashboardnew.php?id_usuario=<?php echo $design_data[6]; ?>"><?php echo '<img class="icono-usuario" src="img/' . $design_data[4] . '">'; ?></a> <br />
            <a style="text-decoration:none; color:black;" href="/urban/dashboardnew.php?id_usuario=<?php echo $design_data[6]; ?>"><span class="nombre-usuario">
                <?php echo $design_data[2]; ?>
              </span><br /></a>
            <div class="descripcion-prenda">
              <h3 class="nombre-prenda"><?php echo $design_data[0]; ?></h3>
              <ul class="collapsible">
                <li>
                  <div class="collapsible-header background-head-collapse">INSPIRACIÓN</div>
                  <div class="collapsible-body">
                    <span> <?php echo $design_data[1]; ?> </span>
                  </div>
                </li>
              </ul>
            </div>
          </div>
          <div class="contenedor-celda">
            <div class="slider height-slide">
              <ul class="slides height-slide">
                <?php $consulta_imagenes = "SELECT `ruta_img`, `id_diseno` FROM img_diseno";
                $resultado_imagenes = mysqli_query($conexion, $consulta_imagenes) or die("Error en la consulta a la base de datos");
                while ($datos_imagenes = mysqli_fetch_array($resultado_imagenes)) {
                  if ($datos_imagenes[1] == $design_data[5]) { ?>
                    <li class="height-slide"> <?php echo '<img src="img/' . $datos_imagenes[0] . '">'; ?></li>
                    <<?php }
                  } ?> </ul>
            </div>
          </div>
          <div class="contenedor-celda padding-celda">
            <?php
            ////HACER UNA CONSULTA SÓLO DE LOS DISENOS
            $consulta_votos = "SELECT `id_diseno` FROM votos_diseno";
            $resultado_votos = mysqli_query($conexion, $consulta_votos) or die("Error en la consulta a la base de datos");
            $i = 0;
            while ($datos_votos = mysqli_fetch_array($resultado_votos)) {
              if ($datos_votos[0] == $design_data[5]) {
                $i++;
              }
            } ?>

            <div class="no-votado">
            </div><br />
            <p class="conteo"><?php echo $i; ?><br />VOTOS</p>
            <p><b>Compártelo</b></p>
            <div class="comentarios">
              <div class="redes">
                <img class="icon-red" src="img/icon-whats-o.svg" />
                <img class="icon-red" src="img/icon-twitter-o.svg" />
                <img class="icon-red" src="img/icon-face-o.svg" />
                <img class="icon-red" src="img/icon-insta-o.svg" />
                <!--<img class="icon-red" src="img/icon-share-c.svg" />-->
              </div>
              <div class="comentarios-scroll">
                <?php $consulta_comentarios = "SELECT t1.nombre, t2.comentario, t1.foto, t2.fecha, t2.id_diseno FROM usuarios AS t1, comentarios AS t2 WHERE t1.id_usuario= t2.id_usuario";
                $resultado_comentarios = mysqli_query($conexion, $consulta_comentarios) or die("Error en la consulta a la base de datos");
                while ($datos_comentarios = mysqli_fetch_array($resultado_comentarios)) {
                  if ($design_data[5] == $datos_comentarios[4]) { ?>
                    <div class='comentario'>
                      <div class='comentario-perfil'>
                        <?php echo '<img class="comentario-img" src="img/' . $datos_comentarios[2] . '">'; ?>
                      </div>
                      <div class='comentario-descripcion'>
                        <span class='comentario-nombre'>
                          <?php
                          echo $datos_comentarios[0];
                          ?>
                        </span>
                        <br />
                        <span class='comentario-texto'><?php echo $datos_comentarios[1]; ?></span>
                        <br />
                        <span class='comentario-tiempo'><?php echo $datos_comentarios[3]; ?></span>
                      </div>
                    </div>
                <?php }
                } ?>
              </div>
              <div class="comentar">
                <label class="comentario-label">Agregar comentario</label>
                <input type="text" class="input-comentario" />
                <div class="div-button-enviar">
                  <span class="label-button">Comentar</span>
                  <img class="comentario-button" src="img/icon-enviar-c.svg" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  <?php $b++;
    }
  } ?>




  <div id="inicio_sesion" class="modal modal-inicio">
    <div class="modal-content modal-padding">
      <span class="close-modal-inicio" onclick="cerrar_modal_inicio()">X</span>
      <img src="img/favicon.svg" class="logo-modal" /><br />
      <span class="titulo-modal">INICIA SESIÓN</span>
      <div class="input-field col s12 margin-inicio">
        <label class="label-inicio">CORREO</label>
        <input type="email" class="input-inicio" id="txtUsuario" name="txtUsuario" />
      </div>
      <div class="input-field col s12 margin-inicio">
        <img src="img/icon-mostrar.svg" class="mostrar-pass" id="mostrar_pass" onclick="mostrar_pass(1)" title="Mostrar contraseña" />
        <label class="label-inicio">CONTRASEÑA</label>
        <input type="password" class="input-inicio" name="txtPassword" id="txtPassword" />
      </div>
      <div class="div-inicio-modal">
        <p>
          <label>
            <input type="checkbox" value="" class="" />
            <span>Recuérdame</span>
          </label>
        </p>
        <a class="enlace-pass" href="#">¿Olvidaste tu contraseña?</a>
      </div>
      <input type="button" class="button-inicio" value="INICIAR SESIÓN" />
      <!-- <input type="button" class="" value="INICIAR CON FACEBOOK" /> -->
      <div class="fb-login-button" data-size="medium" data-button-type="login_with" data-layout="default" data-auto-logout-link="false" data-use-continue-as="false" data-width="" scope="public_profile,email" onlogin="checkLoginState();"></div>
    </div>
    <div class="modal-footer modal-inicio-padding" style="text-align:center;height:auto">
      <div class="inicio-footer"></div><br />
      <span class="texto-modal">¿Aún no tienes cuenta?</span><br />
      <a href="#" class="unete" onclick="registrarse()">¡Únete!</a>
    </div>
  </div>
  <div id="registrarse_modal" class="modal modal-registrarse">
    <div class="modal-content modal-padding" id="contenido-registrarse">
      <span class="close-modal-inicio" onclick="cerrar_modal_registrarse()">X</span>
      <img src="img/favicon.svg" class="logo-modal" /><br />
      <span class="titulo-modal">REGÍSTRATE</span>
      <div class="row">
        <div class="col s12">
          <ul class="tabs" id="tabs-hanger-votante">
            <li class="tab col s6"><a href="#tab-hanger" id="hanger-tab" class="active">HANGER</a></li>
            <li class="tab col s6"><a href="#tab-votante">VOTANTE</a></li>
          </ul>
        </div>
        <div id="tab-hanger" class="col s12 texto-hanger">
          Cuando eres Hanger puedes votar y subir tus propios diseños.<br />
        </div>
        <div id="tab-votante" class="col s12 texto-hanger">
          Los votantes pueden votar por sus diseños favoritos, y cambiar a una cuenta de hanger cuando quieran.
        </div>
      </div>
      <div class="input-field col s12 margin-inicio">
        <label class="label-inicio">Usuario</label>
        <input type="email" class="input-inicio" id="txtUsuarioRegistro" name="txtUsuarioRegistro" />
        <span class="helper-text" data-error="wrong" data-success="right" style="text-align:right">*Este será tu nombre de usuario visto por el público</span>
      </div>
      <div class="input-field col s12 margin-inicio">
        <label class="label-inicio">CORREO</label>
        <input type="email" class="input-inicio" id="txtCorreoRegistro" name="txtCorreoRegistro" />
      </div>
      <div class="input-field col s12 margin-inicio">
        <img src="img/icon-mostrar.svg" class="mostrar-pass-sp" id="mostrar_pass_registro" title="Mostrar contraseña" onclick="mostrar_pass(2)" />
        <label class="label-inicio">CONTRASEÑA</label>
        <input type="password" class="input-inicio" name="txtPasswordRegistro" id="txtPasswordRegistro" />
        <span class="helper-text" data-error="wrong" data-success="right" style="text-align:right">*Contraseña de mínimo 9 carácteres</span>
      </div>
      <div class="input-field col s12 margin-inicio">
        <img src="img/icon-mostrar.svg" class="mostrar-pass" id="mostrar_pass_repetir" title="Mostrar contraseña" onclick="mostrar_pass(3)" />
        <label class="label-inicio">REPETIR CONTRASEÑA</label>
        <input type="password" class="input-inicio" name="txtPasswordRepetir" id="txtPasswordRepetir" />
      </div>
      <div class="margin-inicio">
        <label>
          <input id="check_terminos_condiciones" type="checkbox" name="terminos" />
          <span>Acepto <a target="_blank" href='<?php echo $ruta_global; ?>vistas/assets/terminos_condiciones/General.pdf'>términos y condiciones </a> Urban Hangers.</span>
        </label>
      </div>
      <input type="button" class="button-inicio" value="REGÍSTRATE" /><br />
      <input type="button" class="button-facebook" value="INICIAR CON FACEBOOK" />
    </div>
  </div>
  <div id="status"></div>
  <?php include('footer.php'); ?>
  <script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
  <script type="text/javascript" src="js/custom-select.js"></script>
  <script type="text/javascript" src="js/materialize.min.js?v=<?php echo time(); ?>"></script>
  <script type="text/javascript" src="js/envio_correo.js?v=<?php echo time(); ?>"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="js/functions.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('.collapsible').collapsible();
      $('.slider').slider();
      $('.modal').modal();
      $('.tabs').tabs();
      $('#tabs-hanger-votante').tabs('updateTabIndicator');
    });

    function inicio_sesion() {
      $('#inicio_sesion').modal("open");
    }

    function cerrar_modal_inicio() {
      $('#inicio_sesion').modal("close");
    }

    function cerrar_modal_registrarse() {
      $('#registrarse_modal').modal("close");
    }

    function mostrar_pass(e) {

      switch (e) {
        case 1:
          var eye = document.getElementById('txtPassword');
          var img = document.getElementById('mostrar_pass');
          break;
        case 2:
          var eye = document.getElementById('txtPasswordRegistro');
          var img = document.getElementById('mostrar_pass_registro');
          break;
        case 3:
          var eye = document.getElementById('txtPasswordRepetir');
          var img = document.getElementById('mostrar_pass_repetir');
          break;
        default:
      }


      if (eye.type == 'password') {
        eye.type = 'text';
        img.src = 'img/icon-ocultar.svg';
        img.title = 'Ocultar contraseña';
      } else {
        eye.type = 'password';
        img.src = 'img/icon-mostrar.svg';
        img.title = 'Mostrar contraseña';
      }

    }

    function registrarse() {
      $('#inicio_sesion').modal("close");
      $('#registrarse_modal').modal("open");
    }
  </script>
  <div id="fb-root"></div>
  <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v8.0&appId=2441804282780205&autoLogAppEvents=1" nonce="VvHnUdQY"></script>
</body>

</html>