<?php
 
  $to = "pablo@inkwonders.com, eddy@inkwonders.com";

  $subject = "Contacto desde la página web de URBAN HANGERS";

  $message = "<html>
  			<head>
  				<title>Contacto desde la página web de URBAN HANGERS</title>
  			</head>
  			<body style='padding:0; margin:0; font-family:Arial, Helvetica, sans-serif; color:#000; font-size:12px'>
  				<table style='border:2px solid #e2231a; padding:10px;' cellpadding='0' cellspacing='0'>
  				<tr>
  					<td align='left' style='color:#000'>
    					Te han contactado con el formulario de URBAN HANGERS<br /><br />
    					<b>Nombre:</b> " . $_POST["txtNombre"] . "<br>
    					<b>Correo: </b> ". $_POST["txtCorreo"] . "<br>
    					<b>Mensaje:</b> " . $_POST["txtMensaje"] . "
  					</td>
  				</tr>
  				</table>
  			</body>
  		</html>";

  $headers = "MIME-Version: 1.0" . "\r\n";
  $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
  $headers .= 'From: URBAN HANGERS <contacto@urbanhangers.com>' . "\r\n";

  if(mail($to,$subject,$message,$headers)){
?>
  <div class="success">
    ¡GRACIAS POR ESCRIBIRNOS, NOSOTROS<br />
    NOS PONDREMOS EN CONTACTO CONTIGO!<br /><br />
    <a class="enlace-contacto" onclick="cargar_div()">Enviar otro correo</a>
  </div>
<?php
}else{
?>
  <div class="success">
    Algo salio mal<br /><br />
    <a class="enlace-contacto" onclick="cargar_div()">Enviar otro correo</a>
  </div>
<?php
}
?>
