<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />

    <title>RESTABLECER CONTRASEÑA | URBAN HANGERS®</title>
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $ruta_global; ?>vistas/assets/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $ruta_global; ?>vistas/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $ruta_global; ?>vistas/assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo $ruta_global; ?>vistas/assets/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo $ruta_global; ?>vistas/assets/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <!-- Favicon -->
    <link rel="stylesheet" type="text/css" href="<?php echo $ruta_global; ?>vistas/assets/css/estilo.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $ruta_global; ?>vistas/assets/css/custom-select.css"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="<?php echo $ruta_global; ?>vistas/assets/css/materialize.min.css"  media="screen,projection"/>
  </head>
  <body>

  <script type='text/javascript'>
  window.__lo_site_id = 312183;
  (function() {
  var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
  wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
  })();
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-159600964-2"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-159600964-2');
</script>

    <div class="contenedor-log-in">
      <form id="form_restablecer_contrasena" method="post">
      <div class="div-log-cell padding-log">
        <img src="<?php echo $ruta_global; ?>vistas/assets/img/favicon.svg" class="logo-modal  margin-log" /><br />
        <span class="titulo-modal">Restablece tu contraseña</span><br /><br />
        <div class="input-field col s12 margin-inicio">
          <img src="<?php echo $ruta_global; ?>vistas/assets/img/icon-mostrar.svg" class="mostrar-pass" id="mostrar_pass" onclick="mostrar_pass_re_co(1)" title="Mostrar contraseña"/>
          <label class="label-inicio">NUEVA CONTRASEÑA</label>
          <input type="password" class="input-inicio" name="input_restablecer" id="password_restablecer"/>
        </div>
        <div class="input-field col s12 margin-inicio">
          <img src="<?php echo $ruta_global; ?>vistas/assets/img/icon-mostrar.svg" class="mostrar-pass" id="mostrar_pass_repetir" onclick="mostrar_pass_re_co(2)" title="Mostrar contraseña"/>
          <label class="label-inicio">REPETIR NUEVA CONTRASEÑA</label>
          <input type="password" class="input-inicio" name="input_repetir_restablecer" id="password_repetir_restablecer"/>
        </div>
        <div id="respuesta_restablecer_contrasena"></div>
        <input id="restablecer_boton" type="submit" class="button-inicio  margin-log" value="CAMBIAR CONTRASEÑA"/><br />
      </div>
      </form>
      <div class="div-log-cell background-log"></div>

    </div>    
    <script>
    function mostrar_pass_re_co(e){

      switch (e) {
        case 1:
          var eye = document.getElementById('password_restablecer');
          var img = document.getElementById('mostrar_pass');
          break;
       case 2:
         var eye = document.getElementById('password_repetir_restablecer');
         var img = document.getElementById('mostrar_pass_repetir');
         break;
        default:
      }

      if(eye.type == 'password'){
        eye.type = 'text';
        img.src = '<?php echo $ruta_global; ?>vistas/assets/img/icon-ocultar.svg';
        img.title = 'Ocultar contraseña';
      }else{
        eye.type = 'password';
        img.src = '<?php echo $ruta_global; ?>vistas/assets/img/icon-mostrar.svg';
        img.title = 'Mostrar contraseña';
      }

    }
    </script>
  </body>
</html>
