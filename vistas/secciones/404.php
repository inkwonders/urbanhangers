<script type='text/javascript'>
  window.__lo_site_id = 312183;
  (function() {
  var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
  wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
  })();
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-159600964-2"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-159600964-2');
</script>
    
    <div class="contenedor404">
      <div class="contenido404">
        <span class="titulo404">404</span><br />
        <span class="texto404">Oops, parece que aquí no hay nada...</span><br /><br />
        <div class="button404" onclick="window.location='<?php echo $ruta_global; ?>home'">
        <img src="<?php echo $ruta_global; ?>vistas/assets/img/arrow_lf_404.svg" alt="arrow_lf" class="arr_404">  <span>REGRESAR A HOME</span>
        </div>
      </div>
    </div>
