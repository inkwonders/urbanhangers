<?php

if(isset($_SESSION["iniciarSesion"]) && $_SESSION["iniciarSesion"] == "ok") {

  if ($_SESSION["tipoUsuario"] == 1) {

?>

<script type='text/javascript'>
  window.__lo_site_id = 312183;
  (function() {
  var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
  wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
  })();
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-159600964-2"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-159600964-2');
</script>

<section class="secc_editar_datos">

  <div class="cont_int_editar">

    <form id="editar_perfil" method="POST" enctype="multipart/form-data">

      <h2 class="titulo-seccion">EDITAR PERFIL</h2>

      <div class="cont_imagen_editar_perfil">

          <?php

            $idUsuario = $_SESSION["id_usuario"];

            $peticion = ControladorDashboard::consultaEditarPerfil("usuarios", $idUsuario);

            ?>
            <div id="espacio_foto_perfil">


            <?php
              if(!empty($_SESSION['avatar'])){
                if($_SESSION["modo_registro"] == "directo"){
                  $ruta_foto = "vistas/assets/hangers/".$_SESSION['carpeta']."/".$_SESSION["avatar"];
                  $div_perfil = "<div class='imagen-perfil-editar' style='background: url(vistas/assets/hangers/".$_SESSION['carpeta']."/".$_SESSION["avatar"].")'></div>";
                  echo "<div class='imagen-perfil-editar' style='background-image: url(vistas/assets/hangers/".$_SESSION['carpeta']."/".$_SESSION["avatar"].")'></div>";
                }else{
                  $ruta_foto = $_SESSION["avatar"];
                  $div_perfil = "<div class='imagen-perfil-editar' style='background: url(".$_SESSION["avatar"].")'></div>";
                  echo "<div class='imagen-perfil-editar' style='background-image: url(".$_SESSION["avatar"].")'></div>";
                }
              }else{
                if($_SESSION['sexo'] == "h"){
                  $ruta_foto = "vistas/assets/img/icon-usuario-1.svg";
                  $div_perfil = "<div class='imagen-perfil-editar' style='background: url($ruta_foto)'></div>";
                  echo "<div class='imagen-perfil-editar' style='background-image: url($ruta_foto)'></div>";
                }else {
                  $ruta_foto = "vistas/assets/img/icon-usuario-5.svg";
                  $div_perfil = "<div class='imagen-perfil-editar' style='background: url($ruta_foto)'></div>";
                  echo "<div class='imagen-perfil-editar' style='background-image: url($ruta_foto)'></div>";
                }
              }
              ?>
            </div>
            <?php

            if($_SESSION['modo_registro'] == "facebook"){
              ?>
              <p class="comentario-label padding-20">Tu cuenta esta registrada con facebook</p>
              <?php
            }else {
              ?>
              <div class="cont_lbl_msj_ep">

                <label for="foto_perfil" class="btn_upload btn_upload_fx_perfil">
                  <span class="font_btn">SUBIR NUEVA FOTO</span>
                </label>

                <div class="msj_carga_imagen_perfil wp oculto"><span></span>&nbsp;&nbsp;<a href="#" id="cambiarimagen_perfil">CAMBIAR</a> </div>
              </div>

              <div class="desktop_no img_help_mov_editar_perfil">
                <img src="vistas/assets/img/ico-duda.svg" class="btn_icon_help_mov">
                <span class="msj-muestra-mov">Formato: JPG o PNG <br>Tamaño recomendado: 1:1, 1000 x 1000 px</span>
              </div>

              <input type="file" class="inputfile padding-20" name="foto_perfil" id="foto_perfil" accept=".png, .jpg, .jpeg" />

              <img src="vistas/assets/img/ico-duda.svg" class="ico-btn ico-duda ico-titulo-duda ico-duda-editar-perfil movil_not">
              <span class="msj-titulo msj-oculto-imagen-editar-perfil movil_not" style="opacity: 0;">
                Sube aquí tu foto de perfil. Esta foto será pública. <br>Medidas recomendadas: 1:1, 1000 x 1000 px
              </span>
              <?php
            }
          ?>
        </div>
        <div id="espacio_banner">
          <?php

          if(empty($peticion["banner"])){
              $div_banner = "<div class='banner_perfil_vacio'> <p class='sin_banner_txt'>Sin banner</p> </div>";
              echo "<div class='banner_perfil_vacio'> <p class='sin_banner_txt'>Sin banner</p> </div>";
          }else{
              $div_banner = "<div class='banner-perfil' style='background: url(vistas/assets/hangers/".$_SESSION['carpeta']."/".$_SESSION["banner"]."')> </div>";
              echo "<div class='banner-perfil' style='background: url(vistas/assets/hangers/".$_SESSION['carpeta']."/".$_SESSION["banner"]."')> </div>";
          }

          ?>
        </div>

        <div class="cont_input_banner">
            <label for="input_banner" class="btn_upload pos_fx_10 wt_85_fx">
              <span class="font_btn">SUBIR NUEVA FOTO DE PORTADA</span>
            </label>
            <input type="file" class="inputfile padding-20 " id="input_banner" name="banner_perfil" accept=".png, .jpg, .jpeg" />


              <img src="vistas/assets/img/ico-duda.svg" class="ico-btn ico-duda ico-banner-imagen ico-duda-editar-perfil pos_fx_10 movil_not">
              <span class="msj-banner msj-oculto-imagen-editar-perfil pos_fx_11 movil_not" style="opacity: 0;">
                Sube aquí tu banner de perfil. Este banner será público <br>Medidas recomendadas: 1680 x 338 px
              </span>
        </div>
        <div class="desktop_no img_help_mov_editar_perfil">
          <img src="vistas/assets/img/ico-duda.svg" class="btn_icon_help_mov">
          <span class="msj-muestra-mov">Sube aquí tu banner de perfil. Este banner será público <br>Medidas recomendadas: 1680 x 338 px</span>
        </div>
        <p class="msj_carga_imagen_banner wp oculto"><span></span>&nbsp;&nbsp;<a href="#" id="cambiarimagen_banner">CAMBIAR</a> </p>

        <input type="hidden" value="editar" name="tipo_operacion">
        <input type="hidden" value="<?php echo $idUsuario ?>" name="key_user">
        <input type="hidden" value="<?php echo base64_encode($_SESSION['carpeta']); ?>" name="cp_user">
        <input type="hidden" value="<?php echo $_SESSION['correo']; ?>" name="correo_user">

        <div class="cont-inputs-datos">

            <div class="comentar">
                <label class="comentario-label active">
                  USUARIO*
                </label>
                <input id="input_usuario" name="usuario_editarPerfil" type="text" class="input-comentario" placeholder="Elige el nombre de usuario con el que quieres que te identifique la comunidad" value="<?php echo $peticion['usuario']; ?>" maxlength="23" required>
                <div class="cont_nota_input">

                </div>
                <span id="sp_error_usuario" class="sp_error_inp oculto">*Sólo se permiten letras, números y guión medio, sin espacios</span>
            </div> 
            <div class="cont_nombre_usuario">

              <div id="nombre" class="input_interno">
                  <label class="comentario-label active">
                    NOMBRE(S)*
                  </label>
                  <input id="input_nombres" name="nombre_editarPerfil" type="text" class="input-comentario" placeholder="Escribe tu nombre(s)" value="<?php echo $peticion['nombre']; ?>" maxlength="50" required>
                  <span id="sp_error_nombres" class="sp_error_inp oculto">*No se permiten caracteres especiales</span>
              </div>

              <div id="apellidopm" class="input_interno">
                  <label class="comentario-label active">
                    APELLIDO(S)*
                  </label>
                  <input id="input_apellidos" name="apellido_editarPerfil" type="text" class="input-comentario" placeholder="Escribe tu(s) apellido(s)" value="<?php echo $peticion['apellido']; ?>" maxlength="50" required>                  
                  <span id="sp_error_apellidos" class="sp_error_inp oculto">*No se permiten caracteres especiales</span>
              </div>
            </div>
            <div class="cont_ubicacion">

              <div class="sel" id="pais">
                  <label class="comentario-label active">
                    PAÍS*
                    &nbsp;
                    <span class="msj-ubicacion msj-oculto-input-canvas movil_not" style="opacity: 0;">
                      Elige una colección en la que
                      creas que pertenece tu diseño
                    </span>
                  </label>
                  <select id="select_pais" class="input-select" name="pais_editarPerfil" required>
                    <?php
                    if(!empty($peticion['pais'])){
                      $id_pais=$peticion['pais'];
                      $pais = ControladorUsuarios::consultaPaisHanger("paises", $id_pais);?>
                      <option value="<?php echo $pais[0];?>" selected><?php echo $pais[0];?></option>
                    <?php
                  }else{
                    $id_pais = "";
                    ?>
                    <option disabled selected>Selecciona el país en el que vives actualmente</option>
                    <?php } ?>

                  <?php
                  $pais = ControladorUsuarios::consultaPaises("paises");

                  foreach($pais as $key => $value){?>
                        <option value="<?php echo $value['nombre_pais'];?>"><?php echo $value['nombre_pais'];?></option>";
                  <?php
                    }

                  ?>

                  </select>


              </div>

              <div class="sel" id="estado">
                  <label id="label_estado" class="comentario-label active">
                    ESTADO*
                    &nbsp;
                    <span id="mensaje_estado" class="msj-ubicacion2 msj-oculto-input-canvas movil_not" style="opacity: 0;">
                      Elige una colección en la que
                      creas que pertenece tu diseño
                    </span>
                  </label>
                  <input id="input_estado_extranjero_oculto" name="estado_extranjero" type="text" class="input-comentario" maxlength="50" style="display: none;">
                  <select id="select_estado_oculto" class="input-select" name="estado_editarPerfil" style="display:none;">

                    <?php
                      $estado = ControladorUsuarios::consultaEstados("estados_bd9aefd2");?>
                      <option disabled selected>Selecciona el estado en el que vives actualmente</option>
                      <?php
                    foreach($estado as $key => $value){?>
                          <option value="<?php echo $value['estado'];?>"><?php echo $value['estado'];?></option>";
                        <?php
                      }
                    ?>

                  </select>
                  <?php
                  $pais = ControladorUsuarios::consultaPaisHanger("paises", $id_pais);
                  $id_estado = $peticion['estado'];
                  if(!empty($pais[0])){
                  if($pais[0] == "México"){
                    $estado = ControladorUsuarios::consultaEstadoHanger("estados_bd9aefd2", $id_estado);?>


                    <select id="select_estado" class="input-select" name="estado_editarPerfil">
                      <option value="<?php echo $estado[0];?>"><?php echo $estado[0];?></option>

                      <?php
                        $estado = ControladorUsuarios::consultaEstados("estados_bd9aefd2");

                      foreach($estado as $key => $value){?>
                            <option value="<?php echo $value['estado'];?>"><?php echo $value['estado'];?></option>";
                          <?php
                        }
                      ?>

                    </select>
                    <?php
                  }else{
                    if(!empty($peticion['estado_extranjero'])){?>
                    <input id="input_estado_extranjero" name="estado_extranjero" type="text" class="input-comentario" maxlength="50" value="<?php echo $peticion['estado_extranjero'];?>" placeholder="<?php echo $peticion['estado_extranjero'];?>">
                    <?php
                  }else{ ?>
                    <input id="input_estado_extranjero" name="estado_extranjero" type="text" class="input-comentario" maxlength="50" required>
                      <?php
                    }
                  }
                }else{?>
                  <input id="input_estado_extranjero" placeholder="Escribe el estado en el que vives actualmente" name="estado_extranjero" type="text" class="input-comentario" maxlength="50" required>
                <?php
              }
                  ?>

              </div>
            </div>

            <div class="comentar biografia_div_editarPerfil">
                <label class="comentario-label active">
                  BIOGRAFÍA
                  &nbsp;
                  <span class="msj-biografia msj-oculto-input-canvas movil_not">
                    Elige una colección en la que
                    creas que pertenece tu diseño
                  </span>
                </label>
                <textarea name="biografia_editarPerfil" class="textarea-comentario" placeholder="Cuéntale a la comunidad un poco acerca de ti" maxlength="200"><?php echo $peticion['descripcion']; ?></textarea>

            </div>
            <div class="cont_sexo">
              <label class="comentario-label active pad_fx">
                Género*
              </label>
              <div class="input-field col s12 margin-inicio">
                <div class="cont_sexo">
                    <label class="mar_sex_fx">
                      <input class="sexo_radio"  name="sexoEditar" type="radio" checked value="h" <?php echo ($peticion['sexo'] == 'h')? 'checked' : ''; ?>/>
                      <span class="sexo">Hombre</span>
                    </label>
                    <label class="mar_sex_fx">
                      <input class="sexo_radio" name="sexoEditar" type="radio" value="m" <?php echo ($peticion['sexo'] == 'm')? 'checked' : ''; ?>/>
                      <span class="sexo">Mujer</span>
                    </label>
                </div>
              </div>
            </div>

        </div>

        <div class="linea_gris"></div>

        <h2 class="titulo-seccion">REDES SOCIALES</h2>

        <div class="cont-inputs-datos">

          <div class="comentar" id="editarperfil_biografia">
              <label class="comentario-label ">
                <div class="cont_ico"><img src="vistas/assets/img/icono-be.svg" class="ico_red"></div>&nbsp; BEHANCE
                &nbsp;<img src="vistas/assets/img/ico-duda.svg" class="ico-btn ico-be-perfil movil_not">
                <span class="msj-behance msj-oculto-input-canvas movil_not" style="opacity: 0;">
                  Escribe aquí lo que está después de la última diagonal (/) en el URL de tu perfil. <br />
                  Ejemplo: https://www.behance.net/<b style="font-size: 11px; color:#FF5C1A;">danielaaaif77a</b>
                </span>
              </label>
              <input name="behance_editarPerfil" type="text" class="input-comentario" placeholder="Dirige a las personas hacia tu perfil de Behance" value="<?php echo $peticion['behance']; ?>" maxlength="30">
          </div>

          <div class="comentar">
              <label class="comentario-label ">
                <div class="cont_ico"><img src="vistas/assets/img/icono-internet.svg" class="ico_red"></div>&nbsp; DRIBBLE
                &nbsp;<img src="vistas/assets/img/ico-duda.svg" class="ico-btn ico-dribble-perfil movil_not">
                <span class="msj-dribble msj-oculto-input-canvas movil_not" style="opacity: 0;">
                  Escribe aquí lo que está después de la última diagonal (/) en el URL de tu perfil. <br />
                  Ejemplo: https://www.dribbble.com/<b style="font-size: 11px; color:#FF5C1A;">dany_aimee</b>
                </span>
              </label>
              <input name="dribble_editarPerfil" type="text" class="input-comentario" placeholder="Dirige a las personas hacia tu perfil de Dribble" value="<?php echo $peticion['dribble']; ?>" maxlength="30">

          </div>

          <div class="comentar">
              <label class="comentario-label ">
                <div class="cont_ico"><img src="vistas/assets/img/icono-instagram.svg" class="ico_red"></div>&nbsp; INSTAGRAM
                &nbsp;<img src="vistas/assets/img/ico-duda.svg" class="ico-btn ico-insta-perfil movil_not">
                <span class="msj-instagram msj-oculto-input-canvas movil_not" style="opacity: 0;">
                  Escribe aquí lo que está después de la última diagonal (/) en el URL de tu perfil. <br />
                  Ejemplo: https://www.instagram.com/<b style="font-size: 11px; color:#FF5C1A;">danyaimee_art</b>
                </span>
              </label>
              <input name="instagram_editarPerfil" type="text" class="input-comentario" placeholder="Dirige a las personas hacia tu perfil de Instagram" value="<?php echo $peticion['instagram']; ?>" maxlength="30">

          </div>

        </div>

        <?php

          if($_SESSION["modo_registro"] == "facebook"){

          }else{

        ?>

        <div class="linea_gris"></div>

        <h2 class="titulo-seccion">CONTRASEÑA</h2>

        <div class="cont-inputs-datos">

          <div class="comentar contrasenas_editarPerfil">
              <label class="comentario-label ">
                CONTRASEÑA ACTUAL
              </label>
              <input id="contrasenaActual_editarPerfil" name="contrasenaActual_editarPerfil" type="password" class="input-comentario mg_fx input-comentario-pass" placeholder="Escribe la contraseña que utilizas para iniciar sesión" maxlength="100">
              <img src="<?php echo $ruta_global; ?>vistas/assets/img/icon-mostrar.svg" class="mostrar_pass_editar mostrar_password ojo1_editarPerfil" inp="contrasenaActual_editarPerfil" id="mostrar_pass_actual_ed" title="Mostrar contraseña"/>

          </div>

          <div class="comentar contrasenas_editarPerfil">
              <label class="comentario-label ">
                NUEVA CONTRASEÑA
              </label>
              <input id="contrasenaNueva_editarPerfil" name="contrasenaNueva_editarPerfil" type="password" class="input-comentario mg_fx input-comentario-pass" placeholder="Escribe tu nueva contraseña" maxlength="100">
              <img src="<?php echo $ruta_global; ?>vistas/assets/img/icon-mostrar.svg" class="mostrar_pass_editar mostrar_password ojo2_editarPerfil" inp="contrasenaNueva_editarPerfil" id="mostrar_pass_nueva_ed" title="Mostrar contraseña"/>
              <div class="cont_nota_input">
                <span class="nota_input">Tu contraseña debe ser de mínimo 8 carácteres</span>
              </div>
          </div>

          <div class="comentar contrasenas_editarPerfil">
              <label class="comentario-label ">
                REPETIR NUEVA CONTRASEÑA
              </label>
              <input id="contrasenaNuevaRepetir_editarPerfil" name="contrasenaNuevaRepetir_editarPerfil" type="password" class="input-comentario mg_fx input-comentario-pass" placeholder="Repite tu nueva contraseña" maxlength="100">
              <img src="<?php echo $ruta_global; ?>vistas/assets/img/icon-mostrar.svg" class="mostrar_pass_editar mostrar_password ojo1_editarPerfil" inp="contrasenaNuevaRepetir_editarPerfil" id="mostrar_pass_nueva_repetir_ed" title="Mostrar contraseña"/>

          </div>

        </div>

        <?php

          }

        ?>

        <div id="cont_error_editar" class="oculto">
          <p id="msj_error_editar"></p>
        </div>

        <div class="cont-botones-subir-diseno">

          <div id="div_botones" class="cont-interno-botones-editar-perfil">

            <button id="cancelar_editar_perfil" class="btn_urban fx_btnes_editar btn_cancelar" type="button">CANCELAR</button>

            <button id="aceptar_editar_perfil" class="btn_urban fx_btnes_editar btn_actualizar_perfil" type="submit">GUARDAR <span class="movil_not">CAMBIOS</span></button>

          </div>

          <div id="div_loader" class="cont_loader_gif oculto">

            <img src='<?php echo $ruta_global; ?>vistas/assets/css/ajax-loader.gif' alt='Cargando...'>

          </div>

        </div>

      </form>

  </div>

</section>

<?php

  }else{

?>

<meta http-equiv="refresh" content="0;url=404">

<?php }}else{ ?>

<meta http-equiv="refresh" content="0;url=404">

<?php } ?>

<script type="text/javascript">
  var ruta_global = "<?php echo $ruta_global; ?>";
  var div_perfil = "<?php echo $div_perfil; ?>";
  var div_banner = "<?php echo $div_banner; ?>";    
</script>

<script type="text/javascript" src="<?php echo $ruta_global; ?>vistas/assets/js/script_editarPerfil.min.js?v=<?php echo time(); ?>"></script>
