<?php

  setcookie("usuario", "", time()-3600,"/");
  setcookie("marca", "", time()-3600,"/");
  
  unset($_SESSION["iniciarSesion"]);
  unset($_SESSION["tipoUsuario"]);
  unset($_SESSION["id_usuario"]);
  unset($_SESSION["nombre"]);
  unset($_SESSION["avatar"]);
  unset($_SESSION["facebook"]);
  unset($_SESSION["ruta_perfil"]);

  session_destroy();

  echo '<script>

      location.href = "'.$ruta_global.'home";

    </script>';
