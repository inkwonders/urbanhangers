<?php
if (isset($_SESSION["iniciarSesion"]) && $_SESSION["iniciarSesion"] == "ok") {

  if ($_SESSION["tipoUsuario"] == 1) {
?>

<script type='text/javascript'>
  window.__lo_site_id = 312183;
  (function() {
  var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
  wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
  })();
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-159600964-2"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-159600964-2');
</script>

    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v8.0&appId=2441804282780205&autoLogAppEvents=1" nonce="VvHnUdQY"></script>

    <form id="form_subir_diseno" method="POST" enctype="multipart/form-data">
      <input type="hidden" id="cp_user" name="cp_user" value="<?php echo base64_encode($_SESSION['carpeta']); ?>">
      <section class="cont-subir-diseno">



        <div class="cont-titulo-subir-diseno">

          <h2 class="titulo-subir-diseno">SUBIR DISEÑO</h2>

        </div>

        <div class="cont-centro-subir-diseno">

          <div class="cont-centro-interno-izq">

            <div class="cont-input-subir-diseno">

              <label for="inputdiseno" class="ico-cloud">

                <img class="ico-cloud-fx" src="vistas/assets/img/cloud.svg">

              </label>


              <input type="file" class="inputfile" id="inputdiseno" name="inputdiseno" accept="image/x-png">

              <p class="txt-input">Aquí sólo sube el diseño que se utilizará para la votación. <br>
                Archivo en formato <b>PNG</b> <br>
                Tamaño máximo <b>1000 x 1000 px</b><br>
                Peso máximo del archivo <b>10mb.</b></p>

              <div id="vista_previa">

              </div>

            </div>


            <div class="desktop_no img_help_mov">
              <img src="vistas/assets/img/ico-duda.svg" class="btn_icon_help_mov">
              <span class="msj-muestra-mov">Descarga aquí nuestro “archivo plantilla” de Photoshop para poder generar tus diseños y mockups.</span>
            </div>

            <p class="msj_error_img oculto"><span></span>&nbsp;&nbsp; </p>

            <p class="msj_carga_imagen oculto"><span></span>&nbsp;&nbsp;<a href="#" id="cambiarimagen">CAMBIAR</a> </p>

          </div>
          <input type="hidden" id="hdnSession" data-value=<?php echo $_SESSION['id_usuario']; ?> name="usuario" />
          <div class="cont-centro-interno-der">


            <div class="comentar">
              <label class="comentario-label">TÍTULO *<img src="vistas/assets/img/ico-duda.svg" class="ico-btn ico-duda ico-titulo-duda movil_not"><span class="msj-titulo msj-oculto-input-canvas movil_not">Escribe aquí el nombre de tu diseño (max:20 caracteres, no se permiten caracteres especiales).</span>
                <div class="desktop_no dk_icon_help_cont"> <img src="<?php echo $ruta_global; ?>vistas/assets/img/ico-duda.svg" alt="icon_help" class="dk_icon_help"> <span class="msj-muestra-mov_vr2">Escribe aquí el nombre de tu diseño (max:20 caracteres, no se permiten caracteres especiales).</span> </div>
              </label>
              <input type="text" class="input-comentario" id="input_titulo" name="nombre_diseno" placeholder="Nombre de tu diseño" maxlength="20">
            </div>

            <div class="comentar">
              <label class="comentario-label">INSPIRACIÓN *<img src="vistas/assets/img/ico-duda.svg" class="ico-btn ico-duda ico-inspiracion-duda movil_not"> <span class="msj-inspiracion msj-oculto-input-canvas movil_not">Cuéntale a la comunidad en que te inspiraste para crear este diseño (máx:300 caracteres).</span>
                <div class="desktop_no dk_icon_help_cont"> <img src="<?php echo $ruta_global; ?>vistas/assets/img/ico-duda.svg" alt="icon_help" class="dk_icon_help"> <span class="msj-muestra-mov_vr2">Cuéntale a la comunidad en que te inspiraste para crear este diseño (máx:300 caracteres).</span> </div>
              </label>
              <textarea class="textarea-comentario" id="input_inspiracion" name="inspiracion_diseno" placeholder="¿Qué te inspiró para hacer este diseño?" maxlength="300"></textarea>
            </div>

            <div class="comentar cmt_fix">
              <label class="comentario-label">COLECCIÓN *<img src="vistas/assets/img/ico-duda.svg" class="ico-btn ico-duda ico-coleccion-duda movil_not"> <span class="msj-coleccion msj-oculto-input-canvas movil_not">Elige la colección a la que pertenece tu diseño. Solo puedes seleccionar una. Si crees que no queda en ninguna colección, selecciona “Urban”.</span>
                <div class="desktop_no dk_icon_help_cont"> <img src="<?php echo $ruta_global; ?>vistas/assets/img/ico-duda.svg" alt="icon_help" class="dk_icon_help"> <span class="msj-muestra-mov_vr2">Elige la colección a la que pertenece tu diseño. Solo puedes seleccionar una. Si crees que no queda en ninguna colección, selecciona “Urban”.</span> </div>
              </label>

              <div class="cont-radio-buttons">

                <div class="cont-radio-buttons-izq">


                  <?php
                  $peticion = ControladorDisenos::coleccionesDiseno('colecciones');
                  $tam_colecciones = sizeof($peticion);
                  $mitad_colecciones = round($tam_colecciones / 2);
                  $peticion2 = array_slice($peticion, 0, $mitad_colecciones);
                  $cont_colecciones = 0;
                  foreach ($peticion2 as $key => $value) {
                    $cont_colecciones++;
  
                  ?>
                    <p>
                      <label>
                        <input class="with-gap" nombre_coleccion="<?php echo $value['nombre_coleccion']; ?>" name="coleccion" type="radio" id="<?php echo base64_encode($value['sk_coleccion']); ?>" value="<?php echo base64_encode($value['sk_coleccion']); ?>" <?php echo ($cont_colecciones == 1) ? 'checked' : ''; ?> />
                        <span style="z-index:0;">
                        <?php echo $value['nombre_coleccion']; ?>. 
                        <?php 
                        if($value['sk_coleccion'] == "8a5a62c5-f15f-11eb-b28b-3a33beb3918a"){ ?>
                          Acepto <a href="vistas/assets/terminos_condiciones/Gamer.pdf"  target="_blank"> términos y condiciones </a> del concurso
                        <?php } ?>
                        </span>
                      </label>
                    </p>
                  <?php
                  }
                  ?>
                </div>
                <div class="cont-radio-buttons-der">
                  <?php
                  $peticion3 = array_slice($peticion, $mitad_colecciones, $tam_colecciones);
                  foreach ($peticion3 as $key => $value) {
                  ?>

                    <p>
                      <label>
                        <input class="with-gap" nombre_coleccion="<?php echo $value['nombre_coleccion']; ?>" name="coleccion" type="radio" id="<?php echo base64_encode($value['sk_coleccion']); ?>" value="<?php echo base64_encode($value['sk_coleccion']); ?>" />
                        <span style="z-index:0;">
                        <?php echo $value['nombre_coleccion']; ?>.
                        <?php 
                        if($value['sk_coleccion'] == "8a5a62c5-f15f-11eb-b28b-3a33beb3918a"){ ?>
                          Acepto <a href="vistas/assets/terminos_condiciones/Gamer.pdf"  target="_blank"> términos y condiciones </a> del concurso
                        <?php } ?>
                        </span>
                      </label>
                    </p>


                  <?php } ?>
                </div>

              </div>

            </div>

          </div>


        </div>

        </div>

        <div class="cont-previsualizacion oculto">

          <div class="cont-interno-previzualizacion">

            <div class="cont-titulo-previ">

              <h2>PREVISUALIZACIÓN DE DISEÑOS</h2>
              <a download href="vistas/assets/CANVAS-UH.zip" class="btn_urban btn_canvas btn_urban_mov_bk">
                DESCARGA PLANTILLA
                <img src="vistas/assets/img/ico-flecha-abajo.svg" class="ico-btn">
              </a>
              <br><br>
              <p class="mockups_help mensaje_boton"><b>Descarga aquí nuestro “archivo plantilla” de Photoshop para poder generar tus diseños y mockups.</b></p>
              <br/>
              <p class="mockups_help mensaje_boton mensaje_naranja">*El tamaño máximo del mockup es de <b>920 × 1229px</b>, y debe estar en formato <b>JPG.</b></p><br>
              <p class="mockups_help mensaje_boton mensaje_naranja">*El peso máximo del mockup es de <b>10mb.</b></p>
            </div>

            <div class="cont-dis-pre">


              <?php

              $productos = ControladorProductos::consultaProductosBase('productos_base');
              $contador = 0;
              $contador_dos = 0;
              foreach ($productos as $key => $valueProductos) {
                $contador++;
                if ($valueProductos['genero'] == 1) {
                  $nombre_genero = 'Hombre';
                } else if ($valueProductos['genero'] == 2) {
                  $nombre_genero = 'Mujer';
                } else if ($valueProductos['genero'] == 3) {
                  $nombre_genero = 'Unisex';
                } else {
                  $nombre_genero = '';
                }
              ?>
                <div class="card-pre">

                  <div class="previa_disenios" id="previa_disenios_<?= $contador ?>" style="background-position: center; background-size: 100%;"></div>

                  <div class="previa_disenios" id="previa_disenios_dos_<?= $contador ?>" style="background-position: center; background-size: 100%;"></div>


                  <img class="img_mockup" src="vistas/assets/img/img-productos/<?php echo $valueProductos['imagen']; ?>">

                  <img src="vistas/assets/img/mas.svg" class="btn-mas-centro" id="btn_mas_centro_<?= $contador ?>" onclick="disenios_pares('<?= $contador ?>');">

                  <!--<img src="vistas/assets/img/mas.svg" class="btn-mas-centro" id="btn_mas_centro_der_<?= $contador ?>" onclick="disenios_pares_dos('<?= $contador ?>');" style="display:none">-->


                  <span id="error_modelo_<?= $contador ?>" style="color:red"></span>


                  <!-- input uno -->
                  <input class="input_mockup oculto" id="disenios_file_<?= $contador  ?>" type="file" accept="image/jpeg,image/x-png" name="disenios_file_<?= $valueProductos['clave_producto'] ?>" onchange='validar_disenios("<?= $contador ?>");validar_mockup()'>

                  <input class="input_mockup oculto" id="disenios_file_dos<?= $contador  ?>" type="file" accept="image/jpeg,image/x-png" name="disenios_file_dos_<?= $valueProductos['clave_producto'] ?>" onchange='validar_disenios_dos("<?= $contador ?>");validar_mockup()'>

                  <input type="hidden" id="clave<?= $contador ?>" value="<?= $valueProductos['sk_producto'] ?>">

                  <div class="contenedor_circulo">
                    <div class="circulo_prev circulo_prev<?= $contador ?>" onclick="ver_disenio('<?= $contador  ?>','1');"></div>
                    <div class="circulo_prev circulo_prev_dos<?= $contador ?>" onclick="ver_disenio('<?= $contador  ?>','2');"></div>
                  </div>


                  <div class="caja-txt-card">
                    <h2><?php echo $valueProductos['tipo_producto'] . " " . $valueProductos['descripcion'] . " " . $nombre_genero; ?></h2>
                  </div>
                </div>
              <?php
              }

              ?>

            </div>

          </div>

        </div>

        <div class="cont-botones-subir-diseno">

          <div class="cont-interno-botones-subir-diseno">

            <button class="btn_urban btn_cancelar btn_urban_mov" type="button">CANCELAR</button>

            <button id="btn_publicar_subir_diseno" class="btn_urban btn_publicar deshabilitado btn_urban_mov" type="button">PUBLICAR</button>


          </div>

        </div>

      </section>

      <div class="modal-confirmacion oculto">

        <div class="card-modal">

          <div class="card-titulo">
            <h2>CONFIRMACIÓN DE DISEÑO</h2>
            <p>*Al confirmar tu diseño, se publicará en los Diseños para votación. <br>
              Ya no podrás hacer más cambios.</p>
          </div>

          <div class="card-content">

            <div class="card-content-izq">


            </div>

            <div class="card-content-der" style="padding: 0 3% 0 3%;">

              <h2 id="nombre_subir_diseno" class="wp"></h2>
              <div style=" width: 90%; border: 1px solid #b8b8b8; margin: 2% 0;"></div>
              <p class="subtitulo-modal">INSPIRACION</p>
              <p id="inspiracion_subir_diseno" class="txt-modal-agregar wp"></p>

              <p class="subtitulo-modal">COLECCIÓN</p>
              <p id="coleccion_subir_diseno" class="txt-modal-agregar wp"></p>

            </div>

          </div>

          <div class="card-footer">

            <button class="btn_urban btn_cancelar btn_regresar" type="button">
              REGRESAR
              <!-- <img class="ico-regresar" src="img/regresar.svg"> -->
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 27.34 19.6" class="ico-regresar">
                <defs>
                  <style>
                    .cls-1 {
                      fill: none;
                      stroke: #e15c07;
                      stroke-linecap: round;
                      stroke-linejoin: round;
                      stroke-width: 3px;
                    }
                  </style>
                </defs>
                <title>regresar</title>
                <g id="Capa_2" data-name="Capa 2">
                  <g id="Capa_1-2" data-name="Capa 1">
                    <polyline class="cls-1" points="9.24 18.09 1.5 10.9 9.8 4.26" />
                    <path class="cls-1" d="M1.5,10.9H18.84a7,7,0,0,0,7-7V1.5" />
                  </g>
                </g>
              </svg>
            </button>

            <button id="btn_confirmar_subir_diseno" class="btn_urban btn_publicar btn_confirmar" type="submit">CONFIRMAR</button>

          </div>

        </div>

      </div>
    </form>

  <?php
  } else { ?>
    <meta http-equiv="refresh" content="0;url=404">
  <?php }
} else { ?>
  <meta http-equiv="refresh" content="0;url=404">
<?php } ?>

<script type="text/javascript">
  var ruta_global = "<?php echo $ruta_global; ?>";
</script>
<script type="text/javascript" src="<?php echo $ruta_global; ?>vistas/assets/js/script_subirDiseno.min.js?v=<?php echo time(); ?>"></script>
