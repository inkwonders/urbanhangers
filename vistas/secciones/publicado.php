<?php $part_pag = explode("/", $_GET["pagina"]);
$id_diseno = base64_decode($part_pag[1]);
$diseno_usuario = ControladorDisenos::disenoPublicado('diseno', $id_diseno);
$usuario = $diseno_usuario['id_usuario'];
// $ruta_img = ControladorDisenos::imgDisenoPublicado('img_diseno', $id_diseno);
$ruta_img = $diseno_usuario['ruta_img'];
$peticion = ControladorDashboard::consultaDashboard("usuarios", $usuario);
foreach($peticion as $key => $value){
  $carpeta = $value['carpeta'];
}
?>

<script type='text/javascript'>
  window.__lo_site_id = 312183;
  (function() {
  var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
  wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
  })();
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-159600964-2"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-159600964-2');
</script>

    <section class="cont-publicado">

      <div class="cont-titulo-publicado">

        <img src="<?php echo $ruta_global; ?>vistas/assets/img/paloma.svg" class="ico-paloma">

        <h2 class="titulo-publicado">¡NUEVO DISEÑO <br> PUBLICADO!</h2>

        <div class="card-publicado">

          <div class="card-content-publicado">

            <div class="card-content-izq-publicado">

              <!-- <img src="img/gato.svg" class="ico-gato"> -->
              <div class="ico-gato-div"><img src="<?php echo $ruta_global."vistas/assets/hangers/".$carpeta."/".$ruta_img; ?>" class="ico-gato"></div>

            </div>

            <div class="card-content-der-publicado">

              <h2 class="titulo-publicado-card wp" id="titulo_diseno_publicado"><?php echo $diseno_usuario['nombre_diseno']; ?></h2>

              <p class="subtitulo-modal">INSPIRACION</p>
              <p class="txt-modal-agregar wp" id="inspiracion_diseno_publicado"><?php echo $diseno_usuario['inspiracion']; ?></p>

              <p class="subtitulo-modal">COLECCIÓN</p>
              <p class="txt-modal-agregar wp" id="coleccion_diseno_publicado"><?php echo $diseno_usuario['nombre_coleccion']; ?></p>

              <a class="no_link" id="ir_a_diseno" href="<?php echo $ruta_global.$diseno_usuario['ruta'];?>">
                <button class="btn_urban btn_ir_diseno" type="button"> 
                IR A DISEÑO                
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 27.34 19.6" class="ico-regresar">
                  <defs>
                    <style>
                      .cls-1{fill:none;stroke:#e15c07;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px;}
                    </style>
                  </defs>
                  <title>regresar</title>
                  <g id="Capa_2" data-name="Capa 2">
                    <g id="Capa_1-2" data-name="Capa 1">
                      <polyline class="cls-1" points="9.24 18.09 1.5 10.9 9.8 4.26"/><path class="cls-1" d="M1.5,10.9H18.84a7,7,0,0,0,7-7V1.5"/>
                    </g>
                  </g>
                </svg>
              </button>
            </a>            

            </div>

          </div>

        </div>

      </div>


    </section>
