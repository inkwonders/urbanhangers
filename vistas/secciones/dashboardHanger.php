<?php
  $ruta = $rutas[0];

  $consulta = ControladorDashboard::consultaId("usuarios", $ruta);

  $idUsuario = $consulta["id_usuario"];

?>

<script type='text/javascript'>
  window.__lo_site_id = 312183;
  (function() {
  var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
  wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
  })();
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-159600964-2"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-159600964-2');
</script>

<div id="dash_effect" class="contenedor-dashboard">
  <div class="contenedor-perfil">
  <?php

    $peticion = ControladorDashboard::consultaDashboard("usuarios", $idUsuario);

    foreach($peticion as $key => $value){

      echo "<script> $('title').html('".mb_strtoupper($value['usuario'])." | URBAN HANGERS®'); </script>";

      if(empty($value["banner"])){
          echo "<div class='banner_perfil_vacio'></div>";
      }else{
          echo "<div class='banner-perfil' style='background-image: url(vistas/assets/hangers/".$value['carpeta']."/".$value["banner"]."') alt='banner_".$value["usuario"]."' title='banner_".$value["usuario"]."'> </div>";
      }



  ?>
    <div class="datos-perfil-fx">
      <div class="contenedor-imagen-perfil">
          <?php
          if ($value["modo_registro"] == "directo"){
            if(!empty($value['foto'])){
              $url_foto = "vistas/assets/hangers/".$value['carpeta']."/".$value["foto"];
              echo "<div class='imagen-perfil' style='background-image: url($url_foto)' alt='".$value["usuario"]."' title='".$value["usuario"]."'></div>";
            }else {
              if($value['sexo'] == "h"){
                $url_foto = "vistas/assets/img/icon-usuario-1.svg";
                echo "<div class='imagen-perfil' style='background-image: url($url_foto)' alt='".$value["usuario"]."' title='".$value["usuario"]."'></div>";
              }else {
                $url_foto = "vistas/assets/img/icon-usuario-5.svg";
                echo "<div class='imagen-perfil' style='background-image: url($url_foto)' alt='".$value["usuario"]."' title='".$value["usuario"]."'></div>";
              }
            }
          }else{
            $url_foto = $value["foto"];
            echo "<div class='imagen-perfil' style='background-image: url($url_foto)' alt='".$value["usuario"]."' title='".$value["usuario"]."'></div>";
          }
          ?>
          <!-- <img class="editar-perfil" src="img/icon-editar-perfil.svg" /> -->
      </div>
      <div class="tabla-perfil-fx">

        <div class="celda-tabla-pefil-fx izq-tabla-perfil">

          <div class="grid-insignias">

            <?php

              $insignia = ControladorDashboard::consultaInsignias('insignias');

              foreach($insignia as $key => $valueInsignias){

                $insigniasId = $valueInsignias["id_insignia"];

                $insigniasActivas = ControladorDashboard::consultaInsigniasActivas('insignias_usuarios', $idUsuario, $insigniasId);

            ?>

            <div class="circulo-insignia">

              <?php

                if($insigniasActivas[0] != 0){

                  echo '<img src="vistas/assets/img/'.$valueInsignias["ruta_insignia"].'.svg" class="tooltipped" data-position="bottom" data-tooltip="'.$valueInsignias["nombre_insignia"].'">';

                }else{

                  echo '<img src="vistas/assets/img/'.$valueInsignias["ruta_insignia"].'-gris.svg" class="tooltipped" data-position="bottom" data-tooltip="'.$valueInsignias["nombre_insignia"].'">';

                }

              ?>

            </div>

      <?php }  ?>


          </div>

        </div>

        <div class="celda-tabla-pefil-fx cen-tabla-perfil">

          <span class="titulo-perfil"> <?php echo $value["usuario"];?></span><br /><br />
          <p class="texto-perfil">

            <?php
            $id_pais = $value["pais"];
            $id_estado = $value["estado"];
            $pais = ControladorUsuarios::consultaPaisHanger("paises", $id_pais);
            $estado = ControladorUsuarios::consultaEstadoHanger("estados_bd9aefd2", $id_estado);
            if(!empty($value["estado"]) || !empty($pais)){
              ?>
              <img src="vistas/assets/img/icono-pos.svg" class="icono-pos">
              <?php
            }
            if(empty($value["estado"])){
              echo "";
            }else{
              if($pais[0]=="México"){
                echo $estado[0].", ";
              }else{
                echo $value["estado_extranjero"].", ";
              }
            }

            if(empty($value["pais"])){
              echo "";
            }else{
              echo $pais[0];
            }

            echo "<br>";
            echo "<br class='desktop_no' />";

            if(empty($value["descripcion"])){
              echo "";
            }else{
              echo $value["descripcion"];
            }

            ?>
          </p>
          <br />
          <div class="datos-tabla-perfil">
          <?php

            $noDisenos = ControladorDashboard::consultaNoDisenos('diseno', 'usuarios', $idUsuario);
            $noLikes = ControladorDashboard::consultaNoLikes('diseno', 'votos_diseno', $idUsuario);

          ?>

            <span><b><?php echo $noLikes[0];?></b><br> VOTOS</span>
             <span class="linea-separador-datos"></span>
             <span><b><?php echo $noDisenos[0];?></b> <br> DISEÑOS</span>
          </div>
          <?php
          $redes = ControladorDashboard::consultaRedes('usuarios', $idUsuario);
          foreach($redes as $key => $valueRedes){
          ?>
          <div class="cont-iconos-redes">
            <div class="cont-int-redes">
              <?php if ($valueRedes["behance"] != "") {
                echo "<a href='https://www.behance.net/".$valueRedes["behance"]."' title='Behance' target='_blank'><img src='vistas/assets/img/icono-be.svg' class='icono-red'></a>";
              }
              if ($valueRedes["dribble"] != "") {
                  echo "<a href='https://dribbble.com/".$valueRedes["dribble"]."' title='Dribbble' target='_blank'><img src='vistas/assets/img/icono-internet.svg' class='icono-red'></a>";
                }
                if ($valueRedes["instagram"]!="") {
                  echo "<a href='https://www.instagram.com/".$valueRedes["instagram"]."' title='Instagram' target='_blank'><img src='vistas/assets/img/icono-instagram.svg' class='icono-red'></a>";
                } }}?>
            </div>
          </div>
        </div>
        <?php

        if(isset($_SESSION["tipoUsuario"]) && $_SESSION["tipoUsuario"] == 1){


          if ($idUsuario == $_SESSION["id_usuario"]) {
            echo "<div class='celda-tabla-pefil-fx der-tabla-perfil'>
              <a class='btn-editar-perfil-fx' href='editarPerfil'>EDITAR PERFIL</a>

              <div class='btn-subir-diseno'>
              <a href='subirDiseno' style='text-decoration: none; color:white;'>
                <img src='vistas/assets/img/icono-mas.svg' class='icono-btn-subir-diseno'>
                <p>SUBIR <br> DISEÑO</p>
                </a>
              </div>

            </div>";
          }else{
            echo "<div class='celda-tabla-pefil-fx der-tabla-perfil'> </div>";
          }


        }else{
          echo "<div class='celda-tabla-pefil-fx der-tabla-perfil'> </div>";

        }

        ?>
        </div>


      </div>
<?php

$noDisenos = ControladorDashboard::consultaNoDisenos('diseno', 'usuarios', $idUsuario);

if($noDisenos[0] != 0){

?>
  <div class="disenos_usuario_dashboard">
    <div class="btn_cont_movil">

      <div id="btn_sm_tienda" class="btn_sm btn_tienda inactive_btn">
        <span>TIENDA</span>
      </div>

      <div id="btn_sm_votar" class="btn_sm btn_votar active_btn">
        <span>VOTACIÓN</span>
      </div>

    </div>
    <div class="encabezado-diseño">
      <div id="pestanas_disenos">
        <div class="titulo-inactivo btn_cambio_diseno" id="cambioTienda">
          DISEÑOS EN TIENDA
        </div>
        <div class="titulo btn_cambio_diseno" id="cambioNuevos">
          DISEÑOS EN VOTACIÓN
        </div>
      </div>
      <div class="encabezado-celda encabezado_dashboard evento_filtro_hanger" style="text-align:right">
        <div class="custom-select" id="filtro_dash">
          <select id="select_filtro_dashboard" key="<?php echo base64_encode($idUsuario); ?>">
            <option value="0">FILTRAR POR CATEGORÍA</option>
            <option value="nuevos">NUEVOS</option>
            <option value="mas-votados">MÁS VOTADOS</option>
            <option value="tendencia">TENDENCIA</option>
          </select>
        </div>
      </div>
    </div>

<div id="disenos_tienda_dashboard_movil">
      <section class="tienda_cont">

        <?php

          $slider = ControladorDashboard::consultaSlideFiltro($idUsuario, 'nuevos');

          ?>


        
          <?php
            $contador_disenos=0;
          foreach($slider as $key => $valueSlider){
            $contador_disenos++;
          }if($contador_disenos==0){?>
          <div id="sinresultados_dash" class="cont_no_resultados">
          <img class="img_not_found" src="https://clientes.ink/urbanhangers/vistas/assets/img/not-found-gris.svg">
          <p class="msj_no_resultados" style="font-size: 24px;">No se encontraron resultados.</p>
        </div>
          <?php }else{ ?>
            <!--<div id="disenio_slider" class="slick_slider_disenio">-->

            <?php

              if($contador_disenos > 3){
                  echo "<div id='disenio_slider' class='slick_slider_disenio'>";
              }else{
                  echo "<div id='' class='slick_slider_disenio'>";
              }

            foreach($slider as $key => $valueSlider){
            $contador_disenos++;
            $url_slider = $ruta_global."vistas/assets/hangers/".$value['carpeta']."/".$valueSlider['ruta_img'];
            ?>
            
            <div class='exp_slider_disenio evento_modal_diseno diseno_tres' file="<?php echo base64_encode($value["carpeta"]); ?>" data-inspo="<?php echo $valueSlider["inspiracion"]; ?>" data-nombre="<?php  echo $valueSlider["nombre_diseno"]; ?>" key="<?php echo $valueSlider["id_diseno"]; ?>">

              <section title="<?php echo $valueSlider["nombre_diseno"];?>" alt="<?php echo $valueSlider["nombre_diseno"];?>" class='imagenSlide brd_rd' style='background:url(<?php echo $url_slider;?>)'></section>

                <section class='textoSlide'>
                  <?php
                  echo "<span class='tituloSlide' title='".$valueSlider["nombre_diseno"]."'>".$valueSlider["nombre_diseno"]."</span><br />";
                  ?>

                  <div class="eval_dis_cont">
                    <div class="like_cont">
                      <img src="vistas/assets/img/encanta_btn.svg" alt="like" class="img_sm_mg">
                      <span><?php echo $valueSlider["votos"]; ?></span>
                    </div>

                    <div class="msg_cont">
                      <img src="vistas/assets/img/cmt_btn.svg" alt="comentarios" class="img_sm_mg">
                      <span><?php echo $valueSlider["comentarios"]; ?></span>
                    </div>
                  </div>

                </section>

            </div>
            
            <?php
          }} ?>
          </div>

        

      </section>

    <section class="movil_dis">

      <?php $slider = ControladorDashboard::consultaSlideFiltro($idUsuario, 'nuevos');
      $cont_disenos_slider = 0;
      foreach($slider as $key => $valueSlider){
        $cont_disenos_slider++;
      ?>

      <div class="mv_cont_dis">

        <a class="no_link" href="<?php echo $valueSlider["ruta"]; ?>"><div class="img_dis">
          <?php $url_slider = $ruta_global."vistas/assets/hangers/".$value['carpeta']."/".$valueSlider['ruta_img']; ?>
          <img src="<?php echo $url_slider; ?>" class="fit_img" alt="<?php echo $valueSlider["nombre_diseno"]; ?>" title="<?php echo $valueSlider["nombre_diseno"]; ?>">

        </div></a>

        <div class="txt_cont_img">

          <span class="tit_mov" title="<?php echo $valueSlider["nombre_diseno"]; ?>"><?php echo $valueSlider["nombre_diseno"]; ?></span>

          <div class="eval_dis_cont">

            <div class="like_cont">
              <img src="vistas/assets/img/encanta_btn.svg" alt="like" class="img_sm_mg">
              <span><?php echo $valueSlider["votos"]; ?></span>
            </div>

            <div class="msg_cont">
              <img src="vistas/assets/img/cmt_btn.svg" alt="comentarios" class="img_sm_mg">
              <span><?php echo $valueSlider["comentarios"]; ?></span>
            </div>

          </div>

        </div>

      </div>
      <?php
    }

    if($cont_disenos_slider == 0){
      ?>
      <div id="sinresultados_dash" class="cont_no_resultados">
        <img class="img_not_found" src="<?php echo $ruta_global.'vistas/assets/img/not-found-gris.svg'; ?>">
        <p class="msj_no_resultados" style="font-size: 24px;">No se encontraron resultados.</p>
      </div>
      <?php
    }
    ?>


    </section>
  </div>


    </div>
  </div>

  <div id="modal_detalle" class="modal modal_dashboard_diseno">
    <span class="close_modal_detalle">X</span>
    <div id="datos_modal_dashboard" class="modal-content wh_100 fx_modal_content_dashboard" style="padding:0"></div>
  </div>

<?php
}else{
  ?>
  </div>
  <?php
}
?>
</div>




<script type="text/javascript">
  var ruta_global = "<?php echo $ruta_global; ?>";  
</script>

<script type="text/javascript" src="<?php echo $ruta_global; ?>vistas/assets/js/script_dashboardHanger.js?v=<?php echo time(); ?>"></script>
