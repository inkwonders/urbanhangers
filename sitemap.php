<?php

	// Datos de conexión a la Base de Datos
	$servidor = "localhost";
	$usuario = "root";
	$password = "";
	$db = "urban";

	$ruta = "https://clientes.ink/urbanhangers/";

	// Creo la conexión
	$conexion = new mysqli($servidor, $usuario, $password, $db);

	// Soporte para caracteres y símbolos extraños
	$conexion->set_charset("utf8");

	// Validamos la conexión a la Base de Datos
	if ($conexion->connect_error) {
	    die("Erro en la Conexión a la Base de Datos: " . $conexion->connect_error);
	}

	// Pido el campo 'url' de todos los postres o registros de la tabla 'postres' en la Base de Datos
	$sql_usuarios = "SELECT ruta FROM usuarios WHERE tipo_usuario = 1 AND activo = 1";
	$sql_disenos = "SELECT ruta FROM diseno WHERE activo = 1";
	$sql_colecciones = "SELECT ruta, sk_coleccion FROM colecciones WHERE activo = 1 AND activo = 1";
	$sql_total_disenos  ="SELECT COUNT(*) AS total FROM diseno WHERE activo = 1";

	// Llamo los resultados con los postres o registros
	$resultados_usuarios = $conexion->query($sql_usuarios);
	$resultados_disenos = $conexion->query($sql_disenos);
	$resultados_colecciones = $conexion->query($sql_colecciones);
	$resultados_total_disenos = $conexion->query($sql_total_disenos);

	// Defino mi archivo como XML
	header("Content-Type: text/xml");

	// Inicio la estructura de mi archivo XML
	echo "<?xml version='1.0' encoding='iso-8859-1' ?>" .
	"<urlset
		xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'
      	xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'
      	xsi:schemaLocation='http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd'>";

	echo "<url>
			<loc>".$ruta."</loc>
			<changefreq>weekly</changefreq>
			<priority>"."1.0"."</priority>
		 </url>";

	echo "<url>
			<loc>".$ruta."home</loc>
			<changefreq>weekly</changefreq>
			<priority>"."0.8"."</priority>
		  </url>";

	echo "<url>
			<loc>".$ruta."404</loc>
			<changefreq>weekly</changefreq>
			<priority>"."0.8"."</priority>
		  </url>";

	echo "<url>
			<loc>".$ruta."inicio-sesion</loc>
			<changefreq>weekly</changefreq>
			<priority>"."0.8"."</priority>
		  </url>";

	echo "<url>
			<loc>".$ruta."registro</loc>
			<changefreq>weekly</changefreq>
			<priority>"."0.8"."</priority>
		  </url>";

	if ($resultados_usuarios->num_rows > 0) {

	    while($row = $resultados_usuarios->fetch_assoc()) {

	    	echo "<url>
					<loc>".$ruta."".$row["ruta"]. "</loc>
					<changefreq>weekly</changefreq>
					<priority>"."0.8"."</priority>
				  </url>";
	    }

	}

	if ($resultados_disenos->num_rows > 0) {

	    while($row = $resultados_disenos->fetch_assoc()) {

	    	echo "<url>
					<loc>".$ruta."".$row["ruta"]. "</loc>
					<changefreq>weekly</changefreq>
					<priority>"."0.8"."</priority>
				  </url>";
	    }

	}

	$array_colecciones = array();

	if ($resultados_colecciones->num_rows > 0) {

	    while($row = $resultados_colecciones->fetch_assoc()) {

	    	echo "<url>
					<loc>".$ruta."".$row["ruta"]. "</loc>
					<changefreq>weekly</changefreq>
					<priority>"."0.8"."</priority>
				  </url>";

			array_push($array_colecciones,$row["sk_coleccion"]);
	    }

	}

    while($row = $resultados_total_disenos->fetch_row()) {

    	$total_disenos = $row[0];
    	/*$paginas_home = round(($total_disenos / 20), 0, PHP_ROUND_HALF_UP);*/

			$paginas_home = ceil($total_disenos / 20);

    }

    for($i = 1; $i <= $paginas_home; $i++){
    	echo "<url>
				<loc>".$ruta."home/".$i. "</loc>
				<changefreq>weekly</changefreq>
				<priority>"."0.8"."</priority>
			  </url>";
    }


    for($i = 0; $i < sizeof($array_colecciones); $i++){
    	// $sql_paginas_tipo_coleccion  = "SELECT COUNT(t1.id_diseno) AS total, t2.ruta AS ruta FROM diseno  AS t1, colecciones AS t2 WHERE t1.activo = 1 AND t1.fk_coleccion = '".$array_colecciones[$i]."'";
			$sql_paginas_tipo_coleccion  = "SELECT count(*) AS total, (SELECT ruta FROM colecciones WHERE sk_coleccion = diseno.fk_coleccion LIMIT 1) AS ruta_coleccion FROM diseno WHERE fk_coleccion =  '".$array_colecciones[$i]."'";
			$resultados_paginas_tipo_coleccion = $conexion->query($sql_paginas_tipo_coleccion);

      while($row = $resultados_paginas_tipo_coleccion->fetch_row()) {

	    	$total_disenos_inerno = $row[0];
        $nombre_coleccion = $row[1];
	    	$paginas_home_interno = ceil($total_disenos_inerno / 20);

        for($j = 1; $j <= $paginas_home_interno; $j++){
		    	echo "<url>
						<loc>".$ruta."".$nombre_coleccion."/".$j. "</loc>
						<changefreq>weekly</changefreq>
						<priority>"."0.8"."</priority>
					  </url>";
		    }

	    }

    }

	//print_r($array_colecciones);

 	// Cierre de la etiqueta del archivo XML del Sitemap
	echo "</urlset>";

	// Cierro la conexión a la Base de Datos por seguridad
	$conexion->close();
