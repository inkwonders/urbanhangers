<?php

/*error_reporting(E_ALL);
ini_set('display_errors', '1');*/

header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat, 1 Jul 2000 05:00:00 GMT");

	require_once "controladores/rutas.controlador.php";
	require_once "controladores/plantilla_controlador.php";
	require_once "controladores/disenos.controlador.php";
	require_once "controladores/controlador.usuario.php";
	require_once "controladores/dashboard.controlador.php";
	require_once "controladores/productos.controlador.php";

	require_once "controladores/armado.controlador.php";


	require_once "modelos/diseno.modelo.php";
	require_once "modelos/usuarios.modelo.php";
	require_once "modelos/dashboard.modelo.php";
	require_once "modelos/productos.modelo.php";

	require_once "modelos/armado.modelo.php";

	$plantilla = new ControladorPlantilla();
	$plantilla -> plantilla();
