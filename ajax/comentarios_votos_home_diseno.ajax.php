<?php
require_once "../controladores/rutas.controlador.php";
require_once "../controladores/dashboard.controlador.php";
require_once "../controladores/disenos.controlador.php";

require_once "../modelos/dashboard.modelo.php";
require_once "../modelos/diseno.modelo.php";

  class ComentariosVotosDashboard{

    public function comentarios_dashboard(){
      $id_diseno = $this->id_diseno;
      $id_usuario = $this->id_usuario;
      $sesion = $this->sesion;
      ?>

      <?php
      if($sesion == 'ok'){
        session_start();
      }

                  $comentarios = ControladorDisenos::consultaComentarios('usuarios', $id_diseno);

                  if(empty($comentarios)){
                    echo "<span class='comentario-nombre'>SIN COMENTARIOS</span>";
                  }else{

                    foreach($comentarios as $key => $valueComentarios){

              ?>
              <div class='agregar_comentario'>

                <div class='comentario-perfil'>
                  <?php
                  if($valueComentarios['tipo_usuario'] == 2){
                    if($valueComentarios['sexo'] == "h"){
                      echo '<img class="comentario-img" src="vistas/assets/img/icon-usuario-1.svg">';
                    }else {
                      echo '<img class="comentario-img" src="vistas/assets/img/icon-usuario-5.svg">';
                    }
                  }else if($valueComentarios['tipo_usuario'] == 1){
                    if($valueComentarios['modo_registro'] == "facebook"){
                      echo '<img class="comentario-img" src="'.$valueComentarios['foto'].'">';
                    }else {
                      if(!empty($valueComentarios['foto'])){
                        echo '<img class="comentario-img" src="vistas/assets/hangers/'.$valueComentarios['carpeta'].'/'.$valueComentarios['foto'].'">';
                      }else {
                        if($valueComentarios['sexo'] == "h"){
                          echo '<img class="comentario-img" src="vistas/assets/img/icon-usuario-1.svg">';
                        }else {
                          echo '<img class="comentario-img" src="vistas/assets/img/icon-usuario-5.svg">';
                        }
                      }
                      // echo '<img class="comentario-img" src="vistas/assets/img/'.$valueComentarios['foto'].'">';
                    }
                  }
                  ?>
                </div>

                <div class='comentario-descripcion'>
                  <span class='comentario-nombre negritas'>

                  <?php
                    echo $valueComentarios['usuario'];

                  ?>

                  </span>
                  <br />

                  <span class='comentario-texto'><?php echo $valueComentarios['comentario']; ?></span>

                  <br />
                  <?php
                  $mes = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Agos","Sept","Oct","Nov","Dic");
                  $mes_comentario = $mes[date('n', strtotime($valueComentarios['fecha']))-1];
                  $dia = date("d", strtotime($valueComentarios['fecha']));
                  $año = date("Y", strtotime($valueComentarios['fecha']));
                  $hora = date("g:i A",strtotime($valueComentarios['fecha']));
                  $fecha_hora =  $dia."-".$mes_comentario."-".$año." ".$hora;
                  ?>
                <span class='comentario-tiempo'><?php echo $fecha_hora;?></span>
                </div>
              </div>
            <?php
                    }
                    ?>
                    <?php

                  }
    }

    public function votos_dashboard(){

      $id_diseno = $this->id_diseno;
      $id_usuario = $this->id_usuario;
      $sesion = $this->sesion;

      if($sesion == 'ok'){
        session_start();
      }
      $votos = ControladorDisenos::consultaVotos('votos_diseno', $id_diseno);
      if(isset($_SESSION["iniciarSesion"]) && $_SESSION["iniciarSesion"] == "ok"){
        $id_usuario = $_SESSION['id_usuario'];
        $votado = ControladorDisenos::consultaVotado('votos_diseno', $id_usuario, $id_diseno);
        if($votado[2] == $id_usuario && $votado[1] == 1){ ?>
          <div id='<?php echo base64_encode($id_diseno); ?>' usuario='<?php echo base64_encode($id_usuario); ?>'  class='votado evento_voto identificador_corazon_<?php echo $id_diseno; ?>' status='votado'></div>
          <?php
            }else{
          ?>
          <div id='<?php echo base64_encode($id_diseno); ?>' usuario='<?php echo base64_encode($id_usuario); ?>'  class='no-votado evento_voto identificador_corazon_<?php echo $id_diseno; ?>' status='novotado'></div>
          <?php

            }}else{
          ?>
       <div id='<?php echo  base64_encode($id_diseno); ?>' class='no-votado' onclick='inicio_sesion()'></div>
       <?php

         }
         if($votos[0]>0 && $votos[0]!=1){
          $texto_votos=$votos[0]."<br /> VOTOS";
        }else if($votos[0]==1){
          $texto_votos=$votos[0]."<br /> VOTO";
        }else{
          $texto_votos="SIN <br /> VOTOS";
        }

       ?>
       <p id="numero_votos<?php echo $id_diseno; ?>" numero_de_votos="<?php echo $votos[0]?>" class="conteo"><?php echo $texto_votos;?></p>
      <?php

    }


    public function votos_grid(){
      $idDiseno = $this->id_diseno;
      $idUsuario = $this->id_usuario;
      $sesion = $this->sesion;
      if($sesion == 'ok'){
        session_start();
      }
      $votos = ControladorDisenos::consultaVotos('votos_diseno', $idDiseno);
      if(isset($_SESSION["iniciarSesion"]) && $_SESSION["iniciarSesion"] == "ok"){
        $votado = ControladorDisenos::consultaVotado('votos_diseno', $idUsuario, $idDiseno);
        if($votado[2] == $idUsuario && $votado[1] == 1){ ?>

          <div id_grid='<?php echo  base64_encode($idDiseno); ?>' usuario_grid='<?php echo base64_encode($idUsuario); ?>'  class='votado_grid evento_voto_grid identificador_<?php echo $idDiseno;?>' status_grid='votado'></div>

      <?php
          }else{
      ?>

      <div id_grid='<?php echo base64_encode($idDiseno); ?>' usuario_grid='<?php echo base64_encode($idUsuario); ?>' class='no-votado-grid evento_voto_grid identificador_<?php echo $idDiseno;?>' status_grid='novotado'></div>

      <?php

      }}else{
      ?>

      <div id_grid='<?php echo base64_encode($idDiseno); ?>' class='no-votado-grid' onclick='inicio_sesion()'></div>

      <?php
    }
    ?>
    <p id="numero_votos_grid<?php echo $idDiseno; ?>" numero_de_votos_grid='<?php echo $votos[0]; ?>' class='conteo_grid'>
    <?php
      if($votos[0] > 0 && $votos[0]!=1){
        echo $votos[0]." VOTOS";
      }else if($votos[0] == 1){
        echo $votos[0]." VOTO";
      }else{
        echo "SIN <br /> VOTOS";
      }
    ?>
    </p>
<?php

    }

  }

  $datos = new ComentariosVotosDashboard();

  if( !empty($_POST['id_diseno']) && !empty($_POST['tipo']) && $_POST['tipo'] == 'comentarios' ){

    $datos -> id_diseno = $_POST["id_diseno"];
    $datos -> id_usuario = $_POST["id_usuario"];
    $datos -> sesion = $_POST["sesion"];

    $datos -> comentarios_dashboard();

  }else if(!empty($_POST['id_diseno']) && !empty($_POST['tipo']) && $_POST['tipo'] == 'votos'){

    $datos -> id_diseno = $_POST["id_diseno"];
    $datos -> id_usuario = $_POST["id_usuario"];
    $datos -> sesion = $_POST["sesion"];

    $datos -> votos_dashboard();

  }else if(!empty($_POST['id']) && !empty($_POST['grid'])){
    $datos -> id_diseno = $_POST["id"];
    $datos -> id_usuario = $_POST["id_usuario"];
    $datos -> sesion  = $_POST["sesion"];
    $datos -> votos_grid();

  }else{
    echo "error";
  }
