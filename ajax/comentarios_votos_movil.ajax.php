<?php
require_once "../controladores/rutas.controlador.php";
require_once "../controladores/dashboard.controlador.php";
require_once "../controladores/disenos.controlador.php";

require_once "../modelos/dashboard.modelo.php";
require_once "../modelos/diseno.modelo.php";

  class ComentariosVotosMovil{


    public function votos_home_movil(){
      
      $id_diseno = $this->id_diseno;
      $id_usuario = $this->id_usuario;
      $sesion = $this->sesion;

      if($sesion == 'ok'){
        session_start();
      }

      ?>
      <?php
        if(isset($_SESSION["iniciarSesion"]) && $_SESSION["iniciarSesion"] == "ok"){
          $id_usuario = $_SESSION['id_usuario'];
          $votado = ControladorDisenos::consultaVotado('votos_diseno', $id_usuario, $id_diseno);
          if($votado[2] == $id_usuario && $votado[1] == 1){
            $clase_votado_movil = "votado-mov";
            $status_votado_movil = "votado";
          }else {
            $clase_votado_movil = "no-votado-mov";
            $status_votado_movil = "novotado";
          }
          ?>
          <div id='<?php echo base64_encode($id_diseno); ?>' usuario='<?php echo base64_encode($id_usuario); ?>' class='<?php echo $clase_votado_movil; ?> identificador_corazon_<?php echo $id_diseno; ?> evento_voto_movil' file='1' status='<?php echo $status_votado_movil; ?>'></div>
          <?php
       }else{
         ?>
         <div class='no-votado-mov' onclick='inicio_sesion_movil()'></div>
         <?php
        }

      ?>

      <div class="cont_votos_movil">

        <?php

          $votos = ControladorDisenos::consultaVotos('votos_diseno', $id_diseno);

        ?>

        <span id="num_votos_mov_<?php echo $id_diseno; ?>" numero_de_votos="<?php echo $votos[0]?>" class="txt_tit_mov ln_hg_mov_fx font_fx">
          <?php echo $votos[0] > 0 ?  $votos[0] : "SIN" ?>
        </span>
        <span class="txt_tit_mov ln_hg_mov_fx">VOTOS</span>

      </div>
      <?php

    }

    public function votos_diseno_movil(){
      $id_diseno = $this->id_diseno;
      $id_usuario = $this->id_usuario;
      $sesion = $this->sesion;

      if($sesion == 'ok'){
        session_start();
      }
      $votos = ControladorDisenos::consultaVotos('votos_diseno', $id_diseno);
      ?>
      <div class="cont_votos_movil_dist">
        <span id="num_votos_mov_<?php echo $id_diseno; ?>" numero_de_votos="<?php echo $votos[0]; ?>" class="txt_tit_mov ln_hg_mov_fx font_fx"><?php echo $votos[0] > 0 ?  $votos[0] : "SIN" ?></span>
        <span class="txt_tit_mov ln_hg_mov_fx">VOTOS</span>
      </div>

      <?php
      $votado = ControladorDisenos::consultaVotado('votos_diseno', $id_usuario, $id_diseno);
      if(isset($_SESSION["iniciarSesion"]) && $_SESSION["iniciarSesion"] == "ok"){
        if($votado[2] == $id_usuario && $votado[1] == 1){
          $clase_votado_movil = "votado-mov";
          $status_votado_movil = "votado";
        }else {
          $clase_votado_movil = "no-votado-mov";
          $status_votado_movil = "novotado";
        }
        ?>
        <div id='<?php echo base64_encode($id_diseno); ?>' usuario='<?php echo base64_encode($id_usuario); ?>' class='<?php echo $clase_votado_movil; ?> identificador_corazon_<?php echo $id_diseno; ?> evento_voto_movil' file='2' status='<?php echo $status_votado_movil; ?>'></div>
        <?php
      }else{
        ?>
        <div id='<?php echo base64_encode($id_diseno); ?>' class='no-votado-mov' onclick='inicio_sesion_movil()'></div>
        <?php
      }

    }

    public function comentarios_diseno_movil(){

      $ruta_global = Rutas::ruta_servidor();
      $id_diseno = $this->id_diseno;

      $comentarios = ControladorDisenos::consultaComentarios('usuarios', $id_diseno);
      if(empty($comentarios)){
        echo "<div class='sin_comentarios'><span class='comentario-nombre'>Sin comentarios</span></div>";
      }else{
        foreach($comentarios as $key => $valueComentarios){
        ?>
          <div class="agregar_comentario">

            <div class="comentario-perfil">
              <!-- <img class="comentario-img" src="<?php echo $ruta_global; ?>vistas/assets/hangers/usuario_6_2020-12-17_5fdb97a60b444/perfil_usuario_6_2020-12-17_5fdb8737cc9b9.jpg"> -->
              <?php
              if($valueComentarios['tipo_usuario'] == 2){
                if($valueComentarios['sexo'] == "h"){
                  echo '<img class="comentario-img" src="'.$ruta_global.'vistas/assets/img/icon-usuario-1.svg">';
                }else {
                  echo '<img class="comentario-img" src="'.$ruta_global.'vistas/assets/img/icon-usuario-5.svg">';
                }
              }else if($valueComentarios['tipo_usuario'] == 1){
                if($valueComentarios['modo_registro'] == "facebook"){
                  echo '<img class="comentario-img" src="'.$valueComentarios['foto'].'">';
                }else {
                  if(!empty($valueComentarios['foto'])){
                    echo '<img class="comentario-img" src="'.$ruta_global.'vistas/assets/hangers/'.$valueComentarios['carpeta'].'/'.$valueComentarios['foto'].'">';
                  }else {
                    if($valueComentarios['sexo'] == "h"){
                      echo '<img class="comentario-img" src="'.$ruta_global.'vistas/assets/img/icon-usuario-1.svg">';
                    }else {
                      echo '<img class="comentario-img" src="'.$ruta_global.'vistas/assets/img/icon-usuario-5.svg">';
                    }
                  }
                }
              }
              ?>
            </div>

            <div class="comentario-descripcion">

              <div class="cmt_mov_desc">

                <span class="comentario-nombre-mov negritas"><?php echo $valueComentarios['usuario'];; ?></span>
                <span class="txt_mov_chat"><?php echo $valueComentarios['comentario']; ?></span>
                <?php
                $mes = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Agos","Sept","Oct","Nov","Dic");
                $mes_comentario = $mes[date('n', strtotime($valueComentarios['fecha']))-1];
                $dia = date("d", strtotime($valueComentarios['fecha']));
                $anio = date("Y", strtotime($valueComentarios['fecha']));
                $hora = date("g:i A",strtotime($valueComentarios['fecha']));
                $fecha_hora =  $dia."-".$mes_comentario."-".$anio." ".$hora;
                ?>
                <span class="comentario-tiempo"><?php echo $fecha_hora;?></span>

              </div>

            </div>

          </div>
          <?php
        }
      }

    }

  }

  $datos = new ComentariosVotosMovil();

  if( !empty($_POST['id_diseno']) && $_POST['tipo'] == 'votos' && $_POST['pantalla'] == '2' ){

    $datos -> id_diseno = base64_decode($_POST["id_diseno"]);
    $datos -> id_usuario = base64_decode($_POST["id_usuario"]);
    $datos -> sesion = $_POST["sesion"];

    $datos -> votos_diseno_movil();

  }else if(!empty($_POST['id_diseno']) && $_POST['tipo'] == 'comentarios' && $_POST['pantalla'] == '2' ){

    $datos -> id_diseno = $_POST["id_diseno"];

    $datos -> comentarios_diseno_movil();

  }else if( !empty($_POST['id_diseno']) && $_POST['tipo'] == 'votos' && $_POST['pantalla'] == '1' ){
    $datos -> id_diseno = base64_decode($_POST["id_diseno"]);
    $datos -> id_usuario = base64_decode($_POST["id_usuario"]);
    $datos -> sesion = $_POST["sesion"];

    $datos -> votos_home_movil();
  }else{
    echo "error";
  }
