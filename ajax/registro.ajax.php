<?php


// error_reporting(E_ALL);
// ini_set('display_errors', '1');


  require_once "../controladores/controlador.usuario.php";
  require_once "../modelos/usuarios.modelo.php";

  class Registro{

    public function login_fb(){

      $id_fb = $this->id_fb;
      $ruta_img_fb = $this->ruta_img_fb;
      $nombre_fb = $this->nombre_fb;
      $correo_fb = $this->correo_fb;
      $tipo_usuario = $this->tipo_usuario;

      // echo "id_fb: $id_fb correo_fb: $correo_fb ";

      $consulta_usuario = ControladorUsuarios::consultaUsuarios($id_fb,$correo_fb);

      $usuario_consulta = $consulta_usuario['usuario'];
      $nombre_consulta = $consulta_usuario['nombre'];
      $nombre_facebook = $consulta_usuario['nombre_facebook'];
      $apellido_consulta = $consulta_usuario['apellido'];
      $correo_consulta = $consulta_usuario['correo'];
      $foto_consulta = $consulta_usuario['foto'];
      $banner_consulta = $consulta_usuario['banner'];
      $tipo_usuario_consulta = $consulta_usuario['tipo_usuario'];
      $modo_registro_consulta = $consulta_usuario['modo_registro'];
      $id_usuario_consulta = $consulta_usuario['id_usuario'];
      $id_fb_consulta = $consulta_usuario['id_fb'];
      $ruta_consulta = $consulta_usuario['ruta'];
      $carpeta_consulta = $consulta_usuario['carpeta'];
      $activo_consulta = $consulta_usuario['activo'];

      // print_r($consulta_usuario);

      if(!empty($consulta_usuario) && $modo_registro_consulta === "facebook" && $id_fb_consulta === $id_fb){

            if($correo_consulta != $correo_fb){
              $actualiza_correo = ControladorUsuarios::actualizaCampo("usuarios","correo",$correo_fb,"id_fb",$id_fb);
            }

            if($nombre_facebook != $nombre_fb){
              $actualiza_nombre_facebook = ControladorUsuarios::actualizaCampo("usuarios","nombre_facebook",$nombre_fb,"id_fb",$id_fb);
            }

            $ingresa_usuario = ControladorUsuarios::ingresoUsuarioFB($id_usuario_consulta,$id_fb_consulta,$usuario_consulta,$nombre_consulta,$nombre_facebook,$apellido_consulta,$correo_fb,$ruta_img_fb,$banner_consulta,$tipo_usuario_consulta,$ruta_consulta,$carpeta_consulta,$activo_consulta);

      }else {

        $fecha_registro = date("Y-m-d H:i:s");
        $genera_uuid = ControladorUsuarios::generaUUID();
        $id_usuario = $genera_uuid['uuid'];

        $consulta_usuario_unico = ControladorUsuarios::consultaUsuarioUnico("correo",$correo_fb);

        if(!empty($consulta_usuario_unico['correo'])){
          echo "usuario_duplicado";
        }else{
          $registra_usuario = ControladorUsuarios::registroUsuarioFB($id_usuario,$id_fb,$nombre_fb,$correo_fb,$ruta_img_fb,$tipo_usuario,$fecha_registro);

          if($registra_usuario == "ok"){
            $consulta_usuario = ControladorUsuarios::consultaUsuarios($id_fb,$correo_fb);
            $usuario_consulta = $consulta_usuario['usuario'];
            $nombre_consulta = $consulta_usuario['nombre'];
            $apellido_consulta = $consulta_usuario['apellido'];
            $correo_consulta = $consulta_usuario['correo'];
            $foto_consulta = $consulta_usuario['foto'];
            $banner_consulta = $consulta_usuario['banner'];
            $tipo_usuario_consulta = $consulta_usuario['tipo_usuario'];
            $modo_registro_consulta = $consulta_usuario['modo_registro'];
            $id_usuario_consulta = $consulta_usuario['id_usuario'];
            $id_fb_consulta = $consulta_usuario['id_fb'];
            $ruta_consulta = $consulta_usuario['ruta'];
            $carpeta_consulta = $consulta_usuario['carpeta'];
            $activo_consulta = $consulta_usuario['activo'];
            // echo "$id_usuario_consulta,$id_fb_consulta,$usuario_consulta,$nombre_consulta,$apellido_consulta,$correo_consulta,$ruta_img_fb,$banner_consulta,$tipo_usuario_consulta,$ruta_consulta,$carpeta_consulta";
            $ingresa_usuario = ControladorUsuarios::ingresoUsuarioFB($id_usuario_consulta,$id_fb_consulta,$usuario_consulta,$nombre_consulta,$nombre_facebook,$apellido_consulta,$correo_consulta,$ruta_img_fb,$banner_consulta,$tipo_usuario_consulta,$ruta_consulta,$carpeta_consulta,$activo_consulta);
            echo $ingresa_usuario;
          }else {
            echo "Error al registrar usuario";
          }
          
        }

      }

    }

    public function registro_normal(){
      
      $correo = $this->correo;
      $usuario = $this->usuario;
      $contrasenia = $this->contrasenia;
      $contrasenia_repetir = $this->contrasenia_repetir;
      $genero = $this->genero;
      $tipo_usuario = $this->tipo_usuario;

      if($usuario == "urbanhangersmx" || 
         $usuario == "urbanhangers" || 
         $usuario == "urban" || 
         $usuario == "hangers" || 
         $usuario == "urban-hangers" ||
         $usuario == "mediador" ||
         $usuario == "mediadorurban" ||
         $usuario == "mediadorurbanhangers" ||
         $usuario == "mediador"){
          echo "usuario_reservado";
          exit;
      }

      $consulta_usuario_unico = ControladorUsuarios::consultaUsuarioUnico("usuario",$usuario);

      if(!empty($consulta_usuario_unico['correo'])){
        //existe el usuario
        echo "usuario_duplicado";
      }else {
        // no existe el usuario
        $consulta_usuario = ControladorUsuarios::consultaUsuariosCorreo("correo",$correo);

        $id_usuario_consulta = $consulta_usuario['id_usuario'];
        $correo_consulta = $consulta_usuario['correo'];
        $modo_registro_consulta = $consulta_usuario['modo_registro'];

        if(!empty($consulta_usuario) && $correo_consulta === $correo){
          // existe
          echo $modo_registro_consulta;
        }else {
          // no existe
          $fecha_registro = date("Y-m-d H:i:s");
          $genera_uuid = ControladorUsuarios::generaUUID();
          $id_usuario = $genera_uuid['uuid'];
          $registra_usuario = ControladorUsuarios::registroUsuario($id_usuario,$usuario,$correo,$contrasenia,$contrasenia_repetir,$genero,$tipo_usuario,$fecha_registro);

          if($registra_usuario == "ok"){
            if($tipo_usuario == "1"){
              $id = uniqid();
              $nombre_carpeta = "usuario_".$id_usuario."_".date("Y-m-d");
              $ruta_carpeta_album = "../vistas/assets/hangers/".$nombre_carpeta;

              if(mkdir($ruta_carpeta_album, 0755)){

                $crea_carpeta = ControladorUsuarios::actualizaCampo("usuarios","carpeta",$nombre_carpeta,"id_usuario",$id_usuario);
                echo $crea_carpeta."/".$correo."/".$usuario;

              }else {
                echo "error";
              }

            }else {
              if($registra_usuario == "ok"){
                echo $registra_usuario."/".$correo."/".$usuario;
              }else {
                echo $registra_usuario;
              }
            }
          }else {
            echo $registra_usuario;
          }

        }

      }

    }

    public function actualiza_hanger(){

        session_start();

        $consulta_usuario = ControladorUsuarios::consultaUsuariosVotante($_SESSION['id_usuario'],$_SESSION['correo'],$_SESSION['modo_registro']);

        $id_usuario = $consulta_usuario['id_usuario'];
        $correo = $consulta_usuario['correo'];
        $modo_registro = $consulta_usuario['modo_registro'];

        if(!empty($consulta_usuario['id_usuario'])){
          if($consulta_usuario['tipo_usuario'] == 2){
            // se crea la carpeta para el usuario
            $id = uniqid();
            $nombre_carpeta = "usuario_".$consulta_usuario['id_usuario']."_".date("Y-m-d");
            $ruta_carpeta_album = "../vistas/assets/hangers/".$nombre_carpeta;

            if(mkdir($ruta_carpeta_album, 0755)){

              //se crea la ruta
              $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
              $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
              $ruta = utf8_decode($consulta_usuario['usuario']);
              $ruta = strtr($ruta, utf8_decode($originales), $modificadas);
              $ruta = strtolower($ruta);
              $ruta = str_replace(" ","-",$ruta);

              // actualiza a hanger              

              $actualiza_usuario = ControladorUsuarios::actualizaHanger($id_usuario,$correo,$modo_registro,1,$ruta,$nombre_carpeta);

              if($actualiza_usuario == "ok"){
                echo $ruta;
              }else {
                unlink($ruta_carpeta_album);
                echo $actualiza_usuario;
              }

            }else {
              //fallo crear carpeta
              echo "error";
            }

          }else {
            // ya eres hanger
            echo "error_hanger";
          }
        }else {
          // no existe el usuario
          echo "error";
        }

    }

  }

  $registro = new Registro();

  if(isset($_POST['txt_id_fb'])){

    $registro -> id_fb = $_POST['txt_id_fb'];
    $registro -> ruta_img_fb = $_POST["ruta_img_fb"];
    $registro -> nombre_fb = $_POST['txt_nombre_fb'];
    $registro -> correo_fb = $_POST['txt_correo_fb'];
    $registro -> tipo_usuario = $_POST['tipo_usuario'];
    $registro -> login_fb();

  }

  if(!empty($_POST['txtCorreoRegistro']) && !empty($_POST['txtUsuarioRegistro']) && !empty($_POST['txtPasswordRegistro'])){

    if(
      $_POST["txtUsuarioRegistro"] == "home" ||
      $_POST["txtUsuarioRegistro"] == "error" ||
      $_POST["txtUsuarioRegistro"] == "nuevos" ||
      $_POST["txtUsuarioRegistro"] == "mas-votados" ||
      $_POST["txtUsuarioRegistro"] == "tendencia" ||
      $_POST["txtUsuarioRegistro"] == "salir" ||
      $_POST["txtUsuarioRegistro"] == "editarperfil" ||
      $_POST["txtUsuarioRegistro"] == "subirdiseno" ||
      $_POST["txtUsuarioRegistro"] == "inicio-sesion" ||
      $_POST["txtUsuarioRegistro"] == "registro" ||
      $_POST["txtUsuarioRegistro"] == "verificado" ||
      $_POST["txtUsuarioRegistro"] == "restablecer-contrasena" ||
      $_POST["txtUsuarioRegistro"] == "publicado"
    ){
      echo "error_usuario";
      exit;
    }

    if(strlen($_POST["txtUsuarioRegistro"])>23 || strlen($_POST["txtCorreoRegistro"])>100 ||
       strlen($_POST["txtPasswordRegistro"])>100 || strlen($_POST["txtPasswordRepetir"])>100){
         echo "error";
         exit;
    }

    $registro -> correo = strtolower($_POST['txtCorreoRegistro']);
    $registro -> usuario = strtolower($_POST["txtUsuarioRegistro"]);
    $registro -> contrasenia = $_POST['txtPasswordRegistro'];
    $registro -> contrasenia_repetir = $_POST['txtPasswordRepetir'];
    $registro -> genero = $_POST['sexoRegistro'];
    $registro -> tipo_usuario = $_POST['tipo_usuario'];
    $registro -> registro_normal();

  }

  if(!empty($_POST['correoHanger']) && !empty($_POST['usuarioHanger']) && !empty($_POST['passwordHanger'])){

    if(
      $_POST["usuarioHanger"] == "home" ||
      $_POST["usuarioHanger"] == "error" ||
      $_POST["usuarioHanger"] == "nuevos" ||
      $_POST["usuarioHanger"] == "mas-votados" ||
      $_POST["usuarioHanger"] == "tendencia" ||
      $_POST["usuarioHanger"] == "salir" ||
      $_POST["usuarioHanger"] == "editarperfil" ||
      $_POST["usuarioHanger"] == "subirdiseno" ||
      $_POST["usuarioHanger"] == "inicio-sesion" ||
      $_POST["usuarioHanger"] == "registro" ||
      $_POST["usuarioHanger"] == "verificado" ||
      $_POST["usuarioHanger"] == "restablecer-contrasena" ||
      $_POST["usuarioHanger"] == "publicado"
    ){
      echo "error_usuario";
      exit;
    }

    if(strlen($_POST["usuarioHanger"])>23 || strlen($_POST["correoHanger"])>100 ||
       strlen($_POST["passwordHanger"])>100 || strlen($_POST["passwordHangerRepetir"])>100){
         echo "error";
         exit;
    }

    $registro -> correo = strtolower($_POST['correoHanger']);
    $registro -> usuario = strtolower($_POST["usuarioHanger"]);
    $registro -> contrasenia = $_POST['passwordHanger'];
    $registro -> contrasenia_repetir = $_POST['passwordHangerRepetir'];
    $registro -> genero = $_POST['sexoHanger'];
    $registro -> tipo_usuario = $_POST['tipo_usuario'];
    $registro -> registro_normal();

  }

  if((!empty($_POST['actualiza']) && !empty($_POST['tipo'])) && ($_POST['tipo'] == "hanger" && $_POST['actualiza'] == true) ){

    $registro -> actualiza_hanger();

  }
