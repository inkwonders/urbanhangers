<?php

class Contacto{

    public function correoContactoUsuario(){

      $correo = $this->correo;
      $nombre = $this ->nombre;

      $img_logo = "https://clientes.ink/urbanhangers/vistas/assets/img/correo/logo-urban.png";
      $img_correo = "https://clientes.ink/urbanhangers/vistas/assets/img/correo/correo.png";
      $to = $correo;

      $subject = "Urban Hangers";

      $message = "<html>
                  <head>
                    <title>Gracias por contactar a Urban Hangers</title>
                    </head>
                    <body style='padding:0; margin:0; color:#000;'>
                    <table align='center' border='0' cellpadding='0' cellspacing='0' width='600'>

                    <tr>

                    <td align='center' bgcolor='#FFF' style='padding: 40px 0 30px 0; '>

                    <img src='$img_logo' width='80px' height='80px' style='display: block;' data-imagetype='External' />
                    <span style='font-family: arial; font-size: 30px; font-weight: 900; color: #1A1A1A'>¡Gracias por contactar a Urban Hangers!</span>
                    </td>


                    </tr>

                    <tr>

                    <td align='center' bgcolor='#FFF' style='padding: 40px 0 30px 0;'>

                    <img src='$img_correo' width='180px' height='160px' style='display: block;' data-imagetype='External' /><br /><br /><br />
                    <span style='font-family: arial; font-size: 22px; color: #1A1A1A'>¡<b>$nombre</b> gracias por escribirnos, nosotros nos pondremos en contacto contigo!</span><br /><br /><br />

                    </td>

                    </tr>

                    <tr>
                      <td align='center'>
                      <hr style='color: #ff5c1a; height: 50px; background-color: #ff5c1a;border: none;'>
                      </td>
                    </tr>

                    </table>
                    </body>
                </html>";

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: Urban Hangers<noreply@urbanhangerscommunity.com>' . "\r\n";

        if(mail($to,$subject,$message,$headers)){

            $msj = "ok";

        }else{

            $msj = "error";

        }

      echo $msj;
    }



    public function correoContactoUrban(){
      $correo = $this->correo;
      $nombre = $this ->nombre;
      $mensaje = $this ->mensaje;

      $img_logo = "https://clientes.ink/urbanhangers/vistas/assets/img/correo/logo-urban.png";
      $img_correo = "https://clientes.ink/urbanhangers/vistas/assets/img/correo/correo.png";
      $to = 'hola@urbanhangers.com, eddy@inkwonders.com';

      $subject = "Contacto desde la página web de Urban Hangers";

      $message = "<html>
                  <head>
                    <title>Contacto desde la página web de Urban Hangers</title>
                    </head>
                    <body style='padding:0; margin:0; color:#000;'>
                    <table align='center' border='0' cellpadding='0' cellspacing='0' width='600'>

                    <tr>

                    <td align='center' bgcolor='#FFF' style='padding: 40px 0 30px 0; '>

                    <img src='$img_logo' width='80px' height='80px' style='display: block;' data-imagetype='External' />
                    <span style='font-family: arial; font-size: 45px; font-weight: 900; color: #1A1A1A'>Nuevo contacto desde Urban Hangers</span>
                    </td>


                    </tr>

                    <tr>

                    <td align='center' bgcolor='#FFF'>

                    <img src='$img_correo' width='180px' height='160px' style='display: block;' data-imagetype='External' />
                    <br>
                    <span style='font-family: arial; font-size: 22px; color: #1A1A1A'>Te han contactado con el formulario de URBAN HANGERS</span><br /> <br /><br />


                    </td>

                    </tr>
                    <tr>
                      <td align='left' bgcolor='#FFF'>
                      <span style='font-family: arial; font-size: 22px; color: #1A1A1A'>Nombre: </span><b style='font-family: arial; font-size: 22px; color: #1A1A1A'>  $nombre </b><br />
                      <span style='font-family: arial; font-size: 22px; color: #1A1A1A'>Correo: </span><b style='font-family: arial; font-size: 22px; color: #1A1A1A'>  <a href='mail:$correo' style='text-decoration:none; color:black;'>$correo </a></b><br />
                      <span style='font-family: arial; font-size: 22px; color: #1A1A1A'>Mensaje: </span><b style='font-family: arial; font-size: 22px; color: #1A1A1A'>  $mensaje </b><br /><br /><br />
                      </td>
                    </tr>
                    <tr>
                      <td align='center'>
                      <hr style='color: #ff5c1a; height: 50px; background-color: #ff5c1a;border: none;'>
                      </td>
                    </tr>
                    </table>
                    </body>
                </html>";

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: Urban Hangers<noreply@urbanhangerscommunity.com>' . "\r\n";

        if(mail($to,$subject,$message,$headers)){

            $msj = "ok";

        }else{

            $msj = "error";

        }

      echo $msj;
    }
}

$contacto = new Contacto();


if(isset($_POST["txtNombre"]) && isset($_POST["txtCorreo"]) && isset($_POST["txtMensaje"]) && !empty($_POST["txtNombre"]) && !empty($_POST["txtCorreo"]) && !empty($_POST["txtMensaje"])){
  $contacto -> nombre = $_POST["txtNombre"];
  $contacto -> correo = $_POST["txtCorreo"];
  $contacto -> mensaje = $_POST["txtMensaje"];
  $contacto -> correoContactoUsuario();
  $contacto -> correoContactoUrban();

}else{
  echo "error";
}

?>
