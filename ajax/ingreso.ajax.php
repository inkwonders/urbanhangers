<?php
  require_once "../controladores/controlador.usuario.php";
  require_once "../modelos/usuarios.modelo.php";

  class Ingreso{

    public function login(){
      $email_login = $this->email_login;
      $pass_login = $this->pass_login;
      $recordar = $this->recordar;
      $ruta = $this->ruta;

      $consulta_usuario = ControladorUsuarios::ingresoUsuario($email_login,$pass_login,$recordar,$ruta);
      //echo $ruta;
      echo $consulta_usuario;

    }

  }

  $ingreso = new Ingreso();

  if(isset($_POST['email_login']) && isset($_POST['pass_login'])){


    $ingreso -> email_login = $_POST['email_login'];
    $ingreso -> pass_login = $_POST["pass_login"];
    $ingreso -> recordar = $_POST["recordar"];
    if(isset($_POST["ruta"])|| $_POST["ruta"] != NULL){
      $ingreso -> ruta = $_POST["ruta"];
    }

    $ingreso -> login();

  }
