<?php

require_once "../controladores/dashboard.controlador.php";
require_once "../modelos/dashboard.modelo.php";

require_once "../controladores/controlador.usuario.php";
require_once "../modelos/usuarios.modelo.php";

require_once "../controladores/rutas.controlador.php";

class DisenosDashboard{

  public function consultaDisenos(){

    $ruta_global = Rutas::ruta_servidor();

    $id_usuario = $this->id_usuario;
    $filtro = $this->filtro;
    $tipo = $this->tipo;

    $usuario = ControladorUsuarios::mostrarUsuario('id_usuario',$id_usuario);

    if($tipo == 'votacion'){
      $disenos = ControladorDashboard::consultaSlideFiltro($id_usuario, $filtro);
    }else if($tipo == 'tienda'){
      $disenos = ControladorDashboard::consultaSlideFiltroTienda($id_usuario, $filtro);
    }

    $cont_disenos_slider = 0;

    $disenos_tam = sizeof($disenos);

    if($disenos_tam != 0){
      ?>
      <section class="tienda_cont">
        <div id="disenio_slider" class="slick_slider_disenio">
          <?php
          foreach($disenos as $key => $valueSlider){
            $cont_disenos_slider++;
            ?>
            <div class='exp_slider_disenio <?php echo ($tipo == 'votacion')? 'evento_modal_diseno': 'evento_modal_diseno_tienda'; ?>' tienda="<?php echo $valueSlider['ruta_tienda']; ?>" file="<?php echo base64_encode($usuario["carpeta"]); ?>" data-inspo="<?php echo $valueSlider["inspiracion"]; ?>" data-nombre="<?php echo $valueSlider["nombre_diseno"]; ?>" key="<?php echo $valueSlider["id_diseno"]; ?>">
              <?php $url_slider = $ruta_global."vistas/assets/hangers/".$usuario['carpeta']."/".$valueSlider['ruta_img']; ?>
              <section class='imagenSlide brd_rd' style='background:url(<?php echo $url_slider;?>)'></section>
                <section class='textoSlide'>
                  <span class='tituloSlide'><?php echo $valueSlider["nombre_diseno"]; ?></span><br />
                  <div class="eval_dis_cont">
                    <div class="like_cont">
                      <img src="vistas/assets/img/encanta_btn.svg" alt="like" class="img_sm_mg">
                      <span><?php echo $valueSlider["votos"]; ?></span>
                    </div>

                    <div class="msg_cont">
                      <img src="vistas/assets/img/cmt_btn.svg" alt="comentarios" class="img_sm_mg">
                      <span><?php echo $valueSlider["comentarios"]; ?></span>
                    </div>
                  </div>
                </section>
            </div>
            <?php
          }
          ?>
        </div>
      </section>

    <!--version movil -->
    <section class="movil_dis">

      <?php
      foreach($disenos as $key => $valueSlider){

        if($tipo == 'tienda'){
          $ruta_mov = $valueSlider['ruta_tienda'];
          $attr = 'target="_blank" rel="noopener noreferrer"';
        }else {
          $ruta_mov = $ruta_global.$valueSlider['ruta'];
          $attr = '';
        }
      ?>

      <div class="mv_cont_dis">

        <a class="no_link" href="<?php echo $ruta_mov; ?>" <?php echo $attr; ?>>
          <div class="img_dis"><?php $url_slider = $ruta_global."vistas/assets/hangers/".$usuario['carpeta']."/".$valueSlider['ruta_img']; ?>
            <img src="<?php echo $url_slider; ?>" class="fit_img">
          </div>
        </a>

        <div class="txt_cont_img">

          <span class="tit_mov"><?php echo $valueSlider["nombre_diseno"]; ?></span>

          <div class="eval_dis_cont">

            <div class="like_cont">
              <img src="vistas/assets/img/encanta_btn.svg" alt="like" class="img_sm_mg">
              <span><?php echo $valueSlider["votos"]; ?></span>
            </div>

            <div class="msg_cont">
              <img src="vistas/assets/img/cmt_btn.svg" alt="comentarios" class="img_sm_mg">
              <span><?php echo $valueSlider["comentarios"]; ?></span>
            </div>

          </div>

        </div>

      </div>
    <?php } ?>


    </section>
      <?php
    }else {
      if($cont_disenos_slider == 0){
        ?>
        <div id="sinresultados_dash" class="cont_no_resultados">
          <img class="img_not_found" src="<?php echo $ruta_global.'vistas/assets/img/not-found-gris.svg'; ?>">
          <p class="msj_no_resultados" style="font-size: 24px;">No se encontraron resultados.</p>
        </div>
        <?php
      }
    }


  }

}

$slider = new DisenosDashboard();

if (!empty($_POST["id_usuario"]) && !empty($_POST["filtro"]) && !empty($_POST["tipo"])){
  $slider -> id_usuario = base64_decode($_POST["id_usuario"]);
  $slider -> filtro = $_POST["filtro"];
  $slider -> tipo = $_POST["tipo"];
  $slider -> consultaDisenos();
}

?>
