<?php

require_once "../controladores/disenos.controlador.php";
require_once "../modelos/diseno.modelo.php";

class Likes{

    public function darLike(){

      $idUsuario = $this->idUsuario;
      $idDiseno = $this->idDiseno;

      $like = ControladorDisenos::actualizarVoto('votos_diseno', $idUsuario, $idDiseno);
      echo $like;

    }

    public function quitarLike(){

      $idUsuario = $this->idUsuario;
      $idDiseno = $this->idDiseno;

      $quitarLike = ControladorDisenos::quitarVoto('votos_diseno', $idUsuario, $idDiseno);
      echo $quitarLike;

        
    }

}

$likes = New Likes();

if(isset($_POST["id"]) && !empty($_POST["id"])){

  $likes -> idDiseno = $_POST["id"];
  $likes -> status = $_POST["status"];
  $likes -> idUsuario = $_POST["id_usuario"];


  if($_POST["status"] == "votado"){

    $likes -> quitarLike();

  }else{

    $likes -> darLike();

  }


}


?>
