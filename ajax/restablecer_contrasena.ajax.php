<?php

require_once "../controladores/controlador.usuario.php";
require_once "../modelos/usuarios.modelo.php";

class RestablecerContrasena{

    public function cambiar_contrasena(){

        $nueva_contrasena = $this->nueva_contrasena;
        $contrasena_encriptada = crypt($nueva_contrasena, '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
        $correo = $this->correo;
        $realizar_restablecimiento = ControladorUsuarios::realizarRestablecimientoContrasena('usuarios', $correo, $contrasena_encriptada);
        echo $realizar_restablecimiento;


    }
  }

$restablecer_contrasena = New RestablecerContrasena();

if(isset($_POST['input_restablecer']) && isset($_POST['input_repetir_restablecer']) && !empty($_POST['input_repetir_restablecer']) && !empty($_POST['input_restablecer']) && $_POST['input_restablecer'] == $_POST['input_repetir_restablecer']){

  $restablecer_contrasena -> nueva_contrasena = $_POST['input_restablecer'];
  $restablecer_contrasena -> nueva_contrasena_repetir = $_POST['input_repetir_restablecer'];
  $restablecer_contrasena -> correo = $_POST['correo'];

  $restablecer_contrasena -> cambiar_contrasena();


}else{
  echo "error";
}

?>
