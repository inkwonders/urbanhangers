<?php

require_once "../controladores/controlador.usuario.php";
require_once "../modelos/usuarios.modelo.php";

class Reestablecer{

    public function enviar_correo_reestablecer(){
      $tabla = 'usuarios';
      $correo = $this->correo;
      $email_encriptado = md5($correo);
      $consulta_contrasena = ControladorUsuarios::reestablecerContrasena($tabla, $email_encriptado, $correo);
      echo $consulta_contrasena;
    }
  }

$reestablecer = New Reestablecer();

if(isset($_POST['correoRestablecer'])){

  $reestablecer -> correo = $_POST['correoRestablecer'];
  $reestablecer -> enviar_correo_reestablecer();


}

?>
