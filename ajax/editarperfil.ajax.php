<?php
require_once "../controladores/controlador.usuario.php";
require_once "../modelos/usuarios.modelo.php";

class ActualizarPerfil{

  public function editarPerfil(){
    session_start();
    $query = "";
    $datos_perfil = array();

    $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
    $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
    $ruta = utf8_decode($this->usuario);
    $ruta = strtr($ruta, utf8_decode($originales), $modificadas);
    $ruta = strtolower($ruta);
    $ruta = str_replace(" ","-",$ruta);

    if($this->usuario != NULL){ $datos_perfil["usuario"] = $this->usuario; }
    if($this->usuario != NULL){
      $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
      $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
      $ruta = utf8_decode($this->usuario);
      $ruta = strtr($ruta, utf8_decode($originales), $modificadas);
      $ruta = strtolower($ruta);
      $ruta = str_replace(" ","-",$ruta);
      $datos_perfil["ruta"] = $ruta;
    }

      $estado = $this->estado;
      $id_estado = ControladorUsuarios::consultaIdEstado("estados_bd9aefd2", $estado);

      if($this->nombre != NULL){ $datos_perfil["nombre"] = $this->nombre; }
      if($this->apellido != NULL){ $datos_perfil["apellido"] = $this->apellido; }
      if($this->sexo != NULL){ $datos_perfil["sexo"] = $this->sexo; }
      if($this->pais != NULL){ $datos_perfil["pais"] = $this->pais; }
      if($this->estado != NULL){ $datos_perfil["estado"] = $id_estado[0]; }
      $datos_perfil["estado_extranjero"] = $this->estado_extranjero;
      $datos_perfil["descripcion"] = $this->biografia;
      if($this->behance != NULL){ $datos_perfil["behance"] = $this->behance; }
      if($this->dribble != NULL){ $datos_perfil["dribble"] = $this->dribble; }
      if($this->instagram != NULL){ $datos_perfil["instagram"] = $this->instagram; }
      if($this->contrasena_nueva != NULL){ $datos_perfil["password"] = $this->contrasena_nueva; }
      if($this->key_user != NULL){ $datos_perfil["id_usuario"] = $this->key_user; }
      if($this->foto_perfil != NULL){ $datos_perfil["foto"] = $this->foto_perfil; }
      if($this->banner_perfil != NULL){ $datos_perfil["banner"] = $this->banner_perfil; }



    $cont_array = 0;
    foreach ($datos_perfil as $key => $value) {
      if($key != "id_usuario"){
        $query .= "$key = '$value',";
      }
    }
    $query = trim($query, ',');

    $correo_usuario_unico = ControladorUsuarios::consultaUsuarioUnico('usuario', $this->usuario);

    if(!empty($correo_usuario_unico['correo']) && $correo_usuario_unico['correo'] != $this->correo_user){
        echo "usuario_duplicado";
        exit;
    }

    $contrasena_actual = ControladorUsuarios::revisionContrasena('usuarios', $this->key_user);

    if(($this->contrasena_actual != NULL && $this->contrasena_nueva != NULL) && $contrasena_actual[0] != $this->contrasena_actual){

      echo "error_contrasenia_actual";
      exit;

    }

    if($this->foto_perfil != NULL || $this->banner_perfil != NULL){
      $bool_banner = true;
      $bool_perfil = true;

      $carpeta = $_SESSION['carpeta'];

      if(!empty($_SESSION['avatar'])){

        $ruta_foto = "../vistas/assets/hangers/$carpeta/".$_SESSION['avatar'];

        if($this->foto_perfil != NULL){

          if(!unlink($ruta_foto)){

            $bool_perfil = false;
            $limpia_foto = ControladorUsuarios::vaciaCampo('usuarios', $this->key_user, 'foto', 'id_usuario');
            $_SESSION['avatar'] = NULL;
            // echo "error";
            // exit;

          }else {
            $_SESSION['avatar'] = $this->foto_perfil;
          }

        }

      }else {

        $_SESSION['avatar'] = $this->foto_perfil;

      }

      if(!empty($_SESSION['banner'])){

        $ruta_banner = "../vistas/assets/hangers/$carpeta/".$_SESSION['banner'];

        if($this->banner_perfil != NULL){

          if(!unlink($ruta_banner)){

            $bool_banner = false;
            $limpia_banner = ControladorUsuarios::vaciaCampo('usuarios', $this->key_user, 'banner', 'id_usuario');
            $_SESSION['banner'] = NULL;
            // echo "error";
            // exit;

          }else {

            $_SESSION['banner'] = $this->banner_perfil;

          }

        }

      }else {

        $_SESSION['banner'] = $this->banner_perfil;

      }

      if(!$bool_banner || !$bool_perfil){
        echo "error";
        exit;
      }

    }



    $respuesta = ControladorUsuarios::actualizarDatosPerfil('usuarios', $this->key_user, $query);


    if($respuesta == "ok"){

      $_SESSION['usuario'] = $this->usuario;
      $_SESSION['ruta_perfil'] = $this->usuario;
      $_SESSION['nombre'] = $this->nombre;
      $_SESSION['apellido'] = $this->apellido;
    }

    echo $_SESSION['ruta_perfil'];

    $datos_perfil = null;
    $query = null;

  }

}

$datos_perfil = new ActualizarPerfil();

if(isset($_POST["tipo_operacion"]) && $_POST["tipo_operacion"] == "editar"){

    if(
      $_POST["usuario_editarPerfil"] == "home" ||
      $_POST["usuario_editarPerfil"] == "error" ||
      $_POST["usuario_editarPerfil"] == "nuevos" ||
      $_POST["usuario_editarPerfil"] == "mas-votados" ||
      $_POST["usuario_editarPerfil"] == "tendencia" ||
      $_POST["usuario_editarPerfil"] == "salir" ||
      $_POST["usuario_editarPerfil"] == "editarperfil" ||
      $_POST["usuario_editarPerfil"] == "subirdiseno" ||
      $_POST["usuario_editarPerfil"] == "inicio-sesion" ||
      $_POST["usuario_editarPerfil"] == "registro" ||
      $_POST["usuario_editarPerfil"] == "verificado" ||
      $_POST["usuario_editarPerfil"] == "restablecer-contrasena" ||
      $_POST["usuario_editarPerfil"] == "publicado"
    ){
      echo "error_usuario";
      exit;
    }

    if(!empty($_POST["contrasenaNueva_editarPerfil"]) && !empty($_POST["contrasenaNuevaRepetir_editarPerfil"]) && !empty($_POST["contrasenaActual_editarPerfil"])){
      if(strlen($_POST["contrasenaActual_editarPerfil"])>100 || strlen($_POST["contrasenaNuevaRepetir_editarPerfil"])>100 || strlen($_POST["contrasenaNueva_editarPerfil"])>100){
        echo "error";
        exit;
      }
      $contrasena_actual_encriptada = crypt( $_POST["contrasenaActual_editarPerfil"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
      if($_POST["contrasenaNueva_editarPerfil"] == $_POST["contrasenaNuevaRepetir_editarPerfil"]){
        $contrasena_encriptada = crypt($_POST["contrasenaNueva_editarPerfil"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
      }else {
        echo "error_contrasenias";
        exit;
      }
    }else {
      $contrasena_actual_encriptada = NULL;
      $contrasena_encriptada = NULL;
    }

    // echo preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ-_@]+$/', $_POST["usuario_editarPerfil"])." usuario ";
    // echo preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nombre_editarPerfil"])." nombre ";
    // echo preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["apellido_editarPerfil"])." apellido ";

    if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ-]+$/', $_POST["usuario_editarPerfil"]) &&
       preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nombre_editarPerfil"]) &&
       preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["apellido_editarPerfil"])
      ){

      $datos_perfil -> usuario = strtolower($_POST["usuario_editarPerfil"]);
      $datos_perfil -> nombre = $_POST["nombre_editarPerfil"];
      $datos_perfil -> apellido = $_POST["apellido_editarPerfil"];

    }else {

       echo "error_caracteres";
       exit;

    }

    if(strlen($_POST["usuario_editarPerfil"])>23 || strlen($_POST["nombre_editarPerfil"])>50 ||
       strlen($_POST["apellido_editarPerfil"])>50 || strlen($_POST["biografia_editarPerfil"])>201 ||
       strlen($_POST["behance_editarPerfil"])>30 || strlen($_POST["dribble_editarPerfil"])>30 ||
       strlen($_POST["instagram_editarPerfil"])>30){
         echo "error";
         exit;
    }
    $pais = $_POST["pais_editarPerfil"];

    $id_pais = ControladorUsuarios::consultaIdPais("paises", $pais);

    $datos_perfil -> sexo = $_POST["sexoEditar"];
    $datos_perfil -> pais = $id_pais['id'];
    $datos_perfil -> estado = $_POST["estado_editarPerfil"];
    $datos_perfil -> estado_extranjero = $_POST["estado_extranjero"];
    $datos_perfil -> biografia = $_POST["biografia_editarPerfil"];
    $datos_perfil -> behance = $_POST["behance_editarPerfil"];
    $datos_perfil -> dribble = $_POST["dribble_editarPerfil"];
    $datos_perfil -> instagram = $_POST["instagram_editarPerfil"];
    $datos_perfil -> contrasena_actual = $contrasena_actual_encriptada;
    $datos_perfil -> contrasena_nueva = $contrasena_encriptada;
    $datos_perfil -> key_user = $_POST["key_user"];
    $datos_perfil -> correo_user = $_POST["correo_user"];


    $uniqid = uniqid();

    $carpeta = base64_decode($_POST["cp_user"]);


    $ruta = "../vistas/assets/hangers/$carpeta/";


    if(!empty($_FILES["foto_perfil"]['name'])){  /// validamos foto perfil


      $id_img = uniqid();
      $tipo_imagen = "perfil";
      $archivo_extension = explode(".", $_FILES['foto_perfil']['name']);
      $ext_archivo = end($archivo_extension);
      $nombre_imagen = $tipo_imagen."_usuario_".$_POST['key_user']."_".date("Y-m-d")."_".$id_img.".".$ext_archivo;

      $validar_imagenes_foto = ValidarImagenes($_FILES["foto_perfil"],1500,180,1500,180);

      // echo $_FILES['foto_perfil']['name']." ".$ext_archivo." -- ";
      if($validar_imagenes_foto == "ok"){
        if(move_uploaded_file($_FILES["foto_perfil"]["tmp_name"], $ruta."".$nombre_imagen)){
          $datos_perfil -> foto_perfil = $nombre_imagen;
        }else{
          // $datos_perfil -> foto_perfil = NULL;
          echo "error";
          exit;
        }

      }else {
        echo "error_imagen_perfil";
        exit;
      }



    }else{

      $datos_perfil -> foto_perfil = NULL;



    }

    /***********************************************************************/

    if(!empty($_FILES["banner_perfil"]['name'])){  /// validamos banner perfil

      $id_img = uniqid();
      $tipo_imagen = "banner";
      $archivo_extension = explode(".", $_FILES['banner_perfil']['name']);
      $ext_archivo = end($archivo_extension);
      $nombre_imagen = $tipo_imagen."_usuario_".$_POST['key_user']."_".date("Y-m-d")."_".$id_img.".".$ext_archivo;

      $validar_imagenes_banner = ValidarImagenes($_FILES["banner_perfil"],2080,840,776,169);

      if($validar_imagenes_banner == "ok"){

        if(move_uploaded_file($_FILES["banner_perfil"]["tmp_name"], $ruta.$nombre_imagen)){
          $datos_perfil -> banner_perfil = $nombre_imagen;
        }else{
          // $datos_perfil -> banner_perfil = NULL;
          echo "error";
          exit;
        }

      }else {
        echo "error_imagen_banner";
        exit;
      }

    }else{

      $datos_perfil -> banner_perfil = NULL;

    }

    $datos_perfil->editarPerfil();

}

/*=============================================
VALIDAR IMAGENES
=============================================*/

function ValidarImagenes($file,$maxwidth,$minwidth,$maxheight,$minheight){

  $reporte = null;

  $nombre = $file["name"];
  $tipo = $file["type"];
  $ruta_provisional = $file["tmp_name"];
  $size = $file["size"];

  $dimensiones = getimagesize($ruta_provisional);
  $width = $dimensiones[0];
  $height = $dimensiones[1];

  $extension = explode("/", strtolower($tipo));

  // $ruta_carpeta_album_final = "../vistas/assets/hangers/".$carpeta."/".$tipo_imagen."_".$id_perfil."_".$uniqid.".".$extension[1];
  //
  // $reporte = $ruta_carpeta_album_final;

  if($tipo != 'image/jpeg' && $tipo != 'image/jpg' && $tipo != 'image/png'){
    return "error";
  }else if($size > ((1024*1024)*5)){
    return "error";
  }else if($width > $maxwidth || $height > $maxheight){
    return "error";
  }else if($width < $minwidth || $height < $minheight){
    return "error";
  }else{

      return "ok";

      // if(move_uploaded_file($ruta_provisional, $ruta_carpeta_album_final)){
      //
      //     $reporte = "ok";
      //
      // }else{
      //
      //     $reporte = "error";
      //
      // }

  }

  return $reporte;

}
