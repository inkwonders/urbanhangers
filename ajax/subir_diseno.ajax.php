<?php
error_reporting(0);

require_once "../controladores/controlador.usuario.php";
require_once "../modelos/usuarios.modelo.php";
require_once "../controladores/disenos.controlador.php";
require_once "../modelos/diseno.modelo.php";

class SubirDiseno{

    public function ingresarDiseno(){
      $nombre_diseno = $this->nombre;
      $inspiracion_diseno = $this->inspiracion;
      $usuario_diseno = $this->usuario;
      $coleccion_diseno = $this->coleccion;
      $ruta_diseno = $this->ruta_diseno."-".txtRand();
      $id_diseno = $this->id_diseno;
      date_default_timezone_set('America/Mexico_City');
      $fecha_diseno = date("Y-m-d H:i:s");
      $clave = uniqid();

      // $subirDiseno -> nom_imagen = $nombre_imagen;
      $nombre_imagen = $this->nom_imagen;

      $carpeta_mock_up = $this->ruta_carpeta_mockup;


      $respuesta = ControladorUsuarios::agregarNuevoDiseno('diseno', $nombre_diseno, $ruta_diseno, $usuario_diseno, $inspiracion_diseno, $fecha_diseno, $clave, $id_diseno, $coleccion_diseno,$nombre_imagen,$carpeta_mock_up);
      return $respuesta;

    }

    public function ingresarImagen(){
      $nombre_original= $this->nombre_original_mockup;
      $nombre_imagen= $this->nombre_imagen_mockup;
      $id_diseno = $this->id_diseno;
      $clave = $this->clave_producto_mock;
      date_default_timezone_set('America/Mexico_City');
      $fecha_mock = date("Y-m-d H:i:s");
      $respuesta = ControladorUsuarios::agregarNuevoMockup('mockup',$nombre_imagen,$nombre_original,$id_diseno,$clave,$fecha_mock);

    }

}

$logFile = fopen("subir_deseno.txt", 'a') or die("Error creando archivo");
fwrite($logFile,  "\n".date("d/m/Y H:i:s")."INICIO =================================================================================================================") or die("Error escribiendo en el archivo");

$subirDiseno = new SubirDiseno();

if(isset($_POST["nombre_diseno"]) && isset($_POST["inspiracion_diseno"]) && isset($_POST["coleccion"]) && !empty($_POST["nombre_diseno"]) && !empty($_POST["inspiracion_diseno"]) && !empty($_POST["coleccion"])){

  $arreglo_long = $_POST['arreglo_long'];

  if((strlen($_POST["nombre_diseno"])>40) || (strlen(trim($_POST["inspiracion_diseno"]))>500)){
    echo "error_caracteres";
    exit;
  }

  $para_ruta = str_replace(' ', '', $_POST["nombre_diseno"]);

  if( preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ-]+$/', $para_ruta) ){
    $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
    $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
    $ruta_diseno = utf8_decode($_POST["nombre_diseno"]);
    $ruta_diseno =  trim($ruta_diseno);
    $ruta_diseno = strtr($ruta_diseno, utf8_decode($originales), $modificadas);
    $ruta_diseno = strtolower($ruta_diseno);
    $ruta_diseno = str_replace(" ","-",$ruta_diseno);

  }else {
    echo "error_caracteres";
    exit;
  }

  $genera_uuid = ControladorUsuarios::generaUUID();
  $id_diseno = $genera_uuid['uuid'];

  $subirDiseno -> nombre = $_POST["nombre_diseno"];
  $subirDiseno -> inspiracion = trim($_POST["inspiracion_diseno"]);
  $subirDiseno -> coleccion = base64_decode($_POST["coleccion"]);
  $subirDiseno -> usuario = $_POST["usuario"];
  $subirDiseno -> ruta_diseno = $ruta_diseno;
  $subirDiseno -> id_diseno = $id_diseno;

  $uniqid = uniqid();

  $carpeta = base64_decode($_POST["cp_user"]);
  $ruta = "../vistas/assets/hangers/$carpeta/";

  if(!empty($_FILES["inputdiseno"]['name'])){

    $id_img = uniqid();

    $carpeta_diseno = "diseno_".$id_img;
    $carpeta_mockup = "mockup_".$id_img;

    $tipo_imagen = "diseno";
    $archivo_extension = explode(".", $_FILES['inputdiseno']['name']);
    $ext_archivo = end($archivo_extension);
    $nombre_imagen = $tipo_imagen."_".date("Y-m-d")."_".$id_img.".".$ext_archivo;
    $ruta_imagen = $carpeta_diseno."/".$nombre_imagen;
    $ruta_carpeta_mockup = $carpeta_diseno."/".$carpeta_mockup."/";

    $imagen = $_FILES["inputdiseno"];
    $validar_imagenes_foto = ValidarImagenes($imagen);

    if($validar_imagenes_foto == "ok"){

      fwrite($logFile, "\n".date("d/m/Y H:i:s")." Se valido la imagen a subir") or die("Error escribiendo en el archivo");

      $subirDiseno -> nom_imagen = $ruta_imagen;
      $subirDiseno -> ruta_carpeta_mockup = $ruta_carpeta_mockup;

      $ruta_carpeta_diseno = $ruta.$carpeta_diseno;

      mkdir($ruta_carpeta_diseno, 0755);
      $ruta_mover_diseno = $ruta_carpeta_diseno.'/';

      if($ruta_carpeta_diseno == '' || $ruta_carpeta_diseno == null){ // si no llega nombre de carpeta de diseño paramos todo
        fwrite($logFile, "\n".date("d/m/Y H:i:s")." no llega nombre de carpeta de diseño") or die("Error escribiendo en el archivo");
        echo "error";
        exit;
      }

      fwrite($logFile, "\n".date("d/m/Y H:i:s")." ".$subirDiseno -> nom_imagen." - ".$subirDiseno -> ruta_carpeta_mockup." - ". $ruta_carpeta_diseno." - ".$ruta_mover_diseno) or die("Error escribiendo en el archivo");

      if(move_uploaded_file($_FILES["inputdiseno"]["tmp_name"],  $ruta_mover_diseno.$nombre_imagen)){

        fwrite($logFile, "\n".date("d/m/Y H:i:s")." Se subio el diseño") or die("Error escribiendo en el archivo");

          $bool_subir_diseno = $subirDiseno -> ingresarDiseno();

          if($bool_subir_diseno == 'ok'){

            fwrite($logFile, "\n".date("d/m/Y H:i:s")." Se registro en la base de datos el diseño") or die("Error escribiendo en el archivo");

            $cont_img = 0;

            $bool_todo = base64_encode($id_diseno);

            for ($index_mocks=0; $index_mocks < $arreglo_long ; $index_mocks++) {

              $clave = $_POST['claves_areglo_'.$index_mocks];
              $imagen_mock = $_FILES['imagenes_areglo_'.$index_mocks];


              if (!empty($imagen_mock)){

                fwrite($logFile, "\n".date("d/m/Y H:i:s")." si trae mockups : ".$index_mocks) or die("Error escribiendo en el archivo");

                  $cont_img++;

                  $name_img = $_FILES['imagenes_areglo_'.$index_mocks]['name'];
                  $tipo_img = $_FILES['imagenes_areglo_'.$index_mocks]['type'];
                  $tmp_img = $_FILES['imagenes_areglo_'.$index_mocks]['tmp_name'];
                  $tam_img = $_FILES['imagenes_areglo_'.$index_mocks]['size'];

                  $validar_imagenes_mock = mockUp($tipo_img,$tmp_img,$tam_img);

                  if($validar_imagenes_mock == "ok"){
                    $id_img_mock = uniqid();
                    $tipo_imagen = "mockup";
                    $numero_img = $cont_img;
                    $nombre_original_mockup = $name_img;
                    $archivo_extension = explode(".",$name_img);
                    $ext_archivo = end($archivo_extension);
                    $nombre_imagen_mock = $tipo_imagen."_".$numero_img."_".date("Y-m-d")."_".$id_img.".".$ext_archivo;
                    $ruta_carpeta_mock = $ruta_mover_diseno.$carpeta_mockup.'/';

                    if(!is_dir($ruta_carpeta_mock)){
                      mkdir($ruta_carpeta_mock, 0755);
                      fwrite($logFile, "\n".date("d/m/Y H:i:s")." si crea carpeta de mockups : ".$ruta_carpeta_mock) or die("Error escribiendo en el archivo");
                    }

                    if (move_uploaded_file($tmp_img, $ruta_carpeta_mock.$nombre_imagen_mock) ){
                      fwrite($logFile, "\n".date("d/m/Y H:i:s")." si mueve archivo mockup a carpeta de mockups : ".$ruta_carpeta_mock.$nombre_imagen_mock) or die("Error escribiendo en el archivo");
                      chmod($ruta_carpeta_mock.$nombre_imagen_mock, 0755);
                      $subirDiseno -> id_imagen = $id_img_mock;
                      $subirDiseno -> nombre_original_mockup = $nombre_original_mockup;
                      $subirDiseno -> nombre_imagen_mockup = $nombre_imagen_mock;
                      $subirDiseno -> clave_producto_mock = $clave;
                      $subirDiseno -> ingresarImagen();
                      $bool_todo = base64_encode($id_diseno);
                    }

                  }else{

                    $bool_todo = "error_imagen";

                    $borraMockups = ControladorDisenos::ctrBorrar('mockup','fk_diseno',$id_diseno);
                    $borraDiseno = ControladorDisenos::ctrBorrar('diseno','id_diseno',$id_diseno);

                    if($borraMockups == "ok" && $borraDiseno == "ok"){
                      fwrite($logFile, "\n".date("d/m/Y H:i:s")." se borraron los mockups y los diseños") or die("Error escribiendo en el archivo");
                    }

                    rrmdir($ruta_carpeta_diseno);

                    echo 'error_imagen';
                    exit;

                  }

              }

            }
            echo $bool_todo;
          }else{
            echo 'error';
            exit;
          }

      }else{

        echo "error";
        exit;

      }
    }else {
      // fallo validaciones de imagen
      echo "error_imagen";
      exit;

    }
  }else{
    // no viene imagen
    echo "error";
    exit;
  }

}else{
  echo "error";
}

fwrite($logFile,  "\n".date("d/m/Y H:i:s")."FIN =================================================================================================================") or die("Error escribiendo en el archivo");


function ValidarImagenes($file){
  $reporte = null;

  $nombre = $file["name"];


  $tipo = $file["type"];
  $ruta_provisional = $file["tmp_name"];
  $size = $file["size"];


  $dimensiones = getimagesize($ruta_provisional);
  $width = $dimensiones[0];
  $height = $dimensiones[1];

  $extension = explode("/", strtolower($tipo));

  if($tipo != 'image/png'){
    return "error";
  }else if($size > ((1024*1024)*10)){
    return "error";
  }else if($width > 1000 || $height > 1000){
    return "error";
  }else if($width < 60 || $height < 60){
    return "error";
  }else{
    return "ok";
  }

}


function mockUp($type,$tmp,$size){

  $tipo = $type;
  $ruta_provisional = $tmp;
  $size = $size;

  $dimensiones = getimagesize($ruta_provisional);
  $width = $dimensiones[0];
  $height = $dimensiones[1];

  if($tipo != 'image/jpeg' && $tipo != 'image/jpg' /*&& $tipo != 'image/png'*/){
    return "error";
  }else if($size > ((1024*1024)*10)){
    return "error";
  }else if($width > 920 || $height > 1229){
    return "error";
  }else if($width < 60 || $height < 60){
    return "error";
  }else{
    return "ok";
  }

}

function rrmdir($dir) {
  if (is_dir($dir)) {
    $objects = scandir($dir);
    foreach ($objects as $object) {
      if ($object != "." && $object != "..") {
        if (is_dir($dir. DIRECTORY_SEPARATOR .$object) && !is_link($dir."/".$object))
          rrmdir($dir. DIRECTORY_SEPARATOR .$object);
        else
          unlink($dir. DIRECTORY_SEPARATOR .$object);
      }
    }
    rmdir($dir);
  }
}

function txtRand(){

  $array = array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","0","1","2","3","4","5","6","7","8","9");
  $txt = "";

  $tam_array = sizeof($array);

  $max = $tam_array-1;


  for ($i=0; $i < 3; $i++) {
    $random = rand(0,$max);
    $txt .= $array[$random];
  }

  return $txt;

}
