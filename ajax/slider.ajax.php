<?php
require_once "../controladores/rutas.controlador.php";
require_once "../controladores/disenos.controlador.php";
require_once "../modelos/diseno.modelo.php";


class ajax{
  
  public function consultaSlider(){
    
    $ruta_global = Rutas::ruta_servidor();

    $key = $this->key;
    $carpeta = $this->carpeta;

    $respuesta = ControladorDisenos::consultaSlider('diseno', $key);
    foreach ($respuesta as $key => $value) {
      ?>
      <img id='img_slider_hanger' class='brd_rd' src='<?php echo $ruta_global; ?>vistas/assets/hangers/<?php echo $carpeta."/".$value["ruta_img"]; ?>'>
      <?php
    }

  }


}

$img_slider = new ajax();

if (isset($_POST["consultaSlider"]) && $_POST["consultaSlider"] == "modal") {
  $img_slider -> key = $_POST["key"];
  $img_slider -> carpeta = base64_decode($_POST["carpeta"]);
  $img_slider -> consultaSlider();
}

?>
