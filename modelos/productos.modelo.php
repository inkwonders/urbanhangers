<?php

require_once "conexion.php";

  class productosModelo{

    static public function consultaProductosBase($tabla){

      $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY orden ASC");

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

    }

  }
