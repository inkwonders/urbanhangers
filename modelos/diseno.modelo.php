<?php

  require_once "conexion.php";

  class disenosModelo{

    static public function consultaDiseno($tabla1, $tabla2, $tabla3, $filtro, $limitante){

      switch ($filtro) {
        case 'mas-votados':

          $query = "SELECT u.ruta AS ruta, u.modo_registro AS
                  modo_registro, d.nombre_diseno AS nombre_diseno, d.inspiracion AS inspiracion,
                  u.nombre AS nombre, u.foto AS foto, d.id_diseno AS id_diseno, u.id_usuario AS
                  id_usuario, (SELECT count(*) FROM votos_diseno WHERE id_diseno = d.id_diseno AND
                  voto_activo = 1) AS numvotos, u.carpeta AS carpeta, u.sexo AS sexo, d.ruta_img AS ruta_img, d.ruta AS
                  ruta_diseno FROM $tabla1 AS d INNER JOIN $tabla2 AS u ON d.id_usuario =
                  u.id_usuario WHERE d.id_usuario = u.id_usuario AND d.diseno_en_venta = 0 AND d.activo = 1 AND u.activo = 1 ORDER BY numvotos DESC, d.fecha_diseno DESC LIMIT $limitante,20";
          break;

        case 'nuevos':

          $query = "SELECT u.ruta AS ruta, u.modo_registro AS
                  modo_registro, d.nombre_diseno AS nombre_diseno, d.inspiracion AS inspiracion,
                  u.nombre AS nombre, u.foto AS foto, u.carpeta AS carpeta, d.id_diseno AS
                  id_diseno, u.id_usuario AS id_usuario, u.ruta AS ruta, d.ruta_img AS ruta_img, u.sexo AS sexo, d.ruta AS
                  ruta_diseno FROM $tabla1 AS d, $tabla2 AS u WHERE d.id_usuario = u.id_usuario AND d.diseno_en_venta = 0 AND d.activo = 1 AND u.activo = 1
                  ORDER BY d.fecha_diseno DESC LIMIT $limitante,20";
          break;

        case 'tendencia':

          $query = "SELECT u.ruta AS ruta, u.modo_registro AS
                  modo_registro, d.nombre_diseno AS nombre_diseno, d.inspiracion AS inspiracion,
                  u.nombre AS nombre, u.foto AS foto, u.carpeta AS carpeta, d.id_diseno AS
                  id_diseno, u.id_usuario AS id_usuario, u.ruta AS ruta, u.sexo AS sexo, d.ruta_img AS ruta_img, d.ruta AS
                  ruta_diseno FROM $tabla1 AS d, $tabla2 AS u WHERE d.id_usuario = u.id_usuario AND d.diseno_en_venta = 0 AND d.activo = 1 AND u.activo = 1
                    ORDER BY d.fecha_tendencia DESC, d.tendencia, d.fecha_diseno DESC LIMIT $limitante,20";
          break;

        default:

          $query = "SELECT u.ruta AS ruta, u.modo_registro AS
                  modo_registro, d.nombre_diseno AS nombre_diseno, d.inspiracion AS inspiracion,
                  u.nombre AS nombre, u.foto AS foto, u.carpeta AS carpeta, d.id_diseno AS
                  id_diseno, u.id_usuario AS id_usuario, u.ruta AS ruta, u.sexo AS sexo, d.ruta_img AS ruta_img, d.ruta AS
                  ruta_diseno FROM $tabla1 AS d, $tabla2 AS u WHERE d.id_usuario = u.id_usuario AND d.diseno_en_venta = 0 AND d.activo = 1 AND u.activo = 1 AND d.fk_coleccion = '$filtro'
                  ORDER BY d.fecha_diseno DESC LIMIT $limitante,20";
          break;
      }

      // echo $query;

      $stmt = Conexion::conectar()->prepare($query);

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaImgSlider($tabla1, $idDiseno){
      $stmt = Conexion::conectar()->prepare("SELECT ruta_img, id_diseno FROM img_diseno WHERE id_diseno = '$idDiseno'");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaVotos($tabla1, $idDiseno){

      $stmt = Conexion::conectar()->prepare("SELECT COUNT(*) AS contador FROM votos_diseno WHERE id_diseno = '$idDiseno' AND voto_activo = 1");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaComentarios($tabla1, $idDiseno){
      // echo "SELECT t1.modo_registro AS modo_registro, t1.nombre AS nombre, t2.comentario AS comentario, t1.foto AS foto, t2.fecha AS fecha,
      // t2.id_diseno AS id_diseno, t1.sexo AS sexo, t1.tipo_usuario AS tipo_usuario, t1.carpeta AS carpeta, t1.usuario AS usuario FROM $tabla1 AS t1, comentarios AS t2 WHERE t1.id_usuario=t2.id_usuario
      // AND t2.id_diseno='$idDiseno'";
      $stmt = Conexion::conectar()->prepare("SELECT t1.modo_registro AS modo_registro, t1.nombre AS nombre, t2.comentario AS comentario, t1.foto AS foto, t2.fecha AS fecha, t2.id_diseno AS id_diseno, t1.sexo AS sexo, t1.tipo_usuario AS tipo_usuario, t1.carpeta AS carpeta, t1.usuario AS usuario FROM $tabla1 AS t1, comentarios AS t2 WHERE t1.id_usuario= t2.id_usuario AND t2.id_diseno='$idDiseno' ORDER BY fecha DESC");

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaSlider($tabla1, $key){

      $stmt = Conexion::conectar()->prepare("SELECT ruta_img FROM $tabla1 WHERE id_diseno = '$key'");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaRutaProducto($tabla, $item, $valor){

      $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

  		$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

  		$stmt -> execute();

  		return $stmt -> fetch();

  		$stmt -> close();

  		$stmt = null;

    }


    static public function consultaVotado($tabla, $idUsuario, $idDiseno){

      $stmt = Conexion::conectar()->prepare("SELECT COUNT(id_usuario), voto_activo, id_usuario FROM $tabla WHERE id_usuario = '$idUsuario' AND id_diseno = '$idDiseno'");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;
    }

    static public function insertarVoto($tabla, $idUsuario, $idDiseno){

      $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(id_diseno, id_usuario, voto_activo) VALUES ('$idDiseno', '$idUsuario', '1')");

      if($stmt->execute()){

  			return "ok";

  		}else{

  			return "error";

  		}

  		$stmt->close();
  		$stmt = null;
    }


    static public function regresarVoto($tabla, $idUsuario, $idDiseno){

      $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET voto_activo = '1' WHERE id_usuario = '$idUsuario' AND id_diseno = '$idDiseno'");

      if($stmt->execute()){

  			return "ok";

  		}else{

  			return "error";

  		}

  		$stmt->close();
  		$stmt = null;
    }



    static public function actualizarVoto($tabla, $idUsuario, $idDiseno){

      $stmt = Conexion::conectar()->prepare("SELECT COUNT(id_usuario) AS total, voto_activo, id_usuario FROM $tabla WHERE id_usuario = '$idUsuario' AND id_diseno = '$idDiseno'");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;



    }


    static public function quitarVoto($tabla, $idUsuario, $idDiseno){

      $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET voto_activo = '0' WHERE id_usuario = '$idUsuario' AND id_diseno = '$idDiseno'");

      if($stmt->execute()){

  			return "ok";

  		}else{

  			return "error";

  		}

  		$stmt->close();
  		$stmt = null;
    }

    static public function registrarComentario($tabla, $idUsuario, $idDiseno, $comentario, $fecha){

      $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(id_usuario, id_diseno, comentario, fecha) VALUES ('$idUsuario', '$idDiseno', '$comentario', '$fecha')");

      if($stmt->execute()){

  			return "ok";

  		}else{

  			return "error";

  		}

  		$stmt->close();
  		$stmt = null;
    }


    static public function totalDisenos($tabla,$condicion){      

      $stmt = Conexion::conectar()->prepare("SELECT COUNT(*) AS total FROM $tabla $condicion");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function disenoPublicado($tabla, $id_diseno){
      $stmt = Conexion::conectar()->prepare("SELECT nombre_diseno, inspiracion, ruta, id_diseno, (SELECT nombre_coleccion FROM colecciones WHERE sk_coleccion = diseno.fk_coleccion) AS nombre_coleccion,id_usuario,ruta_img FROM $tabla WHERE id_diseno = '$id_diseno' ORDER BY fecha_diseno DESC LIMIT 1");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;
    }

    static public function insertarImagen($tabla, $id_diseno, $ruta_img){
      $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(id_diseno, ruta_img) VALUES ('$id_diseno','$ruta_img')");

      if($stmt->execute()){

        return 'ok';

      }else{

        return 'error';

      }

      $stmt->close();
      $stmt = null;
    }

    static public function imgDisenoPublicado($tabla, $id_diseno){
      $stmt = Conexion::conectar()->prepare("SELECT ruta_img FROM $tabla WHERE id_diseno = '$id_diseno'");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;
    }

    static public function coleccionesDiseno($tabla){

      $stmt = Conexion::conectar()->prepare("SELECT sk_coleccion, nombre_coleccion, ruta FROM $tabla WHERE activo = 1 ORDER BY nombre_coleccion ASC");

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;
    }

    static public function datosMetas($tabla, $ruta){

      $stmt = Conexion::conectar()->prepare("SELECT id_usuario,nombre_diseno,inspiracion,ruta_img,(SELECT carpeta FROM usuarios WHERE usuarios.id_usuario = diseno.id_usuario LIMIT 0,1) AS carpeta,ruta FROM $tabla WHERE ruta = '$ruta' AND activo = 1");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function mdlBorrar($tabla, $campo, $valor){

      $stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE $campo = '$valor'");

      if($stmt->execute()){

        return 'ok';

      }else{

        return 'error';

      }
      
      $stmt = null;

    }

  }
