<?php

  require_once "conexion.php";

  class ModeloUsuarios{

    /*=============================================
  	REGISTRO DE USUARIO
  	=============================================*/

  	static public function registroUsuario($tabla, $datos){

  		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(id_usuario, usuario, foto, tipo_usuario, correo, password, modo_registro, sexo, email_encriptado, verificacion, fecha_registro, ruta, activo) VALUES (:id_usuario, :usuario, :foto, :tipo_usuario, :email, :password, :modo_registro, :sexo, :emailEncriptado, :verificacion, :fecha_registro, :ruta, 1)");

      $stmt->bindParam(":id_usuario", $datos["id_usuario"], PDO::PARAM_STR);
  		$stmt->bindParam(":usuario", $datos["usuario"], PDO::PARAM_STR);
      $stmt->bindParam(":foto", $datos["foto"], PDO::PARAM_STR);
      $stmt->bindParam(":tipo_usuario", $datos["tipo_usuario"], PDO::PARAM_STR);
      $stmt->bindParam(":email", $datos["email"], PDO::PARAM_STR);
  		$stmt->bindParam(":password", $datos["password"], PDO::PARAM_STR);
  		$stmt->bindParam(":modo_registro", $datos["modo_registro"], PDO::PARAM_STR);
      $stmt->bindParam(":sexo", $datos["sexo"], PDO::PARAM_STR);
  		$stmt->bindParam(":verificacion", $datos["verificacion"], PDO::PARAM_INT);
  		$stmt->bindParam(":emailEncriptado", $datos["emailEncriptado"], PDO::PARAM_STR);
      $stmt->bindParam(":fecha_registro", $datos["fecha_registro"], PDO::PARAM_STR);
      $stmt->bindParam(":ruta", $datos["ruta"], PDO::PARAM_STR);

  		if($stmt->execute()){

  			return "ok";

  		}else{

  			return "error_modelo";

  		}

  		$stmt->close();
  		$stmt = null;

  	}

    /*=============================================
  	MOSTRAR USUARIOS
  	=============================================*/

  	static public function mostrarUsuarios($tabla, $correo){

      $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE correo = '$correo'");

      $stmt -> execute();

      return $stmt -> fetch();

  		$stmt -> close();

  		$stmt = null;

  	}

    static public function consultaUsuarioDashboard($tabla, $item, $valor){

      $stmt = Conexion::conectar()->prepare("SELECT tipo_usuario, ruta, carpeta FROM $tabla WHERE $item = :$item AND activo = 1");

  		$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

  		$stmt -> execute();

  		return $stmt -> fetch();

  		$stmt -> close();

  		$stmt = null;

  	}

    static public function consultaIdUsuario($tabla, $ruta){

      $stmt = Conexion::conectar()->prepare("SELECT id_usuario FROM $tabla WHERE ruta = '$ruta'");

      $stmt -> execute();

      return $stmt -> fetchAll();

  		$stmt -> close();

  		$stmt = null;

	  }

	  static public function consultaUsuarioProducto($tabla1, $tabla2, $idDiseno){


		  $stmt = Conexion::conectar()->prepare("SELECT t1.nombre AS nombre, t1.foto AS foto, t1.ruta AS ruta, t1.sexo, t1.modo_registro, t1.carpeta, t1.usuario, t1.id_usuario FROM $tabla1 AS t1,  $tabla2 AS t2 WHERE t2.id_diseno = :id_diseno AND t1.id_usuario = t2.id_usuario");

      $stmt -> bindParam(":id_diseno", $idDiseno, PDO::PARAM_STR);

		  $stmt -> execute();

      return $stmt -> fetch();

			$stmt -> close();

			$stmt = null;

		}

    static public function consultaUsuarioExistente($tabla, $item1, $item2, $id_fb, $correo){

      $stmt = Conexion::conectar()->prepare("SELECT id_usuario,id_fb,nombre,nombre_facebook,apellido,correo,foto,banner,tipo_usuario,modo_registro,ruta,carpeta,usuario,activo FROM $tabla WHERE $item1 = :$item1");

      $stmt -> bindParam(":".$item1, $id_fb, PDO::PARAM_STR);

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaUsuarioExistenteCorreo($tabla, $item1, $correo, $condicion){

      if(!empty($condicion)){
        $stmt = Conexion::conectar()->prepare("SELECT id_usuario,correo,modo_registro FROM $tabla WHERE $item1 = :$item1 $condicion");
      }else {
        $stmt = Conexion::conectar()->prepare("SELECT id_usuario,correo,modo_registro FROM $tabla WHERE $item1 = :$item1");
      }


      $stmt -> bindParam(":".$item1, $correo, PDO::PARAM_STR);

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaUsuarioUnico($tabla, $item1, $usuario){

      $stmt = Conexion::conectar()->prepare("SELECT correo FROM $tabla WHERE $item1 = :$item1");

      $stmt -> bindParam(":".$item1, $usuario, PDO::PARAM_STR);

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function registroUsuarioFB($tabla, $datos){

      $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(id_usuario, usuario, nombre_facebook, foto, tipo_usuario, correo, modo_registro, verificacion, id_fb, fecha_registro,ruta,carpeta,activo) VALUES (:id_usuario, :usuario, :nombre, :foto, :tipo_usuario, :email, :modo_registro, :verificacion, :id_fb, :fecha_registro, :ruta, :carpeta, 1)");

      $stmt->bindParam(":id_usuario", $datos["id_usuario"], PDO::PARAM_STR);
  		$stmt->bindParam(":usuario", $datos["usuario"], PDO::PARAM_STR);
      $stmt->bindParam(":nombre", $datos["nombre_facebook"], PDO::PARAM_STR);
      $stmt->bindParam(":foto", $datos["foto"], PDO::PARAM_STR);
      $stmt->bindParam(":tipo_usuario", $datos["tipo_usuario"], PDO::PARAM_STR);
      $stmt->bindParam(":email", $datos["email"], PDO::PARAM_STR);
  		$stmt->bindParam(":modo_registro", $datos["modo_registro"], PDO::PARAM_STR);
  		$stmt->bindParam(":verificacion", $datos["verificacion"], PDO::PARAM_INT);
      $stmt->bindParam(":id_fb", $datos["id_fb"], PDO::PARAM_STR);
      $stmt->bindParam(":fecha_registro", $datos["fecha_registro"], PDO::PARAM_STR);
      $stmt->bindParam(":ruta", $datos["ruta"], PDO::PARAM_STR);
      $stmt->bindParam(":carpeta", $datos["carpeta"], PDO::PARAM_STR);

  		if($stmt->execute()){

  			return "ok";

  		}else{

  			return "error_modelo";

  		}

  		$stmt->close();
  		$stmt = null;

    }

    static public function actualizarDatosPerfil($tabla, $id_usuario, $query_update){

      $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $query_update WHERE id_usuario = :id_usuario");

      $stmt -> bindParam(":id_usuario", $id_usuario, PDO::PARAM_STR);

      if($stmt->execute()){

  			return "ok";

  		}else{

  			return "error";

  		}

  		$stmt->close();
  		$stmt = null;

    }

    static public function revisionContrasena($tabla, $id_usuario){

      $stmt = Conexion::conectar()->prepare("SELECT password FROM $tabla WHERE id_usuario = :id_usuario");

      $stmt->bindParam(":id_usuario", $id_usuario, PDO::PARAM_INT);

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function actualizaVerficacion($ee){

      $stmt = Conexion::conectar()->prepare("UPDATE usuarios SET verificacion = 0 WHERE email_encriptado = :email_encriptado");

      $stmt->bindParam(":email_encriptado", $ee, PDO::PARAM_STR);

      if($stmt->execute()){

        return "ok";

      }else{

        return "error";

      }

      $stmt->close();
      $stmt = null;

    }

    /*** agregar cookie de sesion **/

    static public function agregarCookie($cookie, $id_usuario, $tabla){

      $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET cookie = :cookie WHERE id_usuario = :id_usuario");

      $stmt->bindParam(":cookie", $cookie, PDO::PARAM_STR);
      $stmt->bindParam(":id_usuario", $id_usuario, PDO::PARAM_STR);

      if($stmt->execute()){

        return "ok";

      }else{

        return "error";

      }

      $stmt->close();
      $stmt = null;

    }

    static public function verficarCookie($id, $cookie, $tabla, $item1, $item2){

      $stmt = Conexion::conectar()->prepare("SELECT id_usuario, correo FROM $tabla WHERE $item1 = :$item1 AND $item2 = :$item2");

      $stmt -> bindParam(":".$item1, $id, PDO::PARAM_INT);
      $stmt -> bindParam(":".$item2, $cookie, PDO::PARAM_STR);

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function mostrarUsuariosCookie($id, $cookie, $tabla){

      $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE id_usuario = '$id' AND cookie = '$cookie'");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaUsuariosVotante($tabla, $item1, $item2, $item3, $id_usuario, $correo, $modo_registro){

      $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item1 = :$item1 AND $item2 = :$item2 AND $item3 = :$item3 ");

      $stmt -> bindParam(":".$item1, $id_usuario, PDO::PARAM_STR);
      $stmt -> bindParam(":".$item2, $correo, PDO::PARAM_STR);
      $stmt -> bindParam(":".$item3, $modo_registro, PDO::PARAM_STR);

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function actualizaHanger($tabla,$id_usuario,$correo,$modo_registro,$tipo_usuario,$ruta,$carpeta,$item1,$item2,$item3,$item4,$item5,$item6){

      $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $item4 = :$item4, $item5 = :$item5, $item6 = :$item6  WHERE $item1 = :$item1 AND $item2 = :$item2 AND $item3 = :$item3");

      $stmt -> bindParam(":".$item1, $id_usuario, PDO::PARAM_STR);
      $stmt -> bindParam(":".$item2, $correo, PDO::PARAM_STR);
      $stmt -> bindParam(":".$item3, $modo_registro, PDO::PARAM_STR);
      $stmt -> bindParam(":".$item4, $tipo_usuario, PDO::PARAM_INT);
      $stmt -> bindParam(":".$item5, $ruta, PDO::PARAM_STR);
      $stmt -> bindParam(":".$item6, $carpeta, PDO::PARAM_STR);


      if($stmt->execute()){

        return "ok";

      }else{

        return "error";

      }

      $stmt->close();
      $stmt = null;

    }

    static public function vaciaCampo($tabla,$campo_vaciar,$identificador,$valor_identificador){

      $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $item_vaciar = NULL WHERE $identificador = :$identificador ");

      $stmt -> bindParam(":".$identificador, $valor_identificador, PDO::PARAM_STR);

      if($stmt->execute()){

        return "ok";

      }else{

        return "error";

      }

      $stmt->close();
      $stmt = null;

    }

    static public function actualizaCampo($tabla,$campo_actualizar,$valor_insertar,$identificador,$valor_identificador){

      $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $campo_actualizar = :$campo_actualizar WHERE $identificador = :$identificador ");

      $stmt -> bindParam(":".$campo_actualizar, $valor_insertar, PDO::PARAM_STR);
      $stmt -> bindParam(":".$identificador, $valor_identificador, PDO::PARAM_STR);

      if($stmt->execute()){

        return "ok";

      }else{

        return "error";

      }

      $stmt->close();
      $stmt = null;

    }

    static public function generaUUID(){

      $stmt = Conexion::conectar()->prepare("SELECT UUID() AS uuid");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function reestablecerContrasena($tabla, $email_encriptado, $correo){

      $stmt = Conexion::conectar()->prepare("SELECT password FROM $tabla WHERE email_encriptado = '$email_encriptado'");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaUsuariosContrasenaOlvidada($tabla, $correo){


      $stmt = Conexion::conectar()->prepare("SELECT id_usuario,correo,modo_registro FROM $tabla WHERE email_encriptado = '$correo' ");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function realizarRestablecimientoContrasena($tabla, $correo, $contrasena_encriptada){

      $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET password = :password WHERE email_encriptado = :correo");

      $stmt->bindParam(":correo", $correo, PDO::PARAM_STR);
      $stmt->bindParam(":password", $contrasena_encriptada, PDO::PARAM_STR);

      if($stmt->execute()){

        return "ok";

      }else{

        return "error";

      }

      $stmt->close();
      $stmt = null;

    }

    static public function agregarNuevoDiseno($tabla, $nombre_diseno, $ruta_diseno, $usuario_diseno, $inspiracion_diseno, $fecha_diseno, $clave, $id_diseno, $coleccion_diseno,$nombre_imagen,$carpeta_mock_up){


      $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(id_diseno, nombre_diseno, id_usuario, inspiracion, fecha_diseno, diseno_en_venta, ruta, clave_diseno, ruta_img, fk_coleccion, activo, tendencia,carpeta_mockup) VALUES (:id_diseno,'$nombre_diseno', :usuario_diseno, '$inspiracion_diseno', '$fecha_diseno', '0', '$ruta_diseno', '$clave' , '$nombre_imagen', :coleccion_diseno, 1,0,'$carpeta_mock_up')");

      $stmt->bindParam(":id_diseno", $id_diseno, PDO::PARAM_STR);
      $stmt->bindParam(":usuario_diseno", $usuario_diseno, PDO::PARAM_STR);
      $stmt->bindParam(":coleccion_diseno", $coleccion_diseno, PDO::PARAM_STR);

      if($stmt->execute()){

        return 'ok';

      }else{
        return 'error';

      }

      $stmt->close();
      $stmt = null;

    }

    static public function agregarNuevoMockup($tabla, $nombre_archivo, $nombre_original, $fk_diseno,$clave,$fecha_mock){


      $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(`sk_mockup`, `nombre_archivo`, `nombre_original`, `fk_diseno`, `fk_producto`, `fecha_creacion`) VALUES (UUID(),'$nombre_archivo','$nombre_original',:fk_diseno,:clave, '$fecha_mock' )");
      $stmt->bindParam(":fk_diseno", $fk_diseno, PDO::PARAM_STR);
      $stmt->bindParam(":clave", $clave, PDO::PARAM_STR);

      if($stmt->execute()){

        return 'ok';

      }else{
        return 'error ';

      }

      $stmt->close();
      $stmt = null;

    }

    static public function consultaPaises($tabla){


      $stmt = Conexion::conectar()->prepare("SELECT nombre_pais FROM $tabla ORDER BY nombre_pais = 'México' DESC, nombre_pais ASC");

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaEstados($tabla){


      $stmt = Conexion::conectar()->prepare("SELECT estado FROM $tabla ORDER BY estado ASC");

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaIdPais($tabla, $pais){


      $stmt = Conexion::conectar()->prepare("SELECT id FROM $tabla WHERE nombre_pais = '$pais'");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaIdEstado($tabla, $estado){


      $stmt = Conexion::conectar()->prepare("SELECT sk_estado FROM $tabla WHERE estado = '$estado'");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaPaisHanger($tabla, $id_pais){


      $stmt = Conexion::conectar()->prepare("SELECT nombre_pais FROM $tabla WHERE id = '$id_pais'");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaEstadoHanger($tabla, $id_estado){


      $stmt = Conexion::conectar()->prepare("SELECT estado FROM $tabla WHERE sk_estado = '$id_estado'");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function estadoExtranjero($tabla, $idUsuario){


      $stmt = Conexion::conectar()->prepare("UPDATE usuarios SET 'estado_extranjero' = '' WHERE id_usuario = $idUsuario ");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function paisHanger($tabla, $idUsuario){


      $stmt = Conexion::conectar()->prepare("SELECT pais FROM $tabla WHERE id_usuario = '$idUsuario' ");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function datosMetas($tabla, $ruta){


      $stmt = Conexion::conectar()->prepare("SELECT usuario,modo_registro,foto,descripcion,carpeta,ruta,sexo FROM $tabla WHERE ruta = '$ruta' AND activo = 1 AND tipo_usuario = 1");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function coleccionesUsuario($id_usuario,$tienda){


      $stmt = Conexion::conectar()->prepare("SELECT DISTINCT fk_coleccion,(SELECT nombre_coleccion FROM colecciones WHERE colecciones.sk_coleccion = diseno.fk_coleccion LIMIT 0,1) AS nombre_coleccion FROM diseno WHERE id_usuario = '$id_usuario' and activo = 1 AND diseno_en_venta = $tienda ORDER BY nombre_coleccion");

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

    }



  }

?>
