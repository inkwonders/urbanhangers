<?php
require_once "conexion.php";

  class dashboardModelo{

    static public function consultaId($tabla, $ruta){

      $stmt = Conexion::conectar()->prepare("SELECT id_usuario FROM $tabla WHERE ruta = '$ruta'");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaDashboard($tabla, $id){

      $stmt = Conexion::conectar()->prepare("SELECT nombre, foto, estado, pais, descripcion, banner, modo_registro, carpeta, sexo, usuario, estado_extranjero FROM $tabla WHERE id_usuario = '$id'");

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaInsignias($tabla1){

      $stmt = Conexion::conectar()->prepare("SELECT id_insignia, ruta_insignia, nombre_insignia FROM $tabla1 ORDER BY orden ASC");

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaInsigniasActivas($tabla1, $idUsuario, $insigniasId){

      $stmt = Conexion::conectar()->prepare("SELECT COUNT(*) FROM $tabla1  WHERE id_usuario = '$idUsuario' AND id_insignia = '$insigniasId'");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaNoDisenos($tabla1, $tabla2, $idUsuario){

      $stmt = Conexion::conectar()->prepare("SELECT COUNT(t1.id_diseno) FROM $tabla1 AS t1, $tabla2 AS t2 WHERE t1.id_usuario = t2.id_usuario AND t2.id_usuario='$idUsuario'");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaNoLikes($tabla1, $tabla2, $idUsuario){

      $stmt = Conexion::conectar()->prepare("SELECT COUNT(t2.id_voto) FROM $tabla1 AS t1, $tabla2 AS t2 WHERE t1.id_diseno=t2.id_diseno AND t1.id_usuario='$idUsuario'");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaRedes($tabla1, $idUsuario){

      $stmt = Conexion::conectar()->prepare("SELECT behance, dribble, instagram FROM $tabla1 WHERE id_usuario = '$idUsuario'");

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaSlideFiltro($idUsuario, $filtro){

      switch ($filtro) {
        case 'nuevos':
          $where = "";
          $orden_query = "ORDER BY fecha_diseno DESC";
          break;
        case 'mas-votados':
          $where = "";
          $orden_query = "ORDER BY votos DESC, fecha_diseno DESC";
          break;
        case 'tendencia':
          $where = "";
          $orden_query = "ORDER BY tendencia DESC, fecha_tendencia DESC, fecha_diseno DESC";
          break;
        default:
          $where = "AND fk_coleccion = '$filtro'";
          $orden_query = "ORDER BY fecha_diseno DESC";
          break;
      }

      $query = "SELECT id_diseno, nombre_diseno, id_usuario, inspiracion, fecha_diseno, ruta_img, ruta,
                (SELECT COUNT(id_diseno) FROM votos_diseno WHERE
                id_diseno = diseno.id_diseno AND voto_activo = 1 LIMIT 0,1) AS votos, activo, (SELECT carpeta FROM usuarios WHERE id_usuario = '$idUsuario') AS carpeta, (SELECT COUNT(id_diseno) FROM comentarios WHERE
                id_diseno = diseno.id_diseno LIMIT 0,1) AS comentarios, ruta_tienda FROM diseno WHERE id_usuario =
                '$idUsuario' AND activo = 1 AND diseno_en_venta = 0 $where $orden_query";

      // echo $query;

      $stmt = Conexion::conectar()->prepare($query);

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaSlideFiltroTienda($idUsuario, $filtro){

      switch ($filtro) {
        case 'nuevos':
          $where = "";
          $orden_query = "ORDER BY fecha_tienda DESC";
          break;
        case 'mas-votados':
          $where = "";
          $orden_query = "ORDER BY votos DESC, fecha_tienda DESC";
          break;
        default:
          $where = "AND fk_coleccion = '$filtro'";
          $orden_query = "ORDER BY fecha_tienda DESC";
          break;
      }

      $query = "SELECT id_diseno, nombre_diseno, id_usuario, inspiracion, fecha_diseno, ruta_img,
                (SELECT COUNT(id_diseno) FROM votos_diseno WHERE
                id_diseno = diseno.id_diseno LIMIT 0,1) AS votos, activo,ruta_tienda FROM diseno WHERE id_usuario =
                '$idUsuario' AND activo = 1 AND diseno_en_venta = 1 $where $orden_query";

      // echo $query;

      $stmt = Conexion::conectar()->prepare($query);

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaImgSlideNuevos($tabla1, $idDisenoNuevo){

      $stmt = Conexion::conectar()->prepare("SELECT ruta_img, id_diseno FROM $tabla1 WHERE id_diseno = '$idDisenoNuevo'");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaVotos($tabla1, $idDisenoNuevo){

      $stmt = Conexion::conectar()->prepare("SELECT COUNT(id_diseno) FROM $tabla1 WHERE id_diseno = '$idDisenoNuevo'");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaVentas($tabla1, $idDisenoNuevo){

      $stmt = Conexion::conectar()->prepare("SELECT COUNT(id_diseno) FROM $tabla1 WHERE id_diseno = '$idDisenoNuevo'");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaSlideVenta($tabla1, $idUsuario){

      $stmt = Conexion::conectar()->prepare("SELECT id_diseno, nombre_diseno, id_usuario, inspiracion FROM $tabla1 WHERE id_usuario = '$idUsuario' AND diseno_en_venta = 1");

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaImgSlideVenta($tabla1, $idDisenoVenta){

      $stmt = Conexion::conectar()->prepare("SELECT ruta_img, id_diseno FROM $tabla1 WHERE id_diseno = '$idDisenoVenta'");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaVotosVenta($tabla1, $idDisenoVenta){

      $stmt = Conexion::conectar()->prepare("SELECT COUNT(id_diseno) FROM votos_diseno WHERE id_diseno = '$idDisenoVenta'");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaVentasVenta($tabla1, $idDisenoVenta){

      $stmt = Conexion::conectar()->prepare("SELECT COUNT(id_diseno) FROM ventas_diseno WHERE id_diseno = '$idDisenoVenta'");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaEditarPerfil($tabla, $id){

      $stmt = Conexion::conectar()->prepare("SELECT usuario, nombre, apellido, sexo, foto, estado, pais, descripcion, banner, behance, dribble, instagram, modo_registro, estado_extranjero FROM $tabla WHERE id_usuario = '$id'");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaRuta($tabla, $id){

      $stmt = Conexion::conectar()->prepare("SELECT ruta FROM $tabla WHERE id_diseno = '$id'");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

  }

?>
